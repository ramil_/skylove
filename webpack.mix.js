let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/login.js', 'public/js')
    .js('resources/assets/js/dashboard.js', 'public/js')
    .js('resources/assets/js/users.js', 'public/js')
    .js('resources/assets/js/users_deleted.js', 'public/js')
    .js('resources/assets/js/user_edit.js', 'public/js')
    .js('resources/assets/js/user_dialogs.js', 'public/js')
    .js('resources/assets/js/fakes.js', 'public/js')
    .js('resources/assets/js/fake_edit.js', 'public/js')
    .js('resources/assets/js/fake_add.js', 'public/js')
    .js('resources/assets/js/fake_dialogs.js', 'public/js')
    .js('resources/assets/js/settings_admins.js', 'public/js')
    .js('resources/assets/js/settings_admin_add.js', 'public/js')
    .js('resources/assets/js/settings_admin_edit.js', 'public/js')
    .js('resources/assets/js/reports_users.js', 'public/js')
    .js('resources/assets/js/reports_user_show.js', 'public/js')
    .js('resources/assets/js/reports_photos.js', 'public/js')
    .js('resources/assets/js/verification.js', 'public/js')
    .js('resources/assets/js/status_photos.js', 'public/js')
    .js('resources/assets/js/status_photos_inspect_list.js', 'public/js')
    .js('resources/assets/js/status_photos_inspect_show.js', 'public/js')
    .js('resources/assets/js/status_photos_inspect_confirmed_show.js', 'public/js')
    .js('resources/assets/js/status_photos_inspect_changed_show.js', 'public/js')
    .js('resources/assets/js/status_photos_inspect_search.js', 'public/js')
    .js('resources/assets/js/moderators_stats.js', 'public/js')
    .js('resources/assets/js/moderators_rating.js', 'public/js')
    .js('resources/assets/js/moderators_payments.js', 'public/js')
    .extract(['vue', 'jquery', 'lodash', 'moment', 'axios', 'bootstrap-vue'])
    .sass('resources/assets/sass/app.scss', 'public/css')
    .version();