<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\UsersAlert
 *
 * @property int $alert_id
 * @property int $user_id
 * @property int|null $admin_id
 * @property int|null $type
 * @property int $priority
 * @property string|null $data
 * @property string|null $read_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @method static \Illuminate\Database\Eloquent\Builder|UsersAlert newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersAlert newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersAlert query()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersAlert whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersAlert whereAlertId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersAlert whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersAlert whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersAlert wherePriority($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersAlert whereReadAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersAlert whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersAlert whereUserId($value)
 * @mixin \Eloquent
 */
class UsersAlert extends Model
{
    protected $fillable = [
        'user_id', 'admin_id', 'type', 'priority', 'data', 'read_at', 'created_at',
    ];

    public $timestamps = false;
}
