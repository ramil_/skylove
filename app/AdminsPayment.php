<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\AdminsPayment
 *
 * @property int $payment_id
 * @property int $for_admin_id
 * @property int $from_admin_id
 * @property float $amount
 * @property string $date
 * @property string $created_at
 * @method static \Illuminate\Database\Eloquent\Builder|AdminsPayment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AdminsPayment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AdminsPayment query()
 * @method static \Illuminate\Database\Eloquent\Builder|AdminsPayment whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminsPayment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminsPayment whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminsPayment whereForAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminsPayment whereFromAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminsPayment wherePaymentId($value)
 * @mixin \Eloquent
 */
class AdminsPayment extends Model
{
    protected $fillable = [
        'for_admin_id', 'from_admin_id', 'amount', 'date'
    ];

    public $timestamps = false;

    protected $table = 'admins_payments';

    protected $primaryKey = 'payment_id';
}
