<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\AdminsRating
 *
 * @property int $admin_id
 * @property float $rating_quality
 * @property float $rating_shift
 * @property string|null $metadata
 * @property string $created_at
 * @method static \Illuminate\Database\Eloquent\Builder|AdminsRating newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AdminsRating newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AdminsRating query()
 * @method static \Illuminate\Database\Eloquent\Builder|AdminsRating whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminsRating whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminsRating whereMetadata($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminsRating whereRatingQuality($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminsRating whereRatingShift($value)
 * @mixin \Eloquent
 */
class AdminsRating extends Model
{
    protected $table = 'admins_rating';

    protected $primaryKey = 'admin_id';

    protected $fillable = [
        'admin_id', 'rating_quality', 'rating_shift', 'metadata', 'created_at'
    ];

    public $timestamps = false;

    public $incrementing = false;
}
