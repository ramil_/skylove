<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\UsersPhotoReportVerification
 *
 * @property int $verification_id
 * @property int $photo_id
 * @property int $admin_id
 * @property string $date
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhotoReportVerification newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhotoReportVerification newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhotoReportVerification query()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhotoReportVerification whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhotoReportVerification whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhotoReportVerification wherePhotoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhotoReportVerification whereVerificationId($value)
 * @mixin \Eloquent
 */
class UsersPhotoReportVerification extends Model
{
    protected $primaryKey = 'verification_id';

    protected $table = 'users_photos_reports_verification';

    protected $fillable = [
        'photo_id', 'admin_id'
    ];

    public $timestamps = false;
}
