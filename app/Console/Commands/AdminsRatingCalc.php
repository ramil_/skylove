<?php

namespace App\Console\Commands;

use App\AdminsRating;
use App\AdminsRatingCalcQuality;
use App\AdminsRatingCalcShift;
use App\AdminsShift;
use App\User;
use App\UsersPhoto;
use App\UsersPhotoStatusInspect;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\DB;

class AdminsRatingCalc extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admins-rating-calc';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calc moderators rating';

    /**
     * @var array Множитель рейтинга скорости проверки.
     *            Например: $arr[verification][фото_проверено_в_течении_5_минут] = множитель
     */
    protected $ratingInterval = [
        'verification' => [
            '5'  => 0.0005,
            '10' => -0.0005,
            '15' => -0.002,
            '20' => -0.008,
            '25' => -0.01,
            '30' => -0.02,
        ],
        'deleted'      => [
            '5'  => 0,
            '10' => -0.0005,
            '15' => -0.002,
            '20' => -0.008,
            '25' => -0.01,
            '30' => -0.02,
        ],
    ];

    /**
     * @var array Множитель рейтинга качества проверки.
     *            Например: $arr[статус_установленный_модератором][статус_установленный_контролером] = множитель
     */
    protected $ratingQualityMistake = [
        '1' => [
            '2' => -0.0005,
            '3' => -0.002,
            '4' => -0.008,
            '5' => -0.5,
            '6' => -0.5,
        ],
        '2' => [
            '1' => -0.002,
            '3' => -0.0005,
            '5' => -0.5,
            '6' => -0.5,
        ],
        '3' => [
            '1' => -0.002,
            '2' => -0.0005,
            '5' => -0.5,
            '6' => -0.5,
        ],
        '4' => [
            '1' => -0.002,
            '5' => -0.5,
            '6' => -0.5,
        ],
        '5' => [
            '1' => -0.5,
            '2' => -0.5,
            '3' => -0.5,
            '4' => -0.5,
        ],
        '6' => [
            '1' => -0.5,
            '2' => -0.5,
            '3' => -0.5,
            '4' => -0.5,
        ],
    ];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->calcShift();
        $this->calcQuality();
        $this->calcTotal();
    }

    /**
     * Рейтинг скорости проверки
     */
    protected function calcShift()
    {
        // не обработанные смены (завершенные)
        $shifts = AdminsShift::doesntHave('rating')
            ->whereNotNull('finished_at')
            ->get();

        foreach ($shifts as $shift) {
            $rating = 0;
            $metadata = [];

            $shiftStarted = Carbon::parse($shift->started_at);
            $shiftFinished = Carbon::parse($shift->finished_at);

            if (!$shiftStarted || !$shiftFinished) {
                continue;
            }

            // фото загруженные во время смены
            $photos = UsersPhoto::with([
                'statusInspectFirst' => function (Relation $query) {
                    $query->select(['photo_id', 'admin_id', 'date']);
                }])
                ->whereBetween('created_at', [$shiftStarted->toDateTimeString(), $shiftFinished->toDateTimeString()])
                ->select(['photo_id', 'user_id', 'created_at', 'deleted_at'])
                ->get();

            foreach ($photos as $photo) {
                // дата загрузки фото
                if (empty($photo->created_at) || (!$createdDate = Carbon::parse($photo->created_at))) {
                    continue;
                }

                if (!empty($photo->statusInspectFirst->date)) {
                    if ($photo->statusInspectFirst->admin_id != $shift->admin_id) {
                        continue;
                    }

                    // дата проверки фото
                    if (!$verificationDate = Carbon::parse($photo->statusInspectFirst->date)) {
                        continue;
                    }
                    // интервал времени между загрузкой и проверкой
                    $diff = $createdDate->diffInMinutes($verificationDate);

                    if ($diff <= 5) {
                        $interval = 5;
                    } elseif ($diff <= 10) {
                        $interval = 10;
                    } elseif ($diff <= 15) {
                        $interval = 15;
                    } elseif ($diff <= 20) {
                        $interval = 20;
                    } elseif ($diff <= 25) {
                        $interval = 25;
                    } else {
                        $interval = 30;
                    }

                    if (empty($metadata[$interval])) {
                        $metadata[$interval] = 0;
                    }

                    $rating += $this->ratingInterval['verification'][$interval];
                    $metadata[$interval]++;
                } elseif (!empty($photo->deleted_at)) {
                    // TODO: Подумать над подсчетом удаленных фото забаненных пользователей. При бане пользователя из карточки клиента все статусы
                    // фото сбрасываются (из-за этого рейтинг считается неправильно)

                    // дата удаления фото
                    if (!$deletedDate = Carbon::parse($photo->deleted_at)) {
                        continue;
                    }

                
                }
            }

            AdminsRatingCalcShift::create([
                'admin_id' => $shift->admin_id,
                'shift_id' => $shift->shift_id,
                'rating'   => $rating,
                'metadata' => json_encode($metadata),
            ]);
        }
    }

    /**
     * Рейтинг качества проверки
     */
    protected function calcQuality()
    {
        // статусы фото выставленные модераторами
        $adminsStatus = UsersPhotoStatusInspect::whereHas('admin', function (Builder $query) {
            $query->where('level', '<=', 10);
        })
            ->select(['admin_id', 'admin_status', 'inspector_status', DB::raw('count(*) as count')])
            ->groupBy('admin_id', 'admin_status', 'inspector_status')
            ->get();

        $qualityArray = [];
        foreach ($adminsStatus as $status) {
            if ($status->admin_status == -1) {
                continue;
            }

            if (!array_key_exists($status->admin_id, $qualityArray)) {
                $qualityArray[$status->admin_id] = [
                    'rating'   => 0,
                    'metadata' => [
                        'correct' => [],
                        'mistake' => [],
                    ],
                ];
            }

            $ratingMultiple = 0;
            if ($status->admin_status == $status->inspector_status || !$status->inspector_status) {
                $ratingMultiple = 0.0005;

                if (empty($qualityArray[$status->admin_id]['metadata']['correct'][$status->admin_status])) {
                    $qualityArray[$status->admin_id]['metadata']['correct'][$status->admin_status] = $status->count;
                } else {
                    $qualityArray[$status->admin_id]['metadata']['correct'][$status->admin_status] += $status->count;
                }
            } else {
                if (!empty($this->ratingQualityMistake[$status->admin_status][$status->inspector_status])) {
                    $ratingMultiple = $this->ratingQualityMistake[$status->admin_status][$status->inspector_status];
                }

                if (empty($qualityArray[$status->admin_id]['metadata']['mistake'][$status->admin_status][$status->inspector_status])) {
                    $qualityArray[$status->admin_id]['metadata']['mistake'][$status->admin_status][$status->inspector_status] = $status->count;
                } else {
                    $qualityArray[$status->admin_id]['metadata']['mistake'][$status->admin_status][$status->inspector_status] += $status->count;
                }
            }

            if ($ratingMultiple) {
                $qualityArray[$status->admin_id]['rating'] += $ratingMultiple * $status->count;
            }
        }

        foreach ($qualityArray as $adminId => $quality) {
            AdminsRatingCalcQuality::updateOrCreate(['admin_id' => $adminId], [
                'rating'   => $quality['rating'],
                'metadata' => json_encode($quality['metadata']),
            ]);
        }
    }

    /**
     * Расчет итогового значения рейтинга
     */
    protected function calcTotal()
    {
        $admins = User::with(['ratingQuality', 'ratingShift'])
            ->where('level', '<=', 10)
            ->get();

        foreach ($admins as $admin) {
            $ratingQuality = 0;
            $ratingShift = 0;
            $metadata = [
                'quality' => [],
                'shift'   => [],
            ];

            if ($admin->ratingQuality) {
                $metaQuality = json_decode($admin->ratingQuality->metadata, true);

                if (json_last_error() === JSON_ERROR_NONE) {
                    $metadata['quality'] = $metaQuality;
                }

                $ratingQuality += $admin->ratingQuality->rating;
            }

            foreach ($admin->ratingShift as $shift) {
                $metaShift = json_decode($shift->metadata, true);

                if (json_last_error() === JSON_ERROR_NONE) {
                    foreach ($metaShift as $k => $v) {
                        if (!is_string($k) && !is_int($k)) {
                            continue;
                        }

                        if (!array_key_exists($k, $metadata['shift'])) {
                            $metadata['shift'][$k] = 0;
                        }
                        $metadata['shift'][$k] += intval($v);
                    }
                }

                $ratingShift += $shift->rating;
            }

            AdminsRating::updateOrCreate(['admin_id' => $admin->id], [
                'rating_quality' => $ratingQuality,
                'rating_shift'   => $ratingShift,
                'metadata'       => json_encode($metadata),
            ]);
        }
    }
}
