<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

use Exception;
use DateTime;
use DateTimeZone;

use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class Viewfromfake extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fake:getuser {user}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make get user request from random fake`s id';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // id юзера, которого будет "просматривать" вызов
        $user = (int)$this->argument('user');

        $file = env('PATH_TO_FAKESHOW_QUEUE') . '/' . $user . '.json';
        if(!file_exists($file)) {
            throw new Exception('Отсутствует файл');
        } else {
            $filedata = json_decode(file_get_contents($file), TRUE);
        }

        if(!isset($filedata['user'])) {
            throw new Exception('Некорректный формат');
        }
    
        // Оптимизация: если пользователя нет значит файл нужно переместить в обработанные
        // чтобы не гонять повторно циклы
        $query = "SELECT COUNT(*) AS usr FROM users WHERE user_id = $user AND state = 1";
        $data = DB::select($query);
        $is_user = (int)$data[0]->usr;

        if($is_user < 1) {
            $rename = env('PATH_TO_FAKESHOW_PROCESSED') . '/' . $user . '.json';
            rename($file, $rename);
        }

        // определяем текущие координаты пользователя, которого будем "смотреть"
        $query = "SELECT latitude, longitude
            FROM users_info 
            WHERE user_id = $user 
            AND latitude IS NOT NULL
            AND longitude IS NOT NULL";

        $data = DB::select($query);

        if(count($data) <= 0) {
            throw new Exception('Отсутствуют координаты');
        } else {
            $u_latitude = (float)$data[0]->latitude;
            $u_longitude = (float)$data[0]->longitude;
        }

        // check timezone and localtime
        $time = time();
        if(!isset($filedata['timezone'])){
            $client = new Client(); 
            $ulr = 'https://maps.googleapis.com/maps/api/timezone/json?location=';
            $ulr .= $u_latitude . ',' . $u_longitude . '&timestamp=' . $time . '&key=' . env('GOOGLE_API_KEY');
                
            try {
                $response = $client->request('GET', $ulr);
            } catch(GuzzleException $e) {
                throw new Exception('Error timezone get');
            }

            unset($client);
            $tzdata = json_decode($response->getBody()->getContents(), TRUE);
            if(!$tzdata) {
                throw new Exception('Error parse timezone data');
            }

            $filedata['timezone'] = $tzdata['timeZoneId'];
        }

        $date = new DateTime();
        $date->setTimestamp($time);
        $date->setTimezone(new DateTimeZone($filedata['timezone']));
        $hrs = (int)$date->format('H');

        // ограничение по времени отсылки пушей
        if($hrs < 9 || $hrs > 22) {
            die();
        }

        // берем список фейков которые находятся поблизости
        $query = "SELECT u.user_id, 
        (
            3959 * acos (
                cos ( radians( $u_latitude ) )
                * cos( radians( latitude ) )
                * cos( radians( longitude ) - radians( $u_longitude ) )
                + sin ( radians( $u_latitude ) )
                * sin( radians( latitude ) )
            )
        ) AS D
        FROM users_info AS u
        INNER JOIN users AS usr ON usr.user_id = u.user_id
        LEFT JOIN users_search AS s ON s.user_id = $user
        WHERE 
        u.user_id != $user 
        AND usr.state = 1
        AND (s.gender = 0 || u.sex = s.gender) 
        AND u.years >= s.age_min 
        AND u.years <= s.age_max 
        AND usr.email LIKE '%mail.local'
        ORDER BY D ASC";

        $data = DB::select($query);

        $users = [];
        if(count($data) <= 0) {
            throw new Exception('Отсутствуют подходящие.');
        } else {
            foreach($data as $d) {
                $users[] = [
                    'id'    => $d->user_id,
                    'dist'  => $d->D
                ];
            }
        }

        // исключая уже засмотренных
        if(!array_key_exists('processed_users', $filedata)) {
            $filedata['processed_users'] = [];
        }

        foreach($users as $u) {
            if(!in_array($u['id'], $filedata['processed_users'])) {
                echo 'Process w id: ' . $u['id'] . PHP_EOL;
                
                $client = new Client(); 
                $ulr = env('SKYLOVE_API_URL') . 'users/user?user_id=' . $user;
                
                $filedata['processed_users'][] = $u['id'];
                
                $is_debug = (env('FAKESHOW_MODE') === 'debug' ? TRUE : FALSE);
                if(!$is_debug) {
                    try {
                        $response = $client->request('GET', $ulr, [
                            'headers' => [
                                'authentication' => $this->generateAuthToken($u['id'])
                            ]
                        ]);
                    } catch(GuzzleException $e) {
                        throw new Exception('Server request error');
                    }
                }
                
                // за один раз выполняется только ОДИН запрос 
                break;
            }
        }

        file_put_contents($file, json_encode($filedata));
    }

    private function generateAuthToken($user_id) 
    {
        // нужно получить secret и session_id aka sid
        // на всякий случай перестраховка, JOIN на таблицу с LIKE mail.local
        $query = "SELECT us.id, us.secret
        FROM users_sessions AS us
        LEFT JOIN users AS u ON u.user_id = us.user_id
        WHERE us.user_id = " . (int)$user_id . " AND u.email LIKE '%mail.local'";

        $sessions = DB::select($query);

        if(count($sessions) <= 0) {
            throw new Exception('Отсутствуют активные сессии');
        } else {
            $secret = (string)$sessions[0]->secret;
            $sid = (int)$sessions[0]->id;
        }

        $header = json_encode(['typ' => 'JWT', 'alg' => 'HS256']);
        $payload = json_encode(['uid' => $user_id, 'sid' => $sid]);

        // Encode
        $base64UrlHeader = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($header));
        $base64UrlPayload = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($payload));

        // Create Signature Hash & encode
        $signature = hash_hmac('sha256', $base64UrlHeader . "." . $base64UrlPayload, $secret, true);
        $base64UrlSignature = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($signature));

        // Create JWT
        $jwt = $base64UrlHeader . "." . $base64UrlPayload . "." . $base64UrlSignature;

        return $jwt;
    }
}