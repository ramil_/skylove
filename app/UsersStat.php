<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\UsersStat
 *
 * @property int $user_id
 * @property string|null $achievements
 * @property int $photos
 * @property int $gifts
 * @property int $messages
 * @property int $dialogs
 * @property int $likes
 * @property int $online
 * @property string|null $vip_expires
 * @property string|null $vip_order_method
 * @property string|null $app_version
 * @property int $banned_acc
 * @property int $banned_dev
 * @property int $banned_dev_associated
 * @property string|null $banned_date
 * @property string $banned_admin
 * @property int $blocked_photo
 * @property int $total_amount_points
 * @property int $total_amount_vip
 * @property int $total_amount_reveal_views
 * @property int $total_amount_reveal_likes
 * @method static \Illuminate\Database\Eloquent\Builder|UsersStat newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersStat newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersStat query()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersStat whereAchievements($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersStat whereAppVersion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersStat whereBannedAcc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersStat whereBannedAdmin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersStat whereBannedDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersStat whereBannedDev($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersStat whereBannedDevAssociated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersStat whereBlockedPhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersStat whereDialogs($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersStat whereGifts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersStat whereLikes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersStat whereMessages($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersStat whereOnline($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersStat wherePhotos($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersStat whereTotalAmountPoints($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersStat whereTotalAmountRevealLikes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersStat whereTotalAmountRevealViews($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersStat whereTotalAmountVip($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersStat whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersStat whereVipExpires($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersStat whereVipOrderMethod($value)
 * @mixin \Eloquent
 */
class UsersStat extends Model
{
    protected $primaryKey = 'user_id';

    public $timestamps = false;
}
