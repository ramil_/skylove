<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\UsersInfo
 *
 * @property int $user_id
 * @property string|null $name
 * @property int|null $sex
 * @property string|null $birth_date
 * @property int|null $birth_date_change
 * @property int $zodiac_sign
 * @property int|null $years
 * @property string|null $avatar
 * @property string|null $avatar_blurred
 * @property int|null $photo_id
 * @property string|null $city
 * @property int|null $points
 * @property int|null $last_visit
 * @property float|null $latitude
 * @property float|null $longitude
 * @property int|null $vip
 * @property int|null $verification
 * @property int $review
 * @property int|null $vk
 * @property int|null $fb
 * @property int|null $map_visible
 * @property int|null $vip_map_invisible
 * @property int|null $gifts_visible
 * @property int|null $incognito
 * @property int $auto_dating
 * @property int|null $height
 * @property int|null $figure
 * @property int|null $children
 * @property int|null $smoking
 * @property int|null $alcohol
 * @property int|null $education
 * @property int|null $job
 * @property int|null $zodiac
 * @property string|null $purpose
 * @property string $reg_date
 * @property float|null $rating
 * @property float|null $rating_test
 * @property-read \Illuminate\Database\Eloquent\Collection|UsersAppVersion[] $appVersion
 * @property-read UsersPhoto $mainPhoto
 * @property-read \Illuminate\Database\Eloquent\Collection|UsersPhoto[] $photos
 * @property-read UsersStat $stats
 * @method static \Illuminate\Database\Eloquent\Builder|UsersInfo newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersInfo newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersInfo query()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersInfo whereAlcohol($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersInfo whereAutoDating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersInfo whereAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersInfo whereAvatarBlurred($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersInfo whereBirthDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersInfo whereBirthDateChange($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersInfo whereChildren($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersInfo whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersInfo whereEducation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersInfo whereFb($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersInfo whereFigure($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersInfo whereGiftsVisible($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersInfo whereHeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersInfo whereIncognito($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersInfo whereJob($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersInfo whereLastVisit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersInfo whereLatitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersInfo whereLongitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersInfo whereMapVisible($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersInfo whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersInfo wherePhotoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersInfo wherePoints($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersInfo wherePurpose($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersInfo whereRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersInfo whereRatingTest($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersInfo whereRegDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersInfo whereReview($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersInfo whereSex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersInfo whereSmoking($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersInfo whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersInfo whereVerification($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersInfo whereVip($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersInfo whereVipMapInvisible($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersInfo whereVk($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersInfo whereYears($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersInfo whereZodiac($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersInfo whereZodiacSign($value)
 * @mixin \Eloquent
 */
class UsersInfo extends Model
{
    protected $table = 'users_info';

    protected $primaryKey = 'user_id';

    public function appVersion()
    {
        return $this->hasMany(UsersAppVersion::class, 'user_id', 'user_id');
    }

    public function mainPhoto()
    {
        return $this->hasOne(UsersPhoto::class, 'photo_id', 'photo_id');
    }

    public function photos()
    {
        return $this->hasMany(UsersPhoto::class, 'user_id', 'user_id');
    }

    public function stats()
    {
        return $this->hasOne(UsersStat::class, 'user_id', 'user_id');
    }
}
