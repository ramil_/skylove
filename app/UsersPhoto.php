<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\UsersPhoto
 *
 * @property int $photo_id
 * @property int $user_id
 * @property string|null $photo
 * @property int $width
 * @property int $height
 * @property string $created_at
 * @property int $main_photo
 * @property int|null $likes
 * @property \Illuminate\Database\Eloquent\Collection|UsersPhotoReport[] $reports
 * @property int $status
 * @property string|null $deleted_at
 * @property int|null $deleted_admin
 * @property-read UsersStat $stat
 * @property-read \Illuminate\Database\Eloquent\Collection|UsersPhotoStatusInspect[] $statusInspect
 * @property-read UsersPhotoStatusInspect $statusInspectFirst
 * @property-read UsersPhotoStatusInspect $statusInspectLast
 * @property-read Users $user
 * @property-read UsersInfo $userInfo
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhoto newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhoto newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhoto query()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhoto whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhoto whereDeletedAdmin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhoto whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhoto whereHeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhoto whereLikes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhoto whereMainPhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhoto wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhoto wherePhotoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhoto whereReports($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhoto whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhoto whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhoto whereWidth($value)
 * @mixin \Eloquent
 */
class UsersPhoto extends Model
{
    protected $primaryKey = 'photo_id';

    public $timestamps = false;

    public function reports()
    {
        return $this->hasMany('App\UsersPhotoReport', 'photo_id', 'photo_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Users', 'user_id', 'user_id');
    }

    public function userInfo()
    {
        return $this->belongsTo('App\UsersInfo', 'user_id', 'user_id');
    }

    public function stat()
    {
        return $this->belongsTo('App\UsersStat', 'user_id', 'user_id');
    }

    public function statusInspect()
    {
        return $this->hasMany('App\UsersPhotoStatusInspect', 'photo_id', 'photo_id');
    }

    public function statusInspectFirst()
    {
        return $this->hasOne('App\UsersPhotoStatusInspect', 'photo_id', 'photo_id')
            ->oldest('date')
            ->oldest('status_id');
    }

    public function statusInspectLast()
    {
        return $this->hasOne('App\UsersPhotoStatusInspect', 'photo_id', 'photo_id')
            ->latest('date')
            ->latest('status_id');
    }
}
