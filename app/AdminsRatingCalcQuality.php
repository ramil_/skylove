<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\AdminsRatingCalcQuality
 *
 * @property int $admin_id
 * @property float $rating
 * @property string|null $metadata
 * @property string $created_at
 * @method static \Illuminate\Database\Eloquent\Builder|AdminsRatingCalcQuality newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AdminsRatingCalcQuality newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AdminsRatingCalcQuality query()
 * @method static \Illuminate\Database\Eloquent\Builder|AdminsRatingCalcQuality whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminsRatingCalcQuality whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminsRatingCalcQuality whereMetadata($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminsRatingCalcQuality whereRating($value)
 * @mixin \Eloquent
 */
class AdminsRatingCalcQuality extends Model
{
    protected $table = 'admins_rating_calc_quality';

    protected $primaryKey = 'admin_id';

    protected $fillable = [
        'admin_id', 'rating', 'metadata', 'created_at'
    ];

    public $timestamps = false;

    public $incrementing = false;
}
