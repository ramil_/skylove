<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\AdminsRatingCalcShift
 *
 * @property int $shift_id
 * @property int $admin_id
 * @property float $rating
 * @property string|null $metadata
 * @property string $created_at
 * @property-read AdminsShift $shift
 * @method static \Illuminate\Database\Eloquent\Builder|AdminsRatingCalcShift newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AdminsRatingCalcShift newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AdminsRatingCalcShift query()
 * @method static \Illuminate\Database\Eloquent\Builder|AdminsRatingCalcShift whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminsRatingCalcShift whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminsRatingCalcShift whereMetadata($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminsRatingCalcShift whereRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminsRatingCalcShift whereShiftId($value)
 * @mixin \Eloquent
 */
class AdminsRatingCalcShift extends Model
{
    protected $table = 'admins_rating_calc_shift';

    protected $primaryKey = 'shift_id';

    protected $fillable = [
        'shift_id', 'admin_id', 'rating', 'metadata', 'created_at'
    ];

    public $timestamps = false;

    public $incrementing = false;

    public function shift()
    {
        return $this->hasOne('App\AdminsShift', 'shift_id', 'shift_id');
    }
}
