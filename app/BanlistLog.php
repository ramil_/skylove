<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\BanlistLog
 *
 * @property int $ban_id
 * @property int $admin_id
 * @property int|null $user_id
 * @property string|null $device_id
 * @property int $status
 * @property int|null $photo_id
 * @property int|null $main_photo
 * @property string $date
 * @property int $level
 * @method static \Illuminate\Database\Eloquent\Builder|BanlistLog newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BanlistLog newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BanlistLog query()
 * @method static \Illuminate\Database\Eloquent\Builder|BanlistLog whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BanlistLog whereBanId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BanlistLog whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BanlistLog whereDeviceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BanlistLog whereLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BanlistLog whereMainPhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BanlistLog wherePhotoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BanlistLog whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BanlistLog whereUserId($value)
 * @mixin \Eloquent
 */
class BanlistLog extends Model
{
    protected $fillable = [
        'admin_id', 'user_id', 'device_id', 'status', 'photo_id', 'main_photo', 'date'
    ];

    public $timestamps = false;

    protected $table = 'banlist_log';

    protected $primaryKey = 'ban_id';
}
