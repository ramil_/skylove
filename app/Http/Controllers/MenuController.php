<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class MenuController extends Controller
{
    public function getData(Request $request)
    {
        $response = [];

        if ($menu = $request->input('menu')) {
            $menu = json_decode($menu, true);
        }

        if ($menu && $user = Auth::user()) {
            $permissions = $user->permissions()->pluck('permission')->toArray();
            foreach ($menu as $element) {
                if ($perms = array_values(array_filter(explode(',', $element['perm'])))) {
                    $intersect = array_intersect($perms, $permissions);

                    if (count($perms) == count($intersect)) {
                        $response[] = $element;
                    }
                } else {
                    $response[] = $element;
                }
            }
        }

        return response()->json($response);
    }
}
