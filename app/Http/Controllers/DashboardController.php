<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Traits\GeoTimezone;

class DashboardController extends Controller
{
    use GeoTimezone;

    protected $default_tz = '+07:00';

    protected $utc_tz = '+00:00';

    public function __construct()
    {
        try {
            $geo = geoip()->getLocation(\Request::ip());

            if (!$geo->timezone) {
                throw new \Exception();
            }
            $this->default_tz = Carbon::now($geo['timezone'])->format('P');
        } catch (\Exception $e) {
            return response()->json(['error' => 'Cannot get timezone'], 500);
        }

        if (!$this->setGeoTimezone()) {
            return response()->json(['error' => 'Cannot get timezone'], 500);
        }
    }

    public function mainStats()
    {
        $query = 'SELECT users_info.sex, users.email
        FROM users_info
        INNER JOIN users ON users_info.user_id = users.user_id
        WHERE users.state = 1';
        $response = [
            'main_stats' => [
                'all'   => 0,
                'man'   => 0,
                'woman' => 0,
                'fakes' => 0
            ]
        ];

        $users = DB::select($query);

        foreach ($users as $u) {
            if (mb_substr($u->email, -10) === 'mail.local') {
                $response['main_stats']['fakes']++;
            } else {
                if ((int)$u->sex === 1) {
                    $response['main_stats']['man']++;
                } else {
                    $response['main_stats']['woman']++;
                }
            }
        }

        $response['main_stats']['all'] = count($users) - $response['main_stats']['fakes'];

        return response()->json($response);
    }

    public function ageStats()
    {
        $response['age_stats'] = [];
        // чтобы морда не сваливалась нормализация ответа
        $keys = ['m1820', 'w1820', 'm2123', 'w2123', 'm2426', 'w2426', 'm2730', 'w2730', 'm3134',
            'w3134', 'm3540', 'w3540', 'm4145', 'w4145', 'm4650', 'w4650', 'm5155', 'w5155', 'm56plus', 'w56plus'];
        foreach ($keys as $k) {
            $response['age_stats'][$k] = ['in_group_percent' => 0, 'from_total_percent' => 0, 'numeric' => 0];
        }

        $query = 'SELECT users_info.sex, users_info.years, users.email
        FROM users_info
        INNER JOIN users ON users_info.user_id = users.user_id
        WHERE users.state = 1';

        $users = DB::select($query);
        // для расчета %
        $totals = ['1820'   => 0, '2123' => 0, '2426' => 0, '2730' => 0, '3134' => 0, '3540' => 0, '4145' => 0, '4650' => 0, '5155' => 0,
                   '56plus' => 0];

        $total_users_count = 0;
        foreach ($users as $u) {
            if (mb_substr($u->email, -10) === 'mail.local') {
                // это фейк, не нужен в статистике
            } else {
                $k = '';
                if ((int)$u->sex === 1) {
                    $k .= 'm';
                } else {
                    $k .= 'w';
                }

                if ($u->years >= 18 && $u->years <= 20) {
                    $k .= '1820';
                    $totals['1820']++;
                } elseif ($u->years >= 21 && $u->years <= 23) {
                    $k .= '2123';
                    $totals['2123']++;
                } elseif ($u->years >= 24 && $u->years <= 26) {
                    $k .= '2426';
                    $totals['2426']++;
                } elseif ($u->years >= 27 && $u->years <= 30) {
                    $k .= '2730';
                    $totals['2730']++;
                } elseif ($u->years >= 31 && $u->years <= 34) {
                    $k .= '3134';
                    $totals['3134']++;
                } elseif ($u->years >= 35 && $u->years <= 40) {
                    $k .= '3540';
                    $totals['3540']++;
                } elseif ($u->years >= 41 && $u->years <= 45) {
                    $k .= '4145';
                    $totals['4145']++;
                } elseif ($u->years >= 46 && $u->years <= 50) {
                    $k .= '4650';
                    $totals['4650']++;
                } elseif ($u->years >= 51 && $u->years <= 55) {
                    $k .= '5155';
                    $totals['5155']++;
                } else {
                    $k .= '56plus';
                    $totals['56plus']++;
                }
                $response['age_stats'][$k]['numeric']++;
                $total_users_count++;
            }
        }

        // расчет процентов по алгоритму
        // 3) общее количество мужчин И женщин в конкретной возрастной группе = 100% от них считается %
        $percents = [];
        $tpercents = [];
        foreach ($response['age_stats'] as $k => $v) {
            $tkey = substr($k, 1);
            $total = $totals[$tkey] ? $totals[$tkey] : 0;

            if ($total > 0) {
                // для статистики внутри возрастных групп
                $percent = round($v['numeric'] * 100 / $total, PHP_ROUND_HALF_UP);
                $percents[$k] = $percent;
                // для общей статистики
                $tpercent = round($v['numeric'] * 100 / $total_users_count, PHP_ROUND_HALF_UP);
                $tpercents[$k] = $tpercent;
            }
        }

        // while(each()) .. устаревшая типа, по этому второй раз в цикле
        foreach ($percents as $k => $p) {
            $response['age_stats'][$k]['in_group_percent'] = $p;
        }

        foreach ($tpercents as $k => $p) {
            $response['age_stats'][$k]['from_total_percent'] = $p;
        }

        return response()->json($response);
    }

    public function activeStat(Request $request)
    {
        $count_days = 29;
        $keys = [];

        if (!$reg_date = $request->input('reg_date')) {
            return response()->json([], 400);
        }

        $date = Carbon::parse($reg_date);
        for ($i = 1; $i <= $count_days; $i++) {
            if ($date->addDays(1)->gte(now())) {
                break;
            }
            $keys[] = $i;
        }

        try {
            if (!$reg_date = Carbon::parse($reg_date)) {
                throw new \Exception('Cannot get reg_date.');
            }
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }

        $regs = DB::table('users_info')
            ->select('user_id')
            ->whereRaw('date(convert_tz(reg_date, ?, ?)) = ?', [$this->utc_tz, $this->default_tz, $reg_date])
            ->count();

        $enters = DB::table('users_enters as e')
            ->join('users_info as i', 'i.user_id', '=', 'e.user_id')
            ->selectRaw('count(e.user_id) as count')
            ->selectRaw('date(convert_tz(e.date, ?, ?)) as enter_date', [$this->utc_tz, $this->default_tz])
            ->whereRaw('date(convert_tz(reg_date, ?, ?)) = ?', [$this->utc_tz, $this->default_tz, $reg_date->toDateString()])
            ->havingRaw('enter_date between ? and ?', [(clone $reg_date)->addDay()->toDateString(),
                (clone $reg_date)->addDays(count($keys))->toDateString()])
            ->groupBy('enter_date')
            ->pluck('count', 'enter_date');

        $total = [];
        if ($regs > 0) {
            $total[0] = [
                'date'    => $reg_date->format('d.m'),
                'numeric' => $regs,
                'percent' => 100,
                'today'   => 0
            ];

            if ($enters && $reg_date->lt(now())) {
                $now = Carbon::now();
                $date = clone $reg_date;
                foreach ($keys as $v) {
                    $date->addDay();
                    $key = $date->toDateString();

                    if (empty($enters[$key])) {
                        $enters[$key] = 0;
                    }

                    $total[$v] = [
                        'date'    => $date->format('d.m'),
                        'numeric' => $enters[$key],
                        'percent' => round($enters[$key] * 100 / $regs, PHP_ROUND_HALF_UP),
                        'today'   => intval($date->diffInDays($now) === 0)
                    ];
                }
            }
        }

        return response()->json(['active_stats' => $total]);
    }

    public function regEnterStats(Request $request)
    {
        if (empty($request->input('reg_date'))) {
            return response()->json([], 400);
        }

        try {
            if (!$reg_date = Carbon::parse($request->input('reg_date'))) {
                throw new \Exception('Cannot get reg_date.');
            }
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
        switch ($request->input('type')) {
            case 1:
                $period = -28;
                break;
            case 2:
                $period = -42;
                break;
            default:
                $period = -14;
        }

        $start_date = (clone $reg_date)->addDays($period);
        $period = [];
        for ($date = clone $start_date; $date->lte($reg_date); $date->addDay()) {
            $period[] = $date->toDateString();
        }

        $regs = DB::table('users_info')
            ->selectRaw('count(user_id) as count')
            ->selectRaw('date(convert_tz(reg_date, ?, ?)) as date_tz', [$this->utc_tz, $this->default_tz])
            ->havingRaw('date_tz between ? and ?', [$start_date, $reg_date])
            ->groupBy('date_tz')
            ->pluck('count', 'date_tz')
            ->toArray();

        $enters = DB::table('users_enters')
            ->selectRaw('count(user_id) as count')
            ->selectRaw('date(convert_tz(date, ?, ?)) as date_tz', [$this->utc_tz, $this->default_tz])
            ->havingRaw('date_tz between ? and ?', [$start_date, $reg_date])
            ->groupBy('date_tz')
            ->pluck('count', 'date_tz')
            ->toArray();

        $deleted = DB::table('users_delete')
            ->selectRaw('count(user_id) as count')
            ->selectRaw('date(convert_tz(deleted_at, ?, ?)) as date_tz', [$this->utc_tz, $this->default_tz])
            ->havingRaw('date_tz between ? and ?', [$start_date, $reg_date])
            ->groupBy('date_tz')
            ->pluck('count', 'date_tz')
            ->toArray();

        $response = [];
        foreach ($period as $k => $v) {
            $response['labels'][] = Carbon::parse($v)->format('d.m.Y');

            if (array_key_exists($v, $regs)) {
                $response['regs'][] = $regs[$v];
            } else {
                $response['regs'][] = 0;
            }

            if (array_key_exists($v, $enters)) {
                $response['enters'][] = $enters[$v];
            } else {
                $response['enters'][] = 0;
            }

            if (array_key_exists($v, $deleted)) {
                $response['deleted'][] = $deleted[$v];
            } else {
                $response['deleted'][] = 0;
            }
        }

        return response()->json($response);
    }

    /**
     * Статистика по полу и дням онлайн пользователей
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function onlineStatsSum(Request $request)
    {
        if (empty($request->date)) {
            return response()->json([], 400);
        }

        try {
            if (!$date = Carbon::parse($request->date)) {
                throw new \Exception('Cannot get start_date.');
            }
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }

        $date_start = $this->tzConvertToUTC($date->startOfDay()->format('Y-m-d H:i:s'));
        $date_end = $this->tzConvertToUTC($date->addDay()->endOfMinute()->format('Y-m-d H:i:s'));

        $stats = DB::table('online_stats_sum')
            ->whereBetween('date', [$date_start, $date_end])
            ->get()
            ->toArray();

        $period = [];

        foreach ($stats as $v) {
            if ((Carbon::parse($v->date))->minute % 15 === 0) {
                $period[$this->tzConvertToLocal($v->date)] = [
                    'm' => $v->men,
                    'w' => $v->women,
                    't' => $v->men + $v->women
                ];
            }
        }

        return response()->json(['period' => $period]);
    }

    /**
     * Текущее количество онлайн пользователей
     * @return \Illuminate\Http\JsonResponse
     */
    public function onlineCurrent()
    {
        $ts = Carbon::now()->addMinutes(-5)->getTimestamp();
        $current = DB::table('users_info')
            ->where('last_visit', '>', intval($ts))
            ->selectRaw('count(*) as current')
            ->pluck('current')
            ->first();

        return response()->json(['current' => intval($current)]);
    }

    /**
     * Статистика соотношения зарегистрированных мужчин к женщинам
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function genderStats(Request $request)
    {
        if (empty($request->date)) {
            return response()->json([], 400);
        }

        try {
            if (!$date = Carbon::parse($request->date)) {
                throw new \Exception('Cannot get start_date.');
            }
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }

        $date_start = (clone $date)->addDays(-14)->startOfDay()->format('Y-m-d H:i:s');
        $date_end = (clone $date)->endOfDay()->format('Y-m-d H:i:s');

        $before_stat = [
            'm' => 0,
            'w' => 0,
            't' => 0
        ];
        $period_stat = [];

        $before = DB::table('users as u')
            ->join('users_info as i', 'u.user_id', '=', 'i.user_id')
            ->where('u.state', '=', 1)
            ->where(function ($query) {
                /* @var $query \Illuminate\Database\Query\Builder */
                $query->where('u.email', 'not like', '%mail.local')
                    ->orWhereNull('u.email');
            })
            ->where('i.reg_date', '<', $date_start)
            ->groupBy('i.sex')
            ->selectRaw('i.sex, count(*) cnt')
            ->get();

        $period = DB::table('users as u')
            ->join('users_info as i', 'u.user_id', '=', 'i.user_id')
            ->where('u.state', '=', 1)
            ->where(function ($query) {
                /* @var $query \Illuminate\Database\Query\Builder */
                $query->where('u.email', 'not like', '%mail.local')
                    ->orWhereNull('u.email');
            })
            ->whereBetween('i.reg_date', [$date_start, $date_end])
            ->groupBy('date', 'i.sex')
            ->selectRaw('date(i.reg_date) as date, i.sex, count(*) as cnt')
            ->get();

        foreach ($before as $v) {
            if ($v->sex == 1) {
                $before_stat['m'] = $v->cnt;
            } else if ($v->sex == 2) {
                $before_stat['w'] = $v->cnt;
            }
        }

        for ($date = Carbon::parse($date_start); $date->lte(Carbon::parse($date_end)); $date->addDay()) {
            $key = $date->toDateString();

            foreach ($period as $v) {
                if ($v->date == $key) {
                    if ($v->sex == 1) {
                        $before_stat['m'] += $v->cnt;
                    } else if ($v->sex == 2) {
                        $before_stat['w'] += $v->cnt;
                    }
                }
            }

            if ($before_stat['w'] == 0) {
                $period_stat[$key] = [
                    'm' => 0,
                    'w' => 0,
                ];
            } else {
                $period_stat[$key] = [
                    'm' => round($before_stat['m'] / $before_stat['w'], 3),
                    'w' => 1,
                ];
            }
        }

        return response()->json(['period' => $period_stat]);
    }

    /**
     * Статистика соотношения зарегистрированных мужчин к женщинам на данный момент
     * @return \Illuminate\Http\JsonResponse
     */
    public function genderStatsCurrent()
    {
        $stat = [
            'm' => [
                'count'   => 0,
                'percent' => 0,
            ],
            'w' => [
                'count'   => 0,
                'percent' => 0,
            ]
        ];

        $current = DB::table('users as u')
            ->join('users_info as i', 'u.user_id', '=', 'i.user_id')
            ->where('u.state', '=', 1)
            ->where(function ($query) {
                /* @var $query \Illuminate\Database\Query\Builder */
                $query->where('u.email', 'not like', '%mail.local')
                    ->orWhereNull('u.email');
            })
            ->groupBy('i.sex')
            ->selectRaw('i.sex, count(*) cnt')
            ->get();

        foreach ($current as $v) {
            if ($v->sex == 1) {
                $stat['m']['count'] = $v->cnt;
            } elseif ($v->sex == 2) {
                $stat['w']['count'] = $v->cnt;
            }
        }
        $total = $stat['m']['count'] + $stat['w']['count'];

        if ($stat['m']['count'] > 0) {
            $stat['m']['percent'] = round($stat['m']['count'] / $total * 100, 0);
        }

        if ($stat['w']['count'] > 0) {
            $stat['w']['percent'] = round($stat['w']['count'] / $total * 100, 0);
        }

        return response()->json(['current' => $stat]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function getStatPhotoVerification(Request $request)
    {
        if (empty($request->date)) {
            return response()->json([], 400);
        }

        try {
            if (!$date = Carbon::parse($request->date)) {
                throw new \Exception('Cannot get start_date.');
            }
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }

        $dateStart = (clone $date)->startOfDay();
        $dateEnd = (clone $date)->endOfDay();
        $dateStartServer = $this->tzConvertToUTC($dateStart);
        $dateEndServer = $this->tzConvertToUTC($dateEnd);

        // модераторы
        $admins = DB::table('users_photos_status')
            ->selectRaw('admin_id as id')
            ->where('admin_id', '>', 0)
            ->whereBetween('date', [$dateStartServer, $dateEndServer])
            ->groupBy('id')
            ->get();

        // не проверенные фото до даты выборки
        $notReviewed = DB::table('users_photos as up')
            ->join('users as u', 'up.user_id', '=', 'u.user_id')
            ->join('users_info as ui', 'up.user_id', '=', 'ui.user_id')
            ->join('users_stats as us', 'up.user_id', '=', 'us.user_id')
            ->leftJoin('users_photos_status as ups', 'up.photo_id', '=', 'ups.photo_id')
            ->leftJoin('users_photos_status as ups2', function ($join) {
                /* @var \Illuminate\Database\Query\JoinClause $join */
                $join->on('ups.photo_id', '=', 'ups2.photo_id');
                $join->on('ups.date', '>', 'ups2.date');
            })
            ->where('u.state', '=', 1)
            ->where(function ($query) {
                /* @var \Illuminate\Database\Query\Builder $query */
                $query->where('u.email', 'not like', '%mail.local')
                    ->orWhereNull('u.email');
            })
            ->where('us.banned_acc', '=', 0)
            ->where('us.banned_dev', '=', 0)
            ->where('us.blocked_photo', '=', 0)
            ->where('up.created_at', '<', $dateStartServer)
            ->whereNull('up.deleted_at')
            ->where(function ($query) use ($dateStartServer) {
                /* @var \Illuminate\Database\Query\Builder $query */
                $query->whereNull('ups.status_id')
                    ->orWhere('ups.date', '>', $dateStartServer);
            })
            ->whereNull('ups2.photo_id');

        $main = (clone $notReviewed)->where('up.main_photo', '=', 1)->count();
        $inner = (clone $notReviewed)->where('up.main_photo', '=', 0)->count();

        // загруженные фото в период выборки
        $notReviewedRange = DB::table('users_photos as up')
            ->join('users as u', 'up.user_id', '=', 'u.user_id')
            ->join('users_info as ui', 'up.user_id', '=', 'ui.user_id')
            ->join('users_stats as us', 'up.user_id', '=', 'us.user_id')
            ->select('up.photo_id', 'up.main_photo', DB::raw('unix_timestamp(up.created_at) as created_at'))
            ->where('u.state', '=', 1)
            ->where(function ($query) {
                /* @var \Illuminate\Database\Query\Builder $query */
                $query->where('u.email', 'not like', '%mail.local')
                    ->orWhereNull('u.email');
            })
            ->where('us.banned_acc', '=', 0)
            ->where('us.banned_dev', '=', 0)
            ->where('us.blocked_photo', '=', 0)
            ->whereBetween('up.created_at', [$dateStartServer, $dateEndServer])
            ->orderBy('up.created_at')
            ->get();

        // проверенные фото в период выборки
        $reviewedRange = DB::table('users_photos_status as t1')
            ->joinSub(
                DB::table('users_photos_status')
                    ->selectRaw('photo_id, min(date) max_date')
                    ->groupBy('photo_id'),
                't2',
                function ($join) {
                    /* @var \Illuminate\Database\Query\JoinClause $join */
                    $join->on('t1.photo_id', '=', 't2.photo_id');
                    $join->on('t1.date', '=', 't2.max_date');
                }
            )
            ->join('users_photos as up', 't1.photo_id', '=', 'up.photo_id')
            ->select('t1.photo_id', 't1.admin_id', 't1.status', 'up.main_photo', DB::raw('unix_timestamp(t1.date) as date'))
            ->where('t1.status', '>', 0)
            ->whereBetween('t1.date', [$dateStartServer, $dateEndServer])
            ->orderBy('t1.date')
            ->get();

//        // отложенные фото до даты выборки
//        $deferred = DB::table('users_photos_status as t1')
//            ->joinSub(
//                DB::query()->from('users_photos_status')
//                    ->select('photo_id', DB::raw('max(date) as max_date'))
//                    ->groupBy('photo_id'),
//                't2', function ($join) {
//                /* @var \Illuminate\Database\Query\JoinClause $join */
//                $join->on('t1.photo_id', '=', 't2.photo_id')
//                    ->on('t1.date', '=', 't2.max_date');
//            })
//            ->join('users as u', 'u.user_id', '=', 't1.user_id')
//            ->join('users_info as ui', 'ui.user_id', '=', 't1.user_id')
//            ->join('users_photos as up', 'up.photo_id', '=', 't1.photo_id')
//            ->where('u.state', '=', 1)
//            ->where('t1.status', '=', -1)
//            ->where('t1.date', '<', $dateStartServer)
//            ->count();
//
//        // отложенные фото которые были проверены
//        $notDeferred = DB::query()
//            ->fromSub(function ($query) use ($dateStartServer, $dateEndServer) {
//                /* @var \Illuminate\Database\Query\Builder $query */
//                $query->from('users_photos_status as ups1')
//                    ->joinSub(
//                        DB::query()->fromSub(function ($query) {
//                            /* @var \Illuminate\Database\Query\Builder $query */
//                            $query->from('users_photos_status')->orderBy('date');
//                        }, 'x')
//                            ->where('status', '=', -1)
//                            ->whereBetween('date', [$dateStartServer, $dateEndServer])
//                            ->groupBy('photo_id'),
//                        'ups2',
//                        'ups1.photo_id', '=', 'ups2.photo_id'
//                    )
//                    ->join('users as u', 'ups1.user_id', '=', 'u.user_id')
//                    ->join('users_info as ui', 'ups1.user_id', '=', 'ui.user_id')
//                    ->join('users_photos as up', 'ups1.photo_id', '=', 'up.photo_id')
//                    ->where('u.state', '=', 1)
//                    ->select('ups1.*')
//                    ->orderBy('ups1.date');
//            }, 'x')
//            ->select('x.*');

        $return = [
            'main'     => [],
            'inner'    => [],
            'deferred' => [],
        ];

        $admin_list = [
            'main' => [],
            'inner' => [],
            'deferred' => [],
        ];

        $period = new \DatePeriod(
            new \DateTime($dateStartServer, new \DateTimeZone($this->tzServer)),
            new \DateInterval('PT5M'),
            new \DateTime($dateEndServer, new \DateTimeZone($this->tzServer))
        );
        $dateCurOrig = (new \DateTime('now', new \DateTimeZone($this->tzServer)))->getTimestamp();

        $lastNotReviewedMain = $main;
        $lastNotReviewedInner = $inner;

        foreach ($period as $v) {
            /**
             * @var \DateTime $v
             */
            $key = (clone $v)->setTimezone(new \DateTimeZone($this->tzLocal))->format('H:i');
            $dStart = (clone $v)->getTimestamp();
            $dEnd = (clone $v)->modify('+5 minute')->getTimestamp();

            // скрывать фото после текущей даты
            $time = (clone $v)->getTimestamp();
            if ($time > $dateCurOrig) {
                continue;
            }

            $return['main'][$key][0] = $lastNotReviewedMain;
            $return['inner'][$key][0] = $lastNotReviewedInner;

            // добавление модераторов
            foreach ($admins as $admin) {
                $return['main'][$key][$admin->id] = 0;
                $return['inner'][$key][$admin->id] = 0;
                $return['deferred'][$key][$admin->id] = 0;
            }

            // загруженные фото
            foreach ($notReviewedRange as $k => $photo) {
                if ($photo->created_at >= $dStart && $photo->created_at < $dEnd) {
                    if ($photo->main_photo == 1) {
                        $return['main'][$key][0]++;
                        $lastNotReviewedMain++;
                    } else {
                        $return['inner'][$key][0]++;
                        $lastNotReviewedInner++;
                    }

                    $notReviewedRange->forget($k);
                } else {
                    break;
                }
            }

            // проверенные фото
            foreach ($reviewedRange as $k => $photo) {
                if ($photo->date >= $dStart && $photo->date < $dEnd) {
                    if ($photo->main_photo == 1) {
                        if (array_key_exists($photo->admin_id, $return['main'][$key])) {
                            $return['main'][$key][$photo->admin_id]++;
                        } else {
                            $return['main'][$key][$photo->admin_id] = 1;
                        }

                        if (!in_array($photo->admin_id, $admin_list['main'])) {
                            $admin_list['main'][] = $photo->admin_id;
                        }

                        if ($lastNotReviewedMain > 0) {
                            $lastNotReviewedMain--;
                        }
                    } else {
                        if (array_key_exists($photo->admin_id, $return['inner'][$key])) {
                            $return['inner'][$key][$photo->admin_id]++;
                        } else {
                            $return['inner'][$key][$photo->admin_id] = 1;
                        }

                        if (!in_array($photo->admin_id, $admin_list['inner'])) {
                            $admin_list['inner'][] = $photo->admin_id;
                        }

                        if ($lastNotReviewedInner > 0) {
                            $lastNotReviewedInner--;
                        }
                    }

                    $reviewedRange->forget($k);
                } else {
                    break;
                }
            }
        }

        // модераторы
        $admins = DB::table('admins')
            ->pluck('name', 'id')
            ->toArray();

        $data = [];
        foreach ($return as $k => $v) {
            if (!$v) {
                continue;
            }

            $labels = [];
            if ($k == 'deferred') {
                $labels[0] = 'Добавлено';
            } else {
                $labels[0] = 'Не проверенно';
            }

            foreach ($admin_list[$k] as $admin) {
                if (array_key_exists($admin, $admins)) {
                    $labels[$admin] = $admins[$admin];
                } else {
                    $labels[$admin] = 'Удален';
                }
            }

            $data[$k] = [
                'labels' => $labels,
                'data'   => $v
            ];
        }

        return response()->json($data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function getAdminPhotoStats(Request $request)
    {
        if (empty($request->date)) {
            return response()->json([], 400);
        }

        try {
            if (!$date = Carbon::parse($request->date)) {
                throw new \Exception('Cannot get start_date.');
            }
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }

        if ($date->lt(Carbon::parse('2019-04-01')) || $date->gt(Carbon::now())) {
            return response()->json([], 400);
        }

        $deferred = 0;
        if (!empty($request->deferred)) {
            $deferred = 1;
        } else {
            $main_photo = 0;
            if (!empty($request->main_photo)) {
                $main_photo = 1;
            }
        }

        $date_start_orig = (clone $date)->startOfDay();
        $date_end_orig = (clone $date)->endOfDay();

        $date_start_orig = $date_start_orig->format('Y-m-d H:i:s');
        $date_end_orig = $date_end_orig->format('Y-m-d H:i:s');
        $date_cur_orig = $this->tzConvertToLocalUnix(Carbon::now($this->tzServer)->timestamp);

        $date_start = $this->tzConvertToUTC($date_start_orig);
        $date_end = $this->tzConvertToUTC($date_end_orig);

        $admins = DB::table('admins as a')
            ->select('a.id', 'a.name')
            ->join('users_photos_status as ups', function ($join) {
                /**
                 * @var \Illuminate\Database\Query\JoinClause $join
                 */
                $join->on('a.id', '=', 'ups.admin_id');
                $join->whereExists(function ($query) {
                    /**
                     * @var \Illuminate\Database\Query\JoinClause $query
                     */
                    $query->selectRaw('1')
                        ->from('users_photos_status as ups1')
                        ->whereRaw('ups.status_id = ups1.status_id')
                        ->havingRaw('max(ups1.date) = ups.date');
                });
            })
            ->whereBetween('ups.date', [$date_start, $date_end])
            ->groupBy('id');

        if ($deferred) {
            $admins->where('ups.status', '=', -1);
        } else {
            $admins->where('ups.status', '>', 0);
        }

        $admins = $admins->get();

        $q1 = DB::table('users_photos as up')
            ->selectRaw('up.photo_id, unix_timestamp(up.created_at) as created_at, null as admin_id, null as date')
            ->join('users as u', 'up.user_id', '=', 'u.user_id')
            ->join('users_info as ui', 'up.user_id', '=', 'ui.user_id')
            ->join('users_stats as us', 'up.user_id', '=', 'us.user_id')
            ->leftJoin('users_photos_status as ups', function ($join) {
                /**
                 * @var \Illuminate\Database\Query\JoinClause $join
                 */
                $join->on('up.photo_id', '=', 'ups.photo_id');
                $join->whereExists(function ($query) {
                    /**
                     * @var \Illuminate\Database\Query\Builder $query
                     */
                    $query->selectRaw('1')
                        ->from('users_photos_status as ups1')
                        ->whereRaw('ups.status_id = ups1.status_id')
                        ->havingRaw('min(ups1.date) = ups.date');
                });
            })
            ->where('u.state', '=', 1)
            ->where('us.banned_acc', '=', 0)
            ->where('us.banned_dev', '=', 0)
            ->where('up.created_at', '<', $date_end)
            ->whereNull('ups.date');

        $q2 = DB::table('users_photos as up')
            ->selectRaw('up.photo_id, unix_timestamp(up.created_at) as created_at, ups.admin_id as admin_id, unix_timestamp(ups.date) as date')
            ->join('users as u', 'up.user_id', '=', 'u.user_id')
            ->join('users_info as ui', 'up.user_id', '=', 'ui.user_id')
            ->join('users_stats as us', 'up.user_id', '=', 'us.user_id')
            ->leftJoin('users_photos_status as ups', function ($join) {
                /**
                 * @var \Illuminate\Database\Query\JoinClause $join
                 */
                $join->on('up.photo_id', '=', 'ups.photo_id');
                $join->whereExists(function ($query) {
                    /**
                     * @var \Illuminate\Database\Query\Builder $query
                     */
                    $query->selectRaw('1')
                        ->from('users_photos_status as ups1')
                        ->whereRaw('ups.status_id = ups1.status_id')
                        ->havingRaw('max(ups1.date) = ups.date');
                });
            })
            ->join('admins as a', 'ups.admin_id', '=', 'a.id')
            ->where('u.state', '=', 1)
            ->where('us.banned_acc', '=', 0)
            ->where('us.banned_dev', '=', 0)
            ->whereBetween('ups.date', [$date_start, $date_end]);

        if (isset($main_photo)) {
            $q1->where('up.main_photo', '=', $main_photo);
            $q2->where('up.main_photo', '=', $main_photo)
                ->where('ups.status', '>', 0);
        } else {
            $q2->where('ups.status', '=', -1);
        }

        $photos = $q1->unionAll($q2)
            ->orderBy('created_at')
            ->orderBy('date')
            ->get();

        $period = new \DatePeriod(
            new \DateTime($date_start_orig),
            new \DateInterval('PT5M'),
            new \DateTime($date_end_orig)
        );

        foreach ($photos as $photo) {
            if ($photo->created_at) {
                $photo->created_at = $this->tzConvertToLocalUnix($photo->created_at);
            }
            if ($photo->date) {
                $photo->date = $this->tzConvertToLocalUnix($photo->date);
            }
        }

        $return = [];
        foreach ($period as $date) {
            /**
             * @var $date \DateTime
             */
            $time = $date->getTimestamp();
            $key = $date->format('H:i');

            $return[$key][0] = 0;

            // скрывать фото после текущей даты
            if ($time > $date_cur_orig) {
                continue;
            }

            foreach ($admins as $admin) {
                $return[$key][$admin->id] = 0;
            }

            foreach ($photos as $k => $photo) {
                if ($photo->created_at > $time) {
                    break;
                }

                if (!$photo->date || $photo->date > $time) {
                    $return[$key][0]++;
                } else {
                    if ($photo->date <= $time) {
                        $return[$key][0]++;
                    }
                    $return[$key][$photo->admin_id]++;

                    unset($photos[$k]);
                }
            }
        }

        $titles = [0 => 'Не проверено'];
        foreach ($admins as $admin) {
            $titles[$admin->id] = $admin->name;
        }

        $return = [
            'labels' => $titles,
            'data'   => $return
        ];

        return response()->json($return);
    }
}