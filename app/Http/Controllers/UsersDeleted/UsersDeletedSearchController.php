<?php

namespace App\Http\Controllers\UsersDeleted;

use App\Http\Controllers\Controller;
use Datetime;
use DateTimeZone;
use DB;
use Illuminate\Http\Request;
use App\Http\Traits\GeoTimezone;

class UsersDeletedSearchController extends Controller
{
    use GeoTimezone;

    protected $limit = 200;

    protected $tz = 'Asia/Krasnoyarsk';

    public function __construct()
    {
        if (!$this->setGeoTimezone()) {
            return response()->json(['error' => 'Cannot get timezone'], 500);
        }

        return true;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function getData(Request $request)
    {
        $page = $request->input('page', 1) ?: 1;
        if ($page != intval($page) || $page < 1) {
            $page = 1;
        };

        $limit = $request->input('per_page', 0) ?: $this->limit;
        if ($limit != intval($limit) || $limit < 1) {
            $limit = $this->limit;
        };

        $offset = ($page - 1) * $limit;
        $sort = $request->input('sort', 'user_id|desc');
        $filter = $request->input('filter', []);
        $filter = @json_decode($filter, true);

        $users = DB::table(DB::raw('users as u force index (primary)'))
            ->join('users_info as i', 'i.user_id', '=', 'u.user_id')
            ->join('users_stats as s', 's.user_id', '=', 'u.user_id')
            ->join('users_delete as ud', 'ud.user_id', '=', 'u.user_id')
            ->where('u.state', '=', 0)
            ->where(function ($query) {
                /* @var $query \Illuminate\Database\Query\Builder */
                $query->where('u.email', '=', '')
                    ->orWhereNull('u.email');
            });

        if ($filter && is_array($filter)) {
            $filter = [
                'user_id'        => !empty($filter['user_id']) ? intval($filter['user_id']) : '',
                'name'           => !empty($filter['name']) ? $filter['name'] : '',
                'city'           => !empty($filter['city']) ? $filter['city'] : '',
                'gender'         => !empty($filter['gender']) ? $filter['gender'] : '',
                'years_min'      => !empty($filter['years_min']) ? intval($filter['years_min']) : '',
                'years_max'      => !empty($filter['years_max']) ? intval($filter['years_max']) : '',
                'platform'       => !empty($filter['platform']) ? $filter['platform'] : '',
                'vip'            => !empty($filter['vip']) ? $filter['vip'] : '',
                'deleted_at_min' => !empty($filter['deleted_at_min']) ? $filter['deleted_at_min'] : '',
                'deleted_at_max' => !empty($filter['deleted_at_max']) ? $filter['deleted_at_max'] : ''
            ];

            if ($filter['user_id']) {
                $users->where('i.user_id', '=', $filter['user_id']);
            }

            if ($filter['name']) {
                $users->where('i.name', 'like', '%' . $filter['name'] . '%');
            }

            if ($filter['city']) {
                $users->where('i.city', 'like', '%' . $filter['city'] . '%');
            }

            if (in_array($filter['gender'], ['1', '2'])) {
                $users->where('i.sex', '=', $filter['gender']);
            }

            $tz_utc = new DateTimeZone('UTC');
            $tz_local = new DateTimeZone($this->tz);
            if ($filter['deleted_at_min']) {
                $date = new DateTime($filter['deleted_at_min']);
                $date->setTime(0, 0, 0);
                $date = new Datetime($date->format('Y-m-d H:i:s'), $tz_local);
                $date->setTimezone($tz_utc);
                $users->where('ud.deleted_at', '>=', $date->format('Y-m-d H:i:s'));
            }
            if ($filter['deleted_at_max']) {
                $date = new DateTime($filter['deleted_at_max']);
                $date->setTime(23, 59, 59);
                $date = new Datetime($date->format('Y-m-d H:i:s'), $tz_local);
                $date->setTimezone($tz_utc);
                $users->where('ud.deleted_at', '<=', $date->format('Y-m-d H:i:s'));
            }

            if ($filter['years_min']) {
                $users->where('i.years', '>=', $filter['years_min']);
            }
            if ($filter['years_max']) {
                $users->where('i.years', '<=', $filter['years_max']);
            }

            if ($filter['platform'] && in_array($filter['platform'], ['ios', 'android'])) {
                $users->where('s.app_version', 'like', '%' . $filter['platform'] . '%');
            }

            if (in_array($filter['vip'], ['yes', 'no'])) {
                $users->where('i.vip', '=', $filter['vip'] == 'yes' ? 1 : 0);
            }
        }

        $count = clone $users;
        $count->select(DB::raw('count(u.user_id) as count'));

        $users->leftJoin('users_photos as ph', 'ph.photo_id', '=', 'i.photo_id')
            ->select('i.user_id', 'i.avatar', 'i.name', 'i.sex', 'i.years', 'i.city', 'i.points', 'i.vip',
                'i.reg_date', 'ph.photo', 's.achievements as ach', 's.gifts', 's.vip_order_method as vip_order', 's.vip_expires',
                's.messages', 's.dialogs', 's.online as online_total', 's.app_version', 's.banned_acc', 's.banned_dev', 's.banned_date',
                's.banned_admin', 'ud.reason_code as deleted_code', 'ud.reason_text as deleted_text', 'ud.deleted_at');

        if ($sort) {
            $tmp = explode('|', $sort);

            if (is_array($tmp)) {
                if (empty($tmp[1]) || !in_array($tmp[1], ['asc', 'desc'])) {
                    $tmp[1] = 'asc';
                }

                if ($order_fields = explode(',', $tmp[0])) {
                    foreach ($order_fields as $v) {
                        $users->orderBy(trim($v), $tmp[1]);
                    }
                } else {
                    $users->orderBy($tmp[0], $tmp[1]);
                }
            }
        }

        $return = [];

        $users_count = $count->first()->count;
        $users_data = $users->offset($offset)->limit($limit)->get();

        $photos_data = [];

        unset($count, $users);

        $keys = [];
        if ($users_data) {
            $keys = $users_data->keyBy('user_id')->keys();
        }

        if ($keys) {
            // фото полученных пользователей
            $photos = DB::table('users_photos')
                ->select('user_id', 'photo')
                ->whereIn('user_id', $keys)
                ->get();

            foreach ($photos as $v) {
                $photos_data[$v->user_id][] = env('STATIC_PHOTO_URL') . $v->photo;
            }

            unset($photos);
        }

        $now = new DateTime();

        foreach ($users_data as $u) {
            $photos = [];
            if (!empty($photos_data[$u->user_id])) {
                $photos = array_values(array_diff($photos_data[$u->user_id], [env('STATIC_PHOTO_URL') . $u->photo]));
            }

            if ($u->app_version) {
                $app_version = explode(':', $u->app_version);
                if (!empty($app_version)) {
                    $app_version = array_unique($app_version);
                }
            } else {
                $app_version = [];
            }

            $vip_expires_days = null;
            if ($u->vip_expires) {
                $vip_expires = new DateTime($u->vip_expires);
                $vip_expires_days .= $now->diff($vip_expires)->format('%r%a (%H:%I)');
            }

            $rs = [
                'user_id'      => $u->user_id,
                'avatar'       => (!empty($u->avatar) ? (env('STATIC_AVA_URL') . $u->avatar) : ''),
                'photo'        => (!empty($u->photo) ? (env('STATIC_PHOTO_URL') . $u->photo) : ''),
                'name'         => $u->name,
                'gender'       => $u->sex,
                'years'        => $u->years,
                'city'         => $u->city,
                'achievements' => (!empty($u->ach) ? explode(':', $u->ach) : []),
                'gifts'        => $u->gifts,
                'points'       => $u->points,
                'vip'          => [
                    'status'  => (int)$u->vip,
                    'payment' => (int)in_array($u->vip_order, ['itunes', 'googlepay']),
                    'expires' => $vip_expires_days,
                    'vip_adm' => $u->vip_order == 'skylove' ? 1 : 0,
                ],
                'reg_date'     => $this->tzConvertToLocal($u->reg_date),
                'deleted_at'   => $this->tzConvertToLocal($u->deleted_at),
                'deleted_code' => $u->deleted_code,
                'deleted_text' => $u->deleted_text,
                'messages'     => $u->messages ?: 0,
                'dialogs'      => $u->dialogs ?: 0,
                'app_version'  => $app_version,
                'banned'       => ($u->banned_acc || $u->banned_dev),
                'banned_acc' => $u->banned_acc,
                'banned_dev' => $u->banned_dev,
                'banned_date' => $u->banned_acc || $u->banned_dev ? $this->tzConvertToLocal($u->banned_date) : '',
                'banned_admin' => $u->banned_acc || $u->banned_dev ? $u->banned_admin : '',
                'photos'       => $photos
            ];

            $return[] = $rs;
        }

        $response = [
            'total'         => (int)$users_count,
            'per_page'      => (int)$limit,
            'current_page'  => (int)$page,
            'last_page'     => ceil($users_count / $limit),
            'next_page_url' => null,
            'prev_page_url' => null,
            'from'          => intval($offset + 1),
            'to'            => intval($page * $limit),
            'data'          => $return,
        ];

        return response()->json($response);
    }
}
