<?php

namespace App\Http\Controllers\UsersPhotos;

use App\Banlist;
use App\BanlistLog;
use App\Http\Controllers\Controller;
use App\Http\Traits\GeoTimezone;
use App\User;
use App\UsersAlert;
use App\UsersAppVersion;
use App\UsersInfo;
use App\UsersPhoto;
use App\UsersPhotoStatus;
use App\UsersPhotoStatusInspect;
use App\UsersStat;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;

class UsersPhotosStatusInspectController extends Controller
{
    use GeoTimezone;

    protected $limit = 200;

    protected $admin = null;

    protected $permissions = [];

    /**
     * UsersPhotosStatusInspectController constructor.
     * @throws Exception
     */
    public function __construct()
    {
        if (!$this->setGeoTimezone()) {
            throw new Exception('Cannot get timezone', 500);
        }

        if ($user = Auth::user()) {
            $this->admin = $user;
            $this->permissions = $user->permissions()->pluck('permission')->toArray();
        } else {
            throw new Exception('Cannot get auth user information', 500);
        }

        return true;
    }

    /**
     * Список модераторов с количеством установленных статусов
     * @return \Illuminate\Http\JsonResponse
     */
    public function getModerators()
    {
        $status = DB::table('users_photos as up')
            ->join('users_photos_status_inspect as upi', 'up.photo_id', '=', 'upi.photo_id')
            ->join('admins as a', 'upi.admin_id', '=', 'a.id')
            ->groupBy('a.id', 'upi.admin_status', 'up.main_photo')
            ->select('a.id', 'a.name', 'a.deleted_at', 'up.main_photo', 'upi.admin_status as status',
                DB::raw('cast(sum(if(upi.inspector_status is null, 1, 0)) as unsigned) not_confirmed'),
                DB::raw('cast(sum(if(upi.admin_status = upi.inspector_status, 1, 0)) as unsigned) confirmed'),
                DB::raw('cast(sum(if(upi.admin_status != upi.inspector_status, 1, 0)) as unsigned) changed'))
            ->get();

        $data = [];
        foreach ($status as $v) {
            if (empty($data[$v->id])) {
                $data[$v->id] = [
                    'id'         => $v->id,
                    'name'       => $v->name,
                    'deleted_at' => $v->deleted_at,
                    'status'     => [
                        'deferred'           => [
                            'not_confirmed' => 0,
                            'confirmed'     => 0,
                            'changed'       => 0,
                        ],
                        'verification_main'  => [
                            'not_confirmed' => 0,
                            'confirmed'     => 0,
                            'changed'       => 0,
                        ],
                        'blocked'            => [
                            'not_confirmed' => 0,
                            'confirmed'     => 0,
                            'changed'       => 0,
                        ],
                        'verification_inner' => [
                            'not_confirmed' => 0,
                            'confirmed'     => 0,
                            'changed'       => 0,
                        ],
                        'forbidden_avatar'   => [
                            'not_confirmed' => 0,
                            'confirmed'     => 0,
                            'changed'       => 0,
                        ],
                        'forbidden_show'     => [
                            'not_confirmed' => 0,
                            'confirmed'     => 0,
                            'changed'       => 0,
                        ],
                        'banned_acc'         => [
                            'not_confirmed' => 0,
                            'confirmed'     => 0,
                            'changed'       => 0,
                        ],
                        'banned_dev'         => [
                            'not_confirmed' => 0,
                            'confirmed'     => 0,
                            'changed'       => 0,
                        ],
                    ],
                ];
            }

            if ($v->status == -1) {
                $status_key = 'deferred';
            } elseif ($v->status == 1) {
                $status_key = $v->main_photo == 1 ? 'verification_main' : 'verification_inner';
            } elseif ($v->status == 2 && $v->main_photo == 0) {
                $status_key = 'forbidden_avatar';
            } elseif ($v->status == 3 && $v->main_photo == 0) {
                $status_key = 'forbidden_show';
            } elseif ($v->status == 4 && $v->main_photo == 1) {
                $status_key = 'blocked';
            } elseif ($v->status == 5) {
                $status_key = 'banned_acc';
            } elseif ($v->status == 6) {
                $status_key = 'banned_dev';
            } else {
                continue;
            }

            $data[$v->id]['status'][$status_key]['not_confirmed'] += $v->not_confirmed;
            $data[$v->id]['status'][$status_key]['confirmed'] += $v->confirmed;
            $data[$v->id]['status'][$status_key]['changed'] += $v->changed;
        }

        return response()->json(['moderators' => array_values($data)]);
    }

    /**
     * Список фото указанного статуса
     * @param int $admin_id Id модератора
     * @param string $status Имя статуса (допустимы значения: deferred, verification-main, verification-inner, forbidden-avatar, forbidden-show, blocked)
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getModeratorPhotosStatus($admin_id, $status, Request $request)
    {
        $available_status = [
            'deferred',
            'verification-main',
            'verification-inner',
            'forbidden-avatar',
            'forbidden-show',
            'blocked',
            'banned-acc',
            'banned-dev',
        ];

        $admin_id = intval($admin_id);
        $page = (int)$request->input('page', 1);
        $page = $page < 1 ? 1 : $page;
        $offset = ($page - 1) * $this->limit;

        if ($filter = $request->input('filter', [])) {
            if (!is_array($filter)) {
                $filter = [];
            }
        }

        if ($mode = $request->input('mode', '')) {
            if (!in_array($mode, ['confirmed', 'changed'])) {
                $mode = '';
            }
        }

        if (in_array($status, ['deferred', 'banned-acc', 'banned-dev'])) {
            $main_photo = isset($filter['show_main_photo']) ? $filter['show_main_photo'] : '';

            if (!in_array($main_photo, [0, 1])) {
                $main_photo = 0;
            }
        }

        if (!$admin_id) {
            return response()->json(['error' => 'Invalid parameter "admin_id"'], 400);
        }

        if (!in_array($status, $available_status)) {
            return response()->json(['error' => 'Invalid parameter "status"'], 400);
        }

        $moderator = DB::table('admins')
            ->where('id', '=', $admin_id)
            ->select('id', 'name', 'active', 'level', 'deleted_at')
            ->get()
            ->first();

        if (!$moderator) {
            return response()->json(['error' => 'Moderator not found', 400]);
        }

        $photos = DB::table('users_photos as up')
            ->join('users_photos_status_inspect as upi', function ($join) use ($admin_id) {
                /* @var \Illuminate\Database\Query\JoinClause $join */
                $join->on('up.photo_id', '=', 'upi.photo_id');
                $join->on('upi.admin_id', '=', DB::raw($admin_id));
            })
            ->join('admins as a', 'upi.admin_id', '=', 'a.id')
            ->join('users_info as ui', 'up.user_id', '=', 'ui.user_id')
            ->join('users_stats as us', 'up.user_id', '=', 'us.user_id')
            ->leftJoin('push_sessions as ps', 'up.user_id', '=', 'ps.user_id');

        if ($mode == 'confirmed') {
            $photos->whereNotNull('upi.inspector_id')
                ->whereColumn('upi.admin_status', '=', 'upi.inspector_status');
        } elseif ($mode == 'changed') {
            $photos->whereNotNull('upi.inspector_id')
                ->whereColumn('upi.admin_status', '!=', 'upi.inspector_status');
        } else {
            $photos->whereNull('upi.inspector_id');
        }

        if (isset($main_photo)) {
            $photos->where('up.main_photo', $main_photo);
        }

        if ($status == 'deferred') {
            $photos->where('upi.admin_status', -1);
        } elseif ($status == 'verification-main') {
            $photos->where('upi.admin_status', 1)
                ->where('up.main_photo', 1);
        } elseif ($status == 'verification-inner') {
            $photos->where('upi.admin_status', 1)
                ->where('up.main_photo', 0);
        } elseif ($status == 'forbidden-avatar') {
            $photos->where('upi.admin_status', 2)
                ->where('up.main_photo', 0);
        } elseif ($status == 'forbidden-show') {
            $photos->where('upi.admin_status', 3)
                ->where('up.main_photo', 0);
        } elseif ($status == 'blocked') {
            $photos->where('upi.admin_status', 4)
                ->where('up.main_photo', 1);
        } elseif ($status == 'banned-acc') {
            $photos->where('upi.admin_status', 5);
        } elseif ($status == 'banned-dev') {
            $photos->where('upi.admin_status', 6);
        } else {
            return response()->json(['error' => 'Invalid parameter "status"'], 400);
        }

        $photos_count = (clone $photos)->count(DB::raw('DISTINCT up.photo_id'));

        if ($mode == 'changed') {
            $photos->join('users_photos_status_inspect as upi_last', function ($join) {
                /* @var \Illuminate\Database\Query\JoinClause $join */
                $join->on('up.photo_id', '=', 'upi_last.photo_id');
                $join->whereNull('upi_last.inspector_id');
            })
                ->orderBy('upi_last.date', 'desc');
        } else {
            $photos->orderBy('upi.date', 'desc');
        }

        $photos_data = $photos->select('up.photo_id', 'up.photo', 'up.main_photo', 'up.deleted_at',
            'a.name as admin', 'upi.admin_id', 'upi.admin_status as status', 'upi.date', 'upi.level', 'upi.associated',
            'ui.user_id', 'ui.name', 'ui.sex', 'ui.years', 'ui.city', 'ui.avatar', 'ui.vip',
            'us.banned_acc', 'us.banned_dev', DB::raw('count(ps.user_id) as dev_count'))
            ->groupBy('up.photo_id')
            ->offset($offset)
            ->limit($this->limit)
            ->get();

        if ($photos_data->count() == 0 && $page > 1) {
            $last = intval(ceil($photos_count / $this->limit));

            return $this->getModeratorPhotosStatus($admin_id, $status, (new Request())->replace(['page' => $last]));
        }

        $data = [];
        foreach ($photos_data as $v) {
            if ($mode == 'changed') {
                $inspect = DB::table('users_photos_status_inspect as upi')
                    ->join('admins as a', 'upi.admin_id', '=', 'a.id')
                    ->select('upi.admin_id', 'upi.admin_status as status', 'a.name as admin', 'upi.date', 'upi.associated', 'upi.level')
                    ->where('upi.photo_id', $v->photo_id)
                    ->where('upi.date', '>=', $v->date)
                    ->orderBy('upi.date')
                    ->get();

                $row = [
                    'photo_id'          => $v->photo_id,
                    'prev'              => env('STATIC_GALLERY_PREW_URL') . $v->photo,
                    'href'              => env('STATIC_GALLERY_URL') . $v->photo,
                    'main_photo'        => $v->main_photo,
                    'deleted_at'        => $v->deleted_at ? $this->tzConvertToLocal($v->deleted_at) : null,
                    'verification'      => [],
                    'verification_last' => [
                        'admin_id'   => $inspect->last()->admin_id,
                        'admin'      => $inspect->last()->admin,
                        'status'     => $inspect->last()->status,
                        'date'       => $this->tzConvertToLocal($inspect->last()->date),
                        'associated' => $inspect->last()->associated,
                        'level'      => $inspect->last()->level,
                    ],
                    'user'              => [
                        'user_id'    => $v->user_id,
                        'name'       => $v->name,
                        'years'      => $v->years,
                        'city'       => $v->city,
                        'avatar'     => $v->avatar ? env('STATIC_AVA_URL') . $v->avatar : '',
                        'vip'        => $v->vip,
                        'banned_acc' => $v->banned_acc,
                        'banned_dev' => $v->banned_dev,
                        'devices'    => $v->dev_count > 0 ? 1 : 0,
                    ],
                ];

                foreach ($inspect as $i) {
                    $row['verification'][] = [
                        'admin_id'   => $i->admin_id,
                        'admin'      => $i->admin,
                        'status'     => $i->status,
                        'date'       => $this->tzConvertToLocal($i->date),
                        'associated' => $i->associated,
                        'level'      => $i->level,
                    ];
                }

                $data[] = $row;
            } else {
                $data[] = [
                    'photo_id'     => $v->photo_id,
                    'prev'         => env('STATIC_GALLERY_PREW_URL') . $v->photo,
                    'href'         => env('STATIC_GALLERY_URL') . $v->photo,
                    'main_photo'   => $v->main_photo,
                    'deleted_at'   => $v->deleted_at ? $this->tzConvertToLocal($v->deleted_at) : null,
                    'verification' => [
                        'status'     => $v->status,
                        'admin'      => $v->admin,
                        'date'       => $this->tzConvertToLocal($v->date),
                        'associated' => $v->associated,
                    ],
                    'user'         => [
                        'user_id'    => $v->user_id,
                        'name'       => $v->name,
                        'years'      => $v->years,
                        'city'       => $v->city,
                        'avatar'     => $v->avatar ? env('STATIC_AVA_URL') . $v->avatar : '',
                        'vip'        => $v->vip,
                        'banned_acc' => $v->banned_acc,
                        'banned_dev' => $v->banned_dev,
                        'devices'    => $v->dev_count > 0 ? 1 : 0,
                    ],
                ];
            }
        }

        $response = [
            'from'         => intval($offset + 1),
            'to'           => intval($page * $this->limit),
            'total'        => intval($photos_count),
            'current_page' => intval($page),
            'last_page'    => intval(ceil($photos_count / $this->limit)),
            'data'         => [
                'moderator' => $moderator,
                'photos'    => $data,
            ],
        ];

        if ($response['from'] > $response['total']) {
            $response['from'] = $response['total'];
        }

        if ($response['to'] > $response['total']) {
            $response['to'] = $response['total'];
        }

        return response()->json($response);
    }

    /**
     * Поиск проверенных фото пользователя
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function searchUserPhotos(Request $request)
    {
        $userId = $request->input('user');
        $page = (int)$request->input('page', 1);
        $page = $page < 1 ? 1 : $page;
        $offset = ($page - 1) * $this->limit;

        $user = UsersInfo::with(['stats'])
            ->whereUserId($userId)
            ->first();

        if (!$user) {
            return response()->json([]);
        }

        $responseUser = [
            'user_id'    => $user->user_id,
            'name'       => $user->name,
            'years'      => $user->years,
            'city'       => $user->city,
            'avatar'     => $user->avatar ? env('STATIC_AVA_URL') . $user->avatar : '',
            'vip'        => $user->vip,
            'banned_acc' => $user->stats->banned_acc,
            'banned_dev' => $user->stats->banned_dev,
            'devices'    => $user->stats->app_version ? 1 : 0,
        ];
        $responsePhotos = [];

        $photos = UsersPhoto::with([
            'statusInspect' => function ($query) {
                /* @var \Illuminate\Database\Eloquent\Builder $query */
                $query->orderBy('status_id');
            },
            'statusInspect.admin'])
            ->whereHas('statusInspect')
            ->where('user_id', $user->user_id);

        $photosCount = (clone $photos)->count();
        $photos = $photos
            ->offset($offset)
            ->limit($this->limit)
            ->get();

        if ($photos->count() == 0 && $page > 1) {
            $last = intval(ceil($photosCount / $this->limit));

            return $this->searchUserPhotos((new Request())->replace(['user' => $userId, 'page' => $last]));
        }

        foreach ($photos as $photo) {
            /* @var \Illuminate\Database\Eloquent\Collection|UsersPhotoStatusInspect[] $inspect */
            $inspect = $photo->statusInspect;

            $row = [
                'photo_id'     => $photo->photo_id,
                'prev'         => env('STATIC_GALLERY_PREW_URL') . $photo->photo,
                'href'         => env('STATIC_GALLERY_URL') . $photo->photo,
                'main_photo'   => $photo->main_photo,
                'deleted_at'   => $photo->deleted_at ? $this->tzConvertToLocal($photo->deleted_at) : null,
                'verification' => [],
            ];

            foreach ($inspect as $i) {
                $row['verification'][] = [
                    'admin_id'   => $i->admin_id,
                    'admin'      => $i->admin->name,
                    'status'     => $i->admin_status,
                    'date'       => $this->tzConvertToLocal($i->date),
                    'associated' => $i->associated,
                    'level'      => $i->level,
                ];
            }

            $responsePhotos[] = $row;
        }

        $response = [
            'from'         => intval($offset + 1),
            'to'           => intval($page * $this->limit),
            'total'        => intval($photosCount),
            'current_page' => intval($page),
            'last_page'    => intval(ceil($photosCount / $this->limit)),
            'data'         => [
                'user'   => $responseUser,
                'photos' => $responsePhotos,
            ],
        ];

        if ($response['from'] > $response['total']) {
            $response['from'] = $response['total'];
        }

        if ($response['to'] > $response['total']) {
            $response['to'] = $response['total'];
        }

        return response()->json($response);
    }

    /**
     * Подтверждение статуса фото установленного модератором
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function confirmPhotosStatus(Request $request)
    {
        $photos = $request->input('photos', []);
        $admin_id = (int)$request->input('admin_id', 0);
        $user = User::where('id', Auth::user()->id)
            ->get(['id', 'level'])
            ->first();

        if (!$user) {
            return response()->json(['error' => 'Cannot get user level'], 400);
        }

        if (!is_array($photos) || empty($photos)) {
            return response()->json(['error' => 'Empty photos'], 400);
        }

        if ($admin_id <= 0) {
            return response()->json(['error' => 'Empty admin_id'], 400);
        }

        if ($admin_id == $user->id) {
            return response()->json(['error' => 'Invalid admin_id'], 400);
        }

        $photos_id = [];
        foreach ($photos as $photo) {
            $photo = intval($photo);

            if ($photo <= 0) {
                return response()->json(['error' => 'Invalid photo_id'], 400);
            }

            $photos_id[] = $photo;
        }

        $data = DB::table('users_photos as up')
            ->join('users_photos_status_inspect as upi', function ($join) use ($admin_id) {
                /* @var \Illuminate\Database\Query\JoinClause $join */
                $join->on('up.photo_id', '=', 'upi.photo_id');
                $join->on('upi.admin_id', '=', DB::raw($admin_id));
            })
            ->whereNull('upi.inspector_id')
            ->where('upi.level', '<', $user->level)
            ->whereIn('upi.photo_id', $photos_id)
            ->select('upi.status_id', 'upi.photo_id', 'upi.admin_id', 'upi.admin_status', 'upi.level', 'upi.associated')
            ->get();

        if ($data->count() == 0) {
            return response()->json(['error' => 'Photos not found'], 400);
        }

        try {
            DB::beginTransaction();

            foreach ($data as $v) {
                // нельзя подтвердить статусы "не проверенно" и "отложено"
                if ($v->admin_status == -1 || $v->admin_status == 0) {
                    continue;
                }

                DB::table('users_photos_status_inspect')
                    ->where('status_id', $v->status_id)
                    ->update([
                        'inspector_id'     => $user->id,
                        'inspector_status' => $v->admin_status
                    ]);

                DB::table('users_photos_status_inspect')
                    ->insert([
                        'photo_id'     => $v->photo_id,
                        'admin_id'     => $user->id,
                        'admin_status' => $v->admin_status,
                        'level'        => $user->level,
                        'associated'   => $v->associated,
                    ]);
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();

            return response()->json(['error' => 'MySQL error:' . $e->getMessage()], 500);
        }

        return response()->json([]);
    }

    /**
     * Изменение статуса
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function changeStatus(Request $request)
    {
        $changed = [];

        $photos = $request->input('photos', []);
        $status = (int)$request->input('status', 0);

        if (!is_array($photos) || empty($photos)) {
            return response()->json(['error' => 'Empty photos'], 400);
        }

        if (!in_array($status, [1, 2, 3, 4, 5, 6])) {
            return response()->json(['error' => 'Invalid status'], 400);
        }

        try {
            $photosInfo = UsersPhoto::with([
                'stat'          => function ($query) {
                    /* @var \Illuminate\Database\Eloquent\Builder $query */
                    $query->select('user_id', 'banned_acc', 'banned_dev', 'blocked_photo');
                },
                'statusInspect' => function ($query) {
                    /* @var \Illuminate\Database\Eloquent\Builder $query */
                    $query->select('status_id', 'photo_id', 'admin_id', 'admin_status', 'inspector_id', 'inspector_status', 'associated', 'level')
                        ->orderBy('status_id', 'desc')
                        ->orderBy('date', 'desc');
                }])
                ->whereIn('photo_id', $photos)
                ->get(['photo_id', 'user_id', 'main_photo', 'status', 'deleted_at']);

            if ($photosInfo->count() == 0) {
                return response()->json([]);
            }

            $unbannedAcc = [];
            $unbannedDev = [];
            $unbannedAccSkip = [];
            $unbannedDevSkip = [];

            foreach ($photosInfo as $photo) {
                /* @var \Illuminate\Database\Eloquent\Collection|UsersPhotoStatusInspect[] $photoStatusInspect */
                $photoStatusInspect = $photo->statusInspect;
                /* @var UsersPhotoStatusInspect $first */
                $first = $photoStatusInspect->values()->get(0);

                if ($first->admin_status == 5) {
                    if (in_array($photo->user_id, $unbannedAcc)) {
                        $unbannedAccSkip[] = $photo->photo_id;
                    } else {
                        $unbannedAcc[] = $photo->user_id;
                    }
                }

                if ($first->admin_status == 6) {
                    if (in_array($photo->user_id, $unbannedDev)) {
                        $unbannedDevSkip[] = $photo->photo_id;
                    } else {
                        $unbannedDev[] = $photo->user_id;
                    }
                }
            }

            DB::beginTransaction();

            $redis = Redis::connection();
            $redisDel = [];

            foreach ($photosInfo as $photo) {
                /* @var UsersStat $photoUserStat */
                $photoUserStat = $photo->stat;
                /* @var \Illuminate\Database\Eloquent\Collection|UsersPhotoStatusInspect[] $photoStatusInspect */
                $photoStatusInspect = $photo->statusInspect;

                if ($photoStatusInspect->count() == 0) {
                    continue;
                }

                /* @var UsersPhotoStatusInspect $statusFirst */
                $statusFirst = $photoStatusInspect->values()->get(0);
                /* @var array|UsersPhotoStatusInspect $statusSecond */
                $statusSecond = $photoStatusInspect->count() > 1 ? $photoStatusInspect->get(1) : [];

                // основное фото нельзя запретить для аватара и запретить для показа
                // внутренне фото нельзя временно заблокировать
                if ($photo->main_photo == 1 && in_array($status, [2, 3]) || ($photo->main_photo == 0 && $status == 4)) {
                    continue;
                }

                // нельзя изменить статус другого контролера
                if ($statusFirst->admin_id != $this->admin->id && $statusFirst->level >= $this->admin->level) {
                    continue;
                }

                if ($statusFirst->admin_id == $this->admin->id && !$statusSecond) {
                    continue;
                }

                if ($statusFirst->admin_status == 4) {
                    // удаление уведомлений о блокировке
                    UsersAlert::whereUserId($photo->user_id)
                        ->whereType(1)
                        ->whereNull('read_at')
                        ->delete();

                    $redis->publish('fcm_push', json_encode([
                        'user_id'         => $photo->user_id,
                        'push_type'       => 'system_msg',
                        'system_msg_data' => [
                            'type' => 'refresh_badges',
                            'data' => []
                        ]
                    ]));
                } elseif ($statusFirst->admin_status == 5) {
                    if (in_array($photo->photo_id, $unbannedAccSkip)) {
                        continue;
                    }

                    // удаление бана по аккаунту
                    Banlist::whereUserId($photo->user_id)->delete();
                    BanlistLog::create([
                        'admin_id'   => $this->admin->id,
                        'user_id'    => $photo->user_id,
                        'status'     => 0,
                        'photo_id'   => $photo->photo_id,
                        'main_photo' => $photo->main_photo,
                    ]);

                    $redisDel[] = env('REDIS_PREFIX_BAN_USER') . $photo->user_id;

                    $userPhotos = UsersPhoto::with(['statusInspect' => function ($query) {
                            /* @var \Illuminate\Database\Eloquent\Builder $query */
                            $query->select('status_id', 'photo_id', 'admin_id', 'admin_status', 'inspector_id', 'inspector_status', 'associated', 'level')
                                ->orderBy('status_id', 'desc')
                                ->orderBy('date', 'desc');
                        }])
                        ->whereUserId($photo->user_id)
                        ->whereNull('deleted_at')
                        ->get(['photo_id', 'user_id', 'main_photo', 'status']);

                    if ($userPhotos->count() > 0) {
                        // удаление статусов с других фото пользователя
                        foreach ($userPhotos as $userPhoto) {
                            if ($photo->photo_id == $userPhoto->photo_id) {
                                continue;
                            }

                            /* @var \Illuminate\Database\Eloquent\Collection|UsersPhotoStatusInspect[] $userPhotoStatusInspect */
                            $userPhotoStatusInspect = $userPhoto->statusInspect;

                            if ($userPhotoStatusInspect->count() > 0) {
                                /* @var UsersPhotoStatusInspect $userPhotoStatusFirst */
                                $userPhotoStatusFirst = $userPhotoStatusInspect->values()->get(0);

                                // если устройство забанено по этому фото, то пропускаем
                                if ($userPhotoStatusFirst->admin_status == 6) {
                                    continue;
                                }
                            }

                            if ($userPhoto->status != 0) {
                                UsersPhotoStatus::create([
                                    'photo_id'   => $userPhoto->photo_id,
                                    'user_id'    => $userPhoto->user_id,
                                    'admin_id'   => $this->admin->id,
                                    'status'     => 0,
                                    'main_photo' => $userPhoto->main_photo,
                                ]);
                            }
                            UsersPhotoStatusInspect::wherePhotoId($userPhoto->photo_id)->delete();
                        }
                    }
                } elseif ($statusFirst->admin_status == 6) {
                    if (in_array($photo->photo_id, $unbannedDevSkip)) {
                        continue;
                    }

                    // удаление бана по устройству
                    $userDevices = UsersAppVersion::whereUserId($photo->user_id)
                        ->get(['device_id'])
                        ->pluck('device_id')
                        ->toArray();

                    $userPhotos = UsersPhoto::with(['statusInspect' => function ($query) {
                        /* @var \Illuminate\Database\Eloquent\Builder $query */
                        $query->select('status_id', 'photo_id', 'admin_id', 'admin_status', 'inspector_id', 'inspector_status', 'associated', 'level')
                            ->orderBy('status_id', 'desc')
                            ->orderBy('date', 'desc');
                    }])
                        ->whereUserId($photo->user_id)
                        ->whereNull('deleted_at')
                        ->get(['photo_id', 'user_id', 'main_photo', 'status']);

                    if (count($userDevices) > 0) {
                        $deviceUsersId = UsersAppVersion::whereIn('device_id', $userDevices)
                            ->where('user_id', '!=', $photo->user_id)
                            ->get(['user_id']);

                        if ($deviceUsersId->count() > 0) {
                            $deviceUsers = UsersInfo::with([
                                'stats'      => function ($query) {
                                    /* @var \Illuminate\Database\Eloquent\Builder $query */
                                    $query->select('user_id', 'banned_acc', 'banned_dev', 'blocked_photo');
                                },
                                'mainPhoto'  => function ($query) {
                                    /* @var \Illuminate\Database\Eloquent\Builder $query */
                                    $query->select('user_id', 'photo_id', 'status', 'main_photo');
                                },
                                'appVersion' => function ($query) {
                                    /* @var \Illuminate\Database\Eloquent\Builder $query */
                                    $query->select('user_id', 'device_id');
                                },
                                'photos'     => function ($query) {
                                    /* @var \Illuminate\Database\Eloquent\Builder $query */
                                    $query->select('photo_id', 'user_id', 'main_photo', 'status');
                                },
                            ])->whereIn('user_id', $deviceUsersId)
                                ->get(['user_id', 'photo_id']);

                            // пользователи связанные с устройствами
                            foreach ($deviceUsers as $deviceUser) {
                                /* @var UsersStat $deviceUserStat */
                                $deviceUserStat = $deviceUser->stats;
                                /* @var UsersPhoto $deviceUserMainPhoto */
                                $deviceUserMainPhoto = $deviceUser->mainPhoto;
                                /* @var \Illuminate\Database\Eloquent\Collection|UsersAppVersion[] $deviceUserDevices */
                                $deviceUserDevices = $deviceUser->appVersion;
                                /* @var \Illuminate\Database\Eloquent\Collection|UsersPhoto[] $deviceUserPhotos */
                                $deviceUserPhotos = $deviceUser->photos;

                                // пользователь не забанен по устройству
                                if ($deviceUserStat->banned_dev == 0) {
                                    continue;
                                }

                                if ($deviceUserMainPhoto && $deviceUserDevices->count() > 0) {
                                    foreach ($deviceUserDevices as $deviceUserDevice) {
                                        BanlistLog::create([
                                            'admin_id'   => $this->admin->id,
                                            'device_id'  => $deviceUserDevice->device_id,
                                            'status'     => 0,
                                            'photo_id'   => $deviceUserMainPhoto->photo_id,
                                            'main_photo' => 1,
                                        ]);

                                        $redisDel[] = env('REDIS_PREFIX_BAN_DEVICE') . $deviceUserDevice->device_id;
                                    }
                                    $redisDel[] = env('REDIS_PREFIX_BAN_USER_DEVICE') . $deviceUser->user_id;
                                }

                                if ($deviceUserMainPhoto) {
                                    $changed[] = [
                                        'photo_id'   => $deviceUserMainPhoto->photo_id,
                                        'status'     => 0,
                                        'associated' => 1,
                                    ];
                                }

                                // удаление статусов для всех фото пользователя
                                foreach ($deviceUserPhotos as $deviceUserPhoto) {
                                    if ($deviceUserPhoto->status != 0) {
                                        UsersPhotoStatus::create([
                                            'photo_id'   => $deviceUserPhoto->photo_id,
                                            'user_id'    => $deviceUserPhoto->user_id,
                                            'admin_id'   => $this->admin->id,
                                            'status'     => 0,
                                            'main_photo' => $deviceUserPhoto->main_photo,
                                        ]);
                                    }
                                }

                                // удаление всех статусов контроля проверки
                                UsersPhotoStatusInspect::whereIn('photo_id', $deviceUserPhotos->pluck('photo_id'))
                                    ->delete();
                                UsersStat::whereUserId($deviceUser->user_id)
                                    ->update([
                                        'banned_dev_associated' => 0,
                                    ]);
                            }
                        }

                        foreach ($userDevices as $userDevice) {
                            BanlistLog::create([
                                'admin_id'   => $this->admin->id,
                                'device_id'  => $userDevice,
                                'status'     => 0,
                                'photo_id'   => $photo->photo_id,
                                'main_photo' => $photo->main_photo,
                            ]);
                            $redisDel[] = env('REDIS_PREFIX_BAN_DEVICE') . $userDevice;
                        }
                        $redisDel[] = env('REDIS_PREFIX_BAN_USER_DEVICE') . $photo->user_id;

                        Banlist::whereIn('device_id', $userDevices)->delete();

                        // удаление статусов контроля проверки со всех фото кроме выбранной
                        foreach ($userPhotos as $userPhoto) {
                            if ($userPhoto->photo_id != $photo->photo_id) {
                                /* @var \Illuminate\Database\Eloquent\Collection|UsersPhotoStatusInspect[] $userPhotoStatusInspect */
                                $userPhotoStatusInspect = $userPhoto->statusInspect;

                                if ($userPhotoStatusInspect->count() > 0) {
                                    /* @var UsersPhotoStatusInspect $userPhotoStatusFirst */
                                    $userPhotoStatusFirst = $userPhotoStatusInspect->values()->get(0);

                                    // если аккаунт забанен по этому фото, то пропускаем
                                    if ($userPhotoStatusFirst->admin_status == 5) {
                                        continue;
                                    }
                                }

                                UsersPhotoStatus::create([
                                    'photo_id'   => $userPhoto->photo_id,
                                    'user_id'    => $userPhoto->user_id,
                                    'admin_id'   => $this->admin->id,
                                    'status'     => 0,
                                    'main_photo' => $userPhoto->main_photo,
                                ]);
                                UsersPhotoStatusInspect::where('photo_id', '=', $userPhoto->photo_id)
                                    ->delete();
                            }
                        }
                        UsersStat::whereUserId($photo->user_id)
                            ->update([
                                'banned_dev_associated' => 0,
                            ]);
                    }
                }

                if (in_array($status, [1, 2, 3, 4])) {
                    UsersPhotoStatus::create([
                        'photo_id'   => $photo->photo_id,
                        'user_id'    => $photo->user_id,
                        'admin_id'   => $this->admin->id,
                        'status'     => $status,
                        'main_photo' => $photo->main_photo,
                    ]);

                    if ($status == 4) {
                        // блокировка по основному фото
                        UsersAlert::create([
                            'user_id'  => $photo->user_id,
                            'type'     => 1,
                            'priority' => 300,
                        ]);

                        $redis->publish('fcm_push', json_encode([
                            'user_id'         => $photo->user_id,
                            'push_type'       => 'system_msg',
                            'system_msg_data' => [
                                'type' => 'refresh_badges',
                                'data' => []
                            ]
                        ]));
                    }
                } elseif ($status == 5) {
                    // бан аккаунта
                    if ($photoUserStat->banned_acc == 0) {
                        Banlist::create([
                            'user_id'     => $photo->user_id,
                            'admin_id'    => $this->admin->id,
                            'reason_code' => 2,
                        ]);
                        BanlistLog::create([
                            'admin_id'   => $this->admin->id,
                            'user_id'    => $photo->user_id,
                            'status'     => 1,
                            'photo_id'   => $photo->photo_id,
                            'main_photo' => $photo->main_photo,
                        ]);
                    }

                    if ($photo->status != 0) {
                        UsersPhotoStatus::create([
                            'photo_id'   => $photo->photo_id,
                            'user_id'    => $photo->user_id,
                            'admin_id'   => $this->admin->id,
                            'status'     => 0,
                            'main_photo' => $photo->main_photo,
                        ]);
                    }

                    $redisDel[] = env('REDIS_PREFIX_BAN_USER') . $photo->user_id;
                } elseif ($status == 6) {
                    // бан устройства
                    $photoUserDevices = UsersAppVersion::whereUserId($photo->user_id)
                        ->get(['device_id'])
                        ->pluck('device_id')
                        ->toArray();

                    if (count($photoUserDevices) == 0) {
                        throw new Exception('Cannot banned user without devices');
                    }

                    // id пользователей заходившие с устройств этого пользователя
                    $deviceUsersId = UsersAppVersion::whereIn('device_id', $photoUserDevices)
                        ->where('user_id', '!=', $photo->user_id)
                        ->get(['user_id'])
                        ->pluck('user_id')
                        ->unique()
                        ->toArray();

                    if (count($deviceUsersId) > 0) {
                        $deviceUsers = UsersInfo::with([
                            'stats'      => function ($query) {
                                /* @var \Illuminate\Database\Eloquent\Builder $query */
                                $query->select('user_id', 'banned_acc', 'banned_dev', 'blocked_photo');
                            },
                            'mainPhoto'  => function ($query) {
                                /* @var \Illuminate\Database\Eloquent\Builder $query */
                                $query->select('user_id', 'photo_id', 'status', 'main_photo');
                            },
                            'appVersion' => function ($query) {
                                /* @var \Illuminate\Database\Eloquent\Builder $query */
                                $query->select('user_id', 'device_id');
                            }
                        ])->whereIn('user_id', $deviceUsersId)
                            ->get(['user_id', 'photo_id']);

                        foreach ($deviceUsers as $deviceUser) {
                            /* @var UsersStat $deviceUserStat */
                            $deviceUserStat = $deviceUser->stats;
                            /* @var UsersPhoto $deviceUserMainPhoto */
                            $deviceUserMainPhoto = $deviceUser->mainPhoto;
                            /* @var \Illuminate\Database\Eloquent\Collection|UsersAppVersion[] $deviceUserDevices */
                            $deviceUserDevices = $deviceUser->appVersion;

                            // пользователь уже забанен по устройству
                            if ($deviceUserStat->banned_dev == 1) {
                                continue;
                            }

                            // если пользователь забанен по аккаунту, то удаляем бан и сбрасываем статусы со всех фото
                            if ($deviceUserStat->banned_acc == 1) {
                                Banlist::whereUserId($deviceUser->user_id)->delete();

                                if ($deviceUserMainPhoto) {
                                    BanlistLog::create([
                                        'admin_id'   => $this->admin->id,
                                        'user_id'    => $deviceUser->user_id,
                                        'status'     => 0,
                                        'photo_id'   => $deviceUserMainPhoto->photo_id,
                                        'main_photo' => 1,
                                    ]);
                                } else {
                                    BanlistLog::create([
                                        'admin_id' => $this->admin->id,
                                        'user_id'  => $deviceUser->user_id,
                                        'status'   => 0,
                                    ]);
                                }

                                $deviceUserPhotos = UsersPhoto::whereUserId($deviceUser->user_id)
                                    ->where('status', '!=', 0)
                                    ->whereNull('deleted_at')
                                    ->get(['photo_id', 'user_id', 'main_photo']);

                                if ($deviceUserPhotos->count() > 0) {
                                    // удаление статусов со всех фото пользователя
                                    foreach ($deviceUserPhotos as $deviceUserPhoto) {
                                        UsersPhotoStatus::create([
                                            'photo_id'   => $deviceUserPhoto->photo_id,
                                            'user_id'    => $deviceUserPhoto->user_id,
                                            'admin_id'   => $this->admin->id,
                                            'status'     => 0,
                                            'main_photo' => $deviceUserPhoto->main_photo,
                                        ]);
                                        UsersPhotoStatusInspect::wherePhotoId($deviceUserPhoto->photo_id)->delete();
                                    }
                                }

                                $redisDel[] = env('REDIS_PREFIX_BAN_USER') . $deviceUser->user_id;
                            }

                            if ($deviceUserMainPhoto && $deviceUserDevices->count() > 0) {
                                foreach ($deviceUserDevices as $deviceUserDevice) {
                                    if (!in_array($deviceUserDevice->device_id, $photoUserDevices)) {
                                        continue;
                                    }

                                    BanlistLog::create([
                                        'admin_id'   => $this->admin->id,
                                        'device_id'  => $deviceUserDevice->device_id,
                                        'status'     => 1,
                                        'photo_id'   => $deviceUserMainPhoto->photo_id,
                                        'main_photo' => 1,
                                    ]);

                                    $redisDel[] = env('REDIS_PREFIX_BAN_DEVICE') . $deviceUserDevice->device_id;
                                }
                                $redisDel[] = env('REDIS_PREFIX_BAN_USER_DEVICE') . $deviceUser->user_id;
                            }

                            if ($deviceUserMainPhoto) {
                                UsersPhotoStatusInspect::wherePhotoId($deviceUserMainPhoto->photo_id)->delete();
                                UsersPhotoStatusInspect::create([
                                    'photo_id'     => $deviceUserMainPhoto->photo_id,
                                    'admin_id'     => $this->admin->id,
                                    'admin_status' => $status,
                                    'associated'   => 1,
                                    'level'        => $this->admin->level,
                                ]);

                                $changed[] = [
                                    'photo_id'   => $deviceUserMainPhoto->photo_id,
                                    'status'     => $status,
                                    'associated' => 1,
                                ];
                            }

                            UsersStat::whereUserId($deviceUser->user_id)
                                ->update([
                                    'banned_dev_associated' => 1,
                                ]);
                        }
                    }

                    foreach ($photoUserDevices as $photoUserDevice) {
                        if ($photoUserStat->banned_dev == 0) {
                            Banlist::create([
                                'device_id'   => $photoUserDevice,
                                'admin_id'    => $this->admin->id,
                                'reason_code' => 1,
                            ]);
                        }
                        BanlistLog::create([
                            'admin_id'   => $this->admin->id,
                            'device_id'  => $photoUserDevice,
                            'status'     => 1,
                            'photo_id'   => $photo->photo_id,
                            'main_photo' => $photo->main_photo,
                        ]);
                    }

                    UsersStat::whereUserId($photo->user_id)
                        ->update([
                            'banned_dev_associated' => 0,
                        ]);
                }

                if ($statusFirst->admin_id == $this->admin->id) {
                    // обновление своего статуса
                    UsersPhotoStatusInspect::whereStatusId($statusFirst->status_id)
                        ->update([
                            'admin_status' => $status,
                            'date'         => DB::raw('now()'),
                            'level'        => $this->admin->level,
                        ]);
                    UsersPhotoStatusInspect::whereStatusId($statusSecond->status_id)
                        ->update([
                            'inspector_status' => $status,
                            'associated'       => 0,
                        ]);
                } else {
                    // добавление нового статуса
                    UsersPhotoStatusInspect::whereStatusId($statusFirst->status_id)
                        ->update([
                            'inspector_id'     => $this->admin->id,
                            'inspector_status' => $status,
                        ]);
                    UsersPhotoStatusInspect::create([
                        'photo_id'     => $photo->photo_id,
                        'admin_id'     => $this->admin->id,
                        'admin_status' => $status,
                        'level'        => $this->admin->level,
                    ]);
                }

                $changed[] = [
                    'photo_id'   => $photo->photo_id,
                    'status'     => $status,
                    'associated' => 0,
                ];
            }

            if ($redisDel) {
                $redis->del(array_unique($redisDel));
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            \Log::error($e);

            return response()->json(['error' => $e->getMessage()], 500);
        }

        return response()->json(['changed' => $changed]);
    }
}
