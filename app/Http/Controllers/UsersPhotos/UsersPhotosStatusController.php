<?php

namespace App\Http\Controllers\UsersPhotos;

use App\Banlist;
use App\BanlistLog;
use App\Http\Controllers\Controller;
use App\Http\Services\PhotosService;
use App\Http\Traits\GeoTimezone;
use App\Http\Traits\HelperDB;
use App\UsersAppVersion;
use App\UsersInfo;
use App\UsersPhoto;
use App\UsersPhotoStatus;
use App\UsersPhotoStatusInspect;
use App\UsersStat;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;

class UsersPhotosStatusController extends Controller
{
    use GeoTimezone, HelperDB;

    /**
     * @var array Статусы фото
     */
    protected $status = [
        'deferred'         => -1,
        'reset'            => 0,
        'verification'     => 1,
        'forbidden_avatar' => 2,
        'forbidden_show'   => 3,
        'blocked'          => 4,
    ];

    /**
     * @var int Лимит фотографий на одной странице
     */
    protected $limit = 200;

    protected $admin = null;

    protected $admin_id = 0;

    protected $admin_level = 0;

    protected $permissions = [];

    /**
     * UsersPhotosStatusController constructor.
     * @throws Exception
     */
    public function __construct()
    {
        if (!$this->setGeoTimezone()) {
            throw new Exception('Cannot get timezone', 500);
        }

        if ($user = Auth::user()) {
            $this->admin = $user;
            $this->admin_id = $user->id;
            $this->admin_level = $user->level;
            $this->permissions = $user->permissions()->pluck('permission')->toArray();
        } else {
            throw new Exception('Cannot get auth user information', 500);
        }

        return true;
    }

    /**
     * Список фото
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPhotos(Request $request)
    {
        $page = intval($request->input('page', 1)) ?: 1;
        $offset = ($page - 1) * $this->limit;

        $photos = DB::table('users_photos as up')
            ->join('users as u', 'up.user_id', '=', 'u.user_id')
            ->join('users_info as ui', 'up.user_id', '=', 'ui.user_id')
            ->join('users_stats as us', 'up.user_id', '=', 'us.user_id')
            ->leftJoin('users_photos_status_inspect as upi', function ($join) {
                /* @var $join \Illuminate\Database\Query\JoinClause */
                $join->on('upi.photo_id', '=', 'up.photo_id');
                $join->where('upi.status_id', '=', function ($query) {
                    /* @var $query \Illuminate\Database\Query\Builder */
                    $query->selectRaw('max(status_id)')
                        ->from('users_photos_status_inspect')
                        ->whereColumn('photo_id', '=', 'up.photo_id');
                });
            })
            ->where('u.state', '=', 1)
            ->where(function ($query) {
                /* @var $query \Illuminate\Database\Query\Builder */
                $query->where('u.email', 'not like', '%mail.local')
                    ->orWhereNull('u.email');
            })
            ->whereNull('up.deleted_at');

        $photos = $this->setSearchFilter($request->input('filter', []), $photos);
        $photos_count = (clone $photos)->count();

        $photos->leftJoin('admins as a', 'upi.admin_id', '=', 'a.id')
            ->leftJoinSub(
                DB::table('users_photos_status')
                    ->groupBy('user_id')
                    ->where('status', '=', 4)
                    ->select('user_id')
                    ->selectRaw('count(user_id) as count'),
                'ubc', 'up.user_id', '=', 'ubc.user_id')
            ->select('up.photo_id', 'up.photo', 'up.main_photo', 'up.deleted_at', 'ui.user_id', 'ui.name',
                'ui.years', 'ui.city', 'ui.avatar', 'ui.vip', 'ui.last_visit', 'ubc.count as blocked_count',
                'us.banned_dev', 'upi.admin_status as status_value', 'upi.date as status_date',
                'upi.associated as status_associated', 'a.name as status_admin')
            ->orderBy('up.photo_id', 'asc')
            ->offset($offset)
            ->limit($this->limit);

        $photos_data = $photos->get();

        if ($photos_data->count() == 0 && $page > 1) {
            $last = intval(ceil($photos_count / $this->limit));

            return $this->getPhotos((new Request())->replace(['filter' => $request->input('filter', []), 'page' => $last]));
        }

        $data = [];
        foreach ($photos_data as $v) {
            $last_visit_sec = time() - $v->last_visit;

            $data[] = [
                'photo_id'      => $v->photo_id,
                'prev'          => env('STATIC_GALLERY_PREW_URL') . $v->photo,
                'href'          => env('STATIC_GALLERY_URL') . $v->photo,
                'main_photo'    => $v->main_photo,
                'reports_count' => 0,
                'blocked_count' => $v->blocked_count ?: 0,
                'deleted_at'    => $v->deleted_at,
                'verification'  => [
                    'status'     => $v->status_value ?: 0,
                    'admin'      => $v->status_admin ? $v->status_admin : null,
                    'date'       => $v->status_date ? $this->tzConvertToLocal($v->status_date) : null,
                    'associated' => $v->status_associated,
                ],
                'user'          => [
                    'user_id' => $v->user_id,
                    'name'    => $v->name,
                    'years'   => $v->years,
                    'city'    => $v->city,
                    'avatar'  => $v->avatar ? env('STATIC_AVA_URL') . $v->avatar : '',
                    'vip'     => $v->vip,
                    'online'  => $last_visit_sec < 600 ? 2 : ($last_visit_sec < 21600 ? 1 : 0),
                ],
            ];
        }

        if (isset($photos_count)) {
            $response = [
                'from'         => intval($offset + 1),
                'to'           => intval($page * $this->limit),
                'total'        => intval($photos_count),
                'current_page' => intval($page),
                'last_page'    => intval(ceil($photos_count / $this->limit)),
                'data'         => $data
            ];
            if ($response['from'] > $response['total']) {
                $response['from'] = $response['total'];
            }
            if ($response['to'] > $response['total']) {
                $response['to'] = $response['total'];
            }
        } else {
            $response = $data;
        }

        return response()->json($response);
    }

    /**
     * Фильтр для списка фотографий
     * @param string $request
     * @param \Illuminate\Database\Query\Builder $query
     * @return \Illuminate\Database\Query\Builder
     */
    protected function setSearchFilter($request, \Illuminate\Database\Query\Builder $query)
    {
        $request = json_decode($request, true);
        if (json_last_error() !== 0) {
            $request = [];
        }

        $filter = [
            'user_id'        => isset($request['user_id']) ? intval($request['user_id']) : 0,
            'name'           => isset($request['name']) ? $request['name'] : '',
            'city'           => isset($request['city']) ? $request['city'] : '',
            'gender'         => isset($request['gender']) ? $request['gender'] : 'na',
            'years_min'      => isset($request['years_min']) ? intval($request['years_min']) : 0,
            'years_max'      => isset($request['years_max']) ? intval($request['years_max']) : 0,
            'vip'            => isset($request['vip']) ? $request['vip'] : 'na',
            'platform'       => isset($request['platform']) ? $request['platform'] : 'na',
            'created_at_min' => isset($request['created_at_min']) ? $request['created_at_min'] : '',
            'created_at_max' => isset($request['created_at_max']) ? $request['created_at_max'] : '',
            'status'         => isset($request['show_status']) ? $request['show_status'] : 'na',
            'main_photo'     => isset($request['show_main_photo']) ? intval($request['show_main_photo']) : 0,
        ];

        if ($filter['user_id']) {
            $query->where('ui.user_id', $filter['user_id']);
        }

        if ($filter['name']) {
            $query->where('ui.name', 'like', "%{$filter['name']}%");
        }

        if ($filter['city']) {
            $query->where('ui.city', 'like', "%{$filter['city']}%");
        }

        if (in_array($filter['gender'], [1, 2])) {
            $query->where('ui.sex', intval($filter['gender']));
        }

        if ($filter['years_min']) {
            $query->where('ui.years', '>=', $filter['years_min']);
        }

        if ($filter['years_max']) {
            $query->where('ui.years', '<=', $filter['years_max']);
        }

        if (in_array($filter['vip'], ['yes', 'no'])) {
            if ($filter['vip'] == 'yes') {
                $query->where('ui.vip', 1);
            } else {
                $query->where('ui.vip', 0);
            }
        }

        if ($filter['platform'] && in_array($filter['platform'], ['android', 'ios'])) {
            $query->where('us.app_version', 'like', "%{$filter['platform']}%");
        }

        if ($filter['created_at_min']) {
            try {
                $filter['created_at_min'] = $this->tzConvertToUTC(Carbon::parse($filter['created_at_min'])->startOfDay());
            } catch (Exception $e) {
                $filter['created_at_min'] = '';
            }
        }

        if ($filter['created_at_max']) {
            try {
                $filter['created_at_max'] = $this->tzConvertToUTC(Carbon::parse($filter['created_at_max'])->endOfDay());
            } catch (Exception $e) {
                $filter['created_at_max'] = '';
            }
        }

        if (in_array($filter['status'], ['forbidden_avatar', 'forbidden_show'])) {
            $filter['main_photo'] = 0;
        } elseif ($filter['status'] == 'blocked') {
            $filter['main_photo'] = 1;
        } elseif (in_array($filter['status'], ['banned_acc', 'banned_dev'])) {
            $filter['main_photo'] = -1;
        }

        if (in_array($filter['main_photo'], [0, 1])) {
            $query->where('up.main_photo', intval($filter['main_photo']));
        }

        $list_status = [
            -1 => 'deferred',
            1  => 'verification',
            2  => 'forbidden_avatar',
            3  => 'forbidden_show',
            4  => 'blocked',
            5  => 'banned_acc',
            6  => 'banned_dev',
        ];

        if ($filter['status'] == 'na') {
            $query->where(function ($where) {
                /* @var $where \Illuminate\Database\Query\Builder */
                $where->where('up.status', 0);
            });

            if ($filter['created_at_min']) {
                $query->where('up.created_at', '>=', $filter['created_at_min']);
            }

            if ($filter['created_at_max']) {
                $query->where('up.created_at', '<=', $filter['created_at_max']);
            }
        } elseif (in_array($filter['status'], $list_status)) {
            $query->where('upi.admin_id', $this->admin_id)
                ->where('upi.admin_status', array_search($filter['status'], $list_status))
                ->where('upi.level', '<=', $this->admin_level)
                ->whereNull('upi.inspector_status');

            if ($filter['created_at_min']) {
                $query->where('upi.date', '>=', $filter['created_at_min']);
            }

            if ($filter['created_at_max']) {
                $query->where('upi.date', '<=', $filter['created_at_max']);
            }
        }

        if (!in_array($filter['status'], ['banned_acc', 'banned_dev'])) {
            $query->where('us.banned_acc', 0);
            $query->where('us.banned_dev', 0);
        } elseif ($filter['status'] == 'banned_acc') {
            $query->where('us.banned_acc', 1);
        } elseif ($filter['status'] == 'banned_dev') {
            $query->where('us.banned_dev', 1);
        }

        return $query;
    }

    /**
     * Количество фотографий для каждого статуса
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCountStatus()
    {
        $status_list = [
            'deferred'         => -1,
            'reset'            => 0,
            'verification'     => 1,
            'forbidden_avatar' => 2,
            'forbidden_show'   => 3,
            'blocked'          => 4,
            'banned_acc'       => 5,
            'banned_dev'       => 6,
        ];

        $response = [];
        foreach (array_keys($status_list) as $v) {
            $response[$v] = 0;
        }

        $status = DB::table('users_photos_status_inspect as upi')
            ->join('users_photos as up', 'upi.photo_id', '=', 'up.photo_id')
            ->join('users as u', 'up.user_id', '=', 'u.user_id')
            ->join('users_info as ui', 'up.user_id', '=', 'ui.user_id')
            ->join('users_stats as us', 'up.user_id', '=', 'us.user_id')
            ->where('u.state', 1)
            ->where(function ($query) {
                /* @var $query \Illuminate\Database\Query\Builder */
                $query->where('u.email', 'not like', '%mail.local')
                    ->orWhereNull('u.email');
            })
            ->whereNull('up.deleted_at')
            ->where('upi.admin_id', $this->admin_id)
            ->where('upi.level', '<=', $this->admin_level)
            ->whereNull('upi.inspector_id')
            ->where(function ($where) {
                /* @var $where \Illuminate\Database\Query\Builder */
                $where->where(function ($q) {
                    /* @var $q \Illuminate\Database\Query\Builder */
                    $q->where('up.main_photo', 0)
                        ->whereIn('upi.admin_status', [2, 3]);
                });
                $where->orWhere(function ($q) {
                    /* @var $q \Illuminate\Database\Query\Builder */
                    $q->where('up.main_photo', 1)
                        ->where('upi.admin_status', 4);
                });
                $where->orWhereIn('upi.admin_status', [-1, 1, 5, 6]);
            })
            ->where(function ($where) {
                /* @var $where \Illuminate\Database\Query\Builder */
                $where->where(function ($q) {
                    /* @var $q \Illuminate\Database\Query\Builder */
                    $q->where('us.banned_acc', 0)
                        ->where('us.banned_dev', 0)
                        ->where('upi.admin_status', '<=', 4);
                });
                $where->orWhere(function ($q) {
                    /* @var $q \Illuminate\Database\Query\Builder */
                    $q->where('us.banned_acc', 1)
                        ->where('upi.admin_status', 5);
                });
                $where->orWhere(function ($q) {
                    /* @var $q \Illuminate\Database\Query\Builder */
                    $q->where('us.banned_dev', 1)
                        ->where('upi.admin_status', 6);
                });
            })
            ->groupBy('admin_status')
            ->get(['admin_status as status', DB::raw('count(*) as count')]);

        foreach ($status as $v) {
            if (in_array($v->status, $status_list)) {
                $response[array_search($v->status, $status_list)] = $v->count;
            }
        }

        $response['reset'] = DB::table('users_photos as up')
            ->join('users as u', 'up.user_id', '=', 'u.user_id')
            ->join('users_info as ui', 'u.user_id', '=', 'ui.user_id')
            ->join('users_stats as us', 'u.user_id', '=', 'us.user_id')
            ->where('u.state', 1)
            ->where(function ($query) {
                /* @var $query \Illuminate\Database\Query\Builder */
                $query->where('u.email', 'not like', '%mail.local')
                    ->orWhereNull('u.email');
            })
            ->where('us.banned_acc', 0)
            ->where('us.banned_dev', 0)
            ->where('up.status', 0)
            ->whereNull('up.deleted_at')
            ->count();

        return response()->json($response);
    }

    /**
     * Установка статуса фото
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function setStatus(Request $request)
    {
        $photos = $request->input('photos', []);
        $status = $request->input('status', false);

        try {
            if (empty($photos)) {
                throw new Exception('Empty photos', 400);
            }

            $response = (new PhotosService())->setStatus($photos, $status);
        } catch (Exception $e) {
            $code = $e->getCode() ?: 500;

            return response()->json(['error' => $e->getMessage()], $code);
        }

        return response()->json($response);
    }

    /**
     * @param Request $request
     * @param bool $checkPermissions
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function setStatusPhoto(Request $request, $checkPermissions = true)
    {
        $return = [];
        $photos = $request->input('photos');
        $status = $request->input('status');

        try {
            if (!is_array($photos) || !in_array($status, [-1, 1, 2, 3, 4, 5, 6])) {
                throw new Exception('Wrong request parameters', 400);
            }

            $photosInfo = (new PhotosService())->getPhotosInfo($photos);

            if ($photosInfo->count() == 0) {
                throw new Exception('Photos not found', 400);
            }

            $changeBanned = true;
            if ($checkPermissions) {
                $changeBanned = $this->admin->permissions->where('permission', '=', 'verification_photos__change_banned');
            }

            DB::beginTransaction();

            foreach ($photosInfo as $photo) {
                // основное фото нельзя запретить для аватара и запретить для показа
                // внутренне фото нельзя временно заблокировать
                if ($photo->main_photo == 1 && in_array($status, [2, 3]) || ($photo->main_photo == 0 && $status == 4)) {
                    continue;
                }

                // изменение последнего статуса
                if ($photo->statusInspect->count() > 0) {
                    /* @var UsersStat $photoUserStat */
                    $photoUserStat = $photo->stat;
                    /* @var UsersPhotoStatusInspect $photoStatus */
                    $photoStatus = $photo->statusInspect->first();

                    // указанный статус уже установлен
                    if ($photoStatus->admin_status == $status) {
                        throw new Exception('Photo already status:' . $status, 400);
                    }

                    // нельзя изменить статус установленных другим модератором
                    if ($photoStatus->admin_id != $this->admin_id && $photoStatus->level > $this->admin_level) {
                        throw new Exception('Not enough permissions to change photo status', 400);
                    }

                    // у модератора нет доступа на изменение статуса бана
                    if (in_array($photoStatus->admin_status, [5, 6]) && !$changeBanned) {
                        throw new Exception('Not enough permissions to change banned photo status', 400);
                    }

                    // аккаунт пользователя уже забанен
                    if ($status == 5 && $photoUserStat->banned_acc == 1) {
                        throw new Exception('User account already banned', 400);
                    }

                    // устройства пользователя уже забанены
                    if ($status == 6 && $photoUserStat->banned_dev == 1) {
                        throw new Exception('User device already banned', 400);
                    }

                    // обновление последнего статуса для контроля проверки
                    UsersPhotoStatusInspect::where('status_id', $photoStatus->status_id)
                        ->update([
                            'admin_status' => $status,
                        ]);

                    // удаление временной блокировки
                    if ($photoStatus->admin_status == 4) {
                        (new PhotosService())->removeAlertBlocked($photo->user_id);
                    }

                    // удаление бана аккаунта пользователя
                    if ($photoStatus->admin_status == 5) {
                        // сброс статусов со всех фотографий пользователя кроме указанной
                        (new PhotosService())->resetUserPhotosStatus($photo->user_id, 5, $photo->photo_id);

                        Banlist::whereUserId($photo->user_id)->delete();
                        BanlistLog::create([
                            'admin_id'   => $this->admin->id,
                            'user_id'    => $photo->user_id,
                            'status'     => 0,
                            'photo_id'   => $photo->photo_id,
                            'main_photo' => $photo->main_photo,
                        ]);

                        (Redis::connection())->del([env('REDIS_PREFIX_BAN_USER') . $photo->user_id]);
                    }

                    // удаление бана устройств пользователя
                    if ($photoStatus->admin_status == 6) {
                        // устройства пользователя
                        $photoUserDevices = UsersAppVersion::whereUserId($photo->user_id)
                            ->get(['device_id']);

                        if ($photoUserDevices->count() > 0) {
                            $deleteRedis = [];

                            // все пользователи связанные устройствами
                            $deviceUsersId = UsersAppVersion::whereIn('device_id', $photoUserDevices->pluck('device_id'))
                                ->get(['user_id'])
                                ->pluck('user_id')
                                ->unique();

                            // информация о пользователях связанных с устройствами
                            $devicesUsers = UsersInfo::with([
                                'stats'      => function ($query) {
                                    /* @var \Illuminate\Database\Eloquent\Builder $query */
                                    $query->select('user_id', 'banned_acc', 'banned_dev', 'blocked_photo');
                                },
                                'photos'     => function ($query) {
                                    /* @var \Illuminate\Database\Eloquent\Builder $query */
                                    $query->select('user_id', 'photo_id', 'main_photo', 'status');
                                },
                                'mainPhoto'  => function ($query) {
                                    /* @var \Illuminate\Database\Eloquent\Builder $query */
                                    $query->select('user_id', 'photo_id', 'main_photo', 'status');
                                },
                                'appVersion' => function ($query) {
                                    /* @var \Illuminate\Database\Eloquent\Builder $query */
                                    $query->select('user_id', 'device_id');
                                }])
                                ->whereIn('user_id', $deviceUsersId)
                                ->get(['user_id', 'photo_id']);

                            foreach ($devicesUsers as $user) {
                                /* @var UsersStat $userStats */
                                $userStats = $user->stats;
                                /* @var \Illuminate\Database\Eloquent\Collection|UsersPhoto[] $userPhotos */
                                $userPhotos = $user->photos;
                                /* @var UsersPhoto $userMainPhoto */
                                $userMainPhoto = $user->mainPhoto;
                                /* @var \Illuminate\Database\Eloquent\Collection|UsersAppVersion[] $userDevices */
                                $userDevices = $user->appVersion;

                                if ($userStats->banned_dev == 0 || $userDevices->count() == 0) {
                                    continue;
                                }

                                if ($userPhotos->count() == 0 || !$userMainPhoto) {
                                    continue;
                                }

                                $resetPhotosStatus = [];
                                $resetPhotosStatusInspect = [];

                                // сброс статусов с фотографий пользователя
                                foreach ($userPhotos as $userPhoto) {
                                    // для изначально указанного фото с которого снимаем бан
                                    // статусы сбрасывать не нужно
                                    if ($userPhoto->photo_id == $photo->photo_id) {
                                        continue;
                                    }

                                    $resetPhotosStatus[] = [
                                        'photo_id'   => $userPhoto->photo_id,
                                        'user_id'    => $userPhoto->user_id,
                                        'admin_id'   => $this->admin->id,
                                        'status'     => 0,
                                        'main_photo' => $userPhoto->main_photo,
                                    ];

                                    $resetPhotosStatusInspect[] = $userPhoto->photo_id;
                                }

                                // сброс статусов для всех фото пользователя
                                if ($resetPhotosStatus && $resetPhotosStatusInspect) {
                                    UsersPhotoStatus::insert($resetPhotosStatus);
                                    UsersPhotoStatusInspect::whereIn('photo_id', $resetPhotosStatusInspect)->delete();
                                }

                                // добавление лога снятия бана со связанных устройств пользователя
                                if ($photo->user_id != $user->user_id) {
                                    foreach ($userDevices as $device) {
                                        if (in_array($device->device_id, $photoUserDevices->pluck('device_id')->toArray())) {
                                            BanlistLog::create([
                                                'admin_id'   => $this->admin->id,
                                                'device_id'  => $device->device_id,
                                                'status'     => 0,
                                                'photo_id'   => $userMainPhoto->photo_id,
                                                'main_photo' => 1,
                                            ]);
                                            // сброс кеша бана устройств
                                            $deleteRedis[] = env('REDIS_PREFIX_BAN_USER_DEVICE') . $device->user_id;
                                            $deleteRedis[] = env('REDIS_PREFIX_BAN_DEVICE') . $device->device_id;
                                        }
                                    }
                                }

                                if ($photo->user_id != $user->user_id) {
                                    $return[] = [
                                        'user_id'  => $user->user_id,
                                        'photo_id' => $userMainPhoto->photo_id,
                                        'status'   => 0,
                                    ];
                                }
                            }

                            if ($deleteRedis) {
                                (Redis::connection())->del(array_unique($deleteRedis));
                            }

                            // добавление лога снятия бана с устройств пользователя с указанным фото
                            foreach ($photoUserDevices as $photoUserDevice) {
                                BanlistLog::create([
                                    'admin_id'   => $this->admin->id,
                                    'device_id'  => $photoUserDevice->device_id,
                                    'status'     => 0,
                                    'photo_id'   => $photo->photo_id,
                                    'main_photo' => $photo->main_photo,
                                ]);
                            }
                            Banlist::whereIn('device_id', $photoUserDevices->pluck('device_id'))->delete();
                        }
                    }
                } else {
                    // добавление статуса для контроля проверки
                    UsersPhotoStatusInspect::create([
                        'photo_id'     => $photo->photo_id,
                        'admin_id'     => $this->admin->id,
                        'admin_status' => $status,
                        'level'        => $this->admin->level,
                    ]);
                }

                if (in_array($status, [-1, 1, 2, 3, 4])) {
                    // добавление статуса фото
                    UsersPhotoStatus::create([
                        'photo_id'   => $photo->photo_id,
                        'user_id'    => $photo->user_id,
                        'admin_id'   => $this->admin->id,
                        'status'     => $status,
                        'main_photo' => $photo->main_photo,
                    ]);

                    // блокировка основного фото
                    if ($status == 4) {
                        (new PhotosService())->createAlertBlocked($photo->user_id);
                    }
                } elseif ($status == 5) {
                    // добавление бана аккаунта
                    Banlist::create([
                        'user_id'     => $photo->user_id,
                        'admin_id'    => $this->admin->id,
                        'reason_code' => 2,
                    ]);
                    BanlistLog::create([
                        'admin_id'   => $this->admin->id,
                        'user_id'    => $photo->user_id,
                        'status'     => 1,
                        'photo_id'   => $photo->photo_id,
                        'main_photo' => $photo->main_photo,
                    ]);

                    // сброс статуса фото
                    UsersPhotoStatus::create([
                        'photo_id'   => $photo->photo_id,
                        'user_id'    => $photo->user_id,
                        'admin_id'   => $this->admin->id,
                        'status'     => 0,
                        'main_photo' => $photo->main_photo,
                    ]);

                    (Redis::connection())->del([env('REDIS_PREFIX_BAN_USER') . $photo->user_id]);
                } elseif ($status == 6) {
                    // устройства пользователя
                    $photoUserDevices = UsersAppVersion::whereUserId($photo->user_id)
                        ->get(['device_id']);

                    if ($photoUserDevices->count() > 0) {
                        $deleteRedis = [];

                        // все пользователи связанные устройствами
                        $deviceUsersId = UsersAppVersion::whereIn('device_id', $photoUserDevices->pluck('device_id'))
                            ->get(['user_id'])
                            ->pluck('user_id')
                            ->unique();

                        // информация о пользователях связанных с устройствами
                        $devicesUsers = UsersInfo::with([
                            'stats'      => function ($query) {
                                /* @var \Illuminate\Database\Eloquent\Builder $query */
                                $query->select('user_id', 'banned_acc', 'banned_dev', 'blocked_photo');
                            },
                            'photos'     => function ($query) {
                                /* @var \Illuminate\Database\Eloquent\Builder $query */
                                $query->select('user_id', 'photo_id', 'main_photo', 'status');
                            },
                            'mainPhoto'  => function ($query) {
                                /* @var \Illuminate\Database\Eloquent\Builder $query */
                                $query->select('user_id', 'photo_id', 'main_photo', 'status');
                            },
                            'appVersion' => function ($query) {
                                /* @var \Illuminate\Database\Eloquent\Builder $query */
                                $query->select('user_id', 'device_id');
                            }])
                            ->whereIn('user_id', $deviceUsersId)
                            ->get(['user_id', 'photo_id']);

                        foreach ($devicesUsers as $user) {
                            /* @var UsersStat $userStats */
                            $userStats = $user->stats;
                            /* @var \Illuminate\Database\Eloquent\Collection|UsersPhoto[] $userPhotos */
                            $userPhotos = $user->photos;
                            /* @var UsersPhoto $userMainPhoto */
                            $userMainPhoto = $user->mainPhoto;
                            /* @var \Illuminate\Database\Eloquent\Collection|UsersAppVersion[] $userDevices */
                            $userDevices = $user->appVersion;

                            if ($userStats->banned_dev == 1 || $userDevices->count() == 0) {
                                continue;
                            }

                            if ($userPhotos->count() == 0 || !$userMainPhoto) {
                                continue;
                            }

                            if ($photo->user_id == $user->user_id) {
                                continue;
                            }

                            // сброс статуса с основного фото пользователя
                            UsersPhotoStatus::insert([
                                'photo_id'   => $userMainPhoto->photo_id,
                                'user_id'    => $user->user_id,
                                'admin_id'   => $this->admin->id,
                                'status'     => 0,
                                'main_photo' => 1,
                            ]);
                            UsersPhotoStatusInspect::wherePhotoId($userMainPhoto->photo_id)->delete();
                            UsersPhotoStatusInspect::create([
                                'photo_id'     => $userMainPhoto->photo_id,
                                'admin_id'     => $this->admin->id,
                                'admin_status' => $status,
                                'associated'   => 1,
                                'level'        => $this->admin->level,
                            ]);

                            // обновление записи, что это связанное устройство и было забанено не напрямую
                            UsersStat::whereUserId($user->user_id)->update([
                                'banned_dev_associated' => 1,
                            ]);

                            // лог добавления бана для связанных устройств пользователя
                            foreach ($userDevices as $device) {
                                if (in_array($device->device_id, $photoUserDevices->pluck('device_id')->toArray())) {
                                    BanlistLog::create([
                                        'admin_id'   => $this->admin->id,
                                        'device_id'  => $device->device_id,
                                        'status'     => 1,
                                        'photo_id'   => $userMainPhoto->photo_id,
                                        'main_photo' => 1,
                                    ]);
                                    // сброс кеша бана устройств
                                    $deleteRedis[] = env('REDIS_PREFIX_BAN_USER_DEVICE') . $device->user_id;
                                    $deleteRedis[] = env('REDIS_PREFIX_BAN_DEVICE') . $device->device_id;
                                }
                            }

                            $return[] = [
                                'user_id'  => $user->user_id,
                                'photo_id' => $userMainPhoto->photo_id,
                                'status'   => 0,
                            ];
                        }

                        if ($deleteRedis) {
                            (Redis::connection())->del(array_unique($deleteRedis));
                        }

                        // лог добавления бана с устройств пользователя с указанным фото
                        foreach ($photoUserDevices as $photoUserDevice) {
                            BanlistLog::create([
                                'admin_id'   => $this->admin->id,
                                'device_id'  => $photoUserDevice->device_id,
                                'status'     => 1,
                                'photo_id'   => $photo->photo_id,
                                'main_photo' => $photo->main_photo,
                            ]);
                            Banlist::create([
                                'device_id'   => $photoUserDevice->device_id,
                                'admin_id'    => $this->admin->id,
                                'reason_code' => 1,
                            ]);
                        }

                        // сброс статуса забаненного фото
                        UsersPhotoStatus::insert([
                            'photo_id'   => $photo->photo_id,
                            'user_id'    => $photo->user_id,
                            'admin_id'   => $this->admin->id,
                            'status'     => 0,
                            'main_photo' => $photo->main_photo,
                        ]);
                        // обновление статуса фото для контроля проверки
                        UsersPhotoStatusInspect::wherePhotoId($photo->photo_id)
                            ->whereAdminId($this->admin->id)
                            ->update([
                                'admin_status' => $status,
                                'level'        => $this->admin->level,
                                'date'         => DB::raw('now()'),
                            ]);

                        // обновление записи, что это не связанное устройство, а было забанено напрямую
                        UsersStat::whereUserId($photo->user_id)->update([
                            'banned_dev_associated' => 0,
                        ]);
                    }
                }

                $return[] = [
                    'user_id'  => $photo->user_id,
                    'photo_id' => $photo->photo_id,
                    'status'   => $status,
                ];
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            \Log::error($e);

            return response()->json(['error' => $e->getMessage()], in_array($e->getCode(), [400, 500]) ? $e->getCode() : 500);
        }

        return response()->json($return);
    }

    /**
     * Установка статуса фото
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function setPhotoStatus(Request $request)
    {
        // @TODO изменить установку статуса при верификации фото VerificationController@verificationPhoto
        $photos = $request->input('photos');
        $status = $request->input('status');

        if (!is_array($photos) || !in_array($status, $this->status)) {
            return response()->json(['error' => 'Wrong request parameters'], 400);
        }

        $type = '';
        if ($status == $this->status['forbidden_avatar'] || $status == $this->status['forbidden_show']) {
            $type = 'inner';
        } elseif ($status == $this->status['blocked']) {
            $type = 'main';
        }

        if (!$photos = $this->getPhotosId($photos, $type, $status)) {
            return response()->json(['error' => 'Cannot get photos id'], 400);
        }

        if (!$values = $this->prepareStatusValues($photos, $status)) {
            return response()->json(['error' => 'Cannot prepare status values'], 400);
        }

        $photos = [];
        $users = [];
        foreach ($values as $value) {
            $photos[] = $value['photo_id'];
            $users[] = $value['user_id'];
        }

        if (!$photos || !$users) {
            return response()->json(['error' => 'Cannot prepare photos or users id'], 400);
        }

        $r = Redis::connection();

        DB::beginTransaction();

        // если это не блокировка фото, то удаляем уведомления о блокировке
        if ($status != $this->status['blocked']) {
            $alerts = DB::table('users_info')
                ->whereIn('photo_id', $photos)
                ->pluck('user_id')
                ->toArray();

            if ($alerts) {
                try {
                    DB::table('users_alerts')
                        ->whereIn('user_id', $alerts)
                        ->where('type', '=', 1)
                        ->whereNull('read_at')
                        ->delete();
                } catch (Exception $e) {
                    DB::rollback();

                    return response()->json(['error' => $e->getMessage()], 500);
                }

                foreach ($alerts as $v) {
                    $r->publish('fcm_push', json_encode([
                        'user_id'         => $v,
                        'push_type'       => 'system_msg',
                        'system_msg_data' => [
                            'type' => 'refresh_badges',
                            'data' => []
                        ]
                    ]));
                }
            }
        }

        try {
            UsersPhotoStatus::insert($values);
        } catch (Exception $e) {
            DB::rollback();

            return response()->json(['error' => $e->getMessage()], 500);
        }

        // push-уведомления о блокировке по основному фото
        if ($status == $this->status['blocked']) {
            $alerts = [];
            foreach ($users as $user) {
                $alerts[] = [
                    'user_id'  => $user,
                    'type'     => 1,
                    'priority' => 300,
                ];
            }

            try {
                DB::table('users_alerts')
                    ->whereIn('user_id', $users)
                    ->where('type', '=', 1)
                    ->whereNull('read_at')
                    ->delete();
                DB::table('users_alerts')->insert($alerts);
            } catch (Exception $e) {
                DB::rollback();

                return response()->json(['error' => $e->getMessage()], 500);
            }

            foreach ($users as $user) {
                $r->publish('fcm_push', json_encode([
                    'user_id'         => $user,
                    'push_type'       => 'system_msg',
                    'system_msg_data' => [
                        'type' => 'refresh_badges',
                        'data' => []
                    ]
                ]));
            }
        }

        $inspect = DB::table('users_photos_status_inspect')
            ->where('admin_id', $this->admin_id)
            ->whereIn('photo_id', $photos)
            ->whereNull('inspector_id')
            ->get();

        foreach ($photos as $photo) {
            if ($photo_status = $inspect->where('photo_id', $photo)->first()) {
                if (in_array($photo_status->admin_status, [5, 6])) {
                    continue;
                }
                DB::table('users_photos_status_inspect')
                    ->where('status_id', $photo_status->status_id)
                    ->update([
                        'admin_status' => $status,
                        'date'         => DB::raw('now()'),
                        'level'        => $this->admin_level,
                    ]);
            } else {
                DB::table('users_photos_status_inspect')
                    ->insert([
                        'photo_id'     => $photo,
                        'admin_id'     => $this->admin_id,
                        'admin_status' => $status,
                        'level'        => $this->admin_level,
                    ]);
            }
        }

        DB::commit();

        return response()->json([]);
    }

    /**
     * Временная блокировка/разблокировка пользователя по id пользователя
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function blockedPhoto(Request $request)
    {
        $photos = $request->input('photos');
        $status = $request->input('status');

        if (!in_array($status, [0, 1])) {
            return response()->json(['Wrong status'], 401);
        }
        $status = $status == 0
            ? $this->status['reset']
            : $this->status['blocked'];

        $request = (new Request())->replace(['photos' => $photos, 'status' => $status]);

        return $this->setPhotoStatus($request);
    }

    /**
     * Временная блокировка/разблокировка пользователя по id пользователя
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function blockedPhotoByUser(Request $request)
    {
        $users = $request->input('users');
        $status = $request->input('status');

        if (!in_array($status, [0, 1])) {
            return response()->json(['Wrong status'], 401);
        }
        $status = $status == 0
            ? $this->status['reset']
            : $this->status['blocked'];

        $photos = DB::table('users_info')
            ->whereIn('user_id', $users)
            ->pluck('photo_id')
            ->toArray();

        $request = (new Request())->replace(['photos' => $photos, 'status' => $status]);

        return $this->setPhotoStatus($request);
    }

    /**
     * Получение реальных id фотографий
     * @param array $photos Фотографии
     * @param string $type Тип фото (main - основное, inner - внутреннее)
     * @param bool $status
     * @return array|bool
     */
    protected function getPhotosId(array $photos, $type = '', $status = false)
    {
        if (!$photos = $this->prepareWhereInInt($photos)) {
            return false;
        }

        $data = UsersPhoto::whereIn('photo_id', $photos);

        if ($type == 'main' || $type == 'inner') {
            $data->where('main_photo', $type == 'main' ? 1 : 0);
        }

        if ($status !== false && in_array($status, $this->status)) {
            $data->where('status', '!=', $status);
        }

        return $data->get(['photo_id', 'user_id', 'main_photo'])->toArray();
    }

    /**
     * Данные для добавления статуса фото
     * @param array $photos Фотографии
     * @param int $status Статус
     * @return array
     */
    protected function prepareStatusValues($photos, $status)
    {
        $values = [];

        foreach ($photos as $photo) {
            $values[] = [
                'photo_id'   => $photo['photo_id'],
                'user_id'    => $photo['user_id'],
                'admin_id'   => $this->admin_id,
                'status'     => $status,
                'main_photo' => $photo['main_photo'],
            ];
        }

        return $values;
    }

    /**
     * Количество непроверенных фото
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCountNewPhotos()
    {
        $count = DB::table('users_photos as up')
            ->join('users as u', 'up.user_id', '=', 'u.user_id')
            ->join('users_info as ui', 'u.user_id', '=', 'ui.user_id')
            ->join('users_stats as us', 'u.user_id', '=', 'us.user_id')
            ->where('up.status', 0)
            ->where('u.state', 1)
            ->where('us.banned_acc', 0)
            ->where('us.banned_dev', 0)
            ->selectRaw('sum(if(up.main_photo = 1, 1, 0)) as `main`, sum(if(up.main_photo = 0, 1, 0)) as `inner`, count(*) as `total`')
            ->get()
            ->first();

        $response = [
            'main'  => (int)$count->main,
            'inner' => (int)$count->inner,
            'total' => (int)$count->total,
        ];

        return response()->json($response);
    }
}
