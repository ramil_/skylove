<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Exception;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Client;
use App\Http\Traits\GeoTimezone;
use Illuminate\Support\Str;

class UsersDialogsController extends Controller
{
    use GeoTimezone;

    public function __construct()
    {
        if (!$this->setGeoTimezone()) {
            return response()->json(['error' => 'Cannot get timezone'], 500);
        }

        return true;
    }

    /**
     * Информация о пользователе
     * @param int $user_id Id пользователя
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     * @throws Exception
     */
    public function getUserData($user_id)
    {
        $ulr = env('SKYLOVE_API_URL') . 'profile/index';

        $user = [];
        $response = $this->sendRequestDialog($user_id, $ulr);

        if ($response->getStatusCode() != 200) {
            return response()->json($response->getData(), $response->getStatusCode());
        }

        if ($data = @json_decode($response->getBody()->getContents(), true)) {
            if (is_array($data) && !empty($data['user']) && json_last_error() == JSON_ERROR_NONE) {
                $user = [
                    'user_id' => $data['user']['user_id'],
                    'name'    => $data['user']['name'],
                    'sex'     => $data['user']['sex'],
                    'city'    => $data['user']['city'],
                    'age'     => $data['user']['age'],
                    'photo'   => $data['user']['photo'],
                    'avatar'  => $data['user']['avatar'],
                    'search'  => $data['user']['search'],
                ];
            }
        }

        if (!$user) {
            return response()->json($response->getData(), $response->getStatusCode());
        }

        return response()->json($user);
    }

    /**
     * Диалоги пользователя
     * @param int $user_id Id пользователя
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     * @throws Exception
     */
    public function getDialogs($user_id)
    {
        $ulr = env('SKYLOVE_API_URL') . 'chat/chats';

        $response = $this->sendRequestDialog($user_id, $ulr);

        if ($response->getStatusCode() != 200) {
            return response()->json($response->getData(), $response->getStatusCode());
        }

        $data = json_decode($response->getBody()->getContents(), true);

        foreach ($data['chats'] as &$row) {
            $row['user']['last_visit'] = $this->tzConvertToLocalUnix($row['user']['last_visit']);
            $row['last_message']['date'] = $this->tzConvertToLocalUnix($row['last_message']['date']);
        }

        return response()->json($data);
    }

    /**
     * Сообщения диалога
     * @param int $user_id Id пользователя
     * @param int $chat_id Id диалога
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     * @throws Exception
     */
    public function getDialog($user_id, $chat_id)
    {
        $ulr = env('SKYLOVE_API_URL') . 'chat/previous_messages?chat_id=' . $chat_id . '&count=1000';

        $response = $this->sendRequestDialog($user_id, $ulr);

        if ($response->getStatusCode() != 200) {
            return response()->json($response->getData(), $response->getStatusCode());
        }

        $data = json_decode($response->getBody()->getContents(), true);

        foreach ($data['messages'] as &$row) {
            $row['date'] = $this->tzConvertToLocalUnix($row['date']);
        }

        return response()->json($data);
    }

    /**
     * Отметить все сообщения пользователя в диалоге как прочитанные
     * @param int $user_id Id пользователя
     * @param int $chat_id Id диалога
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     * @throws Exception
     */
    public function readDialog($user_id, $chat_id)
    {
        $ulr = env('SKYLOVE_API_URL') . 'chat/read?chat_id=' . $chat_id;

        $response = $this->sendRequestDialog($user_id, $ulr);

        if ($response->getStatusCode() != 200) {
            return response()->json($response->getData(), $response->getStatusCode());
        }

        return response()->json([]);
    }

    /**
     * Отметка о получении/прочтении сообщения
     * @param Request $request
     * @param $user_id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     * @throws Exception
     */
    public function statusMessage(Request $request, $user_id)
    {
        $userInput = $request->all();
        $ulr = env('SKYLOVE_API_URL') . 'chat/msg_status';

        $response = $this->sendRequestDialog($user_id, $ulr, 'POST', ['form_params' => $userInput]);

        if ($response->getStatusCode() != 200) {
            return response()->json($response->getData(), $response->getStatusCode());
        }

        return response()->json([]);
    }

    /**
     * Отправить сообщение
     * @param Request $request
     * @param $user_id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     * @throws Exception
     */
    public function sendMessage(Request $request, $user_id)
    {
        $userInput = $request->all();
        $ulr = env('SKYLOVE_API_URL') . 'chat/message';

        $response = $this->sendRequestDialog($user_id, $ulr, 'POST', ['form_params' => $userInput]);

        if ($response->getStatusCode() != 200) {
            return response()->json($response->getData(), $response->getStatusCode());
        }

        return response()->json($response->getBody()->getContents());
    }

    /**
     * Создать новый диалог
     * @param Request $request
     * @param $user_id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     * @throws Exception
     */
    public function createDialog(Request $request, $user_id)
    {
        return $this->sendMessage($request, $user_id);
    }

    /**
     * Токен пользователя
     * @param int $user_id Id пользователя
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function getToken($user_id)
    {
        if ($token = $this->generateAuthToken($user_id)) {
            return response()->json($token);
        }

        return response()->json([], 400);
    }

    /**
     * Запрос к API
     * @param int $user_id Id пользователя
     * @param string $ulr Адрес запроса
     * @param string $method GET, POST
     * @param array $form Данные формы запроса
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws Exception
     */
    private function sendRequestDialog($user_id, $ulr, $method = 'GET', $form = [])
    {
        $client = new Client();

        $options = [
            'headers' => [
                'authentication' => $this->generateAuthToken($user_id)
            ]
        ];

        if (!in_array($method, ['GET', 'POST'])) {
            $method = 'GET';
        }

        if ($method == 'POST' && $form) {
            $options = array_merge($options, $form);
        }

        try {
            return $client->request($method, $ulr, $options);
        } catch (ClientException $e) {
            $resp = $e->getResponse();
            $json = json_decode($resp->getBody()->getContents(), true);
            $code = $resp->getStatusCode() != 403 ? $resp->getStatusCode() : 400;

            if (!empty($json['error'])) {
                return response()->json($json, $code);
            }

            return response()->json(['error' => 'Client request error'], $code);
        } catch (ServerException $e) {
            $resp = $e->getResponse();
            $json = json_decode($resp->getBody()->getContents(), true);

            if (!empty($json['error'])) {
                return response()->json($json, $resp->getStatusCode());
            }

            return response()->json(['error' => 'Server request error'], $resp->getStatusCode());
        } catch (GuzzleException $e) {
            return response()->json(['error' => 'Unknown request error (' . $e->getMessage() . ')'], 500);
        }
    }

    /**
     * Генератор токена для доступа к api, чтобы не дублировать уже написанный функционал
     * @param int $user_id Id пользователя
     * @return string
     * @throws Exception
     */
    private function generateAuthToken($user_id)
    {
        $user_id = intval($user_id);

        if ($user_id <= 0) {
            return response()->json(['error' => 'Недопустимый id пользователя'], 400);
        }

        $session = DB::table('users_sessions')
            ->where('user_id', '=', $user_id)
            ->where('application_id', '=', 'web_app')
            ->get(['id', 'secret'])
            ->first();

        if (!$session) {
            $secret = Str::random(24);
            $sid = DB::table('users_sessions')->insertGetId([
                'user_id'        => $user_id,
                'application_id' => 'web_app',
                'secret'         => $secret,
                'metadata'       => json_encode([
                    'device_name'     => null,
                    'device_platform' => null
                ])
            ]);
        } else {
            $secret = $session->secret;
            $sid = $session->id;
        }

        if (!$secret || !$sid) {
            return response()->json(['error' => 'Не удалось получить сессию'], 500);
        }

        $header = json_encode(['typ' => 'JWT', 'alg' => 'HS256']);
        $payload = json_encode(['uid' => $user_id, 'sid' => $sid]);

        // Encode
        $base64UrlHeader = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($header));
        $base64UrlPayload = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($payload));

        // Create Signature Hash & encode
        $signature = hash_hmac('sha256', $base64UrlHeader . "." . $base64UrlPayload, $secret, true);
        $base64UrlSignature = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($signature));

        // Create JWT
        $jwt = $base64UrlHeader . "." . $base64UrlPayload . "." . $base64UrlSignature;

        return $jwt;
    }
}
