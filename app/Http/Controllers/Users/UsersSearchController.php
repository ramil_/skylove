<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use Datetime;
use DateTimeZone;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UsersSearchController extends Controller
{
    protected $limit = 200;

    protected $timezone = 'Asia/Krasnoyarsk';

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function getData(Request $request)
    {
        $page = $request->input('page', 1) ?: 1;
        if ($page != intval($page) || $page < 1) {
            $page = 1;
        };

        $limit = $request->input('per_page', 0) ?: $this->limit;
        if ($limit != intval($limit) || $limit < 1) {
            $limit = $this->limit;
        };

        $offset = ($page - 1) * $limit;
        $sort = $request->input('sort', 'user_id|desc');
        $filter = $request->input('filter', []);
        $filter = @json_decode($filter, true);

        $users = DB::table(DB::raw('users as u force index (primary)'))
            ->join('users_info as i', 'i.user_id', '=', 'u.user_id')
            ->join('users_stats as s', 's.user_id', '=', 'u.user_id')
            ->leftJoin('utm_stats as utm', 'utm.user_id', '=', 'u.user_id')
            ->where('u.state', '=', 1)
            ->where(function ($query) {
                /* @var $query \Illuminate\Database\Query\Builder */
                $query->where('u.email', 'not like', '%mail.local')
                    ->orWhereNull('u.email');
            });

        if ($filter && is_array($filter)) {
            $filter = [
                'user_id'      => !empty($filter['user_id']) ? intval($filter['user_id']) : '',
                'email'        => !empty($filter['email']) ? $filter['email'] : '',
                'name'         => !empty($filter['name']) ? $filter['name'] : '',
                'city'         => !empty($filter['city']) ? $filter['city'] : '',
                'gender'       => !empty($filter['gender']) ? $filter['gender'] : '',
                'years_min'    => !empty($filter['years_min']) ? intval($filter['years_min']) : '',
                'years_max'    => !empty($filter['years_max']) ? intval($filter['years_max']) : '',
                'platform'     => !empty($filter['platform']) ? $filter['platform'] : '',
                'vip'          => !empty($filter['vip']) ? $filter['vip'] : '',
                'reg_at_min'   => !empty($filter['reg_at_min']) ? $filter['reg_at_min'] : '',
                'reg_at_max'   => !empty($filter['reg_at_max']) ? $filter['reg_at_max'] : '',
                'enter_at_min' => !empty($filter['enter_at_min']) ? $filter['enter_at_min'] : '',
                'enter_at_max' => !empty($filter['enter_at_max']) ? $filter['enter_at_max'] : '',
                'online'       => !empty($filter['online']) ? $filter['online'] : '',
                'utm'          => !empty($filter['utm']) ? $filter['utm'] : '',
                'verification' => !empty($filter['verification']) ? $filter['verification'] : '',
            ];

            if ($filter['email']) {
                $users->where('u.email', 'like', '%' . $filter['email'] . '%');
            }

            if ($filter['user_id']) {
                $users->where('i.user_id', '=', $filter['user_id']);
            }

            if ($filter['name']) {
                $users->where('i.name', 'like', '%' . $filter['name'] . '%');
            }

            if ($filter['city']) {
                $users->where('i.city', 'like', '%' . $filter['city'] . '%');
            }

            if (in_array($filter['gender'], ['1', '2'])) {
                $users->where('i.sex', '=', $filter['gender']);
            }

            $tz_utc = new DateTimeZone('UTC');
            $tz_local = new DateTimeZone($this->timezone);
            if ($filter['reg_at_min']) {
                $date = new DateTime($filter['reg_at_min']);
                $date->setTime(0, 0, 0);
                $date = new Datetime($date->format('Y-m-d H:i:s'), $tz_local);
                $date->setTimezone($tz_utc);
                $users->where('i.reg_date', '>=', $date->format('Y-m-d H:i:s'));
            }
            if ($filter['reg_at_max']) {
                $date = new DateTime($filter['reg_at_max']);
                $date->setTime(23, 59, 59);
                $date = new Datetime($date->format('Y-m-d H:i:s'), $tz_local);
                $date->setTimezone($tz_utc);
                $users->where('i.reg_date', '<=', $date->format('Y-m-d H:i:s'));
            }

            if ($filter['enter_at_min']) {
                $date = new DateTime($filter['enter_at_min']);
                $date->setTime(0, 0, 0);
                $date = new Datetime($date->format('Y-m-d H:i:s'), $tz_local);
                $date->setTimezone($tz_utc);
                $users->where('i.last_visit', '>=', $date->getTimestamp());
            }
            if ($filter['enter_at_max']) {
                $date = new DateTime($filter['enter_at_max']);
                $date->setTime(23, 59, 59);
                $date = new Datetime($date->format('Y-m-d H:i:s'), $tz_local);
                $date->setTimezone($tz_utc);
                $users->where('i.last_visit', '<=', $date->getTimestamp());
            }

            if ($filter['years_min']) {
                $users->where('i.years', '>=', $filter['years_min']);
            }
            if ($filter['years_max']) {
                $users->where('i.years', '<=', $filter['years_max']);
            }

            if ($filter['platform'] && in_array($filter['platform'], ['ios', 'android'])) {
                $users->where('s.app_version', 'like', '%' . $filter['platform'] . '%');
            }

            if (in_array($filter['vip'], ['yes', 'no'])) {
                $users->where('i.vip', '=', $filter['vip'] == 'yes' ? 1 : 0);
            }

            if (in_array($filter['online'], ['online', 'visited', 'offline'])) {
                if ($filter['online'] == 'online') {
                    $users->where('i.last_visit', '>', time() - 600);
                } elseif ($filter['online'] == 'visited') {
                    $users->where(function ($query) {
                        /* @var $query \Illuminate\Database\Query\Builder */
                        $query->where('i.last_visit', '<', time() - 600)
                            ->where('i.last_visit', '>', time() - 21600);
                    });
                } else {
                    $users->where('i.last_visit', '<', time() - 21600);
                }
            }

            if (in_array($filter['utm'], ['yes', 'no', 'adwords', 'google-play', 'youtube'])) {
                if ($filter['utm'] == 'yes') {
                    $users->whereNotNull('utm.original');
                } elseif ($filter['utm'] == 'adwords') {
                    $users->where('utm.original', 'like', '%adsplayload=%');
                } elseif ($filter['utm'] == 'google-play') {
                    $users->where('utm.source', '=', 'google-play');
                } elseif ($filter['utm'] == 'youtube') {
                    $users->where('utm.source', '=', 'youtube');
                } else {
                    $users->whereNull('utm.original');
                }
            }

            if (in_array($filter['verification'], ['yes', 'no'])) {
                $users->where('i.verification', '=', $filter['verification'] == 'yes' ? 1 : 0);
            }
        }

        $count = clone $users;
        $count->select('u.user_id');

        $users->leftJoin('users_photos as ph', 'ph.photo_id', '=', 'i.photo_id')
            ->leftJoin('users_search as us', 'us.user_id', '=', 'u.user_id')
            ->leftJoinSub(
                DB::table('dialog_users')
                    ->select('user_id')
                    ->selectRaw('count(dialog_id) as dialogs')
                    ->where('messages_total', '>', 0)
                    ->groupBy('user_id'),
                'du', 'du.user_id', '=', 'i.user_id')
            ->select('i.user_id', 'i.avatar', 'i.name', 'i.sex', 'i.years', 'i.city', 'i.verification', 'i.points',
                'i.vk', 'i.fb', 'i.vip', 'i.reg_date', 'i.last_visit', 'us.gender', 'us.age_min', 'us.age_max', 'us.radius',
                'ph.photo', 's.gifts', 's.vip_order_method', 's.vip_expires', 's.messages', 'du.dialogs', 's.online as online_total',
                's.app_version', 's.banned_acc', 's.banned_dev', 's.banned_date', 's.banned_admin', 's.blocked_photo', 's.total_amount_points',
                's.total_amount_vip', 'utm.source as utm_source', 'utm.medium as utm_medium', 'utm.term as utm_term', 'utm.original as utm_original')
            ->selectRaw('(s.total_amount_reveal_views + s.total_amount_reveal_likes) as total_amount_reveal')
            ->selectRaw('(s.total_amount_points + s.total_amount_vip + s.total_amount_reveal_views + s.total_amount_reveal_likes) as total_amount_sum');

        $users->leftJoinSub(
            DB::table('users_alerts')
                ->where('type', '=', 4)
                ->groupBy('user_id')
                ->select('user_id', DB::raw('count(*) as count'), DB::raw('sum(if(read_at is null, 0, 1)) as `read`')),
            'uac', 'uac.user_id', '=', 'i.user_id')
            ->selectRaw('uac.count as custom_alert_count, cast(uac.read as unsigned) as custom_alert_read');

        if ($sort) {
            $tmp = explode('|', trim($sort));

            if (is_array($tmp)) {
                if (empty($tmp[1]) || !in_array($tmp[1], ['asc', 'desc'])) {
                    $tmp[1] = 'asc';
                }

                if ($order_fields = explode(',', $tmp[0])) {
                    foreach ($order_fields as $v) {
                        $users->orderBy(trim($v), $tmp[1]);
                    }
                } else {
                    $users->orderBy($tmp[0], $tmp[1]);
                }
            }
        }

        $return = [];

        $users_count = $count->count();
        $users_data = $users->offset($offset)->limit($limit)->get();

        $photos_data = [];

        unset($count, $users);

        $keys = [];
        if ($users_data) {
            $keys = $users_data->keyBy('user_id')->keys();
        }

        if ($keys) {
            // фото полученных пользователей
            $photos = DB::table('users_photos')
                ->select('user_id', 'photo')
                ->whereIn('user_id', $keys)
                ->get();

            foreach ($photos as $v) {
                $photos_data[$v->user_id][] = env('STATIC_PHOTO_URL') . $v->photo;
            }

            unset($photos);
        }

        $now = new DateTime();
        $utm_adwords = DB::table('utm_adwords')->pluck('title', 'utm_id')->toArray();
        $utm_youtube = [];

        foreach ($users_data as $u) {
            $rgd = new DateTime($u->reg_date);
            $rgd->setTimezone(new DateTimeZone($this->timezone));

            $lvd = new DateTime();
            $lvd->setTimestamp($u->last_visit);
            $lvd->setTimezone(new DateTimeZone($this->timezone));

            $photos = [];
            if (!empty($photos_data[$u->user_id])) {
                $photos = array_values(array_diff($photos_data[$u->user_id], [env('STATIC_PHOTO_URL') . $u->photo]));
            }

            if ($u->app_version) {
                $app_version = explode(':', $u->app_version);
                if (!empty($app_version)) {
                    $app_version = array_unique($app_version);
                }
            } else {
                $app_version = [];
            }

            $online_total = $u->online_total ?: 0;

            $vip_expires_days = null;
            if ($u->vip_expires) {
                $vip_expires = new DateTime($u->vip_expires);
                $vip_expires_days .= $now->diff($vip_expires)->format('%r%a (%H:%I)');
            }

            $utm_source = '';
            $utm_medium = '';
            $utm_term = '';

            if (!empty($u->utm_source)) {
                if ($u->utm_source == 'google-play') {
                    $utm_source = 'Google Play';
                    $utm_medium = $u->utm_medium;
                } elseif ($u->utm_source == 'com.yandex.launcher') {
                    $utm_source = 'Yandex';
                    $utm_medium = $u->utm_medium;
                } elseif ($u->utm_source == 'youtube') {
                    $utm_source = 'YouTube';
                    $utm_medium = $u->utm_medium;
                    $utm_term = $u->utm_term;

                    try {
                        if ($u->utm_medium) {
                            if (!array_key_exists($u->utm_medium, $utm_youtube)) {
                                if ($youtube_title = $this->getYouTubeChannelTitle($u->utm_medium)) {
                                    $utm_youtube[$u->utm_medium] = $youtube_title;
                                    $utm_medium = $youtube_title;
                                }
                            } else {
                                $utm_medium = $utm_youtube[$u->utm_medium];
                            }
                        }
                    } catch (GuzzleException $e) {
                    };
                } else {
                    $utm_source = $u->utm_source;
                    $utm_medium = $u->utm_medium;
                    $utm_term = $u->utm_term;
                }
            } elseif (!empty($u->utm_original)) {
                parse_str($u->utm_original, $utm_tags);

                if (!empty($utm_tags['adsplayload'])) {
                    $utm_source = 'AdWords';

                    if (!empty($utm_tags['campaigntype']) && !empty($utm_tags['campaignid'])) {
                        if (array_key_exists($utm_tags['campaignid'], $utm_adwords)) {
                            $utm_term = $utm_adwords[$utm_tags['campaignid']];
                        } else {
                            $utm_term = $utm_tags['campaignid'];
                        }
                    } else {
                        $utm_term = '-';
                    }
                }
            }

            if ($u->banned_date) {
                $banned_date = new DateTime($u->banned_date);
                $banned_date->setTimezone(new DateTimeZone($this->timezone));
                $banned_date = $banned_date->format('Y-m-d H:i:s');
            } else {
                $banned_date = '';
            }

            $rs = [
                'user_id'            => $u->user_id,
                'avatar'             => (!empty($u->avatar) ? (env('STATIC_AVA_URL') . $u->avatar) : ''),
                'photo'              => (!empty($u->photo) ? (env('STATIC_PHOTO_URL') . $u->photo) : ''),
                'name'               => $u->name,
                'gender'             => $u->sex,
                'years'              => $u->years,
                'city'               => $u->city,
                'verification'       => $u->verification,
                'vk'                 => $u->vk,
                'fb'                 => $u->fb,
                'utm'                => [
                    'source' => $utm_source,
                    'medium' => $utm_medium,
                    'term'   => $utm_term
                ],
                'gifts'              => $u->gifts,
                'points'             => $u->points,
                'vip'                => [
                    'status'  => $u->vip,
                    'expires' => $vip_expires_days,
                    'payment' => in_array($u->vip_order_method, ['googlepay', 'itunes']) ? 1 : 0,
                ],
                'online_total'       => sprintf("%d:%02d", floor($online_total / 60), $online_total % 60),
                'search_params'      => [
                    'gender'    => ($u->gender == 0 ? 'Всех' : ($u->gender == 1 ? 'М' : 'Ж')),
                    'age_range' => $u->age_min . '-' . (int)$u->age_max,
                    'radius'    => round((int)$u->radius / 1000, 2),
                ],
                'reg_date'           => $rgd->format('Y-m-d H:i:s'),
                'last_visit'         => $lvd->format('Y-m-d H:i:s'),
                'messages'           => $u->messages ?: 0,
                'dialogs'            => $u->dialogs ?: 0,
                'app_version'        => $app_version,
                'banned'             => ($u->banned_dev || $u->banned_acc),
                'banned_dev'         => $u->banned_dev,
                'banned_acc'         => $u->banned_acc,
                'banned_date'        => $banned_date,
                'banned_admin'       => $u->banned_admin,
                'blocked_photo'      => $u->blocked_photo,
                'custom_alert_count' => $u->custom_alert_count,
                'custom_alert_read'  => $u->custom_alert_read,
                'total_amount'       => [
                    'points' => $u->total_amount_points,
                    'vip'    => $u->total_amount_vip,
                    'reveal' => $u->total_amount_reveal,
                    'sum'    => $u->total_amount_sum,
                ],
                'photos'             => $photos
            ];

            $return[] = $rs;
        }

        $response = [
            'total'         => (int)$users_count,
            'per_page'      => (int)$limit,
            'current_page'  => (int)$page,
            'last_page'     => ceil($users_count / $limit),
            'next_page_url' => null,
            'prev_page_url' => null,
            'from'          => intval($offset + 1),
            'to'            => intval($page * $limit),
            'data'          => $return,
        ];

        return response()->json($response);
    }

    /**
     * @param $channel
     * @return bool|string
     * @throws GuzzleException
     */
    protected function getYouTubeChannelTitle($channel)
    {
        $title = '';
        $client = new GuzzleClient();
        $result = $client->request('GET', 'https://youtube.com/channel/' . $channel);

        if ($result->getStatusCode() == 200) {
            if ($html = $result->getBody()->getContents()) {
                $dom = new \DOMDocument();
                libxml_use_internal_errors(true);
                $dom->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'));
                libxml_use_internal_errors(false);

                $nodes = $dom->getElementsByTagName('title');
                if ($nodes->length > 0) {
                    $title = trim(preg_replace(['/\s+/', '/^(.*) - YouTube$/i'], [' ', '\\1'], $nodes->item(0)->nodeValue));
                }

                return $title;
            }
        }

        return false;
    }
}
