<?php

namespace App\Http\Controllers\Users;

use App\Banlist;
use App\BanlistLog;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Verification\VerificationController;
use App\Http\Services\PhotosService;
use App\Http\Services\UsersService;
use App\Http\Traits\GeoTimezone;
use App\Http\Traits\HelperDB;
use App\UsersAppVersion;
use App\UsersInfo;
use App\UsersPhoto;
use App\UsersPhotoStatus;
use App\UsersPhotoStatusInspect;
use App\UsersStat;
use DateTime;
use Exception;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Storage;
use Validator;

class UsersController extends Controller
{
    use GeoTimezone, HelperDB;

    protected $admin;

    protected $timezone = 'Asia/Krasnoyarsk';

    /**
     * UsersController constructor.
     * @throws Exception
     */
    public function __construct()
    {
        if (!$this->setGeoTimezone()) {
            throw new Exception('Cannot get timezone', 500);
        }

        if (!$this->admin = Auth::user()) {
            throw new Exception('Cannot get auth user information', 500);
        }
    }

    /**
     * Информация о пользователе
     * @param int $user_id Id пользователя
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function get($user_id)
    {
        if (!$user_id = (int)$user_id) {
            return response()->json([], 400);
        }

        $user = DB::table('users_info as i')
            ->join('users as u', 'u.user_id', '=', 'i.user_id')
            ->join('users_stats as s', 's.user_id', '=', 'i.user_id')
            ->join('users_search as us', 'us.user_id', '=', 'i.user_id')
            ->leftJoin('users_photos as p', 'p.photo_id', '=', 'i.photo_id')
            ->leftJoin('facebook as fb', 'fb.user_id', '=', 'i.user_id')
            ->leftJoin('vkontakte as vk', 'vk.user_id', '=', 'i.user_id')
            ->leftJoin('blocked_users as r', 'r.blocked_user', '=', 'i.user_id')
            ->leftJoinSub(
                DB::table('dialog_users')
                    ->select('user_id')
                    ->selectRaw('count(dialog_id) as dialogs')
                    ->where('messages_total', '>', 0)
                    ->groupBy('user_id'),
                'du', 'du.user_id', '=', 'i.user_id')
            ->select('u.user_id', 'u.email', 'u.state', 'i.name', 'i.years', 'i.sex', 'i.latitude', 'i.longitude',
                'i.city', 'i.reg_date', 'i.last_visit', 'i.points', 'i.vip', 'i.avatar', 'i.verification',
                'i.photo_id', 'fb.fb', 'vk.vk', 'p.photo', 'p.status as photo_status', 's.gifts', 's.messages', 's.online', 's.vip_expires',
                's.vip_order_method', 's.banned_acc', 's.banned_dev', 's.banned_dev_associated', 's.banned_date', 's.banned_admin',
                's.blocked_photo', 's.app_version', 's.total_amount_points', 's.total_amount_vip', 'us.gender as search_gender',
                'us.age_min as search_age_min', 'us.age_max as search_age_max', 'du.dialogs')
            ->selectRaw('count(r.blocked_id) as reports_count')
            ->selectRaw('(s.total_amount_reveal_views + s.total_amount_reveal_likes) as total_amount_reveal')
            ->selectRaw('(s.total_amount_points + s.total_amount_vip + s.total_amount_reveal_views + s.total_amount_reveal_likes) as total_amount_sum')
            ->where('i.user_id', '=', $user_id)
            ->havingRaw('user_id is not null')
            ->get()
            ->first();

        $response = [];

        if ($user) {
            $reg_date = '';
            $last_visit = '';
            $banned_date = '';
            $vip_expires_days = null;

            $now = new DateTime();

            if ($user->vip_expires) {
                $vip_expires = new DateTime($user->vip_expires);
                $vip_expires_days .= $now->diff($vip_expires)->format('%r%a (%H:%I)');
            }
            if ($user->reg_date) {
                $date = new DateTime($user->reg_date);
                $date->setTimezone(new \DateTimeZone('Asia/Krasnoyarsk'));
                $reg_date = $date->format('Y-m-d H:i:s');
            }
            if ($user->reg_date) {
                $date = new DateTime();
                $date->setTimestamp($user->last_visit);
                $date->setTimezone(new \DateTimeZone('Asia/Krasnoyarsk'));
                $last_visit = $date->format('Y-m-d H:i:s');
            }
            if ($user->banned_date) {
                $date = new DateTime($user->banned_date);
                $date->setTimezone(new \DateTimeZone('Asia/Krasnoyarsk'));
                $banned_date = $date->format('Y-m-d H:i:s');
            }

            $dialogs = DB::table('dialog_users as d1')
                ->join('dialog_users as d2', function ($join) use ($user_id) {
                    /* @var $join \Illuminate\Database\Query\JoinClause */
                    $join->on('d2.dialog_id', '=', 'd1.dialog_id');
                    $join->on('d2.user_id', '!=', 'd1.user_id');
                })
                ->join('users_info as i', 'i.user_id', '=', 'd2.user_id')
                ->join('messages as m', 'm.dialog_id', '=', 'd1.dialog_id')
                ->select('i.user_id', 'i.name', 'i.years', 'i.avatar', 'd1.dialog_id', 'd1.messages_inc', 'd1.messages_out')
                ->selectRaw('max(m.message_id) last_message')
                ->where('d1.user_id', '=', $user_id)
                ->where('d1.messages_total', '>', 0)
                ->groupBy('dialog_id')
                ->orderByDesc('last_message')
                ->get()
                ->toArray();

            if ($dialogs) {
                foreach ($dialogs as $v) {
                    $v->avatar = $v->avatar ? env('STATIC_AVA_URL') . $v->avatar : '';
                }
            }

            $alerts = $alert = DB::table('users_alerts as ua')
                ->leftJoin('admins as a', 'ua.admin_id', '=', 'a.id')
                ->select(['ua.alert_id', 'ua.type', 'ua.priority', 'ua.data', 'ua.read_at', 'ua.created_at', 'a.name as admin_name'])
                ->where('ua.user_id', '=', $user_id)
                ->where('ua.type', '=', 4)
                ->orderByDesc('ua.alert_id')
                ->get()
                ->toArray();

            if ($alerts) {
                foreach ($alerts as $v) {
                    $alert_data = @json_decode($v->data);

                    $date = new DateTime($v->created_at);
                    $date->setTimezone(new \DateTimeZone('Asia/Krasnoyarsk'));
                    $v->created_at = $date->format('Y-m-d H:i:s');

                    if ($alert_data) {
                        $v->data = $alert_data;
                    } else {
                        $v->data = json_decode('{}');
                    }
                }
            }

            $response = [
                'email'                 => $user->email,
                'state'                 => $user->state,
                'name'                  => $user->name,
                'age'                   => $user->years,
                'gender'                => $user->sex,
                'latitude'              => (float)$user->latitude,
                'longitude'             => (float)$user->longitude,
                'city'                  => $user->city,
                'verification'          => $user->verification,
                'photo_id'              => $user->photo_id,
                'fb'                    => $user->fb ? 'https://facebook.com/profile.php?id=' . $user->fb : '',
                'vk'                    => $user->vk ? 'https://vk.com/id' . $user->vk : '',
                'gifts'                 => $user->gifts,
                'points'                => $user->points,
                'vip'                   => [
                    'status'  => $user->vip,
                    'expires' => $vip_expires_days,
                    'payment' => in_array($user->vip_order_method, ['googlepay', 'itunes']) ? 1 : 0,
                ],
                'banned_acc'            => $user->banned_acc,
                'banned_dev'            => $user->banned_dev,
                'banned_dev_associated' => $user->banned_dev_associated,
                'banned_date'           => $banned_date,
                'banned_admin'          => $user->banned_admin,
                'blocked_photo'         => $user->blocked_photo,
                'total_amount'          => [
                    'points' => $user->total_amount_points,
                    'vip'    => $user->total_amount_vip,
                    'reveal' => $user->total_amount_reveal,
                    'sum'    => $user->total_amount_sum,
                ],
                'fake'                  => preg_match('/.*mail\.local$/i', $user->email) ? 1 : 0,
                'messages'              => $user->messages,
                'dialogs'               => $user->dialogs,
                'online_total'          => sprintf("%d:%02d", floor($user->online / 60), $user->online % 60),
                'app_version'           => $user->app_version,
                'reg_date'              => $reg_date,
                'last_visit'            => $last_visit,
                'search_age_min'        => $user->search_age_min,
                'search_age_max'        => $user->search_age_max,
                'search_gender'         => $user->search_gender,
                'avatar_link'           => $user->avatar ? env('STATIC_AVA_URL') . $user->avatar : '',
                'main_photo_link'       => $user->photo ? env('STATIC_PHOTO_URL') . $user->photo : '',
                'main_photo_status'     => $user->photo_status,
                'photos'                => $this->getUserPhotos($user_id),
                'reports_count'         => $user->reports_count,
                'dialogs_user'          => $dialogs,
                'alerts'                => $alerts,
            ];
        }

        return response()->json($response);
    }

    /**
     * Список фото пользователя
     * @param int $user_id Id пользователя
     * @return array
     */
    public function getUserPhotos($user_id)
    {
        $photos = [];

        $result = UsersPhoto::with([
            'statusInspect'       => function ($query) {
                /* @var \Illuminate\Database\Eloquent\Builder $query */
                $query->select('status_id', 'photo_id', 'admin_id', 'admin_status', 'associated', 'date')
                    ->orderBy('status_id', 'desc')
                    ->orderBy('date', 'desc');
            },
            'statusInspect.admin' => function ($query) {
                /* @var \Illuminate\Database\Eloquent\Builder $query */
                $query->select('id', 'name');
            }])
            ->where('user_id', $user_id)
            ->orderByRaw('case when deleted_at is null then 0 else 1 end')
            ->orderBy('photo_id', 'desc')
            ->get(['photo_id', 'user_id', 'photo', 'main_photo', 'status', 'deleted_at']);

        foreach ($result as $v) {
            /* @var \Illuminate\Database\Eloquent\Collection|UsersPhotoStatusInspect[] $statusInspect */
            $statusInspect = $v->statusInspect;

            foreach ($statusInspect as $status) {
                $status->date = $this->tzConvertToLocal($status->date);
            }

            if ($statusInspect->count() > 0) {
                /* @var UsersPhotoStatusInspect $inspect */
                $inspect = $statusInspect->first();
            } else {
                $inspect = null;
            }

            $photos[] = [
                'id'         => $v->photo_id,
                'prew'       => $v->photo ? env('STATIC_GALLERY_PREW_URL') . $v->photo : '',
                'photo'      => $v->photo ? env('STATIC_GALLERY_URL') . $v->photo : '',
                'status'     => $v->status,
                'inspect'    => $inspect,
                'deleted_at' => $v->deleted_at,
            ];
        }

        return $photos;
    }

    /**
     * Удаление пользователя
     * @param int $id ID пользователя
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteUser($id)
    {
        DB::table('users')
            ->where('user_id', $id)
            ->update([
                'email' => null,
                'vk'    => null,
                'fb'    => null,
                'state' => 0
            ]);
        DB::table('vkontakte')
            ->where('user_id', $id)->delete();

        DB::table('facebook')
            ->where('user_id', $id)->delete();

        return response()->json([]);
    }

    /**
     * Изменение vip статуса пользователям
     * @param array $users Список пользователей
     * @param integer $state Статус (0/1)
     */
    private function setVipState(array $users, $state = 0)
    {
        if (count($users) > 0) {
            DB::table('users_info')
                ->whereIn('user_id', $users)
                ->update([
                    'vip' => (int)$state
                ]);
        }
    }

    /**
     * Добавление vip статуса с периодом действия
     * @param Request $request Список пользователей
     * @return \Illuminate\Http\JsonResponse
     * @throws GuzzleException
     */
    public function vipAdd(Request $request)
    {
        $data = [];
        $users = $request->input('users');
        $period = (int)$request->input('period');

        if (!is_array($users) || count($users) < 1) {
            return response()->json(['error' => 'Не указаны пользователи'], 400);
        }

        if (!$period) {
            return response()->json(['error' => 'Не указана длительность vip статуса'], 400);
        }

        $expires = strtotime("+" . $period . " days");
        $expires += 3600;

        foreach ($users as $v) {
            if (!intval($v)) {
                continue;
            }
            $data[] = [
                'user_id'      => (int)$v,
                'order_method' => 'skylove',
                'order_id'     => 0,
                'begin'        => time(),
                'expires'      => $expires,
                'status'       => 1
            ];
        }

        if (!$data) {
            return response()->json(['error' => 'Не удалось сформировать данные'], 400);
        }

        DB::table('vip_duration')->insert($data);
        $this->setVipState($users, 1);

        $client = new GuzzleClient();
        $result = $client->request('GET', env('SKYLOVE_API_URL') . 'push/give_vip', [
            'query' => [
                'users'  => $users,
                'period' => $period,
                'key'    => password_hash('skylove.su' . 'give_vip_push', PASSWORD_BCRYPT)
            ]
        ]);

        if ($result->getStatusCode() == 200) {
            return response()->json([], 200);
        }

        return response()->json(['error' => 'Ошибка отправки уведомления'], $result->getStatusCode());
    }

    /**
     * Удаление vip статуса пользователям
     * @param Request $request Список пользователей
     * @return \Illuminate\Http\JsonResponse
     */
    public function vipRemove(Request $request)
    {
        $users = $request->input('users');

        DB::table('vip_duration')
            ->where('order_method', 'skylove')
            ->whereIn('user_id', $users)
            ->delete();
        self::setVipState($users, 0);

        return response()->json([]);
    }

    /**
     * Добавление в бан-лист пользователей
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function banUserAdd(Request $request)
    {
        $users = $this->prepareWhereInInt($request->all());

        if ($users) {
            try {
                $data = collect([]);

                DB::beginTransaction();

                $users = UsersInfo::with('stats')
                    ->whereIn('user_id', $users)
                    ->get(['user_id', 'photo_id']);

                foreach ($users as $user) {
                    if ($user->stats->banned_acc == 1) {
                        continue;
                    }

                    $data->push([
                        'user_id'  => $user->user_id,
                        'photo_id' => $user->photo_id,
                    ]);
                }

                if ($data) {
                    UsersPhotoStatusInspect::whereIn('photo_id', $data->pluck('photo_id')->toArray())->delete();
                    (new UsersService)->addBanAccByUserId($data->pluck('user_id')->toArray());
                }

                DB::commit();
            } catch (Exception $e) {
                return response()->json(['error' => $e->getMessage()], 500);
            }
        }

        return response()->json([]);
    }

    /**
     * Удаление из бан-листа пользователей
     * @param Request $request Список пользователей
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function banUserRemove(Request $request)
    {
        $users = $this->prepareWhereInInt($request->all());

        if ($users) {
            try {
                $data = collect([]);

                DB::beginTransaction();

                $users = UsersInfo::with('stats')
                    ->whereIn('user_id', $users)
                    ->get(['user_id', 'photo_id']);

                foreach ($users as $user) {
                    if ($user->stats->banned_acc == 0) {
                        continue;
                    }

                    $data->push([
                        'user_id'  => $user->user_id,
                        'photo_id' => $user->photo_id,
                    ]);
                }

                if ($data) {
                    UsersPhotoStatusInspect::whereIn('photo_id', $data->pluck('photo_id')->toArray())->delete();
                    (new UsersService)->removeBanAccByUserId($data->pluck('user_id')->toArray());
                }

                DB::commit();
            } catch (Exception $e) {
                return response()->json(['error' => $e->getMessage()], 500);
            }
        }

        return response()->json([]);
    }

    /**
     * Добавление в бан-лист устройства пользователей
     * @param Request $request Список пользователей
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function banDeviceAdd(Request $request)
    {
        $users = $this->prepareWhereInInt($request->all());

        if ($users) {
            try {
                DB::beginTransaction();

                (new UsersService)->addBanDevByUserId($users);

                DB::commit();
            } catch (Exception $e) {
                DB::rollBack();

                \Log::error($e);

                return response()->json(['error' => $e->getMessage()], 500);
            }
        }

        return response()->json([]);
    }

    /**
     * Удаление из бан-листа устройства пользователей
     * @param Request $request Список пользователей
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function banDeviceRemove(Request $request)
    {
        $users = $this->prepareWhereInInt($request->all());

        if ($users) {
            (new UsersService)->removeBanDevByUserId($users);
        }

        return response()->json([]);
    }

    /**
     * Изменение пола пользователей
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeGender(Request $request)
    {
        $users = $request->input('users');
        $gender = $request->input('gender');

        if (count($users) > 0 && ($gender != 1 || $gender != 2)) {
            if (count($users) > 0) {
                DB::table('users_info')
                    ->whereIn('user_id', $users)
                    ->update([
                        'sex' => $gender
                    ]);
            }
        }

        return response()->json([]);
    }

    /**
     * Добавление баллов
     * @param Request $request Список пользователей
     * @return \Illuminate\Http\JsonResponse
     * @throws GuzzleException
     */
    public function pointsAdd(Request $request)
    {
        $data = [];
        $users = $request->input('users');
        $points = (int)$request->input('points');

        if (!is_array($users) || count($users) < 1) {
            return response()->json(['error' => 'Не указаны пользователи'], 400);
        }

        if (!$points) {
            return response()->json(['error' => 'Не указано количество баллов'], 400);
        }

        $users_id = [];
        foreach ($users as $v) {
            if (!intval($v)) {
                continue;
            }
            $users_id[] = $v;
            $data[] = [
                'user_id'     => (int)$v,
                'transaction' => 'internal',
                'service'     => null,
                'description' => 'ADM_ADD',
                'amount'      => $points,
                'date'        => time(),
                'tz'          => date('O', time())
            ];
        }

        if (!$data) {
            return response()->json(['error' => 'Не удалось сформировать данные'], 400);
        }

        DB::table('balance_history')->insert($data);
        DB::table('users_info')
            ->whereIn('user_id', $users_id)
            ->increment('points', $points);

        $client = new GuzzleClient();
        $result = $client->request('GET', env('SKYLOVE_API_URL') . 'push/give_points', [
            'query' => [
                'users'  => $users,
                'points' => $points,
                'key'    => password_hash('skylove.su' . 'give_points_push', PASSWORD_BCRYPT)
            ]
        ]);

        if ($result->getStatusCode() == 200) {
            return response()->json([], 200);
        }

        return response()->json(['error' => 'Ошибка отправки уведомления'], $result->getStatusCode());
    }

    /**
     * Замена аватара пользователя
     * @param Request $request
     * @param integer $user_id Id пользователя
     * @return \Illuminate\Http\JsonResponse
     * @throws \ImagickException
     */
    public function replaceAvatar(Request $request, $user_id)
    {
        $validation = Validator::make($request->all(), [
            'avatar' => 'required|image',
        ]);

        if ($validation->fails()) {
            return response()->json(['error' => 'Файл отсутствует или не является изображением'], 400);
        }

        $original = DB::table('users_info')
            ->select('user_id', 'avatar', 'avatar_blurred')
            ->where('user_id', $user_id)
            ->first();

        if (!$original) {
            return response()->json(['error' => 'Не найдена запись об оригинальном изображении'], 400);
        }

        // директория
        $uploadDir = explode('/', $original->avatar);

        if (!is_array($uploadDir) || count($uploadDir) < 2) {
            $uploadDir = [$this->generatePathSegment($user_id, 'avatar')];
        }
        $uploadPath = 'avatars/' . $uploadDir[0];

        // имена файлов
        $avatarFilename = $this->genFilename(
            $user_id,
            $request->file('avatar')->getPathName(),
            $request->file('avatar')->extension()
        );
        $avatarPath = $uploadPath . '/' . $avatarFilename;

        $blurredFilename = $this->genFilename(
            $user_id,
            $request->file('avatar')->getPathName(),
            $request->file('avatar')->extension()
        );
        $blurredPath = $uploadPath . '/' . $blurredFilename;

        // проверка на дублирование файлов
        if (Storage::disk('s3')->exists($avatarPath)) {
            return response(json_encode(['error' => 'Файл с таким именем "' . $avatarPath . '", уже есть в хранилище']), 400);
        }

        if (Storage::disk('s3')->exists($blurredPath)) {
            return response(json_encode(['error' => 'Файл с таким именем "' . $blurredPath . '", уже есть в хранилище']), 400);
        }

        // размытие аватара
        $blurred = new \Imagick($request->file('avatar')->getPathName());
        $blurred->blurImage(10, 10);
        $blurredContent = $blurred->getImageBlob();
        $blurred->destroy();

        // загрузка аватара
        if (!Storage::disk('s3')->putFileAs($uploadPath, $request->file('avatar'), $avatarFilename, 'public')) {
            return response(json_encode(['error' => 'Ошибка загрузки аватара в хранилище']), 400);
        }

        // загрузка размытого аватара
        if (!Storage::disk('s3')->put($blurredPath, $blurredContent, 'public')) {
            return response(json_encode(['error' => 'Ошибка загрузки размытого аватара в хранилище']), 400);
        }

        // удаляем старые файлы
        if ($original->avatar) {
            $oldAvatarPath = 'avatars/' . $original->avatar;
            if (Storage::disk('s3')->exists($oldAvatarPath)) {
                Storage::disk('s3')->delete($oldAvatarPath);
            }
        }

        if ($original->avatar_blurred) {
            $oldBlurredPath = 'avatars/' . $original->avatar_blurred;
            if (Storage::disk('s3')->exists($oldBlurredPath)) {
                Storage::disk('s3')->delete($oldBlurredPath);
            }
        }

        $avatarUpdate = [
            'avatar'         => $uploadDir[0] . '/' . $avatarFilename,
            'avatar_blurred' => $uploadDir[0] . '/' . $blurredFilename,
        ];
        DB::table('users_info')
            ->where('user_id', $user_id)
            ->update($avatarUpdate);

        $response = [
            'avatar' => env('STATIC_AVA_URL') . $avatarUpdate['avatar'],
        ];

        return response()->json($response);
    }

    /**
     * Удаление аватара пользователя
     * @param integer $user_id Id пользователя
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteAvatar($user_id)
    {
        $original = DB::table('users_info')
            ->select('avatar', 'avatar_blurred')
            ->where('user_id', $user_id)
            ->first();

        if (!$original) {
            return response()->json(['error' => 'Не найдена запись об оригинальном изображении'], 400);
        }

        // удаляем файлы
        $avatarPath = 'avatars/' . $original->avatar;
        if ($original->avatar && Storage::disk('s3')->exists($avatarPath)) {
            Storage::disk('s3')->delete($avatarPath);
        }

        $blurredPath = 'avatars/' . $original->avatar_blurred;
        if ($original->avatar_blurred && Storage::disk('s3')->exists($blurredPath)) {
            Storage::disk('s3')->delete($blurredPath);
        }

        $avatarUpdate = [
            'avatar'         => '',
            'avatar_blurred' => '',
        ];
        DB::table('users_info')
            ->where('user_id', $user_id)
            ->update($avatarUpdate);

        return response()->json([]);
    }

    /**
     * Замена основного фото пользователя
     * @param Request $request
     * @param integer $user_id Id пользователя
     * @return \Illuminate\Http\JsonResponse
     */
    public function replaceMainphoto(Request $request, $user_id)
    {
        $validation = Validator::make($request->all(), [
            'mainphoto' => 'required|image',
        ]);

        if ($validation->fails()) {
            return response()->json(['error' => 'Файл отсутствует или не является изображением'], 400);
        }

        $mainphoto = DB::table('users_info')
            ->join('users_photos', 'users_info.photo_id', '=', 'users_photos.photo_id')
            ->select('users_photos.photo_id', 'users_photos.photo')
            ->where('users_info.user_id', $user_id)
            ->first();

        if (!$mainphoto) {
            return response()->json(['error' => 'Не найдена запись об оригинальном изображении'], 400);
        }

        // директория
        $uploadDir = explode('/', $mainphoto->photo);

        if (!is_array($uploadDir) || count($uploadDir) < 2) {
            $uploadDir = [$this->generatePathSegment($user_id, 'profile_photo')];
        }
        $uploadPath = 'photo/' . $uploadDir[0];

        // имя файла
        $photoFilename = $this->genFilename(
            $user_id,
            $request->file('mainphoto')->getPathName(),
            $request->file('mainphoto')->extension()
        );
        $photoPath = $uploadPath . '/' . $photoFilename;

        // проверка на дублирование файлов
        if (Storage::disk('s3')->exists($photoPath)) {
            return response(json_encode(['error' => 'Файл с таким именем "' . $photoPath . '", уже есть в хранилище']), 400);
        }

        // загрузка файла
        if (!Storage::disk('s3')->putFileAs($uploadPath, $request->file('mainphoto'), $photoFilename, 'public')) {
            return response(json_encode(['error' => 'Ошибка загрузки аватара в хранилище']), 400);
        }

        // удаляем старый файл
        if ($mainphoto->photo) {
            $oldPhotoPath = 'photo/' . $mainphoto->photo;
            if (Storage::disk('s3')->exists($oldPhotoPath)) {
                Storage::disk('s3')->delete($oldPhotoPath);
            }
        }

        $mainphotoUpdate = $uploadDir[0] . '/' . $photoFilename;

        if ($mainphoto->photo_id) {
            $photo_id = $mainphoto->photo_id;
            DB::table('users_photos')
                ->where('photo_id', $photo_id)
                ->update(['photo' => $mainphotoUpdate]);
        } else {
            $photo_id = DB::table('users_photos')->insertGetId([
                'user_id' => $user_id,
                'photo'   => $uploadDir[0] . '/' . $photoFilename
            ]);
            DB::table('users_info')
                ->where('user_id', $user_id)
                ->update(['photo_id' => $photo_id]);
        }

        $response = [
            'main_photo' => env('STATIC_GALLERY_PREW_URL') . $mainphotoUpdate,
            'gallery'    => [
                'id'    => $photo_id,
                'prew'  => env('STATIC_GALLERY_PREW_URL') . $mainphotoUpdate,
                'photo' => env('STATIC_GALLERY_URL') . $mainphotoUpdate,
            ],
        ];

        return response()->json($response);
    }

    /**
     * Удаление основного фото пользователя
     * @param integer $user_id Id пользователя
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteMainphoto($user_id)
    {
        $mainphoto = DB::table('users_info')
            ->join('users_photos', 'users_info.photo_id', '=', 'users_photos.photo_id')
            ->select('users_photos.photo_id', 'users_photos.photo')
            ->where('users_info.user_id', $user_id)
            ->first();

        if (!$mainphoto) {
            return response()->json(['error' => 'Не найдена запись об оригинальном изображении'], 400);
        }

        // удаляем файл
        if ($mainphoto->photo) {
            $oldPhotoPath = 'photo/' . $mainphoto->photo;
            if (Storage::disk('s3')->exists($oldPhotoPath)) {
                Storage::disk('s3')->delete($oldPhotoPath);
            }
        }

        DB::table('users_photos')
            ->where('photo_id', $mainphoto->photo_id)
            ->delete();
        DB::table('users_info')
            ->where('user_id', $user_id)
            ->update(['photo_id' => null]);

        return response()->json([]);
    }

    /**
     * Удаление фото в галерее пользователя
     * @param Request $request
     * @param integer $user_id Id пользователя
     * @return \Illuminate\Http\JsonResponse
     */
    public function deletePhoto(Request $request, $user_id)
    {
        $validation = Validator::make($request->all(), [
            'photo_id' => 'required|integer',
        ]);

        if ($validation->fails()) {
            return response()->json(['error' => 'Не указан id файла'], 400);
        }

        $photo = DB::table('users_photos')
            ->where('user_id', $user_id)
            ->where('photo_id', $request->input('photo_id'))
            ->pluck('photo')
            ->first();

        if (!$photo) {
            return response()->json(['error' => 'Не найдена запись об изображении'], 400);
        }

        $uploadPath = 'photo/' . $photo;

        if (Storage::disk('s3')->exists($uploadPath)) {
            if (!Storage::disk('s3')->delete($uploadPath)) {
                return response()->json(['error' => 'Не удалось удалить файл с сервера'], 400);
            }
        }

        DB::table('users_photos')
            ->where('user_id', $user_id)
            ->where('photo_id', $request->input('photo_id'))
            ->delete();

        return response()->json([]);
    }

    /**
     * Замена фото в галерее пользователя
     * @param Request $request
     * @param integer $user_id Id пользователя
     * @return \Illuminate\Http\JsonResponse
     */
    public function replacePhoto(Request $request, $user_id)
    {
        $validation = Validator::make($request->all(), [
            'photo_id' => 'required|integer',
            'photo'    => 'required|image',
        ]);

        if ($validation->fails()) {
            $error = 'Неизвестная ошибка.';
            $errors = $validation->errors()->keys();

            if (in_array('photo_id', $errors)) {
                $error = 'Не указан id файла';
            } elseif (in_array('photo', $errors)) {
                $error = 'Файл отсутствует или не является изображением';
            }

            return response()->json(['error' => $error], 400);
        }

        $original = DB::table('users_photos')
            ->where('user_id', $user_id)
            ->where('photo_id', $request->input('photo_id'))
            ->pluck('photo')
            ->first();

        if (!$original) {
            return response()->json(['error' => 'Не найдена запись об оригинальном изображении'], 400);
        }

        // директория файла
        $originalPath = explode('/', $original);

        if (!is_array($originalPath) || count($originalPath) < 2) {
            return response()->json(['error' => 'Не удалось получить директорию для загрузки'], 400);
        }
        $uploadPath = 'photo/' . $originalPath[0];
        $originalPath = 'photo/' . $original;

        if (!$storagePath = Storage::disk('s3')->put($uploadPath, $request->file('photo'), 'public')) {
            return response(json_encode(['error' => 'Ошибка загрузки файла в хранилище']), 400);
        }
        $uploadFile = str_replace('photo/', '', $storagePath);

        // удаляем старый файл
        if (Storage::disk('s3')->exists($originalPath)) {
            Storage::disk('s3')->delete($originalPath);
        }

        $uploadUpdate = [
            'photo' => $uploadFile,
        ];

        DB::table('users_photos')
            ->where('user_id', $user_id)
            ->where('photo_id', $request->input('photo_id'))
            ->update($uploadUpdate);

        $response = [
            'id'    => $request->input('photo_id'),
            'prew'  => env('STATIC_GALLERY_PREW_URL') . $uploadUpdate['photo'],
            'photo' => env('STATIC_GALLERY_URL') . $uploadUpdate['photo'],
        ];

        return response()->json($response);
    }

    /**
     * Скачать фото пользователя
     * @param integer $photo_id Id фото
     * @return \Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\StreamedResponse
     */
    public function downloadPhoto($photo_id)
    {
        $photo = DB::table('users_photos')
            ->where('photo_id', $photo_id)
            ->pluck('photo')
            ->first();

        if (!$photo) {
            abort(404);
        }

        $filepath = env('STATIC_GALLERY_URL') . $photo;
        $filename = basename($photo);

        try {
            $content = file_get_contents($filepath);
        } catch (Exception $e) {
            return response()->json([], 400);
        }

        return response()->streamDownload(function () use ($content) {
            echo $content;
        }, $filename);
    }

    private function genFilename($user_id, $tmp_path, $ext)
    {
        return rand(1000, 9999) . md5($user_id . '_' . basename($tmp_path) . time()) . '.' . $ext;
    }

    private function generatePathSegment($user_id, $folder)
    {
        // получаем число из id пользователя по схеме
        // 1...999 => 1
        // 1000..1999 => 2 и т.д.
        $user_num = ($user_id % 1000) + 1;

        // шифруем число для файлового пути по следующему алгоритму
        // берем secret и делим на правую и левую части
        // в текущей версии это хардкод на 5-ти значное число
        // прибавляем четное/нечетное к левой/правой частям
        // собираем обратно в строку, строку в число, число в hex
        switch ($folder) {
            case 'profile_photo':
                $current_secret = '36419';
                break;
            case 'avatar':
                $current_secret = '27153';
                break;
            case 'attachment':
                $current_secret = '82851';
                break;
            case 'misc':
                $current_secret = '29011';
                break;
            default:
                $current_secret = '12345';
        }

        $secret_arr = str_split($current_secret);
        $secret_delimiter = $secret_arr[2];
        $parts = explode($secret_delimiter, $current_secret);
        // $parts[0] -- левая часть для нечетных чисел
        // $parts[1] -- правая часть для четных чисел
        if (($user_num % 2) == 0) { // четное
            $new_part = $parts[0] + $user_num;
            $folder_secret = $new_part . $secret_delimiter . $parts[1];
        } else {
            $new_part = $parts[1] + $user_num;
            $folder_secret = $parts[0] . $secret_delimiter . $new_part;
        }

        settype($folder_secret, 'int');

        return dechex($folder_secret);
    }

    /**
     * Диалог пользователя
     * @param int $dialog_id Id диалога
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function getDialog($dialog_id)
    {
        $dialog_id = (int)$dialog_id;

        if (!$dialog_id) {
            return response()->json([]);
        }

        $result = DB::table('messages as m')
            ->join('users_info as i', 'i.user_id', '=', 'm.user_id')
            ->where('m.dialog_id', '=', $dialog_id)
            ->orderBy('m.message_id', 'asc')
            ->select('m.message_id', 'm.user_id', 'i.name', 'i.years', 'i.avatar', 'm.message', 'm.type', 'm.date')
            ->get()
            ->toArray();

        $messages = [];
        foreach ($result as $v) {
            $date = '';
            if ($v->date) {
                $date = new DateTime();
                $date->setTimestamp($v->date);
                $date->setTimezone(new \DateTimeZone($this->timezone));
                $date = $date->format('d.m.Y H:i');
            }
            $msg = [
                'message_id' => $v->message_id,
                'user_id'    => $v->user_id,
                'name'       => $v->name,
                'years'      => $v->years,
                'avatar'     => $v->avatar ? env('STATIC_AVA_URL') . $v->avatar : '',
                'message'    => $v->message,
                'type'       => $v->type,
                'date'       => $date,
            ];

            if ($msg['type'] != 1) {
                $json = @json_decode($msg['message'], true);
                if (json_last_error() == JSON_ERROR_NONE && is_array($json)) {
                    $msg['message'] = $this->messageStaticURL($json);
                } else {
                    $msg['type'] = 1;
                }
            }

            $messages[] = $msg;
        }

        return response()->json($messages);
    }

    public function verificationUser(Request $request)
    {
        $user_id = $request->input('user_id');
        $status = $request->input('status');

        $data = DB::table('users_info as ui')
            ->join('users_photos as up', 'ui.photo_id', '=', 'up.photo_id')
            ->leftJoin('users_verification as uv', 'ui.photo_id', '=', 'uv.photo_id')
            ->where('ui.user_id', '=', $user_id)
            ->get(['up.photo_id', 'up.photo', 'uv.photo_id as verification'])
            ->first();

        if (!$data || !$data->photo) {
            return response()->json([], 400);
        }

        if (!$data->verification) {
            $insert = [
                'user_id'  => $user_id,
                'photo_id' => $data->photo_id,
                'selfie'   => $data->photo,
                'status'   => 0,
            ];
            DB::table('users_verification')->insert($insert);
        }

        $verificationRequest = new Request();
        $verificationRequest->setMethod('POST');
        $verificationRequest->request->add(['photos' => [$data->photo_id], 'status' => $status]);

        return (new VerificationController)->verificationPhoto($verificationRequest);
    }

    /**
     * Добавление временной блокировки по основному фото
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createBlockedUser(Request $request)
    {
        $users = $request->all();

        try {
            return response()->json($this->blockedUser($users, 1));
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * Удаление временной блокировки по основному фото
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeBlockedUser(Request $request)
    {
        $users = $request->all();

        try {
            return response()->json($this->blockedUser($users, 0));
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * Добавление бана аккаунта
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createBannedAcc(Request $request)
    {
        $users = $request->all();

        try {
            return response()->json($this->bannedAcc($users, 1));
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * Удаление бана аккаунта
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeBannedAcc(Request $request)
    {
        $users = $request->all();

        try {
            return response()->json($this->bannedAcc($users, 0));
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * Добавление бана устройств пользователей
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createBannedDev(Request $request)
    {
        $users = $request->all();

        try {
            return response()->json($this->addBannedDev($users));
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * Удаление бана устройств пользователей
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeBannedDev(Request $request)
    {
        $users = $request->all();

        try {
            return response()->json($this->delBannedDev($users));
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * Добавление/удаление временной блокировки по основному фото
     * @param array $users
     * @param int $status
     * @return array
     * @throws Exception
     */
    protected function blockedUser(array $users, $status)
    {
        try {
            $return = [];

            DB::beginTransaction();

            $usersInfo = UsersInfo::with([
                'stats'     => function ($query) {
                    /* @var \Illuminate\Database\Eloquent\Builder $query */
                    $query->select('user_id', 'banned_acc', 'banned_dev', 'blocked_photo');
                },
                'mainPhoto' => function ($query) {
                    /* @var \Illuminate\Database\Eloquent\Builder $query */
                    $query->select('photo_id', 'user_id', 'main_photo', 'status');
                }])
                ->whereIn('user_id', $users)
                ->get(['user_id', 'photo_id']);

            if ($usersInfo->count() > 0) {
                foreach ($usersInfo as $userInfo) {
                    /* @var UsersStat $userStats */
                    $userStats = $userInfo->stats;
                    /* @var UsersPhoto $mainPhoto */
                    $mainPhoto = $userInfo->mainPhoto;

                    // нет основного фото
                    if (!$mainPhoto) {
                        throw new Exception('Cannot blocked user without main photo');
                    }

                    // пользователь не заблокирован
                    if ($userStats->blocked_photo == 0 && $status == 0) {
                        throw new Exception('User already blocked');
                    }

                    // пользователь уже заблокирован
                    if ($userStats->blocked_photo == 1 && $status == 1) {
                        throw new Exception('User has no blocked');
                    }

                    // аккаунт пользователь забанен
                    if ($userStats->banned_acc == 1) {
                        throw new Exception('Cannot blocked user with account banned');
                    }

                    // устройство пользователя забанено
                    if ($userStats->banned_dev == 1) {
                        throw new Exception('Cannot blocked user with device banned');
                    }

                    // сброс статуса контроля для основного фото пользователя
                    UsersPhotoStatusInspect::wherePhotoId($mainPhoto->photo_id)->delete();

                    // установка статуса фото
                    UsersPhotoStatus::create([
                        'photo_id'   => $mainPhoto->photo_id,
                        'user_id'    => $mainPhoto->user_id,
                        'admin_id'   => $this->admin->id,
                        'status'     => ($status == 1 ? 4 : 0),
                        'main_photo' => 1,
                    ]);
                    (new PhotosService)->removeAlertBlocked($userInfo->user_id);

                    if ($status == 1) {
                        UsersPhotoStatusInspect::create([
                            'photo_id'     => $mainPhoto->photo_id,
                            'admin_id'     => $this->admin->id,
                            'admin_status' => 4,
                            'level'        => $this->admin->level,
                        ]);
                        (new PhotosService)->createAlertBlocked($userInfo->user_id);
                    }

                    $return[] = $userInfo->user_id;
                }
            }

            if (count($return) > 0) {
                DB::commit();
            }

            return $return;
        } catch (Exception $e) {
            Log::error($e);
            DB::rollBack();

            throw new Exception($e->getMessage());
        }
    }

    /**
     * Добавление/удаление бана аккаунта
     * @param array $users
     * @param int $status
     * @return array
     * @throws Exception
     */
    protected function bannedAcc(array $users, $status)
    {
        try {
            $return = [];

            $usersInfo = UsersInfo::with([
                'stats'  => function ($query) {
                    /* @var \Illuminate\Database\Eloquent\Builder $query */
                    $query->select('user_id', 'banned_acc', 'banned_dev', 'blocked_photo');
                },
                'photos' => function ($query) {
                    /* @var \Illuminate\Database\Eloquent\Builder $query */
                    $query->select('user_id', 'photo_id', 'main_photo', 'status');
                }])
                ->whereIn('user_id', $users)
                ->get(['user_id', 'photo_id']);

            if ($usersInfo->count() > 0) {
                $bannedDevUsers = [];

                DB::beginTransaction();

                foreach ($usersInfo as $userInfo) {
                    /* @var UsersStat $userStats */
                    $userStats = $userInfo->stats;
                    /* @var \Illuminate\Database\Eloquent\Collection|UsersPhoto[] $userPhotos */
                    $userPhotos = $userInfo->photos;

                    if (($userStats->banned_acc == 0 && $status == 0) || ($userStats->banned_acc == 1 && $status == 1)) {
                        continue;
                    }

                    if ($userStats->banned_dev == 1) {
                        if (!in_array($userInfo->user_id, $bannedDevUsers)) {
                            $bannedDevUsers[] = $this->resetBannedDeviceUser($userInfo->user_id);
                        }
                    }

                    $resetStatus = [];
                    $resetStatusInspect = [];

                    foreach ($userPhotos as $userPhoto) {
                        if ($userPhoto->status != 0) {
                            $resetStatus[] = [
                                'photo_id'   => $userPhoto->photo_id,
                                'user_id'    => $userPhoto->user_id,
                                'admin_id'   => $this->admin->id,
                                'status'     => 0,
                                'main_photo' => $userPhoto->photo_id,
                            ];
                        }
                        $resetStatusInspect[] = $userPhoto->photo_id;
                    }

                    if ($resetStatusInspect) {
                        // сброс статуса контроля для всех фото пользователя
                        UsersPhotoStatusInspect::whereIn('photo_id', $resetStatusInspect)->delete();
                    }

                    if ($resetStatus) {
                        // сброс статуса фото
                        UsersPhotoStatus::insert($resetStatus);
                    }

                    // если это бан и есть основное фото, то добавляем статус контроля
                    if ($userInfo->photo_id && $status == 1) {
                        UsersPhotoStatusInspect::create([
                            'photo_id'     => $userInfo->photo_id,
                            'admin_id'     => $this->admin->id,
                            'admin_status' => 5,
                            'level'        => $this->admin->level,
                        ]);
                    }

                    if ($status == 0) {
                        Banlist::whereUserId($userInfo->user_id)->delete();
                    } else {
                        Banlist::updateOrCreate(
                            ['user_id' => $userInfo->user_id],
                            ['admin_id' => $this->admin->id, 'reason_code' => 2]
                        );
                    }

                    BanlistLog::create([
                        'admin_id'   => $this->admin->id,
                        'user_id'    => $userInfo->user_id,
                        'status'     => $status,
                        'photo_id'   => $userInfo->photo_id,
                        'main_photo' => $userInfo->photo_id ? 1 : null,
                    ]);
                    (new PhotosService)->removeAlertBlocked($userInfo->user_id);

                    $return[] = [
                        'user_id' => $userInfo->user_id,
                    ];
                }

                if (count($return) > 0) {
                    DB::commit();
                }
            }

            return $return;
        } catch (Exception $e) {
            Log::error($e);
            DB::rollBack();

            throw new Exception($e->getMessage());
        }
    }

    /**
     * Добавление бана аккаунта пользователей (последняя версия)
     * @param int|array $usersId
     * @return array
     * @throws Exception
     */
    public function addBannedAcc($usersId)
    {
        try {
            if (!is_array($usersId)) {
                $usersId = [$usersId];
            }

            if (!$usersId = $this->prepareWhereInInt($usersId)) {
                throw new Exception('Invalid users id');
            }

            $redisDel = [];
            $bannedUsers = [];
            $bannedDevice = [];
            $bannedDevicePhoto = [];

            $users = UsersInfo::with([
                'stats'      => function ($query) {
                    /* @var \Illuminate\Database\Eloquent\Builder $query */
                    $query->select('user_id', 'banned_acc', 'banned_dev', 'blocked_photo');
                },
                'mainPhoto'  => function ($query) {
                    /* @var \Illuminate\Database\Eloquent\Builder $query */
                    $query->select('user_id', 'photo_id', 'main_photo', 'status');
                },
                'appVersion' => function ($query) {
                    /* @var \Illuminate\Database\Eloquent\Builder $query */
                    $query->select('user_id', 'device_id');
                }])
                ->whereIn('user_id', $usersId)
                ->get(['user_id', 'photo_id']);

            if ($users->count() == 0) {
                return $bannedUsers;
            }

            DB::beginTransaction();

            foreach ($users as $user) {
                // аккаунт уже забанен
                if ($user->stats->banned_acc == 1) {
                    continue;
                }

                // если есть бан по устройству
                if ($user->stats->banned_dev == 1) {
                    /* @var \Illuminate\Database\Eloquent\Collection|UsersAppVersion[] $appVersion */
                    $appVersion = $user->appVersion;

                    if ($appVersion->count() > 0) {
                        // информация о пользователях устройств
                        $bannedDeviceUsers = UsersAppVersion::with([
                            'mainPhoto' => function ($query) {
                                /* @var \Illuminate\Database\Eloquent\Builder $query */
                                $query->select('user_id', 'photo_id', 'main_photo', 'status');
                            }])
                            ->whereIn('device_id', $appVersion->pluck('device_id'))
                            ->get(['user_id', 'device_id']);

                        foreach ($bannedDeviceUsers as $v) {
                            $this->resetAllPhotoStatus($v->user_id);

                            // добавление записи в логи, что бан устройства был удален
                            if ($v->mainPhoto) {
                                if (!in_array($v->mainPhoto->photo_id, $bannedDevicePhoto)) {
                                    BanlistLog::create([
                                        'admin_id'   => $this->admin->id,
                                        'device_id'  => $v->device_id,
                                        'status'     => 0,
                                        'photo_id'   => $v->photo_id,
                                        'main_photo' => 1,
                                    ]);
                                    $bannedDevicePhoto[] = $v->mainPhoto->photo_id;
                                }
                            } else {
                                if (!in_array($v->device_id, $bannedDevice)) {
                                    BanlistLog::create([
                                        'admin_id'  => $this->admin->id,
                                        'device_id' => $v->device_id,
                                        'status'    => 0,
                                    ]);
                                    $bannedDevice[] = $v->device_id;
                                }
                            }

                            $redisDel[] = env('REDIS_PREFIX_BAN_DEVICE') . $v->device_id;
                            $redisDel[] = env('REDIS_PREFIX_BAN_USER_DEVICE') . $v->user_id;
                        }

                        // удаляем бан устройств
                        Banlist::whereIn('device_id', $appVersion->pluck('device_id'))->delete();
                    }
                }

                if ($user->mainPhoto) {
                    // бан с основным фото
                    BanlistLog::create([
                        'admin_id'   => $this->admin->id,
                        'user_id'    => $user->user_id,
                        'status'     => 1,
                        'photo_id'   => $user->mainPhoto->photo_id,
                        'main_photo' => 1,
                    ]);
                } else {
                    // удаляем статусы со всех фото
                    $this->resetAllPhotoStatus($user->user_id);

                    // бан без основного фото
                    BanlistLog::create([
                        'admin_id' => $this->admin->id,
                        'user_id'  => $user->user_id,
                        'status'   => 1,
                    ]);
                }

                Banlist::create([
                    'user_id'     => $user->user_id,
                    'admin_id'    => $this->admin->id,
                    'reason_code' => 2,
                ]);

                $redisDel[] = env('REDIS_PREFIX_BAN_USER') . $user->user_id;
                $bannedUsers[] = $user->user_id;
            }

            // после сброса всех статусов
            foreach ($users as $user) {
                if ($user->mainPhoto) {
                    UsersPhotoStatusInspect::create([
                        'photo_id'     => $user->mainPhoto->photo_id,
                        'admin_id'     => $this->admin->id,
                        'admin_status' => 5,
                        'level'        => $this->admin->level,
                    ]);
                }

                // удаление уведомлений о временной блокировки если были
                (new PhotosService())->removeAlertBlocked($user->user_id);
            }

            if ($redisDel) {
                // сброс кеша бана
                (Redis::connection())->del($redisDel);
            }
            DB::commit();

            return $bannedUsers;
        } catch (Exception $e) {
            Log::error($e);
            DB::rollBack();

            throw new Exception($e->getMessage());
        }
    }

    /**
     * Удаление бана аккаунта пользователей (последняя версия)
     * @param int|array $usersId
     * @return array
     * @throws Exception
     */
    public function delBannedAcc($usersId)
    {
        try {
            if (!is_array($usersId)) {
                $usersId = [$usersId];
            }

            if (!$usersId = $this->prepareWhereInInt($usersId)) {
                throw new Exception('Invalid users id');
            }

            $redisDel = [];
            $bannedUsers = [];

            $users = UsersInfo::with([
                'stats'      => function ($query) {
                    /* @var \Illuminate\Database\Eloquent\Builder $query */
                    $query->select('user_id', 'banned_acc', 'banned_dev', 'blocked_photo');
                },
                'mainPhoto'  => function ($query) {
                    /* @var \Illuminate\Database\Eloquent\Builder $query */
                    $query->select('user_id', 'photo_id', 'main_photo', 'status');
                },
                'appVersion' => function ($query) {
                    /* @var \Illuminate\Database\Eloquent\Builder $query */
                    $query->select('user_id', 'device_id');
                }])
                ->whereIn('user_id', $usersId)
                ->get(['user_id', 'photo_id']);

            if ($users->count() == 0) {
                return $bannedUsers;
            }

            DB::beginTransaction();

            foreach ($users as $user) {
                // аккаунт не забанен
                if ($user->stats->banned_acc == 0) {
                    continue;
                }

                if ($user->mainPhoto) {
                    // бан с основным фото
                    BanlistLog::create([
                        'admin_id'   => $this->admin->id,
                        'user_id'    => $user->user_id,
                        'status'     => 0,
                        'photo_id'   => $user->mainPhoto->photo_id,
                        'main_photo' => 1,
                    ]);
                } else {
                    // удаляем статусы со всех фото
                    $this->resetAllPhotoStatus($user->user_id);

                    // бан без основного фото
                    BanlistLog::create([
                        'admin_id' => $this->admin->id,
                        'user_id'  => $user->user_id,
                        'status'   => 0,
                    ]);
                }

                Banlist::whereUserId($user->user_id)->delete();

                $redisDel[] = env('REDIS_PREFIX_BAN_USER') . $user->user_id;
                $bannedUsers[] = $user->user_id;
            }

            // после сброса всех статусов
            foreach ($users as $user) {
                // удаление уведомлений о временной блокировке если были
                (new PhotosService())->removeAlertBlocked($user->user_id);
            }

            if ($redisDel) {
                // сброс кеша бана
                (Redis::connection())->del($redisDel);
            }
            DB::commit();

            return $bannedUsers;
        } catch (Exception $e) {
            Log::error($e);
            DB::rollBack();

            throw new Exception($e->getMessage());
        }
    }

    /**
     * Добавление бана устройств пользователей
     * @param int|array $usersId
     * @return array
     * @throws Exception
     */
    public function addBannedDev($usersId)
    {
        try {
            if (!is_array($usersId)) {
                $usersId = [$usersId];
            }

            if (!$usersId = $this->prepareWhereInInt($usersId)) {
                throw new Exception('Invalid users id');
            }

            $bannedUsers = [];

            // информация о пользователях
            $users = UsersInfo::with([
                'stats'      => function ($query) {
                    /* @var \Illuminate\Database\Eloquent\Builder $query */
                    $query->select('user_id', 'banned_acc', 'banned_dev', 'blocked_photo');
                },
                'appVersion' => function ($query) {
                    /* @var \Illuminate\Database\Eloquent\Builder $query */
                    $query->select('user_id', 'device_id');
                }])
                ->whereIn('user_id', $usersId)
                ->get(['user_id', 'photo_id']);

            // список всех устройств пользователей
            $devices = [];
            foreach ($users as $user) {
                /* @var UsersStat $userStats */
                $userStats = $user->stats;
                /* @var \Illuminate\Database\Eloquent\Collection|UsersAppVersion[] $userDevices */
                $userDevices = $user->appVersion;

                if ($userStats->banned_dev == 1) {
                    continue;
                }

                if ($userDevices->count() == 0) {
                    continue;
                }

                foreach ($userDevices as $device) {
                    $devices[] = $device->device_id;
                }
            }

            if (!$devices) {
                return [];
            }

            // список пользователей связанных с устройствами
            $deviceUsers = UsersAppVersion::whereIn('device_id', array_unique($devices))
                ->get(['user_id'])
                ->pluck('user_id')
                ->unique();

            if ($deviceUsers->count() == 0) {
                return [];
            }

            // информация о пользователях связанных с устройствами
            $users = UsersInfo::with([
                'stats'      => function ($query) {
                    /* @var \Illuminate\Database\Eloquent\Builder $query */
                    $query->select('user_id', 'banned_acc', 'banned_dev', 'blocked_photo');
                },
                'mainPhoto'  => function ($query) {
                    /* @var \Illuminate\Database\Eloquent\Builder $query */
                    $query->select('user_id', 'photo_id', 'main_photo', 'status');
                },
                'appVersion' => function ($query) {
                    /* @var \Illuminate\Database\Eloquent\Builder $query */
                    $query->select('user_id', 'device_id');
                }])
                ->whereIn('user_id', $deviceUsers)
                ->get(['user_id', 'photo_id']);

            DB::beginTransaction();

            $photosService = new PhotosService();

            $deleteRedis = [];
            $deleteBanlistAcc = [];
            $insertBanlistLog = [];
            $insertStatusInspect = [];
            $insertBanlistLogNoPhoto = [];
            foreach ($users as $user) {
                /* @var UsersStat $userStats */
                $userStats = $user->stats;
                /* @var UsersPhoto $userMainPhoto */
                $userMainPhoto = $user->mainPhoto;
                /* @var \Illuminate\Database\Eloquent\Collection|UsersAppVersion[] $userDevices */
                $userDevices = $user->appVersion;

                if ($userStats->banned_dev == 1 || $userDevices->count() == 0) {
                    continue;
                }

                if ($userStats->banned_acc == 1) {
                    if ($userMainPhoto) {
                        $insertBanlistLog[] = [
                            'admin_id'   => $this->admin->id,
                            'user_id'    => $user->user_id,
                            'status'     => 0,
                            'photo_id'   => $userMainPhoto->photo_id,
                            'main_photo' => 1,
                        ];
                    } else {
                        $insertBanlistLog[] = [
                            'admin_id' => $this->admin->id,
                            'user_id'  => $user->user_id,
                            'status'   => 0,
                        ];
                    }

                    $deleteBanlistAcc[] = $user->user_id;
                    $deleteRedis[] = env('REDIS_PREFIX_BAN_USER') . $user->user_id;
                }

                foreach ($userDevices as $device) {
                    if (in_array($device->device_id, $devices)) {
                        if ($userMainPhoto) {
                            $insertBanlistLog[] = [
                                'admin_id'   => $this->admin->id,
                                'device_id'  => $device->device_id,
                                'status'     => 1,
                                'photo_id'   => $userMainPhoto->photo_id,
                                'main_photo' => 1,
                            ];
                        } else {
                            $insertBanlistLogNoPhoto[] = $device->device_id;
                        }
                        $deleteRedis[] = env('REDIS_PREFIX_BAN_DEVICE') . $device->device_id;
                    }
                    $deleteRedis[] = env('REDIS_PREFIX_BAN_USER_DEVICE') . $device->user_id;
                }

                if ($userMainPhoto) {
                    $insertStatusInspect[] = [
                        'photo_id'     => $userMainPhoto->photo_id,
                        'admin_id'     => $this->admin->id,
                        'admin_status' => 6,
                        'level'        => $this->admin->level,
                        'associated'   => in_array($user->user_id, $usersId) ? 0 : 1,
                    ];
                }

                // удаление уведомлений о временной блокировке
                if ($userStats->blocked_photo) {
                    $photosService->removeAlertBlocked($user->user_id);
                }

                $bannedUsers[] = $user->user_id;
            }

            if (!$bannedUsers) {
                DB::rollBack();

                return [];
            }

            // сброс всех статусов с фотографий пользователя
            $this->resetAllPhotoStatus($bannedUsers);

            // статус для фото о бане устройства
            if ($insertStatusInspect) {
                UsersPhotoStatusInspect::insert($insertStatusInspect);
            }

            // лог бана устройства
            if ($insertBanlistLog) {
                BanlistLog::insert($insertBanlistLog);
            }

            // лог бана устройств без основного фото
            if ($insertBanlistLogNoPhoto) {
                $insertBanlistLogNoPhotoData = [];

                foreach (array_unique($insertBanlistLogNoPhoto) as $v) {
                    $insertBanlistLogNoPhotoData = [
                        'admin_id'  => $this->admin->id,
                        'device_id' => $v,
                        'status'    => 0,
                    ];
                }
                BanlistLog::insert($insertBanlistLogNoPhotoData);
            }

            // удаление бана аккаунта
            if ($deleteBanlistAcc) {
                Banlist::whereIn('user_id', $deleteBanlistAcc)->delete();
            }

            // бан устройств
            $insertBanlist = [];
            foreach ($devices as $device) {
                $insertBanlist[] = [
                    'device_id'   => $device,
                    'admin_id'    => $this->admin->id,
                    'reason_code' => 1,
                ];
            }
            Banlist::insert($insertBanlist);

            foreach ($bannedUsers as $bannedUser) {
                // обновление записи, что бан пользователя был через связанное устройство или напрямую
                UsersStat::whereUserId($bannedUser)
                    ->whereBannedDev(1)
                    ->update([
                        'banned_dev_associated' => (in_array($bannedUser, $usersId) ? 0 : 1),
                    ]);
            }

            DB::commit();

            if ($deleteRedis) {
                (Redis::connection())->del(array_unique($deleteRedis));
            }

            return $bannedUsers;
        } catch (Exception $e) {
            Log::error($e);
            DB::rollBack();

            throw new Exception($e->getMessage());
        }
    }

    /**
     * Удаление бана устройств пользователей
     * @param int|array $usersId
     * @return array
     * @throws Exception
     */
    public function delBannedDev($usersId)
    {
        try {
            if (!is_array($usersId)) {
                $usersId = [$usersId];
            }

            if (!$usersId = $this->prepareWhereInInt($usersId)) {
                throw new Exception('Invalid users id');
            }

            $bannedUsers = [];

            // информация о пользователях
            $users = UsersInfo::with([
                'stats'      => function ($query) {
                    /* @var \Illuminate\Database\Eloquent\Builder $query */
                    $query->select('user_id', 'banned_acc', 'banned_dev', 'blocked_photo');
                },
                'mainPhoto'  => function ($query) {
                    /* @var \Illuminate\Database\Eloquent\Builder $query */
                    $query->select('user_id', 'photo_id', 'main_photo', 'status');
                },
                'appVersion' => function ($query) {
                    /* @var \Illuminate\Database\Eloquent\Builder $query */
                    $query->select('user_id', 'device_id');
                }])
                ->whereIn('user_id', $usersId)
                ->get(['user_id', 'photo_id']);

            // список всех устройств пользователей
            $devices = [];
            foreach ($users as $user) {
                /* @var UsersStat $userStats */
                $userStats = $user->stats;
                /* @var \Illuminate\Database\Eloquent\Collection|UsersAppVersion[] $userDevices */
                $userDevices = $user->appVersion;

                if ($userStats->banned_dev == 0) {
                    continue;
                }

                if ($userDevices->count() == 0) {
                    continue;
                }

                foreach ($userDevices as $device) {
                    $devices[] = $device->device_id;
                }
            }

            if (!$devices) {
                return [];
            }

            // список пользователей связанных с устройствами
            $deviceUsers = UsersAppVersion::whereIn('device_id', array_unique($devices))
                ->get(['user_id'])
                ->pluck('user_id')
                ->unique();

            if ($deviceUsers->count() == 0) {
                return [];
            }

            // информация о пользователях связанных с устройствами
            $users = UsersInfo::with([
                'stats'      => function ($query) {
                    /* @var \Illuminate\Database\Eloquent\Builder $query */
                    $query->select('user_id', 'banned_acc', 'banned_dev', 'blocked_photo');
                },
                'mainPhoto'  => function ($query) {
                    /* @var \Illuminate\Database\Eloquent\Builder $query */
                    $query->select('user_id', 'photo_id', 'main_photo', 'status');
                },
                'appVersion' => function ($query) {
                    /* @var \Illuminate\Database\Eloquent\Builder $query */
                    $query->select('user_id', 'device_id');
                }])
                ->whereIn('user_id', $deviceUsers)
                ->get(['user_id', 'photo_id']);

            DB::beginTransaction();

            $photosService = new PhotosService();

            $deleteRedis = [];
            $deleteBanlistAcc = [];
            $insertBanlistLog = [];
            $insertBanlistLogNoPhoto = [];
            foreach ($users as $user) {
                /* @var UsersStat $userStats */
                $userStats = $user->stats;
                /* @var UsersPhoto $userMainPhoto */
                $userMainPhoto = $user->mainPhoto;
                /* @var \Illuminate\Database\Eloquent\Collection|UsersAppVersion[] $userDevices */
                $userDevices = $user->appVersion;

                if ($userStats->banned_dev == 0 || $userDevices->count() == 0) {
                    continue;
                }

                if ($userStats->banned_acc == 1) {
                    if ($userMainPhoto) {
                        $insertBanlistLog[] = [
                            'admin_id'   => $this->admin->id,
                            'user_id'    => $user->user_id,
                            'status'     => 0,
                            'photo_id'   => $userMainPhoto->photo_id,
                            'main_photo' => 1,
                        ];
                    } else {
                        $insertBanlistLog[] = [
                            'admin_id' => $this->admin->id,
                            'user_id'  => $user->user_id,
                            'status'   => 0,
                        ];
                    }

                    $deleteBanlistAcc[] = $user->user_id;
                    $deleteRedis[] = env('REDIS_PREFIX_BAN_USER') . $user->user_id;
                }

                foreach ($userDevices as $device) {
                    if (in_array($device->device_id, $devices)) {
                        if ($userMainPhoto) {
                            $insertBanlistLog[] = [
                                'admin_id'   => $this->admin->id,
                                'device_id'  => $device->device_id,
                                'status'     => 0,
                                'photo_id'   => $userMainPhoto->photo_id,
                                'main_photo' => 1,
                            ];
                        } else {
                            $insertBanlistLogNoPhoto[] = $device->device_id;
                        }
                        $deleteRedis[] = env('REDIS_PREFIX_BAN_DEVICE') . $device->device_id;
                    }
                    $deleteRedis[] = env('REDIS_PREFIX_BAN_USER_DEVICE') . $device->user_id;
                }

                // удаление уведомлений о временной блокировке
                if ($userStats->blocked_photo) {
                    $photosService->removeAlertBlocked($user->user_id);
                }

                $bannedUsers[] = $user->user_id;
            }

            if (!$bannedUsers) {
                DB::rollBack();

                return [];
            }

            // сброс всех статусов с фотографий пользователя
            $this->resetAllPhotoStatus($bannedUsers);

            // лог бана устройства
            if ($insertBanlistLog) {
                BanlistLog::insert($insertBanlistLog);
            }

            // лог бана устройств без основного фото
            if ($insertBanlistLogNoPhoto) {
                $insertBanlistLogNoPhotoData = [];

                foreach (array_unique($insertBanlistLogNoPhoto) as $v) {
                    $insertBanlistLogNoPhotoData = [
                        'admin_id'  => $this->admin->id,
                        'device_id' => $v,
                        'status'    => 0,
                    ];
                }
                BanlistLog::insert($insertBanlistLogNoPhotoData);
            }

            // удаление бана аккаунта
            if ($deleteBanlistAcc) {
                Banlist::whereIn('user_id', $deleteBanlistAcc)->delete();
            }

            // удаление бана устройств
            Banlist::whereIn('device_id', array_unique($devices))->delete();

            DB::commit();

            if ($deleteRedis) {
                (Redis::connection())->del(array_unique($deleteRedis));
            }

            return $bannedUsers;
        } catch (Exception $e) {
            Log::error($e);
            DB::rollBack();

            throw new Exception($e->getMessage());
        }
    }

    /**
     * @param int $userId
     * @return array
     * @throws Exception
     */
    protected function resetBannedDeviceUser($userId)
    {
        $return = [];

        $userDevices = UsersAppVersion::whereUserId($userId)
            ->get(['device_id']);

        if ($userDevices) {
            $devices = UsersAppVersion::whereIn('device_id', $userDevices)
                ->get(['user_id', 'device_id']);
            $usersId = $devices->pluck('user_id')->unique();

            $users = UsersInfo::with([
                'photos' => function ($query) {
                    /* @var \Illuminate\Database\Eloquent\Builder $query */
                    $query->select('user_id', 'photo_id', 'main_photo', 'status');
                }])
                ->whereIn('user_id', $usersId)
                ->get(['user_id', 'photo_id']);

            foreach ($users as $user) {
                $return[] = $user->user_id;

                /* @var \Illuminate\Database\Eloquent\Collection|UsersPhoto[] $photos */
                $photos = $user->photos;
                if ($photos->count() > 0) {
                    foreach ($photos as $photo) {
                        if ($photo->status != 0) {
                            UsersPhotoStatus::create([
                                'photo_id'   => $photo->photo_id,
                                'user_id'    => $photo->user_id,
                                'admin_id'   => $this->admin->id,
                                'status'     => 0,
                                'main_photo' => $photo->main_photo,
                            ]);
                        }
                        UsersPhotoStatusInspect::wherePhotoId($photo->photo_id)->delete();
                    }
                }
            }

            Banlist::whereIn('device_id', $devices->pluck('device_id')->unique())->delete();
        }

        return $return;
    }

    /**
     * Сброс статусов всех фотографий пользователя
     * @param int|array $userId Id пользователя или массив id пользователей
     * @return bool
     */
    public function resetAllPhotoStatus($userId)
    {
        if (!is_array($userId)) {
            $userId = [$userId];
        }

        $usersInfo = UsersInfo::with([
            'photos' => function ($query) {
                /* @var \Illuminate\Database\Eloquent\Builder $query */
                $query->select('user_id', 'photo_id', 'main_photo', 'status');
            }])
            ->whereIn('user_id', $userId)
            ->get(['user_id', 'photo_id']);

        if ($usersInfo->count() == 0) {
            return false;
        }

        $photoStatus = [];
        $photoStatusInspect = [];
        foreach ($usersInfo as $user) {
            /* @var \Illuminate\Database\Eloquent\Collection|UsersPhoto[] $userPhotos */
            $userPhotos = $user->photos;

            if ($userPhotos->count() == 0) {
                continue;
            }

            foreach ($userPhotos as $photo) {
                if ($photo->status != 0) {
                    $photoStatus[] = [
                        'photo_id'   => $photo->photo_id,
                        'user_id'    => $photo->user_id,
                        'admin_id'   => $this->admin->id,
                        'status'     => 0,
                        'main_photo' => $photo->main_photo,
                    ];
                }
                $photoStatusInspect[] = $photo->photo_id;
            }
        }

        if ($photoStatus) {
            UsersPhotoStatus::insert($photoStatus);
        }

        if ($photoStatusInspect) {
            UsersPhotoStatusInspect::whereIn('photo_id', $photoStatusInspect)->delete();
        }

        return true;
    }

    private function messageStaticURL($message)
    {
        foreach ($message as $k => $v) {
            if (empty($v)) {
                continue;
            }

            switch ($k) {
                case 'imageURL':
                case 'audioURL':
                case 'musicURL':
                case 'videoURL':
                case 'thumbVideoURL':
                    $path = env('STATIC_ATTACHMENT_URL');
                    break;
                case 'thumbImageURL':
                    $path = env('STATIC_ATTACHMENT_PREW_URL');
                    break;
                default:
                    $path = '';
                    break;
            }

            if ($path) {
                $message[$k] = $path . $v;
            }
        }

        return $message;
    }
}
