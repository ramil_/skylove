<?php

namespace App\Http\Controllers\Reports;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Users\UsersController;
use App\Http\Traits\GeoTimezone;
use App\Http\Traits\HelperDB;
use DateTime;
use DateTimeZone;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;

class ReportsUsersController extends Controller
{
    use GeoTimezone, HelperDB;

    protected $limit = 200;

    protected $timezone = 'Asia/Krasnoyarsk';

    protected $admin;

    protected $protected_users = [322];

    protected $reasons_list = [
        '-1' => 'Неизвестно',
        '0'  => 'Порнографическое фото',
        '1'  => 'Оскорбление или домогательства',
        '2'  => 'Пол не соответствует фотографиям',
        '3'  => 'Украденное фото',
        '4'  => 'Реклама или попытка обмана',
        '5'  => 'Другое',
    ];

    /**
     * ReportsUsersController constructor.
     * @throws Exception
     */
    public function __construct()
    {
        if (!$this->setGeoTimezone()) {
            throw new Exception('Cannot get timezone', 500);
        }

        if (!$this->admin = Auth::user()) {
            throw new Exception('Cannot get auth user information', 500);
        }
    }

    /**
     * Список пользователей
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function getData(Request $request)
    {
        $page = $request->input('page', 1) ?: 1;
        if ($page != intval($page) || $page < 1) {
            $page = 1;
        };

        $limit = $request->input('per_page', 0) ?: $this->limit;
        if ($limit != intval($limit) || $limit < 1) {
            $limit = $this->limit;
        };

        $offset = ($page - 1) * $limit;
        $sort = $request->input('sort', 'id|desc');
        $filter = $request->input('filter', []);
        $filter = @json_decode($filter, true);

        $reports = DB::table('users_info as i')
            ->join('users as u', 'u.user_id', '=', 'i.user_id')
            ->join('blocked_users as b', 'b.blocked_user', '=', 'i.user_id')
            ->join('users_stats as s', 's.user_id', '=', 'i.user_id')
            ->leftJoin('users_photos as p', 'i.photo_id', '=', 'p.photo_id')
            ->leftJoinSub(
                DB::table('users_reports_verification')
                    ->select('user_id', 'admin_id', 'date')
                    ->where('status', '=', 1)
                    ->orderBy('date', 'desc')
                    ->groupBy('user_id'),
                'udv', 'udv.user_id', '=', 'i.user_id')
            ->leftJoin('admins as a', 'a.id', '=', 'udv.admin_id')
            ->leftJoinSub(
                DB::table('dialog_users')
                    ->select('user_id')
                    ->selectRaw('count(dialog_id) as dialogs')
                    ->where('messages_total', '>', 0)
                    ->groupBy('user_id'),
                'du', 'du.user_id', '=', 'i.user_id')
            ->where('u.state', '=', '1')
            ->whereNotIn('u.user_id', $this->protected_users)
            ->groupBy('i.user_id')
            ->select('u.email', 'i.user_id', 'i.name', 'i.sex', 'i.years', 'i.city', 'i.reg_date',
                'i.last_visit', 'i.avatar', 'i.vip', 'p.photo', 's.vip_expires', 's.vip_order_method',
                's.messages', 'du.dialogs', 's.online as online_total', 's.banned_acc', 's.banned_dev',
                's.banned_date', 's.banned_admin', 's.blocked_photo', 's.total_amount_points', 's.total_amount_vip',
                's.app_version', 'udv.date as verification_date', 'a.name as verification_name')
            ->selectRaw('count(b.blocked_id) as reports')
            ->selectRaw('group_concat(b.reason order by b.reason asc separator ",") as reason')
            ->selectRaw('min(b.verification) as report_verification')
            ->selectRaw('(s.total_amount_points + s.total_amount_vip) as total_amount_sum');

        $filter = [
            'user_id'           => !empty($filter['user_id']) ? intval($filter['user_id']) : '',
            'name'              => !empty($filter['name']) ? $filter['name'] : '',
            'city'              => !empty($filter['city']) ? $filter['city'] : '',
            'gender'            => !empty($filter['gender']) ? $filter['gender'] : '',
            'years_min'         => !empty($filter['years_min']) ? intval($filter['years_min']) : '',
            'years_max'         => !empty($filter['years_max']) ? intval($filter['years_max']) : '',
            'vip'               => !empty($filter['vip']) ? $filter['vip'] : '',
            'online'            => !empty($filter['online']) ? $filter['online'] : '',
            'reason'            => isset($filter['reason']) ? $filter['reason'] : '',
            'show_banned'       => !empty($filter['show_banned']) ? $filter['show_banned'] : '',
            'show_fake'         => !empty($filter['show_fake']) ? $filter['show_fake'] : '',
            'show_verification' => !empty($filter['show_verification']) ? $filter['show_verification'] : 'na',
        ];

        if ($filter['user_id']) {
            $reports->where('i.user_id', '=', $filter['user_id']);
        }

        if ($filter['name']) {
            $reports->where('i.name', 'like', '%' . $filter['name'] . '%');
        }

        if ($filter['city']) {
            $reports->where('i.city', 'like', '%' . $filter['city'] . '%');
        }

        if (in_array($filter['gender'], ['1', '2'])) {
            $reports->where('i.sex', '=', $filter['gender']);
        }

        if ($filter['years_min']) {
            $reports->where('i.years', '>=', $filter['years_min']);
        }
        if ($filter['years_max']) {
            $reports->where('i.years', '<=', $filter['years_max']);
        }

        if (in_array($filter['vip'], ['yes', 'no'])) {
            if ($filter['vip'] == 'yes') {
                $reports->where('i.vip', '=', '1');
            } elseif ($filter['vip'] == 'no') {
                $reports->where('i.vip', '=', '0');
            }
        }

        if (in_array($filter['online'], ['online', 'visited', 'offline'])) {
            if ($filter['online'] == 'online') {
                $reports->where('i.last_visit', '>', time() - 600);
            } elseif ($filter['online'] == 'visited') {
                $reports->where(function ($query) {
                    /* @var $query \Illuminate\Database\Query\Builder */
                    $query->where('i.last_visit', '<', time() - 600)
                        ->where('i.last_visit', '>', time() - 21600);
                });
            } else {
                $reports->where('i.last_visit', '<', time() - 21600);
            }
        }

        if (in_array($filter['reason'], ['0', '1', '2', '3', '4', '5'])) {
            $reports->having('reason', 'like', '%' . $filter['reason'] . '%');
        }

        if (!empty($filter['show_banned']) && $filter['show_banned'] != 'na') {
            if ($filter['show_banned'] == 'banned') {
                $reports->where(function ($query) {
                    /* @var $query \Illuminate\Database\Query\Builder */
                    $query->where('s.banned_acc', '=', 1)
                        ->orWhere('s.banned_dev', '=', 1);
                });
            }
        } else {
            $reports->where('s.banned_acc', '=', 0)
                ->where('s.banned_dev', '=', 0);
        }

        if (!empty($filter['show_fake']) && $filter['show_fake'] != 'na') {
            if ($filter['show_fake'] == 'fake') {
                $reports->where('u.email', 'like', '%mail.local');
            }
        } else {
            $reports->where(function ($query) {
                /* @var $query \Illuminate\Database\Query\Builder */
                $query->where('u.email', 'not like', '%mail.local')
                    ->orWhereNull('u.email');
            });
        }

        if (!empty($filter['show_verification']) && in_array($filter['show_verification'], ['na', 'verification'])) {
            if ($filter['show_verification'] == 'verification') {
                $reports->having('report_verification', '=', 1);
            } else {
                $reports->having('report_verification', '=', 0);
            }
        }

        if ($sort) {
            $tmp = explode('|', trim($sort));

            if (is_array($tmp)) {
                if (empty($tmp[1]) || !in_array($tmp[1], ['asc', 'desc'])) {
                    $tmp[1] = 'asc';
                }

                if ($order_fields = explode(',', $tmp[0])) {
                    foreach ($order_fields as $v) {
                        $reports->orderBy(trim($v), $tmp[1]);
                    }
                } else {
                    $reports->orderBy($tmp[0], $tmp[1]);
                }
            }
        }

        $return = [];
        $count = $reports->get()->count();

        $reports_alerts = DB::table('users_alerts')
            ->select(DB::raw('max(alert_id) as alert_id'), 'user_id', 'data', 'read_at')
            ->groupBy('user_id');
        $reports->leftJoinSub(
            DB::table(DB::raw("({$reports_alerts->toSql()}) as a1"))
                ->join('users_alerts as a2', 'a1.alert_id', '=', 'a2.alert_id')
                ->where('type', '=', 3)
                ->select('a2.user_id', 'a2.data', 'a2.read_at'),
            'ra', 'ra.user_id', '=', 'i.user_id')
            ->selectRaw('ra.data as reasons_alert, ra.read_at as reasons_alert_read');

        $reports->leftJoinSub(
            DB::table('users_alerts')
                ->where('type', '=', 3)
                ->groupBy('user_id')
                ->select('user_id', DB::raw('count(*) as count')),
            'uac', 'uac.user_id', '=', 'i.user_id')
            ->selectRaw('uac.count as reasons_alert_count');

        $reports->leftJoinSub(
            DB::table('users_alerts')
                ->where('type', '=', 4)
                ->groupBy('user_id')
                ->select('user_id', DB::raw('count(*) as count'), DB::raw('sum(if(read_at is null, 0, 1)) as `read`')),
            'uac2', 'uac2.user_id', '=', 'i.user_id')
            ->selectRaw('uac2.count as custom_alert_count, cast(uac2.read as unsigned) as custom_alert_read');

        $reports_data = $reports->offset($offset)->limit($limit)->get();

        $keys = [];
        if ($reports_data) {
            $keys = $reports_data->keyBy('user_id')->keys();
        }

        if ($keys) {
            // фото полученных пользователей
            $photos = DB::table('users_photos')
                ->select('user_id', 'photo')
                ->whereIn('user_id', $keys)
                ->get();

            foreach ($photos as $v) {
                $photos_data[$v->user_id][] = env('STATIC_PHOTO_URL') . $v->photo;
            }

            unset($photos);
        }

        $now = new DateTime();

        foreach ($reports_data as $v) {
            $vip_expires_days = null;
            if ($v->vip_expires) {
                $vip_expires = new DateTime($v->vip_expires);
                $vip_expires_days .= $now->diff($vip_expires)->format('%r%a (%H:%I)');
            }

            $rgd = new DateTime($v->reg_date);
            $rgd->setTimezone(new DateTimeZone($this->timezone));

            $lvd = new DateTime();
            $lvd->setTimestamp($v->last_visit);
            $lvd->setTimezone(new DateTimeZone($this->timezone));

            $photos = [];
            if (!empty($photos_data[$v->user_id])) {
                $photos = array_values(array_diff($photos_data[$v->user_id], [env('STATIC_PHOTO_URL') . $v->photo]));
            }

            $reasons = [];
            $reasons_user = array_count_values(explode(',', $v->reason));
            foreach ($reasons_user as $reason_k => $reason_v) {
                $reason_key = '-1';
                if (array_key_exists($reason_k, $this->reasons_list)) {
                    $reason_key = $reason_k;
                }
                $reasons[] = [
                    'key'   => $reason_key,
                    'text'  => $reason_text = $this->reasons_list[$reason_key],
                    'count' => $reason_v
                ];
            }

            $reasons_alerts = [];
            $tmp = @json_decode($v->reasons_alert, true);
            if (!empty($tmp['reasons'])) {
                foreach ($tmp['reasons'] as $tmp_v) {
                    $reasons_alerts[$tmp_v['reason']] = $tmp_v['count'];
                }
            }

            if ($v->banned_date) {
                $banned_date = new DateTime($v->banned_date);
                $banned_date->setTimezone(new DateTimeZone($this->timezone));
                $banned_date = $banned_date->format('Y-m-d H:i:s');
            } else {
                $banned_date = '';
            }

            if ($v->app_version) {
                $app_version = explode(':', $v->app_version);
                if (!empty($app_version)) {
                    $app_version = array_unique($app_version);
                }
            } else {
                $app_version = [];
            }

            $online_total = $v->online_total ?: 0;

            $return[] = [
                'user_id'             => $v->user_id,
                'avatar'              => (!empty($v->avatar) ? (env('STATIC_AVA_URL') . $v->avatar) : ''),
                'photo'               => (!empty($v->photo) ? (env('STATIC_PHOTO_URL') . $v->photo) : ''),
                'name'                => $v->name,
                'gender'              => $v->sex,
                'years'               => $v->years,
                'city'                => $v->city,
                'vip'                 => [
                    'status'  => $v->vip,
                    'expires' => $vip_expires_days,
                    'payment' => in_array($v->vip_order_method, ['googlepay', 'itunes']) ? 1 : 0,
                ],
                'online_total'        => sprintf("%d:%02d", floor($online_total / 60), $online_total % 60),
                'reg_date'            => $rgd->format('Y-m-d H:i:s'),
                'last_visit'          => $lvd->format('Y-m-d H:i:s'),
                'messages'            => $v->messages ?: 0,
                'dialogs'             => $v->dialogs ?: 0,
                'app_version'         => $app_version,
                'reports'             => $v->reports,
                'reasons'             => $reasons,
                'reasons_alert'       => $reasons_alerts,
                'reasons_alert_read'  => $v->reasons_alert_read,
                'reasons_alert_count' => $v->reasons_alert_count,
                'custom_alert_count'  => $v->custom_alert_count,
                'custom_alert_read'   => $v->custom_alert_read,
                'photos'              => $photos,
                'banned'              => ($v->banned_dev || $v->banned_acc),
                'banned_dev'          => $v->banned_dev,
                'banned_acc'          => $v->banned_acc,
                'banned_date'         => $banned_date,
                'banned_admin'        => $v->banned_admin,
                'blocked_photo'       => $v->blocked_photo,
                'total_amount'        => [
                    'points' => $v->total_amount_points,
                    'vip'    => $v->total_amount_vip,
                    'sum'    => $v->total_amount_sum,
                ],
                'fake'                => preg_match('/.*mail\.local$/i', $v->email) ? 1 : 0,
                'report_verification' => [
                    'status' => $v->report_verification,
                    'date'   => $v->verification_date,
                    'name'   => $v->verification_name,
                ],
            ];
        }

        $response = [
            'total'         => (int)$count,
            'per_page'      => (int)$limit,
            'current_page'  => (int)$page,
            'last_page'     => ceil($count / $limit),
            'next_page_url' => null,
            'prev_page_url' => null,
            'from'          => intval($offset + 1),
            'to'            => intval($page * $limit),
            'data'          => $return
        ];

        return response()->json($response);
    }

    /**
     * Информация о пользователя
     * @param int $user_id Id пользователя
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function get($user_id)
    {
        if (!$user_id = (int)$user_id) {
            return response()->json([], 400);
        }

        if (in_array($user_id, $this->protected_users)) {
            return response()->json([], 404);
        }

        $user = DB::table('users_info as i')
            ->join('users as u', 'u.user_id', '=', 'i.user_id')
            ->join('users_stats as s', 's.user_id', '=', 'i.user_id')
            ->join('users_search as us', 'us.user_id', '=', 'i.user_id')
            ->join('blocked_users as b', 'b.blocked_user', '=', 'i.user_id')
            ->leftJoin('users_photos as p', 'p.photo_id', '=', 'i.photo_id')
            ->leftJoin('facebook as fb', 'fb.user_id', '=', 'i.user_id')
            ->leftJoin('vkontakte as vk', 'vk.user_id', '=', 'i.user_id')
            ->leftJoinSub(
                DB::table('dialog_users')
                    ->select('user_id')
                    ->selectRaw('count(dialog_id) as dialogs')
                    ->where('messages_total', '>', 0)
                    ->groupBy('user_id'),
                'du', 'du.user_id', '=', 'i.user_id')
            ->select('u.user_id', 'u.email', 'i.name', 'i.years', 'i.sex', 'i.latitude', 'i.longitude',
                'i.city', 'i.reg_date', 'i.last_visit', 'i.points', 'i.vip', 'i.avatar', 'i.photo_id', 'fb.fb', 'vk.vk',
                'p.photo', 's.gifts', 's.messages', 'du.dialogs', 's.online', 's.vip_expires',
                's.vip_order_method', 's.banned_acc', 's.banned_dev', 's.banned_date', 's.banned_admin', 's.blocked_photo',
                's.app_version', 's.total_amount_points', 's.total_amount_vip', 'us.gender as search_gender',
                'us.age_min as search_age_min', 'us.age_max as search_age_max')
            ->selectRaw('min(b.verification) as report_verification')
            ->selectRaw('(s.total_amount_points + s.total_amount_vip) as total_amount_sum')
            ->where('i.user_id', '=', $user_id)
            ->first();

        if ($user->user_id) {
            $alerts = $alert = DB::table('users_alerts as ua')
                ->leftJoin('admins as a', 'ua.admin_id', '=', 'a.id')
                ->select(['ua.alert_id', 'ua.type', 'ua.priority', 'ua.data', 'ua.read_at', 'ua.created_at', 'a.name as admin_name'])
                ->where('ua.user_id', '=', $user_id)
                ->where('ua.type', '=', 4)
                ->orderByDesc('ua.alert_id')
                ->get()
                ->toArray();

            if ($alerts) {
                foreach ($alerts as $v) {
                    $alert_data = @json_decode($v->data);

                    $date = new DateTime($v->created_at);
                    $date->setTimezone(new DateTimeZone('Asia/Krasnoyarsk'));
                    $v->created_at = $date->format('Y-m-d H:i:s');

                    if ($alert_data) {
                        $v->data = $alert_data;
                    } else {
                        $v->data = json_decode('{}');
                    }
                }
            }

            $reg_date = '';
            $last_visit = '';
            $banned_date = '';
            $vip_expires_days = null;

            $now = new DateTime();

            if ($user->vip_expires) {
                $vip_expires = new DateTime($user->vip_expires);
                $vip_expires_days .= $now->diff($vip_expires)->format('%r%a (%H:%I)');
            }

            if ($user->reg_date) {
                $date = new DateTime($user->reg_date);
                $date->setTimezone(new DateTimeZone('Asia/Krasnoyarsk'));
                $reg_date = $date->format('Y-m-d H:i:s');
            }
            if ($user->reg_date) {
                $date = new DateTime();
                $date->setTimestamp($user->last_visit);
                $date->setTimezone(new DateTimeZone('Asia/Krasnoyarsk'));
                $last_visit = $date->format('Y-m-d H:i:s');
            }
            if ($user->banned_date) {
                $date = new DateTime($user->banned_date);
                $date->setTimezone(new DateTimeZone('Asia/Krasnoyarsk'));
                $banned_date = $date->format('Y-m-d H:i:s');
            }

            $response = [
                'name'                => $user->name,
                'age'                 => $user->years,
                'gender'              => $user->sex,
                'latitude'            => (float)$user->latitude,
                'longitude'           => (float)$user->longitude,
                'city'                => $user->city,
                'fb'                  => $user->fb ? 'https://facebook.com/profile.php?id=' . $user->fb : '',
                'vk'                  => $user->vk ? 'https://vk.com/id' . $user->vk : '',
                'gifts'               => $user->gifts,
                'points'              => $user->points,
                'vip'                 => [
                    'status'  => $user->vip,
                    'expires' => $vip_expires_days,
                    'payment' => in_array($user->vip_order_method, ['googlepay', 'itunes']) ? 1 : 0,
                ],
                'photo_id'            => $user->photo_id,
                'banned_acc'          => $user->banned_acc,
                'banned_dev'          => $user->banned_dev,
                'banned_date'         => $banned_date,
                'banned_admin'        => $user->banned_admin,
                'blocked_photo'       => $user->blocked_photo,
                'total_amount'        => [
                    'points' => $user->total_amount_points,
                    'vip'    => $user->total_amount_vip,
                    'sum'    => $user->total_amount_sum,
                ],
                'messages'            => $user->messages,
                'dialogs'             => $user->dialogs,
                'online_total'        => sprintf("%d:%02d", floor($user->online / 60), $user->online % 60),
                'app_version'         => $user->app_version,
                'reg_date'            => $reg_date,
                'last_visit'          => $last_visit,
                'search_age_min'      => $user->search_age_min,
                'search_age_max'      => $user->search_age_max,
                'search_gender'       => $user->search_gender,
                'avatar_link'         => $user->avatar ? env('STATIC_AVA_URL') . $user->avatar : '',
                'main_photo_link'     => $user->photo ? env('STATIC_PHOTO_URL') . $user->photo : '',
                'fake'                => preg_match('/.*mail\.local$/i', $user->email) ? 1 : 0,
                'report_verification' => $user->report_verification,
                'alerts'              => $alerts,
                'photos'              => (new UsersController())->getUserPhotos($user_id),
            ];

            if ($reports = $this->getUserReports($user_id)) {
                $response['reports'] = $reports;

                if ($dialogs = $this->getUserReportsDialog($user_id, $reports)) {
                    $response['dialogs_id'] = $dialogs;
                }
            }
        } else {
            return response()->json([], 404);
        }

        return response()->json($response);
    }

    /**
     * Список жалоб на пользователя
     * @param int $user_id Id пользователя
     * @return array
     */
    protected function getUserReports($user_id)
    {
        $reports = [];

        $result = DB::table('blocked_users as b')
            ->select('b.blocked_id', 'b.user_id', 'i.name', 'i.years', 'i.sex', 'i.avatar', 'b.reason', 'b.reason_text', 'b.date')
            ->selectRaw('count(r.blocked_id) as reports_count')
            ->join('users_info as i', 'i.user_id', '=', 'b.user_id')
            ->leftJoin('blocked_users as r', 'r.blocked_user', '=', 'i.user_id')
            ->where('b.blocked_user', '=', $user_id)
            ->orderBy('b.blocked_id', 'desc')
            ->groupBy('b.user_id')
            ->get();
        foreach ($result as $v) {
            $reason_key = '-1';
            $reason_text = $v->reason_text;
            if (array_key_exists($v->reason, $this->reasons_list)) {
                $reason_key = $v->reason;
            }

            if ($reason_key != 5 || empty(trim($v->reason_text))) {
                $reason_text = $this->reasons_list[$reason_key];
            }

            $reports[] = [
                'user_id'       => $v->user_id,
                'name'          => $v->name,
                'years'         => $v->years,
                'gender'        => $v->sex,
                'avatar'        => $v->avatar ? env('STATIC_AVA_URL') . $v->avatar : '',
                'reason'        => $v->reason,
                'reason_text'   => $reason_text,
                'date'          => $v->date ?: null,
                'reports_count' => $v->reports_count
            ];
            $reports_list[] = $v->user_id;
        }

        return $reports;
    }

    /**
     * Диалоги с пожаловавшимся пользователя
     * @param int $user_id Id пользователя
     * @param array $reports Список жалоб
     * @return array|\Illuminate\Support\Collection
     */
    protected function getUserReportsDialog($user_id, $reports)
    {
        $dialogs = [];
        $reports_list = [];

        if (!$reports || !is_array($reports)) {
            return $dialogs;
        }

        foreach ($reports as $v) {
            $reports_list[] = $v['user_id'];
        }

        if ($reports_list) {
            $dialogs = DB::table('dialog_users as d')
                ->join('dialog_users as d2', 'd.dialog_id', '=', 'd2.dialog_id')
                ->join('messages as m', 'd.dialog_id', '=', 'm.dialog_id')
                ->where('d.user_id', '=', $user_id)
                ->where('d2.user_id', '!=', $user_id)
                ->whereIn('d2.user_id', $reports_list)
                ->groupBy('d2.dialog_id')
                ->select('d2.dialog_id', 'd2.user_id')
                ->selectRaw('sum(case when m.user_id = d.user_id then 1 else 0 end) as cnt_out')
                ->selectRaw('sum(case when m.user_id != d.user_id then 1 else 0 end) as cnt_inc')
                ->get()
                ->keyBy('user_id');

            foreach ($dialogs as $k => $v) {
                $dialogs[$k] = [
                    'dialog_id' => $v->dialog_id,
                    'cnt_out'   => (int)$v->cnt_out,
                    'cnt_inc'   => (int)$v->cnt_inc,
                ];
            }
        }

        return $dialogs;
    }

    /**
     * Сообщения диалога
     * @param int $dialog_id Id диалога
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function getDialog($dialog_id)
    {
        $dialog_id = (int)$dialog_id;

        if (!$dialog_id) {
            return response()->json([]);
        }

        $result = DB::table('messages as m')
            ->join('users_info as i', 'i.user_id', '=', 'm.user_id')
            ->where('m.dialog_id', '=', $dialog_id)
            ->orderBy('m.message_id', 'asc')
            ->select('m.message_id', 'm.user_id', 'i.name', 'i.years', 'i.avatar', 'm.message', 'm.type', 'm.date')
            ->get()
            ->toArray();

        $messages = [];
        foreach ($result as $v) {
            $date = '';
            if ($v->date) {
                $date = new DateTime();
                $date->setTimestamp($v->date);
                $date->setTimezone(new DateTimeZone($this->timezone));
                $date = $date->format('d.m.Y H:i');
            }
            $msg = [
                'message_id' => $v->message_id,
                'user_id'    => $v->user_id,
                'name'       => $v->name,
                'years'      => $v->years,
                'avatar'     => $v->avatar ? env('STATIC_AVA_URL') . $v->avatar : '',
                'message'    => $v->message,
                'type'       => $v->type,
                'date'       => $date,
            ];

            if ($msg['type'] != 1) {
                $json = @json_decode($msg['message'], true);
                if (json_last_error() == JSON_ERROR_NONE && is_array($json)) {
                    $msg['message'] = $this->messageStaticURL($json);
                } else {
                    $msg['type'] = 1;
                }
            }

            $messages[] = $msg;
        }

        return response()->json($messages);
    }

    /**
     * Url для видео и фото в сообщениях диалога
     * @param array $message Сообщение
     * @return mixed
     */
    private function messageStaticURL($message)
    {
        foreach ($message as $k => $v) {
            if (empty($v)) {
                continue;
            }

            switch ($k) {
                case 'imageURL':
                case 'audioURL':
                case 'musicURL':
                case 'videoURL':
                case 'thumbVideoURL':
                    $path = env('STATIC_ATTACHMENT_URL');
                    break;
                case 'thumbImageURL':
                    $path = env('STATIC_ATTACHMENT_PREW_URL');
                    break;
                default:
                    $path = '';
                    break;
            }

            if ($path) {
                $message[$k] = $path . $v;
            }
        }

        return $message;
    }

    /**
     * Изменение пола пользователей
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeGender(Request $request)
    {
        $users = $request->input('users');
        $gender = $request->input('gender');

        foreach ($users as $k => $v) {
            if (in_array($v, $this->protected_users)) {
                unset($users[$k]);
            }
        }

        if (count($users) > 0 && ($gender != 1 || $gender != 2)) {
            DB::table('users_info')
                ->whereIn('user_id', $users)
                ->update([
                    'sex' => $gender
                ]);
        }

        return response()->json([]);
    }

    /**
     * Пометка пользователей как проверенных
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function verification(Request $request)
    {
        $users = $request->input('users');
        $status = (int)$request->input('status');

        if ($status != 0 && $status != 1) {
            return response()->json([], 400);
        }

        foreach ($users as $k => $v) {
            if (in_array($v, $this->protected_users)) {
                unset($users[$k]);
            }
        }

        if (count($users) > 0) {
            DB::table('blocked_users')
                ->whereIn('blocked_user', $users)
                ->update(['verification' => $status]);

            $this->setVerificator($users, $status);
        }

        return response()->json([]);
    }

    /**
     * Отправка пользователям уведомления о поступивших на них жалобах
     * @param Request $request
     * @throws Exception
     */
    public function alert(Request $request)
    {
        $users = $this->prepareWhereInInt($request->input('users'));

        $blocked = DB::table('blocked_users')
            ->whereIn('blocked_user', $users)
            ->groupBy('blocked_user', 'reason')
            ->get(['blocked_user', 'reason', DB::raw('count(*) as count')]);

        $data = [];
        $reasons = [];
        if ($blocked) {
            foreach ($blocked as $v) {
                $reasons[$v->blocked_user][] = [
                    'reason' => $v->reason,
                    'count'  => $v->count,
                ];
            }

            foreach ($reasons as $k => $v) {
                $data[] = [
                    'user_id'  => $k,
                    'type'     => 3,
                    'priority' => 300,
                    'data'     => json_encode(['reasons' => $v])
                ];
            }
        }

        if ($data) {
            DB::beginTransaction();

            DB::table('users_alerts')
                ->whereIn('user_id', $users)
                ->where('type', '=', 3)
                ->whereNull('read_at')
                ->update(['read_at' => DB::raw('NOW()')]);

            DB::table('users_alerts')->insert($data);

            DB::commit();

            $r = Redis::connection();
            foreach ($users as $user) {
                $r->publish('fcm_push', json_encode([
                    'user_id'         => $user,
                    'push_type'       => 'system_msg',
                    'system_msg_data' => [
                        'type' => 'refresh_badges',
                        'data' => []
                    ]
                ]));
            }
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAlertMessagesList()
    {
        $messages = DB::table('users_alerts_messages')
            ->orderBy('message_id', 'desc')
            ->get(['message_id', 'title', 'message', 'priority'])
            ->toArray();

        return response()->json(['messages' => $messages]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAlertMessage(Request $request)
    {
        $message_id = $request->input('message_id');

        if (!is_int($message_id)) {
            return response()->json([], 400);
        }

        $message = DB::table('users_alerts_messages')
            ->where('message_id', '=', $message_id)
            ->get(['title', 'message', 'priority'])
            ->first()
            ->toArray();

        return response()->json(['message' => $message]);
    }

    /**
     * @param Request $request
     * @return bool|int
     */
    public function createAlertMessage(Request $request)
    {
        $title = $this->prepareAlertText($request->input('title'));
        $message = $this->prepareAlertText($request->input('message'));
        $priority = $request->input('priority');

        if (!$title || !$message || !in_array($priority, [100, 200, 300])) {
            return response()->json([], 400);
        }

        if (mb_strlen($title) > 60 || mb_strlen($message) > 200) {
            return response()->json([], 400);
        }

        $message_id = DB::table('users_alerts_messages')
            ->insertGetId([
                'title'    => $title,
                'message'  => $message,
                'priority' => $priority,
            ]);

        return response()->json(['message_id' => $message_id]);
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function updateAlertMessage(Request $request)
    {
        $title = $this->prepareAlertText($request->input('title'));
        $message = $this->prepareAlertText($request->input('message'));
        $priority = $request->input('priority');
        $message_id = $request->input('message_id');

        if (!is_int($message_id) || !$title || !$message || !in_array($priority, [100, 200, 300])) {
            return response()->json([], 400);
        }

        if (mb_strlen($title) > 60 || mb_strlen($message) > 200) {
            return response()->json([], 400);
        }

        $alert = DB::table('users_alerts_messages')
            ->where('message_id', '=', $message_id)
            ->select('message_id')
            ->first();

        if ($alert->message_id) {
            $update = DB::table('users_alerts_messages')
                ->where('message_id', '=', $message_id)
                ->update([
                    'title'    => $title,
                    'message'  => $message,
                    'priority' => $priority
                ]);

            if ($update) {
                return response()->json([]);
            }
        }

        return response()->json([], 400);
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function deleteAlertMessage(Request $request)
    {
        $message_id = $request->input('message_id');

        if (!is_int($message_id)) {
            return response()->json([], 400);
        }

        DB::table('users_alerts_messages')
            ->where('message_id', '=', $message_id)
            ->delete();

        return response()->json([]);
    }

    /**
     * @param Request $request
     * @return bool
     * @throws Exception
     */
    public function sendAlertMessage(Request $request)
    {
        $users = $this->prepareWhereInInt($request->input('users'));
        $title = $this->prepareAlertText($request->input('title'));
        $message = $this->prepareAlertText($request->input('message'));
        $priority = $request->input('priority');

        if (!$title || !$message || !in_array($priority, [100, 200, 300])) {
            return response()->json(['error' => 'empty title or message'], 400);
        }

        if (mb_strlen($title) > 60 || mb_strlen($message) > 200) {
            return response()->json(['error' => 'limit string title or message'], 400);
        }

        $data = [];
        $alert = [
            'title'   => $title,
            'message' => $message,
        ];

        foreach ($users as $user) {
            $data[] = [
                'user_id'  => $user,
                'admin_id' => $this->admin->id,
                'type'     => 4,
                'priority' => $priority,
                'data'     => json_encode($alert)
            ];
        }

        if ($data) {
            DB::table('users_alerts')->insert($data);

            $r = Redis::connection();
            foreach ($users as $user) {
                $r->publish('fcm_push', json_encode([
                    'user_id'         => $user,
                    'push_type'       => 'system_msg',
                    'system_msg_data' => [
                        'type' => 'refresh_badges',
                        'data' => []
                    ]
                ]));
            }

            return response()->json([]);
        }

        return response()->json([], 400);
    }

    /**
     * @param $text
     * @return string|string[]|null
     */
    protected function prepareAlertText($text)
    {
        if (!is_string($text)) {
            return '';
        }

        return preg_replace('/\s+/u', ' ', preg_replace('/\s+/u', ' ', trim($text)));
    }

    /**
     * Запись о том кто проверял пользователя
     * @param array $users
     * @param int $status
     */
    protected function setVerificator(array $users, $status)
    {
        $data = [];

        foreach ($users as $user) {
            $data[] = [
                'user_id'  => $user,
                'admin_id' => $this->admin->id,
                'status'   => $status
            ];
        }

        if ($data) {
            DB::table('users_reports_verification')->insert($data);
        }
    }
}
