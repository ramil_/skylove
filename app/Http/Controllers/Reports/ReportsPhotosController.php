<?php

namespace App\Http\Controllers\Reports;

use App\Http\Controllers\Controller;
use App\Http\Controllers\UsersPhotos\UsersPhotosStatusController;
use App\Http\Traits\GeoTimezone;
use App\Http\Traits\HelperDB;
use App\UsersPhoto;
use App\UsersPhotoReportVerification;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ReportsPhotosController extends Controller
{
    use GeoTimezone, HelperDB;

    /**
     * @var int Лимит фотографий на одной странице
     */
    protected $limit = 200;

    protected $admin = null;

    protected $admin_id = 0;

    protected $admin_level = 0;

    protected $permissions = [];

    /**
     * ReportsPhotosController constructor.
     * @throws Exception
     */
    public function __construct()
    {
        if (!$this->setGeoTimezone()) {
            throw new Exception('Cannot get timezone', 500);
        }

        if ($user = Auth::user()) {
            $this->admin = $user;
            $this->admin_id = $user->id;
            $this->admin_level = $user->level;
            $this->permissions = $user->permissions()->pluck('permission')->toArray();
        } else {
            throw new Exception('Cannot get auth user information', 500);
        }

        return true;
    }

    /**
     * Список жалоб
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getData(Request $request)
    {
        $page = intval($request->input('page', 1)) ?: 1;
        $offset = ($page - 1) * $this->limit;

        $photos = UsersPhoto::with([
            'userInfo'                => function (\Illuminate\Database\Eloquent\Relations\Relation $query) use ($request) {
                $query->select(['user_id', 'name', 'sex', 'years', 'avatar', 'city', 'vip', 'verification', 'review'])
                    ->selectRaw('(unix_timestamp() - last_visit) as last_visit_sec');
            },
            'statusInspectLast'       => function (\Illuminate\Database\Eloquent\Relations\Relation $query) {
                $query->select(['photo_id', 'admin_id', 'admin_status as status', 'associated', 'level', 'date']);
            },
            'statusInspectLast.admin' => function (\Illuminate\Database\Eloquent\Relations\Relation $query) {
                $query->select(['id', 'name']);
            }])
            ->select(['photo_id', 'user_id', 'photo', 'main_photo', 'reports', 'deleted_at'])
            ->withCount('reports')
            ->where('reports', '>', 0)
            ->whereNull('deleted_at');

        if ($filter = $request->input('filter')) {
            $filter = [
                'user_info' => [
                    'user_id'   => intval(Arr::get($filter, 'user_id', 0)),
                    'name'      => strval(Arr::get($filter, 'name', '')),
                    'city'      => strval(Arr::get($filter, 'city', '')),
                    'gender'    => intval(Arr::get($filter, 'gender', 0)),
                    'years_min' => intval(Arr::get($filter, 'years_min', 0)),
                    'years_max' => intval(Arr::get($filter, 'years_max', 0)),
                    'vip'       => strval(Arr::get($filter, 'vip', '')),
                ],
                'stat'      => [
                    'platform' => strval(Arr::get($filter, 'platform', '')),
                ]
            ];

            if (!in_array($filter['user_info']['gender'], [1, 2])) {
                $filter['user_info']['gender'] = 0;
            }

            if (!in_array($filter['user_info']['vip'], ['yes', 'no'])) {
                $filter['user_info']['vip'] = 0;
            }

            if (!in_array($filter['stat']['platform'], ['android', 'ios'])) {
                $filter['stat']['platform'] = 0;
            }

            $setFilter = [];
            foreach ($filter as $key => $values) {
                $setFilter[$key] = 0;

                foreach ($values as $v) {
                    if (!empty($v)) {
                        $setFilter[$key] = 1;

                        break;
                    }
                }
            }

            if ($setFilter['user_info']) {
                $photos->whereHas('userInfo', function (\Illuminate\Database\Eloquent\Builder $q) use ($filter) {
                    $q->when($filter['user_info']['user_id'], function (\Illuminate\Database\Eloquent\Builder $q, $user_id) {
                        return $q->where('user_id', $user_id);
                    });

                    $q->when($filter['user_info']['name'], function (\Illuminate\Database\Eloquent\Builder $q, $name) {
                        return $q->where('name', 'like', "%{$name}%");
                    });

                    $q->when($filter['user_info']['city'], function (\Illuminate\Database\Eloquent\Builder $q, $city) {
                        return $q->where('city', 'like', "%{$city}%");
                    });

                    $q->when($filter['user_info']['gender'], function (\Illuminate\Database\Eloquent\Builder $q, $gender) {
                        return $q->where('sex', $gender);
                    });

                    $q->when($filter['user_info']['years_min'], function (\Illuminate\Database\Eloquent\Builder $q, $years) {
                        return $q->where('years', '>=', $years);
                    });

                    $q->when($filter['user_info']['years_max'], function (\Illuminate\Database\Eloquent\Builder $q, $years) {
                        return $q->where('years', '<=', $years);
                    });

                    $q->when($filter['user_info']['vip'], function (\Illuminate\Database\Eloquent\Builder $q, $vip) {
                        return $q->where('vip', $vip == 'yes' ? 1 : 0);
                    });
                });
            }

            if ($setFilter['stat']) {
                $photos->whereHas('stat', function (\Illuminate\Database\Eloquent\Builder $q) use ($filter) {
                    $q->when($filter['stat']['platform'], function (\Illuminate\Database\Eloquent\Builder $q, $platform) {
                        return $q->where('app_version', 'like', "%{$platform}%");
                    });
                });
            }
        }

        $photos_count = (clone $photos)->count();
        $photosData = $photos->orderByDesc('reports')
            ->offset($offset)
            ->limit($this->limit)
            ->get();

        if ($photosData->count() == 0 && $page > 1) {
            $last = intval(ceil($photos_count / $this->limit));

            return $this->getData((new Request())->replace(['filter' => $request->input('filter', []), 'page' => $last]));
        }

        $urlPhotoPrev = env('STATIC_GALLERY_PREW_URL');
        $urlPhotoOrig = env('STATIC_GALLERY_URL');
        $urlAvatar = env('STATIC_AVA_URL');
        $data = [];

        foreach ($photosData as $v) {
            $user = $v->userInfo;
            $status = $v->statusInspectLast;

            $data[] = [
                'photo_id'          => $v->photo_id,
                'prev'              => $urlPhotoPrev . $v->photo,
                'href'              => $urlPhotoOrig . $v->photo,
                'main_photo'        => $v->main_photo,
                'reports_count'     => $v->reports,
                'reports_count_all' => $v->reports_count,
                'blocked_count'     => 0,
                'deleted_at'        => $v->deleted_at,
                'verification'      => [
                    'status'     => $status ? $status->status : 0,
                    'admin'      => $status ? $status->admin->name : null,
                    'date'       => $status ? $this->tzConvertToLocal($status->date) : null,
                    'associated' => $status ? $status->associated : 0,
                    'admin_id'   => $status ? $status->admin_id : 0,
                    'level'      => $status ? $status->level : 0,
                ],
                'user'              => [
                    'user_id' => $user->user_id,
                    'name'    => $user->name,
                    'years'   => $user->years,
                    'city'    => $user->city,
                    'avatar'  => $user->avatar ? $urlAvatar . $user->avatar : '',
                    'vip'     => $user->vip,
                    'online'  => $user->last_visit_sec < 600 ? 2 : ($user->last_visit_sec < 21600 ? 1 : 0),
                ],
            ];
        }

        if (isset($photos_count)) {
            $response = [
                'from'         => intval($offset + 1),
                'to'           => intval($page * $this->limit),
                'total'        => intval($photos_count),
                'current_page' => intval($page),
                'last_page'    => intval(ceil($photos_count / $this->limit)),
                'data'         => $data
            ];
            if ($response['from'] > $response['total']) {
                $response['from'] = $response['total'];
            }
            if ($response['to'] > $response['total']) {
                $response['to'] = $response['total'];
            }
        } else {
            $response = $data;
        }

        return response()->json($response);
    }

    /**
     * Проверка жалобы
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function setReview(Request $request)
    {
        $photos = $request->input('photos');
        $status = $request->input('status');

        try {
            DB::beginTransaction();

            $photosId = UsersPhoto::select(['photo_id'])
                ->whereIn('photo_id', $photos)
                ->where('reports', '>', 0)
                ->get();

            if (!$photosId->count()) {
                if ($status) {
                    DB::rollBack();

                    return response()->json([]);
                }

                throw new Exception('Photos not found', 400);
            }

            $verification = [];
            foreach ($photosId->pluck('photo_id') as $photoId) {
                $verification[] = [
                    'photo_id' => $photoId,
                    'admin_id' => $this->admin->id,
                ];
            }

            if ($verification) {
                UsersPhoto::whereIn('photo_id', $photosId->pluck('photo_id'))->update(['reports' => 0]);
                UsersPhotoReportVerification::insert($verification);
            }

            DB::commit();

            return response()->json($photosId);
        } catch (Exception $e) {
            DB::rollBack();
            Log::error($e);

            return response()->json(['error' => $e->getMessage()], $e->getCode());
        }
    }

    /**
     * Установка статуса фото и проверка жалобы
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function setStatusPhoto(Request $request)
    {
        $photos = $request->input('photos');
        $status = $request->input('status');

        try {
            if (empty($photos) || !in_array($status, [1, 2, 3, 4, 5, 6])) {
                throw new Exception('Wrong request parameters', 400);
            }

            DB::beginTransaction();

            $reportReviewRequest = (new Request())->replace(['photos' => $photos, 'status' => $status]);
            $reportReviewResponse = $this->setReview($reportReviewRequest);

            if ($reportReviewResponse->getStatusCode() != 200) {
                throw new Exception('Cannot set review report', $reportReviewResponse->getStatusCode());
            }

            $photoStatusRequest = (new Request())->replace(['photos' => $photos, 'status' => $status]);
            $photoStatusResponse = (new UsersPhotosStatusController)->setStatusPhoto($photoStatusRequest, false);

            if ($photoStatusResponse->getStatusCode() != 200) {
                $photoStatusData = $photoStatusResponse->getData();

                if (!empty($photoStatusData->error)) {
                    $message = $photoStatusData->error;
                } else {
                    $message = 'Cannot set status photo';
                }

                throw new Exception($message, $photoStatusResponse->getStatusCode());
            }

            DB::commit();

            return response()->json($photoStatusResponse->getData());
        } catch (Exception $e) {
            DB::rollBack();
            Log::error($e);

            return response()->json(['error' => $e->getMessage()], $e->getCode() ?: 500);
        }
    }
}
