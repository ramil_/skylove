<?php

namespace App\Http\Controllers\Moderators;

use App\AdminsShift;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ShiftController extends Controller
{
    protected $admin;

    /**
     * ShiftController constructor.
     * @throws Exception
     */
    public function __construct()
    {
        if (!$this->admin = Auth::user()) {
            throw new Exception('Cannot get auth user information', 500);
        }
    }

    /**
     * Установка начала смены модератора
     * @return \Illuminate\Http\JsonResponse
     */
    public function setStartedShift()
    {
        $shiftCurrent = AdminsShift::with('admin')
            ->latest('started_at')
            ->latest('shift_id')
            ->first();

        if ($shiftCurrent && !$shiftCurrent->finished_at) {
            if ($shiftCurrent->admin_id == $this->admin->id) {
                return response()->json(['Shift already exists'], 400);
            }

            $shiftCurrent->finished_at = DB::raw('now()');
            $shiftCurrent->save();
        }

        AdminsShift::create([
            'admin_id'   => $this->admin->id,
            'started_at' => DB::raw('now()'),
        ]);

        return response()->json([], 200);
    }

    /**
     * Установка завершения смены модератора
     * @return \Illuminate\Http\JsonResponse
     */
    public function setFinishedShift()
    {
        $shiftCurrent = AdminsShift::with('admin')
            ->latest('started_at')
            ->latest('shift_id')
            ->first();

        if ($shiftCurrent && !$shiftCurrent->finished_at) {
            if ($shiftCurrent->admin_id != $this->admin->id) {
                return response()->json(['Cannot finished shift owned another moderator'], 400);
            }

            $shiftCurrent->finished_at = DB::raw('now()');
            $shiftCurrent->save();

            return response()->json([], 200);
        }

        return response()->json(['Shift not found'], 400);
    }

    /**
     * Информация о текущей смене
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCurrentShift()
    {
        $response = [
            'admin_id'    => 0,
            'started_at'  => '',
            'finished_at' => '',
            'owner'       => null,
        ];

        $shift = AdminsShift::with([
            'admin' => function (\Illuminate\Database\Eloquent\Relations\Relation $query) {
                $query->select(['id', 'name']);
            }])
            ->select(['admin_id', 'started_at', 'finished_at'])
            ->latest('started_at')
            ->latest('shift_id')
            ->first();

        if ($shift && !$shift->finished_at) {
            $response = $shift->toArray();
            $response['owner'] = $shift->admin_id == $this->admin->id;
        }

        return response()->json($response);
    }
}
