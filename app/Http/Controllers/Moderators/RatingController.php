<?php

namespace App\Http\Controllers\Moderators;

use App\User;
use App\Http\Controllers\Controller;

class RatingController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRatingList()
    {
        $admins = User::with('rating')
            ->where('level', '<=', 10)
            ->get(['id', 'name', 'active', 'deleted_at']);

        foreach ($admins as $admin) {
            if ($admin->rating) {
                $admin->rating->metadata = json_decode($admin->rating->metadata);
            }
        }

        return response()->json($admins);
    }
}
