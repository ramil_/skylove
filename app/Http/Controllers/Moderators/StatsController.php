<?php

namespace App\Http\Controllers\Moderators;

use App\AdminsPayment;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class StatsController extends Controller
{
    protected $admin_id = 0;

    protected $price_main = 0.35;

    protected $price_inner = 0.50;

    protected $tzServer = 'UTC';

    protected $tzLocal = 'Asia/Krasnoyarsk';

    public function __construct()
    {
        if ($user = Auth::user()) {
            $this->admin_id = $user->id;
        } else {
            return response()->json(['error' => 'Cannot get user'], 401);
        }

        return true;
    }

    /**
     * Статистика проверки фото модератором
     * @param int $admin_id Id модератора
     * @return array
     */
    protected function getStats($admin_id)
    {
        $data = [];

        // выплаты
        $query = "
            select amount, date(convert_tz(date, '{$this->tzServer}', '{$this->tzLocal}')) as date,
                   convert_tz(created_at, '{$this->tzServer}', '{$this->tzLocal}') as created_at
            from admins_payments
            where for_admin_id = {$admin_id}
            order by date";
        $pays = collect(DB::select($query))->map(function ($x) {
            return (array)$x;
        })->toArray();

        // проверка фото
        $query = "
            select cast(sum(case when a.status = -1 then 1 else 0 end) as unsigned) as deferred,
                   cast(sum(case when a.status = 0 then 1 else 0 end) as unsigned) as reset,
                   cast(sum(case when a.status = 1 and a.main_photo = 1 then 1 else 0 end) as unsigned) as accept_main,
                   cast(sum(case when a.status = 1 and a.main_photo = 0 then 1 else 0 end) as unsigned) as accept_inner,
                   cast(sum(case when a.status = 2 then 1 else 0 end) as unsigned) as forbidden_ava,
                   cast(sum(case when a.status = 3 then 1 else 0 end) as unsigned) as forbidden_show,
                   cast(sum(case when a.status = 4 then 1 else 0 end) as unsigned) as blocked,
                   date(convert_tz(a.date, '{$this->tzServer}', '{$this->tzLocal}')) as tz_date
            from users_photos_status a
            inner join (
                select photo_id, max(convert_tz(date, '{$this->tzServer}', '{$this->tzLocal}')) as tz_date
                from users_photos_status
                where admin_id = {$admin_id}
                group by photo_id
            ) x on a.photo_id = x.photo_id and convert_tz(a.date, '{$this->tzServer}', '{$this->tzLocal}') = x.tz_date
            group by date(convert_tz(a.date, '{$this->tzServer}', '{$this->tzLocal}'))";
        $photo = collect(DB::select($query))->map(function ($x) {
            return (array)$x;
        })->toArray();

        $sumStat = function ($v) use (&$data) {
            $key = $v['tz_date'];
            if (!array_key_exists($key, $data)) {
                $data[$key] = [
                    'deferred'         => 0,
                    'reset'            => 0,
                    'accept_main'      => 0,
                    'accept_inner'     => 0,
                    'forbidden_ava'    => 0,
                    'forbidden_show'   => 0,
                    'blocked'          => 0,
                    'banned_acc'       => 0,
                    'banned_dev'       => 0,
                    'banned_acc_main'  => 0,
                    'banned_dev_main'  => 0,
                    'banned_acc_inner' => 0,
                    'banned_dev_inner' => 0,
                ];
            }
            unset($v['tz_date']);

            return array_replace($data[$key], $v);
        };

        foreach ($photo as $v) {
            $data[$v['tz_date']] = $sumStat($v);
        }

        // баны пользователей
        $query = "
            select cast(ifnull(sum(case when user_id is not null then 1 else 0 end), 0) as unsigned) as banned_acc,
                   cast(ifnull(sum(case when device_id is not null then 1 else 0 end), 0) as unsigned) as banned_dev,
                   cast(ifnull(sum(case when user_id is not null and main_photo = 1 then 1 else 0 end), 0) as unsigned) as banned_acc_main,
                   cast(ifnull(sum(case when device_id is not null and main_photo = 1 then 1 else 0 end), 0) as unsigned) as banned_dev_main,
                   cast(ifnull(sum(case when user_id is not null and main_photo = 0 then 1 else 0 end), 0) as unsigned) as banned_acc_inner,
                   cast(ifnull(sum(case when device_id is not null and main_photo = 0 then 1 else 0 end), 0) as unsigned) as banned_dev_inner,
                   date(convert_tz(date, '{$this->tzServer}', '{$this->tzLocal}')) as tz_date
            from (
                 select *
                 from banlist_log
                 where admin_id = {$admin_id}
                   and status = 1
                   and photo_id is not null
                 group by photo_id
             ) x
             group by tz_date";
        $ban = collect(DB::select($query))->map(function ($x) {
            return (array)$x;
        })->toArray();

        foreach ($ban as $v) {
            $data[$v['tz_date']] = $sumStat($v);
        }

        foreach ($data as $k => &$v) {
            $main = $v['accept_main'] + $v['blocked'] + $v['banned_acc_main'] + $v['banned_dev_main'];
            $inner = $v['accept_inner'] + $v['forbidden_ava'] + $v['forbidden_show'] + $v['banned_acc_inner'] + $v['banned_dev_inner'];

            $v['paid_main'] = round($main * $this->price_main, 2);
            $v['paid_inner'] = round($inner * $this->price_inner, 2);
        }

        $stats = [];

        $sum = 0;
        $period = [];
        $pay_date = '';
        $pay_index = 0;

        if (!empty($pays[$pay_index])) {
            $pay_date = Carbon::parse($pays[$pay_index]['date'])->startOfDay()->format('Y-m-d');
        }

        ksort($data);

        foreach ($data as $day => &$stat) {
            if ($pay_date) {
                if (Carbon::parse($pay_date)->lt(Carbon::parse($day))) {
                    krsort($period);

                    $stats[] = [
                        'data'    => $period,
                        'total'   => round($sum, 2),
                        'payment' => $pays[$pay_index],
                    ];

                    $sum = 0;
                    $period = [];
                    $pay_date = '';
                    $pay_index++;

                    if (!empty($pays[$pay_index])) {
                        $pay_date = Carbon::parse($pays[$pay_index]['date'])->startOfDay()->format('Y-m-d');
                    }
                }
            }

            $sum += $stat['paid_main'] + $stat['paid_inner'];
            $period[$day] = $stat;
        }

        if ($sum && $period) {
            krsort($period);

            $payment = json_decode(json_encode([]));
            if (!empty($pays[$pay_index])) {
                $payment = $pays[$pay_index];
            }

            $stats[] = [
                'data'    => $period,
                'total'   => round($sum, 2),
                'payment' => $payment,
            ];
        }

        krsort($stats);

        return array_values($stats);
    }

    /**
     * Статистика проверки фото текущим модератором
     * @return \Illuminate\Http\JsonResponse
     */
    public function getStatsOwner()
    {
        return response()->json($this->getStats($this->admin_id));
    }

    /**
     * Статистика проверки фото указанного модератора
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getStatsSelected(Request $request)
    {
        $admin_id = 0;

        if ($request->exists('admin')) {
            $admin_id = intval($request->input('admin'));
        }

        $info = User::select(['id', 'name', 'active'])
            ->selectRaw('if(deleted_at is null, 0, 1) as deleted')
            ->where('id', '=', $admin_id)
            ->first();

        $response = [
            'info' => $info,
            'stat' => $this->getStats($admin_id)
        ];

        return response()->json($response);
    }

    /**
     * Список модераторов
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPayments(Request $request)
    {
        $data = [];

        $status = $request->input('status', 0);

        if (!in_array($status, ['all', 'deleted', 'active', 'inactive'])) {
            $status = 'all';
        }

        $query = "
            select a.id, a.name, a.active, if(a.deleted_at is not null, 1, 0) as deleted, ap.amount as pay_amount,
                   convert_tz(ap.date, '{$this->tzServer}', '{$this->tzLocal}') as pay_date,
                   convert_tz(ap.created_at, '{$this->tzServer}', '{$this->tzLocal}') as created_at
            from admins as a
            left join (
                select t1.for_admin_id, t1.amount, t1.date, t1.created_at
                from admins_payments as t1
                inner join (
                    select for_admin_id, max(date) as max_date
                    from admins_payments
                    group by for_admin_id
                ) as t2 on t1.for_admin_id = t2.for_admin_id and t1.date =t2.max_date
            ) as ap on a.id = ap.for_admin_id";

        switch ($status) {
            case 'deleted':
                $query .= " where a.deleted_at is not null";

                break;
            case 'active':
                $query .= " where a.deleted_at is null and a.active = 1";

                break;
            case 'inactive':
                $query .= " where a.deleted_at is null and a.active = 0";

                break;
        }

        if (!$admins = DB::select($query)) {
            return response()->json([]);
        }

        foreach ($admins as $admin) {
            if ($pay = $this->getPaymentInfo($admin)) {
                $data[$pay['admin']['id']] = $pay;
            }
        }

        return response()->json($data);
    }

    /**
     * Добавление оплаты модератору
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function payment(Request $request)
    {
        if (!$request->exists('admin_id')) {
            return response()->json(['error' => 'Empty admin_id'], 400);
        }

        if (!$request->exists('date')) {
            return response()->json(['error' => 'Empty date'], 400);
        }

        if (!$admin_id = intval($request->input('admin_id'))) {
            return response()->json(['error' => 'Invalid admin_id'], 400);
        }

        if (!$date = Carbon::parse($request->input('date'), $this->tzLocal)) {
            return response()->json(['error' => 'Cannot parse date'], 400);
        }
        $date = Carbon::parse($date->endOfDay(), $this->tzLocal)->timezone($this->tzServer);

        $query = "
            select a.id, a.name, a.active, if(a.deleted_at is not null, 1, 0) as deleted, ap.amount as pay_amount,
                   convert_tz(ap.date, '{$this->tzServer}', '{$this->tzLocal}') as pay_date,
                   convert_tz(ap.created_at, '{$this->tzServer}', '{$this->tzLocal}') as created_at
            from admins as a
            left join (
                select t1.for_admin_id, t1.amount, t1.date, t1.created_at
                from admins_payments as t1
                inner join (
                    select for_admin_id, max(date) as max_date
                    from admins_payments
                    where amount > 0
                    group by for_admin_id
                ) as t2 on t1.for_admin_id = t2.for_admin_id and t1.date =t2.max_date
            ) as ap on a.id = ap.for_admin_id
            where a.id = {$admin_id}";
        $admin = collect(DB::select($query))->first();

        if (!$admin->pay_date) {
            $admin->pay_date = '1970-01-01 00:00:00';
        }

        if (!$admin) {
            return response()->json(['error' => 'Moderator not found'], 400);
        }

        if ((Carbon::parse($admin->pay_date))->gte($date)) {
            return response()->json(['error' => 'Payment date less than the last payment'], 400);
        }

        if (!$info = $this->getPaymentInfo($admin, $date)) {
            return response()->json(['error' => 'Cannot get payment info'], 400);
        }

        if (!$info['for_pay']['total']) {
            return response()->json(['error' => 'Empty amount'], 400);
        }

        $pay = AdminsPayment::create([
            'for_admin_id'  => $admin->id,
            'from_admin_id' => $this->admin_id,
            'amount'        => $info['for_pay']['total'],
            'date'          => $date->toDateTimeString()
        ]);

        if (!$pay) {
            return response()->json(['error' => 'Cannot insert payment'], 500);
        }

        $response = [
            'admin_id' => $admin->id,
            'amount'   => $info['for_pay']['total'],
            'date'     => $date->timezone($this->tzLocal)->toDateString(),
        ];

        return response()->json($response, 200);
    }

    /**
     * Сумма платежа на выбранную дату
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPaymentAmount(Request $request)
    {
        if (!$request->exists('admin_id')) {
            return response()->json(['error' => 'Empty admin_id'], 400);
        }

        if (!$request->exists('date')) {
            return response()->json(['error' => 'Empty date'], 400);
        }

        if (!$admin_id = intval($request->input('admin_id'))) {
            return response()->json(['error' => 'Invalid admin_id'], 400);
        }

        if (!$date = Carbon::parse($request->input('date'), $this->tzLocal)) {
            return response()->json(['error' => 'Cannot parse date'], 400);
        }
        $date = Carbon::parse($date->endOfDay(), $this->tzLocal)->timezone($this->tzServer);

        $query = "
            select a.id, a.name, a.active, if(a.deleted_at is not null, 1, 0) as deleted, ap.amount as pay_amount,
                   convert_tz(ap.date, '{$this->tzServer}', '{$this->tzLocal}') as pay_date,
                   convert_tz(ap.created_at, '{$this->tzServer}', '{$this->tzLocal}') as created_at
            from admins as a
            left join (
                select t1.for_admin_id, t1.amount, t1.date, t1.created_at
                from admins_payments as t1
                inner join (
                    select for_admin_id, max(date) as max_date
                    from admins_payments
                    where amount > 0
                    group by for_admin_id
                ) as t2 on t1.for_admin_id = t2.for_admin_id and t1.date =t2.max_date
            ) as ap on a.id = ap.for_admin_id
            where a.id = {$admin_id}";
        $admin = collect(DB::select($query))->first();

        if (!$info = $this->getPaymentInfo($admin, $date)) {
            return response()->json(['error' => 'Cannot get payment info'], 400);
        }

        return response()->json($info['for_pay']['total']);
    }

    /**
     * Информация о платеже модератору
     * @param object $admin Информация о модераторе
     * @param string $date Дата платежа
     * @return array
     */
    public function getPaymentInfo($admin, $date = '')
    {
        $data = [];

        if (!$admin->pay_date) {
            $admin->pay_date = '1970-01-01 00:00:00';
        } else {
            $admin->pay_date = Carbon::parse($admin->pay_date, $this->tzLocal)->timezone($this->tzServer)->toDateTimeString();
        }

        if (!$date) {
            $date = Carbon::now($this->tzLocal)->endOfDay()->timezone($this->tzServer)->toDateTimeString();
        } else {
            $date = Carbon::parse($date)->toDateTimeString();
        }

        // проверка фото
        $query = "
            select cast(ifnull(sum(case when ups.status = -1 then 1 else 0 end), 0) as unsigned) as deferred,
                   cast(ifnull(sum(case when ups.status = 0 then 1 else 0 end), 0) as unsigned) as reset,
                   cast(ifnull(sum(case when ups.status = 1 then 1 else 0 end), 0) as unsigned) as accept,
                   cast(ifnull(sum(case when ups.status = 1 and ups.main_photo = 1 then 1 else 0 end), 0) as unsigned) as accept_main,
                   cast(ifnull(sum(case when ups.status = 1 and ups.main_photo = 0 then 1 else 0 end), 0) as unsigned) as accept_inner,
                   cast(ifnull(sum(case when ups.status = 2 then 1 else 0 end), 0) as unsigned) as forbidden_ava,
                   cast(ifnull(sum(case when ups.status = 3 then 1 else 0 end), 0) as unsigned) as forbidden_show,
                   cast(ifnull(sum(case when ups.status = 4 then 1 else 0 end), 0) as unsigned) as blocked
            from users_photos_status as ups
            inner join (
                select photo_id, max(date) as tz_date
                from users_photos_status
                where admin_id = {$admin->id}
                  and photo_id is not null
                group by photo_id
            ) as x on ups.photo_id = x.photo_id and ups.date = x.tz_date
            where ups.date between '{$admin->pay_date}' and '{$date}'";

        $photo = collect(DB::select($query))->map(function ($x) {
            return (array)$x;
        })->first();

        // баны пользователей
        $query = "
            select cast(ifnull(sum(case when user_id is not null and main_photo = 1 then 1 else 0 end), 0) as unsigned) as acc_main,
                   cast(ifnull(sum(case when device_id is not null and main_photo = 1 then 1 else 0 end), 0) as unsigned) as dev_main,
                   cast(ifnull(sum(case when user_id is not null and main_photo = 0 then 1 else 0 end), 0) as unsigned) as acc_inner,
                   cast(ifnull(sum(case when device_id is not null and main_photo = 0 then 1 else 0 end), 0) as unsigned) as dev_inner
            from (
                 select user_id, device_id, photo_id, main_photo, date
                 from banlist_log
                 where admin_id = {$admin->id}
                   and status = 1
                   and photo_id is not null
                 group by photo_id
             ) x
             where `date` between '{$admin->pay_date}' and '{$date}'";
        $ban = collect(DB::select($query))->map(function ($x) {
            return (array)$x;
        })->first();

        $main = round(($photo['accept_main'] + $photo['blocked'] + $ban['acc_main'] + $ban['dev_main']) * $this->price_main, 2);
        $inner = round(($photo['accept_inner'] + $photo['forbidden_ava'] + $photo['forbidden_show'] + $ban['acc_inner'] + $ban['dev_inner']) * $this->price_inner, 2);
        $total = round($main + $inner, 2);

        if ($admin->pay_amount || $total) {
            $data = [
                'admin'    => [
                    'id'      => $admin->id,
                    'name'    => $admin->name,
                    'active'  => $admin->active,
                    'deleted' => $admin->deleted,
                ],
                'for_pay'  => [
                    'main'  => $main,
                    'inner' => $inner,
                    'total' => $total
                ],
                'last_pay' => [
                    'amount'     => $admin->pay_amount,
                    'date'       => $admin->pay_date,
                    'created_at' => $admin->created_at,
                ]
            ];
        }

        return $data;
    }
}
