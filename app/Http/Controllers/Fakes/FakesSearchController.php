<?php

namespace App\Http\Controllers\Fakes;

use App\Http\Controllers\Controller;
use Datetime;
use DateTimeZone;
use DB;
use Illuminate\Http\Request;

class FakesSearchController extends Controller
{
    protected $limit = 200;

    protected $tz = 'Asia/Krasnoyarsk';

    public function __construct()
    {

    }

    public function getData(Request $request)
    {
        $page = $request->input('page', 1) ?: 1;
        if ($page != intval($page) || $page < 1) {
            $page = 1;
        };

        $limit = $request->input('per_page', 0) ?: $this->limit;
        if ($limit != intval($limit) || $limit < 1) {
            $limit = $this->limit;
        };

        $offset = ($page - 1) * $limit;
        $sort = $request->input('sort', 'user_id|desc');
        $filter = $request->input('filter', []);
        $filter = @json_decode($filter, true);

        $users = DB::table(DB::raw('users as u force index (primary)'))
            ->join('users_info as i', 'i.user_id', '=', 'u.user_id')
            ->join('users_stats as s', 's.user_id', '=', 'u.user_id')
            ->where('u.state', '=', 1)
            ->where('u.email', 'like', '%mail.local');

        if ($filter && is_array($filter)) {
            $filter = [
                'user_id'   => !empty($filter['user_id']) ? intval($filter['user_id']) : '',
                'name'      => !empty($filter['name']) ? $filter['name'] : '',
                'city'      => !empty($filter['city']) ? $filter['city'] : '',
                'gender'    => !empty($filter['gender']) ? $filter['gender'] : '',
                'years_min' => !empty($filter['years_min']) ? intval($filter['years_min']) : '',
                'years_max' => !empty($filter['years_max']) ? intval($filter['years_max']) : '',
                'vip'       => !empty($filter['vip']) ? $filter['vip'] : '',
                'unread'    => !empty($filter['unread']) ? 1 : 0,
            ];

            if ($filter['user_id']) {
                $users->where('i.user_id', '=', $filter['user_id']);
            }

            if ($filter['name']) {
                $users->where('i.name', 'like', '%' . $filter['name'] . '%');
            }

            if ($filter['city']) {
                $users->where('i.city', 'like', '%' . $filter['city'] . '%');
            }

            if (in_array($filter['gender'], ['1', '2'])) {
                $users->where('i.sex', '=', $filter['gender']);
            }

            if ($filter['years_min']) {
                $users->where('i.years', '>=', $filter['years_min']);
            }
            if ($filter['years_max']) {
                $users->where('i.years', '<=', $filter['years_max']);
            }

            if (in_array($filter['vip'], ['yes', 'no'])) {
                $users->where('i.vip', '=', $filter['vip'] == 'yes' ? 1 : 0);
            }

            if ($filter['unread']) {
                $users->having('unread', '>', 0);
            }
        }

        $users->leftJoin('users_photos as ph', 'ph.photo_id', '=', 'i.photo_id')
            ->leftJoin('users_search as us', 'us.user_id', '=', 'u.user_id')
            ->leftJoin(
                DB::raw('
                    (select d.user_id, m.user_id as writer, m.status
                        from messages as m
                        inner join dialog_users as d on d.dialog_id = m.dialog_id
                    ) as m'
                ), function ($join) {
                /* @var $join \Illuminate\Database\Query\JoinClause */
                $join->on('m.user_id', '=', 'u.user_id')
                    ->on('m.writer', '!=', 'u.user_id');
            })
            ->select('i.user_id', 'i.avatar', 'i.name', 'i.sex', 'i.years', 'i.city', 'i.points', 'i.vip',
                'i.reg_date', 'i.last_visit', 'us.gender', 'us.age_min', 'us.age_max', 'us.radius', 'ph.photo',
                's.gifts', 's.vip_order_method as vip_order', 's.vip_expires', 's.messages', 's.dialogs')
            ->selectRaw('count(m.writer) as total_received')
            ->selectRaw('sum(case when m.status != 2 then 1 else 0 end) as unread_received')
            ->groupBy('u.user_id');

        if ($sort) {
            $tmp = explode('|', trim($sort));

            if (is_array($tmp)) {
                if (empty($tmp[1]) || !in_array($tmp[1], ['asc', 'desc'])) {
                    $tmp[1] = 'asc';
                }

                if ($order_fields = explode(',', $tmp[0])) {
                    foreach ($order_fields as $v) {
                        $users->orderBy(trim($v), $tmp[1]);
                    }
                } else {
                    $users->orderBy($tmp[0], $tmp[1]);
                }
            }
        }

        $return = [];

        $users_count = $users->get()->count();
        $users_data = $users->offset($offset)->limit($limit)->get();

        unset($count, $users);

        $tz_local = new DateTimeZone($this->tz);
        $now = new DateTime();

        foreach ($users_data as $u) {
            $rgd = new DateTime($u->reg_date);
            $rgd->setTimezone($tz_local);

            $lvd = new DateTime();
            $lvd->setTimestamp($u->last_visit);
            $lvd->setTimezone($tz_local);

            $vip_expires_days = null;
            if ($u->vip_expires) {
                $vip_expires = new DateTime($u->vip_expires);
                $vip_expires_days .= $now->diff($vip_expires)->format('%r%a (%H:%I)');
            }

            $rs = [
                'user_id'         => $u->user_id,
                'avatar'          => (!empty($u->avatar) ? (env('STATIC_AVA_URL') . $u->avatar) : ''),
                'photo'           => (!empty($u->photo) ? (env('STATIC_PHOTO_URL') . $u->photo) : ''),
                'name'            => $u->name,
                'gender'          => $u->sex,
                'years'           => $u->years,
                'city'            => $u->city,
                'achievements'    => (!empty($u->ach) ? explode(':', $u->ach) : []),
                'gifts'           => $u->gifts,
                'points'          => $u->points,
                'vip'             => [
                    'status'  => (int)$u->vip,
                    'payment' => (int)in_array($u->vip_order, ['itunes', 'googlepay']),
                    'expires' => $vip_expires_days,
                    'vip_adm' => $u->vip_order == 'skylove' ? 1 : 0,
                ],
                'search_params'   => [
                    'gender'    => ($u->gender == 0 ? 'Всех' : ($u->gender == 1 ? 'М' : 'Ж')),
                    'age_range' => $u->age_min . '-' . (int)$u->age_max,
                    'radius'    => round((int)$u->radius / 1000, 2),
                ],
                'reg_date'        => $rgd->format('Y-m-d H:i:s'),
                'last_visit'      => $lvd->format('Y-m-d H:i:s'),
                'dialogs'         => $u->dialogs ?: 0,
                'total_received'  => $u->total_received,
                'unread_received' => $u->unread_received
            ];

            $return[] = $rs;
        }

        $response = [
            'total'         => (int)$users_count,
            'per_page'      => (int)$limit,
            'current_page'  => (int)$page,
            'last_page'     => ceil($users_count / $limit),
            'next_page_url' => null,
            'prev_page_url' => null,
            'from'          => intval($offset + 1),
            'to'            => intval($page * $limit),
            'data'          => $return,
        ];

        return response()->json($response);
    }
}
