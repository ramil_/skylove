<?php

namespace App\Http\Controllers\Fakes;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Validator;
use Exception;

use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class FakesDialogs extends Controller
{
    public function __construct()
    {

    }

    public function getUserData($user_id)
    {
        $client = new Client(); 
        $ulr = env('SKYLOVE_API_URL') . 'profile/index';
        try {
            $response = $client->request('GET', $ulr, [
                'headers' => [
                    'authentication' => $this->generateAuthToken($user_id)
                ]
            ]);
        } catch(GuzzleException $e) {
            throw new Exception('Server request error');
        }

        $data = $response->getBody()->getContents();

        return response($data);
    }

    public function getChats($user_id)
    {
        $client = new Client(); 
        $ulr = env('SKYLOVE_API_URL') . 'chat/chats';
        try {
            $response = $client->request('GET', $ulr, [
                'headers' => [
                    'authentication' => $this->generateAuthToken($user_id)
                ]
            ]);
        } catch(GuzzleException $e) {
            throw new Exception('Server request error');
        }

        $data = $response->getBody()->getContents();

        return response($data);
    }

    public function getChatMessages($user_id, $сhat_id)
    {
        $client = new Client(); 
        $ulr = env('SKYLOVE_API_URL') . 'chat/previous_messages?chat_id=' . $сhat_id . '&count=1000&';
        try {
            $response = $client->request('GET', $ulr, [
                'headers' => [
                    'authentication' => $this->generateAuthToken($user_id)
                ]
            ]);
        } catch(GuzzleException $e) {
            throw new Exception('Server request error');
        }

        $data = $response->getBody()->getContents();

        return response($data);
    }

    public function setReadMessages($user_id, $сhat_id)
    {
        $client = new Client(); 
        $ulr = env('SKYLOVE_API_URL') . 'chat/read?chat_id=' . $сhat_id;
        try {
            $response = $client->request('GET', $ulr, [
                'headers' => [
                    'authentication' => $this->generateAuthToken($user_id)
                ]
            ]);
        } catch(GuzzleException $e) {
            throw new Exception('Server request error');
        }

        $data = $response->getBody()->getContents();

        return response($data);
    }

    public function sendChatMessage(Request $request, $user_id)
    {
        $userInput = $request->all();

        $client = new Client(); 
        $ulr = env('SKYLOVE_API_URL') . 'chat/message';
        try {
            $response = $client->request('POST', $ulr, [
                'headers' => [
                    'authentication' => $this->generateAuthToken($user_id)
                ],
                'form_params' => $userInput
            ]);
        } catch(GuzzleException $e) {
            throw new Exception('Server request error');
        }

        $data = $response->getBody()->getContents();

        return response($data);
    }

    // эта функция "подделывает" токен фейка с целью отправки действия
    // типа как от лица реального фейка на стороне API
    // чтобы не дублировать уже написанный функционал диалогов
    private function generateAuthToken($user_id) 
    {
        // нужно получить secret и session_id aka sid
        // на всякий случай перестраховка, JOIN на таблицу с LIKE mail.local
        $query = "SELECT us.id, us.secret
        FROM users_sessions AS us
        LEFT JOIN users AS u ON u.user_id = us.user_id
        WHERE us.user_id = " . (int)$user_id . " AND u.email LIKE '%mail.local'";

        $sessions = DB::select($query);

        if(count($sessions) <= 0) {
            throw new Exception('Отсутствуют активные сессии');
        } else {
            $secret = (string)$sessions[0]->secret;
            $sid = (int)$sessions[0]->id;
        }

        $header = json_encode(['typ' => 'JWT', 'alg' => 'HS256']);
        $payload = json_encode(['uid' => $user_id, 'sid' => $sid]);

        // Encode
        $base64UrlHeader = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($header));
        $base64UrlPayload = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($payload));

        // Create Signature Hash & encode
        $signature = hash_hmac('sha256', $base64UrlHeader . "." . $base64UrlPayload, $secret, true);
        $base64UrlSignature = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($signature));

        // Create JWT
        $jwt = $base64UrlHeader . "." . $base64UrlPayload . "." . $base64UrlSignature;

        return $jwt;
    }
}
