<?php

namespace App\Http\Controllers\Fakes;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;

use Validator;

class FakesController extends Controller
{
    public function __construct()
    {

    }

    public function get($id)
    {
        $id = intval($id);
        $response = [];

        $fake = DB::table('users as u')
            ->join('users_info as ui', 'u.user_id', '=', 'ui.user_id')
            ->join('users_photos as up', 'u.user_id', '=', 'up.user_id')
            ->join('users_search as us', 'u.user_id', '=', 'us.user_id')
            ->select('ui.name', 'ui.years', 'ui.sex', 'ui.latitude', 'ui.longitude', 'ui.city',
                'ui.avatar', 'up.photo', 'us.age_min as search_age_min', 'us.age_max as search_age_max',
                'us.gender as search_gender')
            ->where('u.user_id', $id)
            ->where('u.email', 'like', '%mail.local')
            ->where('u.state', 1)
            ->get()
            ->first();

        if ($fake) {
            $response = [
                'name'            => $fake->name,
                'age'             => $fake->years,
                'gender'          => $fake->sex,
                'latitude'        => $fake->latitude,
                'longitude'       => $fake->longitude,
                'city'            => $fake->city,
                'search_age_min'  => $fake->search_age_min,
                'search_age_max'  => $fake->search_age_max,
                'search_gender'   => $fake->search_gender,
                'avatar_link'     => ($fake->avatar != '' ? (env('STATIC_AVA_URL') . $fake->avatar) : ''),
                'main_photo_link' => ($fake->photo != '' ? (env('STATIC_PHOTO_URL') . $fake->photo) : ''),
            ];
        }

        return response()->json($response);
    }

    public function add(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'name'           => 'required',
            'age'            => 'required|integer|min:18|max:100',
            'gender'         => 'required|integer|in:1,2',
            'latitude'       => 'required|numeric',
            'longitude'      => 'required|numeric',
            'city'           => 'required|alpha_dash',
            'search_gender'  => 'required|integer|in:0,1,2',
            'search_age_min' => 'required|integer|min:18|max:100',
            'search_age_max' => 'required|integer|min:18|max:100',
        ]);

        if ($validation->fails()) {
            return response(json_encode([
                'error'      => 'Некорректные данные формы.',
                'error_data' => $validation->errors()
            ]), 400);
        }

        $time = time();
        $user_id = false;

        $user_id = DB::table('users')->insertGetId([
            'email'  => $time . '@fake.mail.local',
            'passwd' => password_hash(md5($time), PASSWORD_DEFAULT),
            'state'  => 1
        ],
            'user_id'
        );

        if ($user_id) {
            DB::table('users_info')->insert([
                'user_id'    => $user_id,
                'city'       => $request->input('city'),
                'name'       => $request->input('name'),
                'sex'        => $request->input('gender'),
                'birth_date' => null,
                'years'      => $request->input('age'),
                'last_visit' => $time,
                'latitude'   => $request->input('latitude'),
                'longitude'  => $request->input('longitude')
            ]);
            DB::table('users_search')->insert([
                'user_id' => $user_id,
                'gender'  => $request->input('search_gender'),
                'age_min' => $request->input('search_age_min'),
                'age_max' => $request->input('search_age_max'),
                'radius'  => 1000
            ]);
        }

        return response()->json(['fake_id' => $user_id]);
    }

    public function update(Request $request, $id)
    {
        $response = [
            'status' => 1,
            'id'     => $id,
            'gender' => $request->input('gender')
        ];

        $update = [];
        if (!empty((string)$request->input('name'))) {
            $update['name'] = (string)$request->input('name');
        }

        if ((int)$request->input('age') >= 18) {
            $update['years'] = (int)$request->input('age');
        }

        if (in_array($request->input('gender'), ['1', '2'])) {
            $update['sex'] = (int)$request->input('gender');
        }

        if (!empty((string)$request->input('latitude'))) {
            $update['latitude'] = (float)$request->input('latitude');
        }

        if (!empty((string)$request->input('longitude'))) {
            $update['longitude'] = (float)$request->input('longitude');
        }

        if (!empty((string)$request->input('city'))) {
            $update['city'] = (string)$request->input('city');
        }

        if (count($update) > 0) {
            DB::table('users_info')
                ->where('user_id', $id)
                ->update($update);
        }

        $update_search = [];
        if (in_array($request->input('search_gender'), ['0', '1', '2'])) {
            $update_search['gender'] = (int)$request->input('search_gender');
        }

        if ((int)$request->input('search_age_min') >= 18) {
            $update_search['age_min'] = (int)$request->input('search_age_min');
        }

        if ((int)$request->input('search_age_max') >= 18) {
            $update_search['age_max'] = (int)$request->input('search_age_max');
        }

        if (count($update_search) > 0) {
            DB::table('users_search')
                ->where('user_id', $id)
                ->update($update_search);
        }
        return response()->json($response);
    }

    public function replaceAvatar(Request $request, $id)
    {
        $validation = Validator::make($request->all(), [
            'avatar' => 'required|image'
        ]);

        if ($validation->fails()) {
            return response(json_encode(['error' => 'Файл отсутствует или не является изображением.']), 400);
        }

        $avatarDir = 'avatars/00000';
        $avatarName = [
            'avatar'         => md5($id . 12345) . '.' . $request->avatar->extension(),
            'avatar_blurred' => md5($id . 12345 . 'blurred') . '.' . $request->avatar->extension()
        ];

        if (!Storage::disk('s3')->putFileAs($avatarDir, $request->file('avatar'), $avatarName['avatar'], 'public')) {
            return response(json_encode(['error' => 'Ошибка загрузки автара в хранилище.']), 400);
        }

        // размытие аватара
        $avatarContent = Storage::disk('s3')->get($avatarDir . '/' . $avatarName['avatar']);
        $blurred = new \Imagick();
        $blurred->readImageBlob($avatarContent);
        $blurred->blurImage(10, 10);

        if (!Storage::disk('s3')->put($avatarDir . '/' . $avatarName['avatar_blurred'], $blurred->getImageBlob(), 'public')) {
            return response(json_encode(['error' => 'Ошибка загрузки размытого автара в хранилище.']), 400);
        }
        $blurred->destroy();

        $updateAvatar = [
            'avatar'         => '00000/' . $avatarName['avatar'],
            'avatar_blurred' => '00000/' . $avatarName['avatar_blurred']
        ];

        DB::table('users_info')
            ->where('user_id', $id)
            ->update($updateAvatar);

        $href = env('STATIC_AVA_URL') . $updateAvatar['avatar'];

        return response()->json(['src' => $href]);
    }

    public function deleteAvatar($id)
    {
        $oldAvatar = self::getPathAvatar($id);

        if ($oldAvatar->avatar && Storage::disk('s3')->exists('avatars/' . $oldAvatar->avatar)) {
            Storage::disk('s3')->delete('avatars/' . $oldAvatar->avatar);
        }

        if ($oldAvatar->avatar_blurred && Storage::disk('s3')->exists('avatars/' . $oldAvatar->avatar_blurred)) {
            Storage::disk('s3')->delete('avatars/' . $oldAvatar->avatar_blurred);
        }

        $updateAvatar = [
            'avatar'         => '',
            'avatar_blurred' => ''
        ];

        DB::table('users_info')
            ->where('user_id', $id)
            ->update($updateAvatar);

        return response()->json([]);
    }

    public function getPathAvatar($id)
    {
        return DB::table('users_info')
            ->select(['avatar', 'avatar_blurred'])
            ->where('user_id', $id)
            ->first();
    }

    public function replaceMainPhoto(Request $request, $id)
    {
        $validation = Validator::make($request->all(), [
            'mainphoto' => 'required|image'
        ]);

        if ($validation->fails()) {
            return response(json_encode(['error' => 'Файл отсутствует или не является изображением.']), 400);
        }

        $storagePath = Storage::disk('s3')->put("photo/00000", $request->file('mainphoto'), 'public');
        if (!$storagePath || empty((string)$storagePath)) {
            return response(json_encode(['error' => 'Ошибка загрузки файла в хранилище.']), 400);
        }

        $path = str_replace('photo/', '', $storagePath);
        $href = '';

        $oldPhotos = DB::table('users_photos')
            ->where('user_id', $id)
            ->pluck('photo');

        $photo_id = DB::table('users_photos')->insertGetId(
            ['user_id' => $id, 'photo' => $path, 'width' => 0, 'height' => 0],
            'photo_id'
        );

        DB::table('users_photos')
            ->where('user_id', $id)
            ->where('photo_id', '<>', $photo_id)
            ->delete();

        if ($photo_id) {
            DB::table('users_info')
                ->where('user_id', $id)
                ->update(['photo_id' => $photo_id]);

            $href = env('STATIC_PHOTO_URL') . $path;
        }

        return response()->json(['src' => $href]);
    }

    public function deleteMainPhoto($id)
    {
        DB::table('users_info')
            ->where('user_id', $id)
            ->update(['photo_id' => null]);

        return response()->json([]);
    }

    public function deleteFake($id)
    {
        DB::table('users')
            ->where('user_id', $id)
            ->update(['state' => 0]);

        return response()->json([]);
    }

    /**
     * Изменение vip статуса пользователям
     * @param array $users Список пользователей
     * @param integer $state Статус (0/1)
     */
    private function setVipState(array $users, $state = 0)
    {
        if (count($users) > 0) {
            DB::table('users_info')
                ->whereIn('user_id', $users)
                ->update([
                    'vip' => (int)$state
                ]);
        }
    }

    /**
     * Добавление vip статуса с периодом действия
     * @param Request $request Список пользователей
     * @return json
     */
    public function vipAdd(Request $request)
    {
        $data = [];
        $fakes = $request->input('fakes');
        $period = (int)$request->input('period');

        if (!is_array($fakes) || count($fakes) < 1) {
            return response()->json(['error' => 'empty users']);
        }

        if (!$period) {
            return response()->json(['error' => 'empty period']);
        }

        $expires = strtotime("+" . $period . " days");
        $expires += 3600;

        foreach ($fakes as $v) {
            if (!intval($v)) {
                continue;
            }
            $data[] = [
                'user_id'      => (int)$v,
                'order_method' => 'skylove',
                'order_id'     => 0,
                'begin'        => time(),
                'expires'      => $expires,
                'status'       => 1
            ];
        }

        if (!$data) {
            return response()->json(['error' => 'Empty data for vip_duration']);
        }

        DB::table('vip_duration')->insert($data);
        $this->setVipState($fakes, 1);

        return response()->json([]);
    }

    /**
     * Удаление vip статуса пользователям
     * @param Request $request Список пользователей
     * @return json
     */
    public function vipRemove(Request $request)
    {
        $fakes = $request->input('fakes');

        DB::table('vip_duration')
            ->where('order_method', 'skylove')
            ->whereIn('user_id', $fakes)
            ->delete();
        self::setVipState($fakes, 0);

        return response()->json([]);
    }

    private function normalize_result($result)
    {
        $return = [
            'name'            => (string)$result[0]->name,
            'age'             => (int)$result[0]->years,
            'gender'          => (int)$result[0]->sex,
            'latitude'        => (float)$result[0]->latitude,
            'longitude'       => (float)$result[0]->longitude,
            'city'            => (string)$result[0]->city,
            'search_age_min'  => (int)$result[0]->age_min,
            'search_age_max'  => (int)$result[0]->age_max,
            'search_gender'   => (int)$result[0]->gender,
            'avatar_link'     => (string)((string)$result[0]->avatar != '' ? (env('STATIC_AVA_URL') . $result[0]->avatar) : ''),
            'main_photo_link' => (string)((string)$result[0]->photo != '' ? (env('STATIC_PHOTO_URL') . $result[0]->photo) : ''),
        ];

        return $return;
    }
}
