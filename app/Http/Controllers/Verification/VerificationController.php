<?php

namespace App\Http\Controllers\Verification;

use App\Http\Controllers\UsersPhotos\UsersPhotosStatusController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Http\Traits\GeoTimezone;
use Illuminate\Support\Facades\Redis;

class VerificationController extends Controller
{
    use GeoTimezone;

    protected $limit = 100;

    protected $balance_description = 'VERIFICATION_PHOTO';

    protected $balance_points = 300;

    protected $rating_verification = 30;

    public function __construct()
    {
        if (!$this->setGeoTimezone()) {
            return response()->json(['error' => 'Cannot get timezone'], 500);
        }

        return true;
    }

    public function getData(Request $request)
    {
        $limit = $request->input('limit', 0);
        if ($limit != intval($limit) || $limit < 1 || $limit > $this->limit) {
            $limit = 0;
        };

        $page = $request->input('page', 1);
        if ($page != intval($page) || $page < 1) {
            $page = 1;
        };

        if ($limit) {
            $offset = $page * $this->limit - $limit;
            $this->limit = $limit;
        } else {
            $offset = ($page - 1) * $this->limit;
        }

        if ($filter = $request->input('filter', [])) {
            $filter = json_decode($filter, true);
            if (json_last_error() !== 0) {
                $filter = [];
            }
        }

        $photos = DB::table('users_verification as uv')
            ->join('users as u', 'uv.user_id', '=', 'u.user_id')
            ->join('users_info as ui', 'uv.user_id', '=', 'ui.user_id')
            ->join('users_stats as us', 'uv.user_id', '=', 'us.user_id')
            ->join('users_photos as up', 'uv.photo_id', '=', 'up.photo_id')
            ->where('u.state', '=', 1)
            ->where('us.banned_acc', '=', 0)
            ->where('us.banned_dev', '=', 0)
            ->where('us.blocked_photo', '=', 0);

        if (!empty($filter['user_id']) && intval($filter['user_id']) > 0) {
            $photos->where('u.user_id', '=', intval($filter['user_id']));
        }

        if (!empty($filter['name'])) {
            $photos->where('ui.name', 'like', '%' . $filter['name'] . '%');
        }

        if (!empty($filter['city'])) {
            $photos->where('ui.city', 'like', '%' . $filter['city'] . '%');
        }

        if (!empty($filter['gender']) && in_array($filter['gender'], [1, 2])) {
            $photos->where('ui.sex', '=', $filter['gender']);
        }

        if (!empty($filter['years_min']) && intval($filter['years_min']) > 0) {
            $photos->where('ui.years', '>=', intval($filter['years_min']));
        }

        if (!empty($filter['years_max']) && intval($filter['years_max']) > 0) {
            $photos->where('ui.years', '<=', intval($filter['years_max']));
        }

        if (!empty($filter['vip']) && in_array($filter['vip'], ['yes', 'no'])) {
            if ($filter['vip'] == 'yes') {
                $photos->where('ui.vip', '=', '1');
            } elseif ($filter['vip'] == 'no') {
                $photos->where('ui.vip', '=', '0');
            }
        }

        if (!empty($filter['platform']) && in_array($filter['platform'], ['ios', 'android'])) {
            $photos->where('us.app_version', 'like', '%' . $filter['platform'] . '%');
        }

        if (in_array($filter['online'], ['online', 'visited', 'offline'])) {
            if ($filter['online'] == 'online') {
                $photos->where('ui.last_visit', '>', time() - 600);
            } elseif ($filter['online'] == 'visited') {
                $photos->where(function ($query) {
                    /* @var $query \Illuminate\Database\Query\Builder */
                    $query->where('ui.last_visit', '<', time() - 600)
                        ->where('ui.last_visit', '>', time() - 21600);
                });
            } else {
                $photos->where('ui.last_visit', '<', time() - 21600);
            }
        }

        if (!empty($filter['created_at_min'])) {
            if ($date = (new \Carbon\Carbon($filter['created_at_min']))->startOfDay()) {
                if ($date_s = $this->tzConvertToUTC($date)) {
                    $photos->where('uv.verified_at', '>', $date_s);
                }
            }
        }

        if (!empty($filter['created_at_max'])) {
            if ($date = (new \Carbon\Carbon($filter['created_at_max']))->endOfDay()) {
                if ($date_e = $this->tzConvertToUTC($date)) {
                    $photos->where('uv.verified_at', '<', $date_e);
                }
            }
        }

        if (!empty($filter['verification']) && in_array($filter['verification'], ['na', 'accept', 'reject'])) {
            if ($filter['verification'] == 'na') {
                $photos->where('uv.status', '=', 0);
            } elseif ($filter['verification'] == 'accept') {
                $photos->where('uv.status', '=', 1);
            } else {
                $photos->where('uv.status', '=', 2);
            }
        }

        $count = clone $photos;
        $count->selectRaw('count(*) as count');
        $count_data = $count->pluck('count')->first();

        $photos->leftJoin('admins as a', 'uv.admin_id', '=', 'a.id')
            ->select('up.photo_id', 'up.photo', 'uv.selfie', 'uv.created_at', 'uv.verified_at', 'uv.status', 'a.name as verification_admin',
                'ui.user_id', 'ui.name', 'ui.years', 'ui.city', 'ui.avatar', 'ui.vip', 'us.banned_acc', 'us.banned_dev', 'us.app_version')
            ->selectRaw('unix_timestamp() - ui.last_visit as last_visit_sec')
            ->orderByRaw('case when uv.status > 0 and uv.verified_at is not null then uv.verified_at end desc')
            ->orderBy('uv.created_at', 'asc')
            ->orderBy('uv.photo_id', 'desc');
        $photos_data = $photos->offset($offset)->limit($this->limit)->get();

        $data = [];
        foreach ($photos_data as $v) {
            $created_at = $v->created_at ? $this->tzConvertToLocal($v->created_at) : '';
            $verified_at = $v->verified_at ? $this->tzConvertToLocal($v->verified_at) : '';

            $row = [
                'photo_id'     => $v->photo_id,
                'photo_prev'   => env('STATIC_GALLERY_PREW_URL') . $v->photo,
                'photo_href'   => env('STATIC_GALLERY_URL') . $v->photo,
                'selfie_prev'  => env('STATIC_GALLERY_PREW_URL') . $v->selfie,
                'selfie_href'  => env('STATIC_GALLERY_URL') . $v->selfie,
                'verification' => [
                    'status'      => $v->status,
                    'admin'       => $v->verification_admin ? $v->verification_admin : '',
                    'created_at'  => $created_at,
                    'verified_at' => $v->status > 0 && $verified_at ? $verified_at : '',
                ],
                'user'         => [
                    'user_id'     => $v->user_id,
                    'name'        => $v->name,
                    'years'       => $v->years,
                    'city'        => $v->city,
                    'avatar'      => env('STATIC_AVA_URL') . $v->avatar,
                    'vip'         => $v->vip,
                    'online'      => $v->last_visit_sec < 600 ? 2 : ($v->last_visit_sec < 21600 ? 1 : 0),
                    'banned_acc'  => $v->banned_acc,
                    'banned_dev'  => $v->banned_dev,
                    'app_version' => array_unique(explode(':', $v->app_version)),
                ]
            ];
            $data[] = $row;
        }

        if (isset($count_data)) {
            $response = [
                'from'         => intval($offset + 1),
                'to'           => intval($page * $this->limit),
                'total'        => intval($count_data),
                'current_page' => intval($page),
                'last_page'    => intval(ceil($count_data / $this->limit)),
                'data'         => $data,
                'count'        => count($data)
            ];
            if ($response['from'] > $response['total']) {
                $response['from'] = $response['total'];
            }
            if ($response['to'] > $response['total']) {
                $response['to'] = $response['total'];
            }
        } else {
            $response = $data;
        }

        return response()->json($response);
    }

    /**
     * Пометка фото как проверенных
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function verificationPhoto(Request $request)
    {
        $photos = $request->input('photos');
        $status = $request->input('status');
        $admin_id = \Auth::user()->id;

        if (!in_array($status, [0, 1, 2])) {
            return response()->json([], 400);
        }

        $photos_id = [];
        foreach ($photos as $photo) {
            if ($photo == intval($photo)) {
                $photos_id[] = $photo;
            }
        }

        $photos_id = DB::table('users_photos')
            ->whereIn('photo_id', $photos_id)
            ->pluck('photo_id')
            ->toArray();

        if (!$photos_id) {
            return response()->json([], 400);
        }

        $r = Redis::connection();

        if ($status == 1) {
            $users = DB::table('users_verification')
                ->whereIn('photo_id', $photos_id)
                ->groupBy('user_id')
                ->pluck('user_id')
                ->toArray();

            $ignore = DB::table('balance_history')
                ->whereIn('user_id', $users)
                ->where('description', '=', $this->balance_description)
                ->groupBy('user_id')
                ->pluck('user_id')
                ->toArray();

            // помечаем фото как проверенное в разделе проверка фото
            $verificationRequest = new Request();
            $verificationRequest->setMethod('POST');
            $verificationRequest->request->add(['photos' => $photos_id, 'status' => 1]);

            // @TODO: подумать над верификацией основного фото
            /*try {
                (new UsersPhotosStatusController())->setPhotoStatus($verificationRequest);
            } catch (\Exception $e) {
                return response()->json($e->getMessage(), 500);
            }*/

            // начисление баллов за первое верифицированное фото
            if ($points = array_diff($users, $ignore)) {
                $history = [];
                foreach ($points as $v) {
                    $history[] = [
                        'user_id'     => $v,
                        'transaction' => 'internal',
                        'description' => $this->balance_description,
                        'amount'      => $this->balance_points,
                        'date'        => now()->timestamp,
                        'tz'          => '+0000',
                    ];
                }

                if ($history) {
                    // начисление баллов
                    DB::table('users_info')
                        ->whereIn('user_id', $points)
                        ->increment('points', $this->balance_points);
                    DB::table('balance_history')->insert($history);
                    // начисление рейтинга
                    DB::table('users_info')
                        ->whereIn('user_id', $points)
                        ->increment('rating', $this->rating_verification);
                }
            }

            // данные для push-уведомления
            $push_data = DB::table('users_info')
                ->whereIn('user_id', $users)
                ->get(['user_id', 'name', 'years as age', 'sex', 'city', 'avatar'])
                ->toArray();
            foreach ($push_data as $k => $push) {
                $push->avatar = $push->avatar ? env('STATIC_AVA_URL') . $push->avatar : '';
                $push->points = !in_array($push->user_id, $ignore) ? $this->balance_points : 0;

                $r->publish('fcm_push', json_encode([
                    'user_id'                  => $push->user_id,
                    'title'                    => 'profile_verification_adm_title',
                    'body'                     => 'profile_verification_adm_body',
                    'push_type'                => 'profile_verification_adm',
                    'profile_verification_adm' => $push,
                ]));
            }
        } else {
            $alerts = [];
            $users = DB::table('users_photos')
                ->whereIn('photo_id', $photos_id)
                ->pluck('user_id');
            foreach ($users as $user) {
                $alerts[] = [
                    'user_id'  => $user,
                    'type'     => 2,
                    'priority' => 200,
                ];
            }

            if ($alerts) {
                DB::table('users_alerts')
                    ->whereIn('user_id', $users)
                    ->where('type', '=', 2)
                    ->whereNull('read_at')
                    ->delete();
                DB::table('users_alerts')->insert($alerts);
            }
        }

        DB::table('users_verification')
            ->whereIn('photo_id', $photos_id)
            ->update(['status' => $status, 'admin_id' => $admin_id]);

        $verification = $status == 1 ? 1 : 0;
        DB::table('users_info')
            ->whereIn('photo_id', $photos_id)
            ->update(['verification' => $verification]);

        // отправка push-уведомлений
        foreach ($users as $user) {
            $r->publish('fcm_push', json_encode([
                'user_id'         => $user,
                'push_type'       => 'system_msg',
                'system_msg_data' => [
                    'type' => 'refresh_badges',
                    'data' => []
                ]
            ]));
        }

        return response()->json([]);
    }
}
