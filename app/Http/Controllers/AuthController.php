<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Validator;

class AuthController extends Controller
{
    protected $redirectPath = '/';

    protected $loginPath = '/login';

    public function login()
    {
        $credentials = request(['email', 'password']);

        $rules = [
            'email'    => 'required|email',
            'password' => 'required',
        ];
        $validator = Validator::make($credentials, $rules);
        if ($validator->fails()) {
            return response()->json(['success' => false, 'error' => $validator->messages()], 400);
        }

        $credentials['active'] = 1;
        $credentials['deleted_at'] = null;

        if (!$token = auth()->setTTL(1200)->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        return $this->respondWithToken($token);
    }

    public function logout()
    {
        auth()->logout();
    }

    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token'      => $token,
            'token_auth_string' => 'Bearer ' . $token,
            'token_type'        => 'bearer',
            'expires_in'        => auth()->factory()->getTTL() * 60
        ]);
    }

    public function getPermissions()
    {
        $permissions = [];
        $user = Auth::user();

        if ($user) {
            $permissions = $user->permissions()->pluck('permission')->toArray();
        }

        return response()->json($permissions);
    }

    public function getCurrentUser()
    {
        if ($user = Auth::user()) {
            return response()->json($user);
        }

        return response()->json([], 404);
    }
}
