<?php

namespace App\Http\Controllers\Settings\Admins;

use App\User;
use App\Permission;
use Auth;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class AdminsController extends Controller
{
    protected $limit = 200;

    public function getData(Request $request)
    {
        $page = $request->input('page', 1) ?: 1;
        if ($page != intval($page) || $page < 1) {
            $page = 1;
        };

        $limit = $request->input('per_page', 0) ?: $this->limit;
        if ($limit != intval($limit) || $limit < 1) {
            $limit = $this->limit;
        };

        $offset = ($page - 1) * $limit;
        $sort = $request->input('sort', 'id|desc');
        $filter = $request->input('filter', []);
        $filter = @json_decode($filter, true);

        $admins = DB::table('admins')
            ->select('id', 'name', 'email', 'active', 'deleted_at', 'level');

        if ($sort) {
            $tmp = explode('|', $sort);

            if (is_array($tmp)) {
                if (empty($tmp[1]) || !in_array($tmp[1], ['asc', 'desc'])) {
                    $tmp[1] = 'asc';
                }

                if ($order_fields = explode(',', $tmp[0])) {
                    foreach ($order_fields as $v) {
                        $admins->orderBy(trim($v), $tmp[1]);
                    }
                } else {
                    $admins->orderBy($tmp[0], $tmp[1]);
                }
            }
        }

        if ($filter && is_array($filter)) {
            $filter = [
                'id'      => !empty($filter['id']) ? intval($filter['id']) : '',
                'name'    => !empty($filter['name']) ? $filter['name'] : '',
                'email'   => !empty($filter['email']) ? $filter['email'] : '',
                'deleted' => !empty($filter['deleted']) ? $filter['deleted'] : '',
            ];

            if ($filter['id']) {
                $admins->where('i.user_id', '=', $filter['id']);
            }

            if ($filter['name']) {
                $admins->where('name', 'like', '%' . $filter['name'] . '%');
            }

            if ($filter['email']) {
                $admins->where('email', 'like', '%' . $filter['email'] . '%');
            }
        }

        if (empty($filter['deleted'])) {
            $admins->whereNull('deleted_at');
        }

        $return = [];
        $count = $admins->get()->count();
        $admins_data = $admins->offset($offset)->limit($limit)->get()->toArray();

        foreach ($admins_data as $v) {
            $return[] = [
                'id'         => $v->id,
                'name'       => $v->name,
                'email'      => $v->email,
                'active'     => $v->active,
                'deleted_at' => $v->deleted_at,
                'level'      => $v->level,
            ];
        }

        $response = [
            'total'         => (int)$count,
            'per_page'      => (int)$limit,
            'current_page'  => (int)$page,
            'last_page'     => ceil($count / $limit),
            'next_page_url' => null,
            'prev_page_url' => null,
            'from'          => intval($offset + 1),
            'to'            => intval($page * $limit),
            'data'          => $return
        ];

        return response()->json($response);
    }

    public function get($id)
    {
        $user = User::all('id', 'name', 'email', 'active', 'deleted_at', 'level')
            ->where('id', $id)
            ->where('deleted_at', '=', null)
            ->first();

        if (!$user) {
            return response()->json([]);
        }

        $response = [
            'id'          => $user->id,
            'name'        => $user->name,
            'email'       => $user->email,
            'active'      => $user->active,
            'permissions' => $user->permissions->pluck('id')->toArray(),
            'level'       => $user->level,
        ];

        return response()->json($response);
    }

    public function add(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'name'     => 'required',
            'email'    => 'required|unique:admins|email',
            'password' => 'required|confirmed|min:6',
            'level'    => 'required|integer',
            'active'   => 'required|integer|in:0,1',
        ]);

        if ($validation->fails()) {
            return response(json_encode([
                'error'      => 'Некорректные данные формы.',
                'error_data' => $validation->errors(),
            ]), 400);
        }

        $id = DB::table('admins')->insertGetId([
            'name'     => $request->input('name'),
            'email'    => $request->input('email'),
            'password' => password_hash($request->input('password'), PASSWORD_DEFAULT),
            'level'    => $request->input('level'),
            'active'   => $request->input('active'),
        ]);


        $user = User::find($id);
        $sync = [];
        foreach ($request->input('permissions') as $v) {
            $sync[] = (int)$v;
        }
        $user->permissions()->sync($sync);

        return response()->json(['id' => $id]);
    }

    public function update(Request $request, $id)
    {
        $id = intval($id);

        $validation = Validator::make($request->all(), [
            'name'     => 'required',
            'email'    => 'required|email|unique:admins,email,' . $id,
            'password' => 'sometimes|nullable|confirmed|min:6',
            'level'    => 'required|integer',
            'active'   => 'required|integer|in:0,1',
        ]);

        if ($validation->fails()) {
            return response(json_encode([
                'error'      => 'Некорректные данные формы.',
                'error_data' => $validation->errors(),
            ]), 400);
        }

        $update = [
            'name'   => $request->input('name'),
            'email'  => $request->input('email'),
            'active' => $request->input('active'),
        ];

        if (!empty($request->input('password'))) {
            $update['password'] = password_hash($request->input('password'), PASSWORD_DEFAULT);
        }

        if ($id !== Auth::User()->id) {
            $update['level'] = $request->input('level');
        }

        $user = User::find($id);
        $user->update($update);

        if ($id !== Auth::User()->id) {
            $sync = [];
            foreach ($request->input('permissions') as $v) {
                $sync[] = (int)$v;
            }
            $user->permissions()->sync($sync);
        }

        return response()->json(['id' => $id]);
    }

    public function delete($id)
    {
        $id = intval($id);

        if (!$current = Auth::User()->id) {
            return response()->json(['error' => 'Не удалось получить id текущего пользователя'], 400);
        }

        if ($id == $current) {
            return response()->json(['error' => 'Вы не можете удалить свою учетную запись'], 400);
        }

        DB::table('admins')
            ->where('id', $id)
            ->update([
                'email'      => null,
                'active'     => 0,
                'deleted_at' => DB::raw('now()')
            ]);

        return response()->json([]);
    }

    public function getPermissions()
    {
        $tree = [];

        $result = Permission::orderBy('parent_id')
            ->orderBy('sort')
            ->get(['id', 'permission', 'title', 'parent_id'])
            ->toArray();

        if ($result) {
            $tree = $this->treePermission($result);
        }

        return response()->json($tree);
    }

    private function treePermission($permissions, $parent_id = null)
    {
        $tree = [];

        foreach ($permissions as $permission) {
            if ($permission['parent_id'] == $parent_id) {
                $nodes = $this->treePermission($permissions, $permission['id']);
                if ($nodes) {
                    $permission['nodes'] = $nodes;
                }
                $tree[$permission['id']] = $permission;
            }
        }

        return $tree;
    }
}
