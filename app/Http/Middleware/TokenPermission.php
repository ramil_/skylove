<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class TokenPermission extends Controller
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request  $request
     * @param Closure $next
     * @param string $required
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function handle($request, Closure $next, $required = '')
    {
        $deny = false;

        if ($user = Auth::User()) {
            if ($user->active == 0 || $user->deleted_at) {
                return response()->json(['error' => 'Unauthorized'], 401);
            } else {
                $permissions = $user->permissions()->pluck('permission')->toArray();
                if ($required = explode(',', $required)) {
                    if (!$permissions) {
                        $deny = true;
                    } else {
                        foreach ($required as $permission) {
                            if (!in_array(trim($permission), $permissions)) {
                                $deny = true;
                            }
                        }
                    }
                }
            }
        } else {
            $deny = true;
        }

        if ($deny) {
            return response()->json(['error' => 'Forbidden'], 403);
        }

        return $next($request);
    }
}
