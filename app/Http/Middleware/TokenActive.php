<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use JWTAuth;

class TokenActive
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($user = Auth::User()) {
            if ($user->active == 1 && !$user->deleted_at) {
                return $next($request);
            }

            if ($token = JWTAuth::getToken()) {
                JWTAuth::setToken($token)->invalidate();
            }
        }

        return response()->json(['error' => 'Unauthorized'], 401);
    }
}
