<?php

namespace App\Http\Traits;

trait HelperDB
{
    /**
     * @param array $where
     * @return array|bool
     */
    protected function prepareWhereInInt($where)
    {
        if (empty($where) || !is_array($where)) {
            return false;
        }

        $return = [];
        foreach ($where as $key) {
            if ($key != (int)$key || $key < 1) {
                continue;
            }
            $return[] = $key;
        }

        $return = array_unique($return);

        return $return ? $return : false;
    }
}