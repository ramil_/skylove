<?php

namespace App\Http\Traits;

trait GeoTimezone
{
    protected $tzLocal;

    protected $tzServer;

    protected function setGeoTimezone()
    {
        try {
            $geo = geoip()->getLocation(\Request::ip());

            if (!$geo->timezone) {
                throw new \Exception();
            }

            $this->tzLocal = !empty($geo['timezone']) ? $geo['timezone'] : 'Asia/Krasnoyarsk';
            $this->tzServer = 'UTC';
        } catch (\Exception $e) {
            return false;
        }

        return true;
    }

    protected function tzConvertToLocal($datetime)
    {
        return (new \Carbon\Carbon($datetime, $this->tzServer))
            ->setTimezone($this->tzLocal)
            ->toDateTimeString();
    }

    protected function tzConvertToUTC($datetime)
    {
        return (new \Carbon\Carbon($datetime, $this->tzLocal))
            ->setTimezone($this->tzServer)
            ->toDateTimeString();
    }

    protected function tzConvertToLocalUnix($timestamp)
    {
        return strtotime($this->tzConvertToLocal(date('Y-m-d H:i:s', $timestamp)));
    }
}