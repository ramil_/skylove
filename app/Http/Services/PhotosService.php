<?php

namespace App\Http\Services;

use App\Banlist;
use App\BanlistLog;
use App\Http\Traits\GeoTimezone;
use App\Http\Traits\HelperDB;
use App\User;
use App\UsersAlert;
use App\UsersPhoto;
use App\UsersPhotoStatus;
use App\UsersPhotoStatusInspect;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;

class PhotosService
{
    use GeoTimezone, HelperDB;

    protected $admin;

    /**
     * PhotosService constructor
     * @throws Exception
     */
    public function __construct()
    {
        if (!$this->admin = Auth::user()) {
            throw new Exception('Cannot get auth user information', 500);
        }
    }

    /**
     * Информация о фото
     * @param array $photos
     * @return \Illuminate\Support\Collection
     */
    public function getAdminPhotos(array $photos)
    {
        return DB::table('users_photos as up')
            ->join('users_stats as us', 'up.user_id', '=', 'us.user_id')
            ->leftJoin('users_photos_status_inspect as upi', function ($join) {
                /* @var \Illuminate\Database\Query\JoinClause $join */
                $join->where('upi.status_id', '=', function ($query) {
                    /* @var \Illuminate\Database\Query\Builder $query */
                    $query->select(DB::raw('max(status_id)'))
                        ->from('users_photos_status_inspect')
                        ->whereColumn('photo_id', '=', 'up.photo_id');
                });
            })
            ->whereIn('up.photo_id', $photos)
            ->whereNull('up.deleted_at')
            ->select(['up.user_id', 'up.photo_id', 'upi.admin_id', 'upi.admin_status', 'upi.inspector_id', 'upi.inspector_status', 'upi.level',
                'up.main_photo', 'us.banned_acc', 'us.banned_dev', 'us.blocked_photo', 'up.deleted_at'])
            ->get();
    }

    /**
     * Статусы фото
     * @param array $photos
     * @return \Illuminate\Support\Collection
     */
    protected function getStatusPhotos(array $photos)
    {
        return DB::table('users_photos_status_inspect')
            ->whereIn('photo_id', $photos)
            ->orderBy('date')
            ->get();
    }

    /**
     * Добавление записи статуса
     * @param object $photo
     * @param int $status
     * @return bool
     */
    public function createStatus($photo, $status)
    {
        $result = DB::table('users_photos_status')
            ->insert([
                'photo_id'   => $photo->photo_id,
                'user_id'    => $photo->user_id,
                'admin_id'   => $this->admin->id,
                'status'     => $status,
                'main_photo' => $photo->main_photo,
            ]);

        return $result ? true : false;
    }

    /**
     * Добавление записи статуса контролера
     * @param object $photo
     * @param int $status
     * @return bool
     */
    protected function createStatusInspect($photo, $status)
    {
        $result = DB::table('users_photos_status_inspect')
            ->insert([
                'photo_id'     => $photo->photo_id,
                'admin_id'     => $this->admin->id,
                'admin_status' => $status,
                'level'        => $this->admin->level,
            ]);

        return $result ? true : false;
    }

    /**
     * Обновление записи статуса контролера
     * @param object $photo
     * @param int $status
     * @return bool
     */
    protected function updateStatusInspect($photo, $status)
    {
        DB::table('users_photos_status_inspect')
            ->where('photo_id', $photo->photo_id)
            ->where('inspector_id', $this->admin->id)
            ->update([
                'inspector_status' => $status,
            ]);

        $result = DB::table('users_photos_status_inspect')
            ->where('photo_id', $photo->photo_id)
            ->where('admin_id', $this->admin->id)
            ->where('level', '<=', $this->admin->level)
            ->whereNull('inspector_id')
            ->update([
                'admin_status' => $status,
                'date'         => DB::raw('now()'),
                'level'        => $this->admin->level,
            ]);
        if (!$result) {
            return false;
        }

        return true;
    }

    /**
     * Добавление уведомления о блокировке по основному фото
     * @param array $users
     */
    protected function createBlockedAlert(array $users)
    {
        $users_main = DB::table('users_info')
            ->whereIn('photo_id', $users)
            ->pluck('user_id')
            ->toArray();

        if ($users_main) {
            DB::table('users_alerts')
                ->whereIn('user_id', $users_main)
                ->where('type', '=', 1)
                ->whereNull('read_at')
                ->delete();

            foreach ($users_main as $v) {
                $alerts[] = [
                    'user_id'  => $v,
                    'type'     => 1,
                    'priority' => 300,
                ];
            }

            if (!empty($alerts)) {
                DB::table('users_alerts')->insert($alerts);

                $r = Redis::connection();

                foreach ($users_main as $v) {
                    $r->publish('fcm_push', json_encode([
                        'user_id'         => $v,
                        'push_type'       => 'system_msg',
                        'system_msg_data' => [
                            'type' => 'refresh_badges',
                            'data' => []
                        ]
                    ]));
                }
            }
        }
    }

    /**
     * Удаление уведомления о блокировке по основному фото
     * @param array $users
     */
    protected function deleteBlockedAlert(array $users)
    {
        $users_main = DB::table('users_info')
            ->whereIn('photo_id', $users)
            ->pluck('user_id')
            ->toArray();

        if ($users_main) {
            DB::table('users_alerts')
                ->whereIn('user_id', $users_main)
                ->where('type', '=', 1)
                ->whereNull('read_at')
                ->delete();

            $r = Redis::connection();

            foreach ($users_main as $v) {
                $r->publish('fcm_push', json_encode([
                    'user_id'         => $v,
                    'push_type'       => 'system_msg',
                    'system_msg_data' => [
                        'type' => 'refresh_badges',
                        'data' => []
                    ]
                ]));
            }
        }
    }

    /**
     * Установка статуса фотографии пользователя
     * @param array $photos
     * @param int $status
     * @return array
     * @throws Exception
     */
    public function setStatus(array $photos, $status)
    {
        $return = [];

        try {
            DB::beginTransaction();

            if (!in_array($status, [-1, 1, 2, 3, 4, 5, 6])) {
                throw new Exception('Invalid status', 400);
            }

            if (!$photos = $this->prepareWhereInInt($photos)) {
                throw new Exception('Invalid photos', 400);
            }

            if (!$admin_photos = $this->getAdminPhotos($photos)) {
                throw new Exception('Cannot get photos status', 400);
            }

            // если бан, то в списке должно быть не более одного фото для каждого пользователя
            if (in_array($status, [5, 6])) {
                $users = [];
                foreach ($admin_photos as $v) {
                    if (in_array($v->user_id, $users)) {
                        throw new Exception('Unable to banned more than one user photo', 400);
                    }
                    $users[] = $v->user_id;
                }
            }

            $change_banned = $this->admin->permissions->where('permission', '=', 'verification_photos__change_banned');

            foreach ($admin_photos as $k => $v) {
                if ($v->admin_id) {
                    if ($v->inspector_id || $v->admin_status == $status) {
                        continue;
                    }

                    if ($v->admin_id != $this->admin->id || $v->level > $this->admin->level) {
                        throw new Exception('Not enough permissions to change photo status', 400);
                    }

                    if (in_array($v->admin_status, [5, 6]) && !$change_banned) {
                        throw new Exception('Not enough permissions to change banned photo status', 400);
                    }
                }

                if ($v->main_photo == 1 && $status == 2) {
                    throw new Exception('Status "Forbidden for avatar" available for inner photo only', 400);
                }

                if ($v->main_photo == 1 && $status == 3) {
                    throw new Exception('Status "Forbidden to display" available for inner photo only', 400);
                }

                if ($v->main_photo == 0 && $status == 4) {
                    throw new Exception('Status "Temporarily blocked" available for main photo only', 400);
                }

                if ($status == 4 && $v->blocked_photo == 1) {
                    throw new Exception('User account already blocked', 400);
                }

                if ($status == 5 && $v->banned_acc == 1) {
                    throw new Exception('User account already banned', 400);
                }

                if ($status == 6 && $v->banned_dev == 1) {
                    throw new Exception('User device already banned', 400);
                }

                if ($v->admin_status != 5 && $v->banned_acc == 1) {
                    throw new Exception('Unable to set status for banned account', 400);
                }

                if ($v->admin_status != 6 && $v->banned_dev == 1) {
                    throw new Exception('Unable to set status for banned device', 400);
                }

                // добавление статуса
                if (!$this->createStatus($v, $status)) {
                    throw new Exception('Cannot create status', 500);
                }

                // добавление статуса для контролера
                if ($v->admin_id) {
                    if (!$this->updateStatusInspect($v, $status)) {
                        throw new Exception('Cannot update status inspect', 500);
                    }
                } else {
                    if (!$this->createStatusInspect($v, $status)) {
                        throw new Exception('Cannot create status inspect', 500);
                    }
                }

                if ($v->banned_acc == 1) {
                    // удаление бана аккаунта
                    if (!(new UsersService)->removeBanAccByPhotoId([$v->photo_id])) {
                        throw new Exception('Cannot delete ban account', 500);
                    }
                }

                if ($v->banned_dev == 1) {
                    // удаление бана устройства
                    if (!(new UsersService)->removeBanDevByPhotoId([$v->photo_id])) {
                        throw new Exception('Cannot delete ban device', 500);
                    }
                }

                $return[] = [
                    'photo_id' => $v->photo_id,
                    'user_id'  => $v->user_id,
                    'status'   => $status,
                ];
            }

            if ($return) {
                if ($status == 4) {
                    // добавление уведомления о временной блокировке
                    $this->createBlockedAlert(array_column($return, 'photo_id'));
                } else {
                    // удаление уведомления о временной блокировке
                    $this->deleteBlockedAlert(array_column($return, 'photo_id'));
                }

                if ($status == 5) {
                    // добавление бана аккаунта
                    if (!(new UsersService)->addBanAccByPhotoId(array_column($return, 'photo_id'))) {
                        throw new Exception('Cannot create ban account', 500);
                    }
                }

                if ($status == 6) {
                    // бан устройства пользователя
                    if (!(new UsersService)->addBanDevByPhotoId(array_column($return, 'photo_id'))) {
                        throw new Exception('Cannot create ban device', 500);
                    }
                }

                DB::commit();
            }
        } catch (Exception $e) {
            DB::rollBack();

            throw new Exception($e->getMessage(), $e->getCode());
        }

        return $return;
    }

    /**
     * Информация о фото
     * @param array $photos_id
     * @return UsersPhoto[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Query\Builder[]|\Illuminate\Support\Collection
     */
    public function getPhotosInfo(array $photos_id)
    {
        return UsersPhoto::with([
            'user'          => function ($query) {
                /* @var \Illuminate\Database\Eloquent\Builder $query */
                $query->select('user_id', 'state');
            },
            'stat'          => function ($query) {
                /* @var \Illuminate\Database\Eloquent\Builder $query */
                $query->select('user_id', 'banned_acc', 'banned_dev');
            },
            'statusInspect' => function ($query) {
                /* @var \Illuminate\Database\Eloquent\Builder $query */
                $query->orderByDesc('status_id')
                    ->orderByDesc('date');
            }])
            ->whereIn('photo_id', $photos_id)
            ->whereNull('deleted_at')
            ->get(['photo_id', 'user_id', 'main_photo', 'status']);
    }

    /**
     * @param array $users_id
     * @return UsersPhoto[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Query\Builder[]|\Illuminate\Support\Collection
     */
    public function getUsersPhotosInfo(array $users_id)
    {
        return UsersPhoto::with([
            'user'          => function ($query) {
                /* @var \Illuminate\Database\Eloquent\Builder $query */
                $query->select('user_id', 'state');
            },
            'stat'          => function ($query) {
                /* @var \Illuminate\Database\Eloquent\Builder $query */
                $query->select('user_id', 'banned_acc', 'banned_dev');
            },
            'statusInspect' => function ($query) {
                /* @var \Illuminate\Database\Eloquent\Builder $query */
                $query->orderByDesc('status_id')
                    ->orderByDesc('date');
            }])
            ->whereIn('user_id', $users_id)
            ->whereNull('deleted_at')
            ->get(['photo_id', 'user_id', 'main_photo', 'status']);
    }

    /**
     * Подтверждение статуса фото контролером
     * @param array $photos
     * @return array
     * @throws Exception
     */
    public function confirmPhotoStatus(array $photos)
    {
        $return = [];

        try {
            $photosInfo = $this->getPhotosInfo($photos);

            if ($photosInfo->count() == 0) {
                return $return;
            }

            DB::beginTransaction();

            foreach ($photosInfo as $photo) {
                if ($photo->statusInspect->count() > 0) {
                    continue;
                }

                /* @var UsersPhotoStatusInspect $photoStatus */
                $photoStatus = $photo->statusInspect->first();

                // некорректный статус фото
                if (!in_array($photoStatus->admin_status, [1, 2, 3, 4, 5, 6])) {
                    continue;
                }

                // недостаточно прав доступа
                if ($photoStatus->admin_id == $this->admin->id || $photoStatus->level >= $this->admin->level) {
                    continue;
                }

                UsersPhotoStatusInspect::whereStatusId($photoStatus->status_id)->update([
                    'inspector_id'     => $this->admin->id,
                    'inspector_status' => $photoStatus->admin_status,
                ]);
                UsersPhotoStatusInspect::create([
                    'photo_id'     => $photoStatus->photo_id,
                    'admin_id'     => $this->admin->id,
                    'admin_status' => $photoStatus->admin_status,
                    'level'        => $this->admin->level,
                ]);

                $return[] = [
                    'photo_id' => $photoStatus->photo_id,
                    'status'   => $photoStatus->admin_status,
                ];
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();

            throw new Exception($e->getMessage(), $e->getCode());
        }

        return $return;
    }

    /**
     * Фото пользователя со статусами
     * @param $userId
     * @return UsersPhoto[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Query\Builder[]|\Illuminate\Support\Collection
     */
    public function getUserPhotos($userId)
    {
        return UsersPhoto::with([
            'statusInspect' => function($query) {
                /* @var \Illuminate\Database\Eloquent\Builder $query */
                $query->orderByDesc('status_id')
                    ->orderByDesc('date');
            }])
            ->whereUserId($userId)
            ->whereNull('deleted_at')
            ->get(['photo_id', 'user_id', 'main_photo']);
    }

    /**
     * Сброс статусов со всех фото пользователя с указанным статусом
     * @param int $userId
     * @param int $status
     * @param int $photoId
     * @throws Exception
     */
    public function resetUserPhotosStatus($userId, $status, $photoId = 0)
    {
        $photos = $this->getUserPhotos($userId);

        if ($photos->count() > 0) {
            foreach ($photos as $photo) {
                if ($photo->statusInspect->count() > 0) {
                    if ($photoId == $photo->photo_id) {
                        continue;
                    }

                    /* @var UsersPhotoStatusInspect $userPhotoStatus */
                    $photoStatus = $photo->statusInspect->first();

                    // удаляем все указанные статусы фото
                    if ($photoStatus->admin_status == $status) {
                        UsersPhotoStatus::create([
                            'photo_id'   => $photo->photo_id,
                            'user_id'    => $photo->user_id,
                            'admin_id'   => $this->admin->id,
                            'status'     => 0,
                            'main_photo' => $photo->main_photo,
                        ]);
                        UsersPhotoStatusInspect::wherePhotoId($photo->photo_id)->delete();
                    }
                }
            }
        }
    }

    /**
     * Добавление/изменение статуса фото
     * @param array $photos
     * @param int $status
     * @return array
     * @throws Exception
     */
    public function changePhotoStatus(array $photos, $status)
    {
        $return = [];

        // некорректный статус фото
        if (!in_array($status, [-1, 0, 1, 2, 3, 4, 5, 6])) {
            return $return;
        }

        try {
            $photosInfo = $this->getPhotosInfo($photos);

            if ($photosInfo->count() == 0) {
                return $return;
            }

            DB::beginTransaction();

            foreach ($photosInfo as $photo) {
                if (($photo->main_photo == 1 && in_array($status, [2, 3])) || ($photo->main_photo == 0 && $status == 4)) {
                    continue;
                }

                if (in_array($status, [-1, 0, 1, 2, 3, 4])) {
                    // у фото уже есть статус
                    if ($photo->statusInspect->count() > 0) {
                        /* @var UsersPhotoStatusInspect $photoStatus */
                        $photoStatus = $photo->statusInspect->first();

                        if ($photoStatus->admin_status == $status) {
                            continue;
                        }

                        // была временная блокировка
                        if ($photoStatus->admin_status == 4) {
                            $this->removeAlertBlocked($photo->user_id);
                        }

                        // был бан аккаунта
                        if ($photoStatus->admin_status == 5) {
                            $this->resetUserPhotosStatus($photo->user_id, 5);
                        }

                        // был бан устройства
                        if ($photoStatus->admin_status == 6) {
                        }
                    } else {
                        UsersPhotoStatus::create([
                            'photo_id'   => $photo->photo_id,
                            'user_id'    => $photo->user_id,
                            'admin_id'   => $this->admin->id,
                            'status'     => $status,
                            'main_photo' => $photo->main_photo,
                        ]);
                        UsersPhotoStatusInspect::create([
                            'photo_id'     => $photo->photo_id,
                            'admin_id'     => $this->admin->id,
                            'admin_status' => $status,
                            'level'        => $this->admin->level,
                        ]);

                        // временная блокировка по фото
                        if ($status == 4) {
                            $this->createAlertBlocked($photo->user_id);
                        }
                    }
                } elseif ($status == 5) {
                    // бан аккаунта

                } elseif ($status == 6) {
                    // бан устройства
                }
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();

            throw new Exception($e->getMessage(), $e->getCode());
        }

        return $return;
    }

    /**
     * @param array $photos
     * @param $status
     * @return array
     */
    public function setPhotoStatus(array $photos, $status)
    {
        $return = [];
        $statusCreate = [];
        $statusInspectCreate = [];
        $statusInspectUpdate = [];
        $removeBlocked = [];
        $removeBannedAcc = [];
        $removeBannedDev = [];

        $photosInfo = $this->getPhotosInfo($photos);

        if ($photosInfo->count() == 0) {
            return [];
        }

        foreach ($photosInfo as $photo) {
            if (($photo->main_photo == 1 && in_array($status, [2, 3])) || ($photo->main_photo == 0 && $status == 4)) {
                continue;
            }

            if ($photo->statusInspect->count() > 0) {
                /* @var UsersPhotoStatusInspect $photoStatus */
                $photoStatus = $photo->statusInspect->first();

                // недостаточно прав доступа
                if ($photoStatus->admin_id != $this->admin->id && $photoStatus->level > $this->admin->level) {
                    continue;
                }

                // такой статус уже установлен
                if ($photoStatus->admin_id == $photo->status && $photoStatus->admin_id == $status) {
                    continue;
                }

                if ($photoStatus->admin_status == 4) {
                    $removeBlocked[] = $photo->user_id;
                } elseif ($photoStatus->admin_status == 5) {
                    $removeBannedAcc[] = [
                        'user_id'    => $photo->user_id,
                        'photo_id'   => $photo->photo_id,
                        'main_photo' => $photo->main_photo,
                    ];
                } elseif ($photoStatus->admin_status == 6) {
                    $removeBannedDev[] = [
                        'user_id'    => $photo->user_id,
                        'photo_id'   => $photo->photo_id,
                        'main_photo' => $photo->main_photo,
                    ];
                }

                $statusInspectUpdate[] = [
                    'status_id'        => $photoStatus->status_id,
                    'inspector_id'     => $this->admin->id,
                    'inspector_status' => $status,
                ];
            }

            $statusInspectCreate[] = [
                'photo_id'     => $photo->photo_id,
                'admin_id'     => $this->admin->id,
                'admin_status' => $status,
                'level'        => $this->admin->level,
            ];

            $statusCreate[] = [
                'photo_id'   => $photo->photo_id,
                'user_id'    => $photo->user_id,
                'admin_id'   => $this->admin->id,
                'status'     => $status,
                'main_photo' => $photo->main_photo,
            ];
        }

        // добавление статуса фото
        UsersPhotoStatus::create($statusCreate);
        // добавление статуса для контроля проверки
        UsersPhotoStatusInspect::create($statusInspectCreate);

        if ($statusInspectUpdate) {
            $collect = collect($statusInspectUpdate);

            // обновление последнего статуса для контроля проверки
            UsersPhotoStatusInspect::whereIn('status_id', $collect->pluck('status_id'))
                ->update($collect->except(['status_id'])->toArray());
        }

        // удаление временной блокировки
        if ($removeBlocked) {
            $this->removeBlocked(collect($removeBlocked));
        }

        // удаление бана аккаунта
        if ($removeBannedAcc) {
            $this->removeBannedAcc(collect($removeBannedAcc));
        }

        // удаление бана устройств
        if ($removeBannedDev) {
            $this->removeBannedDev(collect($removeBannedDev));
        }

        return $return;
    }

    /**
     * @param $user_id
     * @throws Exception
     */
    public function createAlertBlocked($user_id)
    {
        UsersAlert::whereUserId($user_id)
            ->whereType(1)
            ->whereNull('read_at')
            ->delete();
        UsersAlert::create([
            'user_id'  => $user_id,
            'type'     => 1,
            'priority' => 300,
        ]);

        (new RedisService())->sendSystemPush($user_id, 'refresh_badges');
    }

    /**
     * @param $user_id
     * @throws Exception
     */
    public function removeAlertBlocked($user_id)
    {
        UsersAlert::whereUserId($user_id)
            ->whereType(1)
            ->whereNull('read_at')
            ->delete();

        (new RedisService())->sendSystemPush($user_id, 'refresh_badges');
    }

    public function removeBlocked(\Illuminate\Support\Collection $users_id)
    {
        $redis = Redis::connection();

        UsersAlert::whereIn('user_id', $users_id->all())
            ->where('type', 1)
            ->whereNull('read_at')
            ->delete();

        foreach ($users_id as $user) {
            $redis->publish('fcm_push', json_encode([
                'user_id'         => $user,
                'push_type'       => 'system_msg',
                'system_msg_data' => [
                    'type' => 'refresh_badges',
                    'data' => []
                ]
            ]));
        }
    }

    public function removeBannedAcc(\Illuminate\Support\Collection $data)
    {
        $users_id = Banlist::whereIn('user_id', $data->pluck('user_id'))->get(['user_id']);

        if ($users_id->count() == 0) {
            return [];
        }

        Banlist::whereIn('users_id', $users_id->pluck('user_id'))->delete();

        $banlistLog = [];
        $redis = [];
        foreach ($users_id->pluck('user_id') as $user) {
            if ($row = $data->where('user_id', '=', $user)->first()) {
                if ($row->photo_id) {
                    $banlistLog[] = [
                        'user_id'    => $user,
                        'admin_id'   => $this->admin->id,
                        'status'     => 0,
                        'photo_id'   => $row->photo_id,
                        'main_photo' => $row->main_photo,
                    ];
                } else {
                    $banlistLog[] = [
                        'user_id'  => $user,
                        'admin_id' => $this->admin->id,
                        'status'   => 0,
                    ];
                }
            }
            $redis[] = env('REDIS_PREFIX_BAN_USER') . $user;
        }

        if ($banlistLog) {
            BanlistLog::create($banlistLog);
        }

        if ($redis) {
            (Redis::connection())->del($redis);
        }

        return true;
    }

    public function removeBannedDev(\Illuminate\Support\Collection $data)
    {

    }
}