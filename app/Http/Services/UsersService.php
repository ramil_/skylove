<?php

namespace App\Http\Services;

use App\Banlist;
use App\BanlistLog;
use App\UsersAppVersion;
use App\UsersInfo;
use App\UsersPhoto;
use App\UsersPhotoStatus;
use App\UsersPhotoStatusInspect;
use App\UsersStat;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;

class UsersService
{
    protected $admin;

    protected $admin_id;

    protected $admin_level;

    /**
     * UsersService constructor
     * @throws Exception
     */
    public function __construct()
    {
        if (!$admin = Auth::user()) {
            throw new Exception('Cannot get auth user information', 500);
        }
        $this->admin = $admin;
        $this->admin_id = $admin->id;
        $this->admin_level = $admin->level;
    }

    /**
     * Добавление в бан-лист пользователей по их id
     * @param array $users_id Id пользователей
     * @return bool
     */
    public function addBanAccByUserId(array $users_id)
    {
        $redis = [];
        $users = UsersInfo::with('mainPhoto')
            ->whereIn('user_id', $users_id)
            ->get(['user_id', 'photo_id']);

        $banned = Banlist::whereIn('user_id', $users_id)
            ->pluck('user_id')
            ->all();

        foreach ($users as $user) {
            if (in_array($user->user_id, $banned)) {
                continue;
            }

            Banlist::updateOrCreate(
                ['user_id' => $user->user_id],
                ['admin_id' => $this->admin_id, 'reason_code' => 2]
            );

            if ($user->photo_id) {
                BanlistLog::create([
                    'user_id'    => $user->user_id,
                    'admin_id'   => $this->admin_id,
                    'status'     => 1,
                    'photo_id'   => $user->photo_id,
                    'main_photo' => 1,
                ]);

                if ($user->mainPhoto->status != 0) {
                    UsersPhotoStatus::create([
                        'photo_id'   => $user->photo_id,
                        'user_id'    => $user->user_id,
                        'admin_id'   => $this->admin_id,
                        'status'     => 0,
                        'main_photo' => 1,
                    ]);
                }

                UsersPhotoStatusInspect::where('photo_id', $user->photo_id)->delete();
                UsersPhotoStatusInspect::create([
                    'photo_id'     => $user->photo_id,
                    'admin_id'     => $this->admin_id,
                    'admin_status' => 5,
                    'level'        => $this->admin_level,
                ]);
            } else {
                BanlistLog::create([
                    'user_id'  => $user->user_id,
                    'admin_id' => $this->admin_id,
                    'status'   => 1,
                ]);
            }

            $redis[] = env('REDIS_PREFIX_BAN_USER') . $user->user_id;
            $banned[] = $user->user_id;
        }

        if ($redis) {
            (Redis::connection())->del($redis);
        }

        return true;
    }

    /**
     * Добавление в бан-лист пользователей по их фото id
     * @param array $photos_id Id фото пользователей
     * @return bool
     */
    public function addBanAccByPhotoId(array $photos_id)
    {
        $redis = [];
        $users = UsersPhoto::whereIn('photo_id', $photos_id)
            ->get(['user_id', 'photo_id', 'main_photo', 'status']);

        if (!$users->count()) {
            return false;
        }

        $banned = Banlist::whereIn('user_id', $users->pluck('user_id'))
            ->pluck('user_id')
            ->all();

        foreach ($users as $user) {
            if (in_array($user->user_id, $banned)) {
                continue;
            }

            Banlist::updateOrCreate(
                ['user_id' => $user->user_id],
                ['admin_id' => $this->admin_id, 'reason_code' => 2]
            );
            BanlistLog::create([
                'user_id'    => $user->user_id,
                'admin_id'   => $this->admin_id,
                'status'     => 1,
                'photo_id'   => $user->photo_id,
                'main_photo' => $user->main_photo,
            ]);

            if ($user->mainPhoto->status != 0) {
                UsersPhotoStatus::create([
                    'photo_id'   => $user->photo_id,
                    'user_id'    => $user->user_id,
                    'admin_id'   => $this->admin_id,
                    'status'     => 0,
                    'main_photo' => $user->main_photo,
                ]);
            }

            UsersPhotoStatusInspect::where('photo_id', $user->photo_id)->delete();
            UsersPhotoStatusInspect::create([
                'photo_id'     => $user->photo_id,
                'admin_id'     => $this->admin_id,
                'admin_status' => 5,
                'level'        => $this->admin_level,
            ]);

            $redis[] = env('REDIS_PREFIX_BAN_USER') . $user->user_id;
            $banned[] = $user->user_id;
        }

        if ($redis) {
            (Redis::connection())->del($redis);
        }

        return true;
    }

    /**
     * Добавление в бан-лист устройств пользователей по их id
     * @param array $users Id пользователей
     * @return bool
     */
    public function addBanDevByUserId(array $users)
    {
        $devices = UsersAppVersion::whereIn('user_id', $users)
            ->get(['user_id', 'device_id']);

        $bannedDev = Banlist::whereIn('device_id', $devices->pluck('device_id'))
            ->pluck('device_id')
            ->all();

        $users = UsersAppVersion::with([
            'userStat'  => function ($query) {
                /* @var \Illuminate\Database\Eloquent\Builder $query */
                $query->select('user_id', 'banned_acc', 'banned_dev');
            },
            'mainPhoto' => function ($query) {
                /* @var \Illuminate\Database\Eloquent\Builder $query */
                $query->select('user_id', 'photo_id', 'status');
            }])
            ->whereIn('device_id', $devices->pluck('device_id'))
            ->get(['user_id', 'device_id']);

        $bannedPhoto = [];
        $statusCreate = [];
        $statusInspectDelete = [];
        $statusInspectCreate = [];
        foreach ($users as $user) {
            if (!$user->mainPhoto || in_array($user->mainPhoto->photo_id, $bannedPhoto)) {
                continue;
            }

            if ($user->mainPhoto->status != 0) {
                $statusCreate[] = [
                    'photo_id'   => $user->mainPhoto->photo_id,
                    'user_id'    => $user->user_id,
                    'admin_id'   => $this->admin_id,
                    'status'     => 0,
                    'main_photo' => 1,
                ];
            }

            $statusInspectDelete[] = $user->mainPhoto->photo_id;
            $statusInspectCreate[] = [
                'photo_id'     => $user->mainPhoto->photo_id,
                'admin_id'     => $this->admin_id,
                'admin_status' => 6,
                'level'        => $this->admin_level,
            ];

            $bannedPhoto[] = $user->mainPhoto->photo_id;
        }

        if ($statusCreate) {
            UsersPhotoStatus::create($statusCreate);
        }

        if ($statusInspectDelete && $statusInspectCreate) {
            UsersPhotoStatusInspect::where('photo_id', $statusInspectDelete)->delete();
            UsersPhotoStatusInspect::create($statusInspectCreate);
        }

        $redis = [];
        foreach ($devices as $device) {
            if (in_array($device->device_id, $bannedDev)) {
                continue;
            }

            Banlist::updateOrCreate(
                ['device_id' => $device->device_id],
                ['admin_id' => $this->admin_id, 'reason_code' => 1]
            );
            BanlistLog::create([
                'device_id' => $device->device_id,
                'admin_id'  => $this->admin_id,
                'status'    => 1,
            ]);

            $redis[] = env('REDIS_PREFIX_BAN_DEVICE') . $device->device_id;
            $redis[] = env('REDIS_PREFIX_BAN_USER_DEVICE') . $device->user_id;
            $bannedDev[] = $device->device_id;
        }

        if ($redis) {
            (Redis::connection())->del($redis);
        }

        return true;
    }

    /**
     * Добавление в бан-лист устройств пользователей по их фото id
     * @param array $photos Id фото пользователей
     * @return bool
     */
    public function addBanDevByPhotoId(array $photos)
    {
        $redis = [];
        $users = UsersPhoto::whereIn('photo_id', $photos)
            ->groupBy('user_id')
            ->get(['user_id', 'photo_id', 'main_photo']);

        if (!$users->count()) {
            return false;
        }

        $devices = DB::table('users_app_version')
            ->select('user_id', 'device_id')
            ->whereIn('user_id', $users->pluck('user_id'))
            ->get();

        if (!$devices->count()) {
            return false;
        }

        $banned = Banlist::whereIn('device_id', $devices->pluck('device_id'))
            ->pluck('device_id')
            ->all();

        foreach ($devices as $device) {
            if (in_array($device->device_id, $banned)) {
                continue;
            }

            if (!$photo = $users->where('user_id', $device->user_id)->first()) {
                continue;
            }

            Banlist::updateOrCreate(
                ['device_id' => $device->device_id],
                ['admin_id' => $this->admin_id, 'reason_code' => 1]
            );
            BanlistLog::create([
                'device_id'  => $device->device_id,
                'admin_id'   => $this->admin_id,
                'status'     => 1,
                'photo_id'   => $photo->photo_id,
                'main_photo' => $photo->main_photo,
            ]);

            $redis[] = env('REDIS_PREFIX_BAN_DEVICE') . $device->device_id;
            $redis[] = env('REDIS_PREFIX_BAN_USER_DEVICE') . $device->user_id;
            $banned[] = $device->device_id;
        }

        if ($redis) {
            (Redis::connection())->del($redis);
        }

        return true;
    }

    /**
     * Удаление из бан-листа пользователей по их id
     * @param array $users Id пользователей
     * @return bool
     */
    public function removeBanAccByUserId(array $users)
    {
        $redis = [];
        $banned = Banlist::whereIn('user_id', $users)
            ->pluck('user_id');

        if (!$banned->count()) {
            return false;
        }

        Banlist::whereIn('user_id', $users)
            ->delete();

        foreach ($banned as $user_id) {
            BanlistLog::create([
                'user_id'  => $user_id,
                'admin_id' => $this->admin_id,
                'status'   => 0
            ]);

            $redis[] = env('REDIS_PREFIX_BAN_USER') . $user_id;
        }

        if ($redis) {
            (Redis::connection())->del($redis);
        }

        return true;
    }

    /**
     * Удаление из бан-листа пользователей по их фото id
     * @param array $photos Id фото пользователей
     * @return bool
     */
    public function removeBanAccByPhotoId(array $photos)
    {
        $redis = [];
        $users = UsersPhoto::whereIn('photo_id', $photos)
            ->groupBy('user_id')
            ->get(['user_id', 'photo_id', 'main_photo']);

        if (!$users->count()) {
            return false;
        }

        $banned = Banlist::whereIn('user_id', $users->pluck('user_id'))
            ->pluck('user_id')
            ->all();

        if (!$banned) {
            return false;
        }

        Banlist::whereIn('user_id', $banned)
            ->delete();

        foreach ($users as $user) {
            if (!in_array($user->user_id, $banned)) {
                continue;
            }

            BanlistLog::create([
                'user_id'    => $user->user_id,
                'admin_id'   => $this->admin_id,
                'status'     => 0,
                'photo_id'   => $user->photo_id,
                'main_photo' => $user->main_photo,
            ]);

            $redis[] = env('REDIS_PREFIX_BAN_USER') . $user->user_id;
        }

        if ($redis) {
            (Redis::connection())->del($redis);
        }

        return true;
    }

    /**
     * Удаление из бан-листа устройств пользователей по их id
     * @param array $users Id пользователей
     * @return bool
     */
    public function removeBanDevByUserId(array $users)
    {
        $devices = UsersAppVersion::whereIn('user_id', $users)
            ->get(['user_id', 'device_id']);

        if (!$devices->count()) {
            return false;
        }

        $banned = Banlist::whereIn('device_id', $devices->pluck('device_id'))
            ->pluck('device_id')
            ->all();

        if (!$banned) {
            return false;
        }

        // информация о бане и основном фото пользователей с указанными устройствами
        $users = UsersAppVersion::with([
            'userStat'  => function ($query) {
                /* @var \Illuminate\Database\Eloquent\Builder $query */
                $query->select('user_id', 'banned_acc', 'banned_dev');
            },
            'mainPhoto' => function ($query) {
                /* @var \Illuminate\Database\Eloquent\Builder $query */
                $query->select('user_id', 'photo_id', 'status');
            }])
            ->whereIn('device_id', $devices->pluck('device_id'))
            ->get(['user_id', 'device_id']);

        $photosStatus = [];
        $photosStatusInspect = [];

        foreach ($users as $user) {
            if ($user->userStat->banned_dev == 0 || !$user->mainPhoto || in_array($user->mainPhoto->photo_id, $photosStatusInspect)) {
                continue;
            }

            if ($user->mainPhoto->status != 0) {
                $photosStatus[] = [
                    'photo_id'   => $user->mainPhoto->photo_id,
                    'user_id'    => $user->user_id,
                    'admin_id'   => $this->admin_id,
                    'status'     => 0,
                    'main_photo' => 1,
                ];
            }

            $photosStatusInspect[] = $user->mainPhoto->photo_id;
        }

        // сброс статуса основного фото
        if ($photosStatus) {
            UsersPhotoStatus::create($photosStatus);
        }

        if ($photosStatusInspect) {
            UsersPhotoStatusInspect::whereIn('photo_id', $photosStatusInspect)->delete();
        }

        Banlist::whereIn('device_id', $banned)->delete();

        $redis = [];

        foreach ($devices as $device) {
            if (!in_array($device->device_id, $banned)) {
                continue;
            }

            BanlistLog::create([
                'device_id' => $device->device_id,
                'admin_id'  => $this->admin_id,
                'status'    => 0,
            ]);

            $redis[] = env('REDIS_PREFIX_BAN_DEVICE') . $device->device_id;
            $redis[] = env('REDIS_PREFIX_BAN_USER_DEVICE') . $device->user_id;
            $banned[] = $device->device_id;
        }

        if ($redis) {
            (Redis::connection())->del($redis);
        }

        return true;
    }

    /**
     * Удаление из бан-листа устройств пользователей по их фото id
     * @param array $photos Id фото пользователей
     * @return bool
     */
    public function removeBanDevByPhotoId(array $photos)
    {
        $redis = [];
        $users = UsersPhoto::whereIn('photo_id', $photos)
            ->groupBy('user_id')
            ->get(['user_id', 'photo_id', 'main_photo']);

        if (!$users->count()) {
            return false;
        }

        $devices = DB::table('users_app_version')
            ->select('user_id', 'device_id')
            ->whereIn('user_id', $users->pluck('user_id'))
            ->get();

        if (!$devices->count()) {
            return false;
        }

        $banned = Banlist::whereIn('device_id', $devices->pluck('device_id'))
            ->pluck('device_id')
            ->all();

        if (!$banned) {
            return false;
        }

        Banlist::whereIn('device_id', $banned)
            ->delete();

        foreach ($devices as $device) {
            if (!$photo = $users->where('user_id', $device->user_id)->first()) {
                continue;
            }

            BanlistLog::create([
                'device_id'  => $device->device_id,
                'admin_id'   => $this->admin_id,
                'status'     => 0,
                'photo_id'   => $photo->photo_id,
                'main_photo' => $photo->main_photo,
            ]);

            $redis[] = env('REDIS_PREFIX_BAN_DEVICE') . $device->device_id;
            $redis[] = env('REDIS_PREFIX_BAN_USER_DEVICE') . $device->user_id;
            $banned[] = $device->device_id;
        }

        if ($redis) {
            (Redis::connection())->del($redis);
        }

        return true;
    }

    /**
     * @param $photos_id
     * @return array
     * @throws Exception
     */
    public function createBanAccWithPhoto($photos_id)
    {
        $return = [];

        try {
            $photos = UsersPhoto::with([
                'stats'         => function ($query) {
                    /* @var \Illuminate\Database\Eloquent\Builder $query */
                    $query->select('user_id', 'banned_acc', 'banned_dev', 'blocked_photo');
                },
                'statusInspect' => function ($query) {
                    /* @var \Illuminate\Database\Eloquent\Builder $query */
                    $query->orderByDesc('status_id')
                        ->orderByDesc('date');
                }])
                ->whereIn('photo_id', $photos_id)
                ->get(['photo_id', 'user_id']);

            if ($photos->count() == 0) {
                return [];
            }

            DB::beginTransaction();

            $createBanned = [];
            $createStatus = [];

            /* @var UsersPhoto $photo */
            foreach ($photos as $photo) {
                /* @var UsersStat $userStats */
                $userStats = $photo->stat;

                if ($userStats->banned_acc == 0 && !in_array($photo->user_id, $createBanned)) {
                    Banlist::create([
                        'user_id'     => $photo->user_id,
                        'admin_id'    => $this->admin_id,
                        'reason_code' => 2,
                    ]);

                    $createBanned[] = $photo->user_id;
                }

                if (!in_array($photo->photo_id, $createStatus)) {
                    BanlistLog::create([
                        'admin_id'   => $this->admin_id,
                        'user_id'    => $photo->photo_id,
                        'status'     => 1,
                        'photo_id'   => $photo->photo_id,
                        'main_photo' => $photo->main_photo,
                    ]);

                    if ($photo->statusInspect->count()) {
                        /* @var UsersPhotoStatusInspect $status */
                        $status = $photo->statusInspect->first();

                        if ($status->admin_status != 5) {
                            UsersPhotoStatusInspect::whereStatusId($status->status_id)->update([
                                'inspector_id'     => $this->admin_id,
                                'inspector_status' => $status->admin_status,
                            ]);
                            UsersPhotoStatusInspect::create([
                                'photo_id'     => $status->photo_id,
                                'admin_id'     => $this->admin_id,
                                'admin_status' => $status->admin_status,
                                'level'        => $this->admin_level,
                            ]);
                        }

                        // TODO проверять все фото на блокировку и бан устройства
                    }

                    $createStatus[] = $photo->photo_id;
                }
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();

            throw new Exception($e->getMessage(), $e->getCode());
        }

        return $return;
    }

    /**
     * @param $users_id
     * @return array
     * @throws Exception
     */
    public function removeBanAccWithPhoto($users_id)
    {
        $return = [];

        try {
            $users = UsersInfo::with([
                'stats'                => function ($query) {
                    /* @var \Illuminate\Database\Eloquent\Builder $query */
                    $query->select('user_id', 'banned_acc', 'banned_dev', 'blocked_photo');
                },
                'photos'               => function ($query) {
                    /* @var \Illuminate\Database\Eloquent\Builder $query */
                    $query->select('photo_id', 'status');
                },
                'photos.statusInspect' => function ($query) {
                    /* @var \Illuminate\Database\Eloquent\Builder $query */
                    $query->orderByDesc('status_id')
                        ->orderByDesc('date');
                }])
                ->whereIn('user_id', $users_id)
                ->get(['user_id']);

            if ($users->count() == 0) {
                return [];
            }

            DB::beginTransaction();

            foreach ($users as $user) {
                /* @var UsersStat $userStats */
                $userStats = $user->stats;
                /* @var UsersPhoto[] $userPhotos */
                $userPhotos = $user->photos;

                if ($userStats->banned_acc == 0) {
                    continue;
                }

                Banlist::whereUserId($user->user_id)->delete();

                $reset = [];
                foreach ($userPhotos as $photo) {
                    if ($photo->statusInspect->count() == 0) {
                        continue;
                    }

                    /* @var UsersPhotoStatusInspect $status */
                    $status = $photo->statusInspect->first();

                    if ($status->admin_status == 5 && !in_array($photo->photo_id, $reset)) {
                        BanlistLog::create([
                            'admin_id'   => $this->admin_id,
                            'user_id'    => $photo->user_id,
                            'status'     => 0,
                            'photo_id'   => $photo->photo_id,
                            'main_photo' => $photo->main_photo,
                        ]);
                        UsersPhotoStatus::create([
                            'photo_id'   => $photo->photo_id,
                            'user_id'    => $photo->user_id,
                            'admin_id'   => $this->admin_id,
                            'status'     => 0,
                            'main_photo' => $photo->main_photo,
                        ]);
                        UsersPhotoStatusInspect::wherePhotoId($photo->photo_id)->delete();

                        $reset[] = $photo->photo_id;
                    }
                }
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();

            throw new Exception($e->getMessage(), $e->getCode());
        }

        return $return;
    }
}