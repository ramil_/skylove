<?php

namespace App\Http\Services;

use Illuminate\Support\Facades\Redis;

class RedisService
{
    protected $redis;

    public function __construct()
    {
        $this->redis = Redis::connection();
    }

    public function sendSystemPush($user_id, $type, $data = [])
    {
        $this->redis->publish('fcm_push', json_encode([
            'user_id'         => $user_id,
            'push_type'       => 'system_msg',
            'system_msg_data' => [
                'type' => $type,
                'data' => $data
            ]
        ]));
    }
}