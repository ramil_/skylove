<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\UsersPhotoStatus
 *
 * @property int $status_id
 * @property int $photo_id
 * @property int $user_id
 * @property int $admin_id
 * @property int $status
 * @property int $main_photo
 * @property string $date
 * @property int $level
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhotoStatus newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhotoStatus newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhotoStatus query()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhotoStatus whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhotoStatus whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhotoStatus whereLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhotoStatus whereMainPhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhotoStatus wherePhotoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhotoStatus whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhotoStatus whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhotoStatus whereUserId($value)
 * @mixin \Eloquent
 */
class UsersPhotoStatus extends Model
{
    protected $fillable = [
        'photo_id', 'user_id', 'admin_id', 'status', 'main_photo', 'date'
    ];

    public $timestamps = false;

    protected $table = 'users_photos_status';
}
