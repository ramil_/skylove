<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\AdminsShift
 *
 * @property int $shift_id
 * @property int $admin_id
 * @property string|null $started_at
 * @property string|null $finished_at
 * @property string $created_at
 * @property-read User $admin
 * @property-read AdminsRatingCalcShift $rating
 * @method static \Illuminate\Database\Eloquent\Builder|AdminsShift newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AdminsShift newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AdminsShift query()
 * @method static \Illuminate\Database\Eloquent\Builder|AdminsShift whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminsShift whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminsShift whereFinishedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminsShift whereShiftId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminsShift whereStartedAt($value)
 * @mixin \Eloquent
 */
class AdminsShift extends Model
{
    protected $table = 'admins_shift';

    protected $primaryKey = 'shift_id';

    protected $fillable = [
        'admin_id', 'started_at', 'finished_at'
    ];

    public $timestamps = false;

    public function admin()
    {
        return $this->belongsTo('App\User', 'admin_id', 'id');
    }

    public function rating()
    {
        return$this->hasOne('App\AdminsRatingCalcShift', 'shift_id', 'shift_id');
    }
}
