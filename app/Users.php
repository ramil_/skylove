<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Users
 *
 * @property int $user_id
 * @property string|null $email
 * @property string|null $passwd
 * @property int|null $vk
 * @property int|null $fb
 * @property int $state
 * @property int $fake
 * @method static \Illuminate\Database\Eloquent\Builder|Users newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Users newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Users query()
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereFake($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereFb($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users wherePasswd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Users whereVk($value)
 * @mixin \Eloquent
 */
class Users extends Model
{
    protected $primaryKey = 'user_id';

    protected $table = 'users';
}
