<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Banlist
 *
 * @property int $ban_id
 * @property int $user_id
 * @property string|null $device_id
 * @property int $admin_id
 * @property int|null $reason_code
 * @property string|null $custom_message
 * @property string|null $date
 * @method static \Illuminate\Database\Eloquent\Builder|Banlist newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Banlist newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Banlist query()
 * @method static \Illuminate\Database\Eloquent\Builder|Banlist whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Banlist whereBanId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Banlist whereCustomMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Banlist whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Banlist whereDeviceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Banlist whereReasonCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Banlist whereUserId($value)
 * @mixin \Eloquent
 */
class Banlist extends Model
{
    protected $fillable = [
        'user_id', 'device_id', 'admin_id', 'reason_code', 'custom_message', 'date'
    ];

    public $timestamps = false;

    protected $table = 'banlist';

    protected $primaryKey = 'ban_id';
}
