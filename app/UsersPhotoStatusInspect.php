<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\UsersPhotoStatusInspect
 *
 * @property int $status_id
 * @property int $photo_id
 * @property int $admin_id
 * @property int|null $inspector_id
 * @property int $admin_status
 * @property int|null $inspector_status
 * @property int $associated
 * @property string $date
 * @property int $level
 * @property-read User $admin
 * @property-read User $inspector
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhotoStatusInspect newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhotoStatusInspect newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhotoStatusInspect query()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhotoStatusInspect whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhotoStatusInspect whereAdminStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhotoStatusInspect whereAssociated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhotoStatusInspect whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhotoStatusInspect whereInspectorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhotoStatusInspect whereInspectorStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhotoStatusInspect whereLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhotoStatusInspect wherePhotoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhotoStatusInspect whereStatusId($value)
 * @mixin \Eloquent
 */
class UsersPhotoStatusInspect extends Model
{
    protected $fillable = [
        'photo_id', 'admin_id', 'inspector_id', 'admin_status', 'inspector_status', 'associated', 'date', 'level'
    ];

    public $timestamps = false;

    protected $table = 'users_photos_status_inspect';

    public function admin()
    {
        return $this->hasOne('App\User', 'id', 'admin_id');
    }

    public function inspector()
    {
        return $this->hasOne('App\User', 'id', 'inspector_id');
    }
}
