<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\UsersAppVersion
 *
 * @property int $user_id
 * @property string|null $device_id
 * @property string|null $platform
 * @property string|null $app_version
 * @property string $date
 * @property-read UsersPhoto $mainPhoto
 * @property-read UsersInfo $userInfo
 * @property-read UsersStat $userStat
 * @method static \Illuminate\Database\Eloquent\Builder|UsersAppVersion newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersAppVersion newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersAppVersion query()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersAppVersion whereAppVersion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersAppVersion whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersAppVersion whereDeviceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersAppVersion wherePlatform($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersAppVersion whereUserId($value)
 * @mixin \Eloquent
 */
class UsersAppVersion extends Model
{
    protected $table = 'users_app_version';

    public function userInfo()
    {
        return $this->hasOne(UsersInfo::class, 'user_id', 'user_id');
    }

    public function userStat()
    {
        return $this->hasOne(UsersStat::class, 'user_id', 'user_id');
    }

    public function mainPhoto()
    {
        return $this->belongsTo(UsersPhoto::class, 'user_id', 'user_id')->where('main_photo', 1);
    }
}
