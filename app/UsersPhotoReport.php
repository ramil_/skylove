<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\UsersPhotoReport
 *
 * @property int $user_id
 * @property int $photo_id
 * @property int $reason
 * @property string|null $reason_text
 * @property string|null $date
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhotoReport newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhotoReport newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhotoReport query()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhotoReport whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhotoReport wherePhotoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhotoReport whereReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhotoReport whereReasonText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhotoReport whereUserId($value)
 * @mixin \Eloquent
 */
class UsersPhotoReport extends Model
{
    protected $table = 'blocked_photo';
}
