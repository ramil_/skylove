<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dashboard');
});

Route::get('/login', function () {
    return view('auth/login');
});

Route::get('/users', function () {
    return view('users');
});

Route::get('/users/user/edit{id?}', function () {
    return view('user_edit');
});

Route::get('/users/user/photo/{id}', 'Users\\UsersController@downloadPhoto');

Route::get('/users/user/dialogs{id?}', function () {
    return view('user_dialogs');
});

Route::get('/users-deleted', function () {
    return view('users_deleted');
});

Route::get('/fakes', function () {
    return view('fakes');
});

Route::get('/fakes/fake/add', function () {
    return view('fake_add');
});

Route::get('/fakes/fake/edit{id?}', function () {
    return view('fake_edit');
});

Route::get('/fakes/fake/dialogs{id?}', function () {
    return view('fake_dialogs');
});

Route::get('/settings/admins/', function () {
    return view('settings_admins');
});

Route::get('/settings/admins/admin/add', function () {
    return view('settings_admin_add');
});

Route::get('/settings/admins/admin/edit{id?}', function () {
    return view('settings_admin_edit');
});

Route::get('/reports/users', function () {
    return view('reports_users');
});

Route::get('/reports/users/user/show{id?}', function () {
    return view('reports_user_show');
});

Route::get('/reports/photos', function () {
    return view('reports_photos');
});

Route::get('/verification', function () {
    return view('verification');
});

Route::get('/status-photos', function () {
    return view('status_photos');
});

Route::get('/status-photos', function () {
    return view('status_photos');
});

Route::get('/moderators/stats', function () {
    return view('moderators_stats');
});

Route::get('/moderators/rating', function () {
    return view('moderators_rating');
});

Route::get('/moderators/payments', function () {
    return view('moderators_payments');
});

Route::get('/status-photos-inspect', function () {
    return view('status_photos_inspect_list');
});

Route::get('/status-photos-inspect/moderator/{id}/{status}', function () {
    return view('status_photos_inspect_show');
});

Route::get('/status-photos-inspect/confirmed/moderator/{id}/{status}/', function () {
    return view('status_photos_inspect_show_confirmed');
});

Route::get('/status-photos-inspect/changed/moderator/{id}/{status}', function () {
    return view('status_photos_inspect_show_changed');
});

Route::get('/status-photos-inspect/search/{id}', function () {
    return view('status_photos_inspect_search');
});