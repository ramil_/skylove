<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'AuthController@login');

Route::group(['middleware' => ['jwt.auth', 'jwt.active']], function () {
    Route::get('logout', 'AuthController@logout');

    /**
     * Stats
     */
    Route::get('dashboard/main-stats', 'DashboardController@mainStats');
    Route::get('dashboard/age-stats', 'DashboardController@ageStats');
    Route::post('dashboard/active-stat', 'DashboardController@activeStat');
    Route::post('dashboard/reg-enter-stats', 'DashboardController@regEnterStats');
    Route::post('dashboard/online-stats-sum', 'DashboardController@onlineStatsSum');
    Route::get('dashboard/online-current', 'DashboardController@onlineCurrent');
    Route::post('dashboard/gender-stats', 'DashboardController@genderStats');
    Route::get('dashboard/gender-stats-current', 'DashboardController@genderStatsCurrent');
    Route::post('dashboard/photo-stats', 'DashboardController@getAdminPhotoStats');
    Route::post('dashboard/photo-stats-verification', 'DashboardController@getStatPhotoVerification');

    /**
     * Users
     */
    Route::group(['middleware' => ['permission:users']], function () {
        // list users
        Route::get('users/get-data', 'Users\UsersSearchController@getData');
        Route::group(['middleware' => ['permission:users__edit']], function () {
            // show user informations
            Route::get('user/{id}', 'Users\UsersController@get');
            // change gender users
            Route::post('users/change-gender', 'Users\UsersController@changeGender');
            // add points users
            Route::post('users/points-add', 'Users\UsersController@pointsAdd');
            // vip state users
            Route::post('users/vip-add', 'Users\UsersController@vipAdd');
            Route::post('users/vip-remove', 'Users\UsersController@vipRemove');
            Route::post('users/get-key-vip-push', 'Users\UsersController@getKeyVipPush');
            // blocked
            Route::post('users/blocked-user-add', 'Users\UsersController@createBlockedUser');
            Route::post('users/blocked-user-remove', 'Users\UsersController@removeBlockedUser');
            // ban state users
            Route::post('users/ban-user-add', 'Users\UsersController@createBannedAcc');
            Route::post('users/ban-user-remove', 'Users\UsersController@removeBannedAcc');
            // ban state device users
            Route::post('users/ban-device-add', 'Users\UsersController@createBannedDev');
            Route::post('users/ban-device-remove', 'Users\UsersController@removeBannedDev');
            // images uploads and replace (delete) for users
            Route::post('user/avatar/replace/{id}', 'Users\UsersController@replaceAvatar');
            Route::delete('user/avatar/{id}', 'Users\UsersController@deleteAvatar');
            Route::post('user/mainphoto/replace/{id}', 'Users\UsersController@replaceMainPhoto');
            Route::delete('user/mainphoto/{id}', 'Users\UsersController@deleteMainPhoto');
            Route::post('user/photo/replace/{id}', 'Users\UsersController@replacePhoto');
            Route::delete('user/photo/{id}', 'Users\UsersController@deletePhoto');
            // dialogs
            Route::get('users/user/dialog/{id}', 'Users\UsersController@getDialog');
            // alerts
            Route::get('users/alert-messages-list', 'Reports\ReportsUsersController@getAlertMessagesList');
            Route::get('users/alert-message', 'Reports\ReportsUsersController@getAlertMessage');
            Route::post('users/alert-message', 'Reports\ReportsUsersController@createAlertMessage');
            Route::put('users/alert-message', 'Reports\ReportsUsersController@updateAlertMessage');
            Route::delete('users/alert-message', 'Reports\ReportsUsersController@deleteAlertMessage');
            Route::post('users/alert-message-send', 'Reports\ReportsUsersController@sendAlertMessage');
        });

        Route::group(['middleware' => ['permission:users__edit']], function () {
            Route::post('user/verification', 'Users\UsersController@verificationUser');
            Route::get('user/get-token/{id}', 'Users\UsersDialogsController@getToken');
            Route::get('user/get-user/{id}', 'Users\UsersDialogsController@getUserData');
            Route::get('user/get-dialogs/{id}', 'Users\UsersDialogsController@getDialogs');
            // работа с чатами
            Route::get('user/dialog/get/{uid}/{chat_id}', 'Users\UsersDialogsController@getDialog');
            Route::get('user/dialog/read/{uid}/{chat_id}', 'Users\UsersDialogsController@readDialog');
            Route::post('user/dialog/status/{uid}', 'Users\UsersDialogsController@statusMessage');
            Route::post('user/dialog/send/{uid}', 'Users\UsersDialogsController@sendMessage');
            Route::post('user/dialog/create/{uid}', 'Users\UsersDialogsController@createDialog');
        });

        Route::group(['middleware' => ['permission:users__delete']], function () {
            // delete user
            Route::delete('user/{id}', 'Users\UsersController@deleteUser');
        });
    });

    /**
     * Deleted users
     */
    Route::group(['middleware' => ['permission:users_del']], function () {
        Route::get('users-deleted/get-data', 'UsersDeleted\UsersDeletedSearchController@getData');
        Route::group(['middleware' => ['permission:users_del__edit']], function () {
            // ban state device users
            Route::post('users-deleted/ban-device-add', 'Users\UsersController@createBannedDev');
            Route::post('users-deleted/ban-device-remove', 'Users\UsersController@removeBannedDev');
        });
    });

    /**
     * Fakes
     */
    Route::group(['middleware' => ['permission:fakes']], function () {
        // list fakes
        Route::get('fakes/get-data', 'Fakes\FakesSearchController@getData');
        Route::group(['middleware' => ['permission:fakes__edit']], function () {
            // show fake informations
            Route::get('fake/userdata/{id}', 'Fakes\FakesDialogs@getUserData');
            // edit/update fake informations
            Route::post('fake/add', 'Fakes\FakesController@add');
            Route::get('fake/{id}', 'Fakes\FakesController@get');
            Route::post('fake/{id}', 'Fakes\FakesController@update');
            // vip state fakes
            Route::post('fakes/vip-add', 'Fakes\FakesController@vipAdd');
            Route::post('fakes/vip-remove', 'Fakes\FakesController@vipRemove');
            // images uploads and replace (delete) for fakes
            Route::post('fake/avatar/replace/{id}', 'Fakes\FakesController@replaceAvatar');
            Route::delete('fake/avatar/{id}', 'Fakes\FakesController@deleteAvatar');
            Route::post('fake/mainphoto/replace/{id}', 'Fakes\FakesController@replaceMainPhoto');
            Route::delete('fake/mainphoto/{id}', 'Fakes\FakesController@deleteMainPhoto');
        });
        Route::group(['middleware' => ['permission:fakes__delete']], function () {
            // delete fake
            Route::delete('fake/{id}', 'Fakes\FakesController@deleteFake');
        });
        Route::group(['middleware' => ['permission:fakes__dialogs']], function () {
            // получение списка чатов для фейка
            Route::get('fake/chats/{id}', 'Fakes\FakesDialogs@getChats');
            // работа с чатами
            Route::get('fchats/chatmessages/{uid}/{chat_id}', 'Fakes\FakesDialogs@getChatMessages');
            Route::get('fchats/setread/{uid}/{chat_id}', 'Fakes\FakesDialogs@setReadMessages');
            Route::post('fchats/sendmessage/{uid}', 'Fakes\FakesDialogs@sendChatMessage');
        });
    });

    /**
     * Admins
     */
    Route::group(['middleware' => ['permission:settings_admins']], function () {
        Route::get('settings/admins/get-data', 'Settings\Admins\AdminsController@getData');
        Route::post('settings/admin/add', 'Settings\Admins\AdminsController@add');
        Route::get('settings/admin/{id}', 'Settings\Admins\AdminsController@get');
        Route::post('settings/admin/{id}', 'Settings\Admins\AdminsController@update');
        Route::delete('settings/admin/{id}', 'Settings\Admins\AdminsController@delete');
        Route::get('settings/admins/get-permissions', 'Settings\Admins\AdminsController@getPermissions');
    });

    /**
     * Users reports
     */
    Route::group(['middleware' => ['permission:reports_users']], function () {
        Route::get('reports/users/get-data', 'Reports\ReportsUsersController@getData');
        Route::group(['middleware' => ['permission:reports_users__edit']], function () {
            Route::get('reports/users/user/{id}', 'Reports\ReportsUsersController@get');
            Route::get('reports/users/user/dialog/{id}', 'Reports\ReportsUsersController@getDialog');
            Route::post('reports/users/change-gender', 'Reports\ReportsUsersController@changeGender');
            Route::post('reports/users/blocked-user-add', 'Users\UsersController@createBlockedUser');
            Route::post('reports/users/blocked-user-remove', 'Users\UsersController@removeBlockedUser');
            Route::post('reports/users/ban-user-add', 'Users\UsersController@createBannedAcc');
            Route::post('reports/users/ban-user-remove', 'Users\UsersController@removeBannedAcc');
            Route::post('reports/users/ban-device-add', 'Users\UsersController@createBannedDev');
            Route::post('reports/users/ban-device-remove', 'Users\UsersController@removeBannedDev');
            Route::post('reports/users/verification', 'Reports\ReportsUsersController@verification');
            Route::post('reports/users/alert', 'Reports\ReportsUsersController@alert');

            Route::get('reports/users/alert-messages-list', 'Reports\ReportsUsersController@getAlertMessagesList');
            Route::get('reports/users/alert-message', 'Reports\ReportsUsersController@getAlertMessage');
            Route::post('reports/users/alert-message', 'Reports\ReportsUsersController@createAlertMessage');
            Route::put('reports/users/alert-message', 'Reports\ReportsUsersController@updateAlertMessage');
            Route::delete('reports/users/alert-message', 'Reports\ReportsUsersController@deleteAlertMessage');
            Route::post('reports/users/alert-message-send', 'Reports\ReportsUsersController@sendAlertMessage');

            // images uploads and replace (delete) for users
            Route::post('reports/users/avatar/replace/{id}', 'Users\UsersController@replaceAvatar');
            Route::delete('reports/users/avatar/{id}', 'Users\UsersController@deleteAvatar');
            Route::post('reports/users/mainphoto/replace/{id}', 'Users\UsersController@replaceMainPhoto');
            Route::delete('reports/users/mainphoto/{id}', 'Users\UsersController@deleteMainPhoto');
            Route::post('reports/users/photo/replace/{id}', 'Users\UsersController@replacePhoto');
            Route::delete('reports/users/photo/{id}', 'Users\UsersController@deletePhoto');
        });
    });

    /**
     * Photos reports
     */
    Route::group(['middleware' => ['permission:reports_photos']], function () {
        Route::post('reports/photos/get-data', 'Reports\ReportsPhotosController@getData');
        Route::post('reports/photos/review', 'Reports\ReportsPhotosController@setReview');
        Route::post('reports/photos/status', 'Reports\ReportsPhotosController@setStatusPhoto');
    });

    /**
     * Photos status
     */
    Route::group(['middleware' => ['permission:verification_photos']], function () {
        Route::get('status-photos/get-data', 'UsersPhotos\UsersPhotosStatusController@getPhotos');
        Route::get('status-photos/get-count-status', 'UsersPhotos\UsersPhotosStatusController@getCountStatus');
        Route::get('status-photos/get-count-new-photos', 'UsersPhotos\UsersPhotosStatusController@getCountNewPhotos');
        Route::get('status-photos/get-current-shift', 'Moderators\ShiftController@getCurrentShift');
        Route::post('status-photos/set-started-shift', 'Moderators\ShiftController@setStartedShift');
        Route::post('status-photos/set-finished-shift', 'Moderators\ShiftController@setFinishedShift');

        // set status
        Route::group(['middleware' => ['permission:verification_photos__verification']], function () {
            Route::post('status-photos/status', 'UsersPhotos\UsersPhotosStatusController@setStatusPhoto');
        });
    });

    /**
     * Photos status inspect
     */
    Route::group(['middleware' => ['permission:verification_photos']], function () {
        Route::get('status-photos-inspect/moderators', 'UsersPhotos\UsersPhotosStatusInspectController@getModerators');
        Route::post('status-photos-inspect/moderator/{id}/{status}', 'UsersPhotos\UsersPhotosStatusInspectController@getModeratorPhotosStatus');

        Route::group(['middleware' => ['permission:verification_photos__inspect']], function () {
            Route::post('status-photos-inspect/confirm', 'UsersPhotos\UsersPhotosStatusInspectController@confirmPhotosStatus');
            Route::post('status-photos-inspect/set-status', 'UsersPhotos\UsersPhotosStatusInspectController@changeStatus');
            Route::post('status-photos-inspect/search', 'UsersPhotos\\UsersPhotosStatusInspectController@searchUserPhotos');
        });
    });

    /**
     * Users verification
     */
    Route::group(['middleware' => ['permission:verification_users']], function () {
        Route::get('verification/get-data', 'Verification\VerificationController@getData');
        Route::group(['middleware' => ['permission:verification_users__verification']], function () {
            Route::post('verification/verification', 'Verification\VerificationController@verificationPhoto');
            // ban state users
            Route::post('verification/ban-user-add', 'Users\UsersController@createBannedAcc');
            // ban state device users
            Route::post('verification/ban-device-add', 'Users\UsersController@createBannedDev');
        });
    });

    /**
     * Moderators stats
     */
    Route::get('moderators/stats', 'Moderators\StatsController@getStatsOwner');

    /**
     * Moderators ratings
     */
    Route::group(['middleware' => ['permission:moderators_rating']], function () {
        Route::get('moderators/rating-list', 'Moderators\RatingController@getRatingList');
    });

    /**
     * Moderators payments
     */
    Route::group(['middleware' => ['permission:moderators_payments']], function () {
        Route::get('moderators/payments', 'Moderators\StatsController@getPayments');
        Route::post('moderators/payment', 'Moderators\StatsController@payment');
        Route::get('moderators/payment-amount', 'Moderators\StatsController@getPaymentAmount');
        Route::get('moderators/stats-selected', 'Moderators\StatsController@getStatsSelected');
    });

    /**
     * Other
     */
    Route::get('auth/get-current-user', 'AuthController@getCurrentUser');
    Route::post('auth/get-permissions', 'AuthController@getPermissions');
    Route::post('menu/get-data', 'MenuController@getData');
});