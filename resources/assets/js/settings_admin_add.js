
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import Vue from 'vue'
import AdminAdd from './components/settings_admin_add'
import router from './router'
import store from './store'

new Vue({
    el: '#app',
    router,
    store,
    template: '<AdminAdd/>',
    components: { AdminAdd },
});

