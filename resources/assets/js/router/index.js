import Vue from 'vue'
import Router from 'vue-router'
import store from '../store'

Vue.use(Router);

const ifNotAuthenticated = (to, from, next) => {
    if (!store.getters.isAuthenticated) {
        next();
        return
    }
    next('/')
};

const ifAuthenticated = (to, from, next) => {
    if (store.getters.isAuthenticated) {
        next();
        return
    }
    next('/login')
};

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'Dashboard',
        },
        {
            path: '/status-photos-inspect',
            name: 'StatusPhotosInspectList',
        },
        {
            path: '/status-photos-inspect/moderator/:id/:status',
            name: 'StatusPhotosInspectShow',
        },
        {
            path: '/status-photos-inspect/confirmed/moderator/:id/:status',
            name: 'StatusPhotosInspectShowConfirmed',
        },
        {
            path: '/status-photos-inspect/changed/moderator/:id/:status',
            name: 'StatusPhotosInspectShowChanged',
        },
        {
            path: '/status-photos-inspect/search/:id',
            name: 'StatusPhotosInspectSearch',
        },
        {
            path: '/login',
            name: 'Login',
            beforeEnter: ifNotAuthenticated,
        },
    ],
})
