// left menu show-hide

window.onload = function () {
    $('.fa-bars').click(function () {
        let aside = $("#sidebar > ul");
        let container = $("#container");

        if (aside.is(":visible") === true) {
            aside.hide();
            container.addClass("sidebar-closed");
        } else {
            aside.show();
            container.removeClass("sidebar-closed");
        }
    });
};