import axios from '../../utils/axios'

export default {
    data() {
        return {
            current_user: {},
            permissions: [],
            current_user_available: false,
        }
    },
    methods: {
        async getCurrentUser() {
             await axios.get('/api/auth/get-current-user')
                 .then((response) => {
                     this.current_user = response.data;
                 }).catch((error) => {
                     console.log(error);
                 });
            await axios.post('/api/auth/get-permissions')
                .then((response) => {
                    this.permissions = response.data;
                }).catch((error) => {
                    console.log(error);
                });

            return this.current_user.hasOwnProperty('id');
        },
        checkPermission(required) {
            if (!this.current_user.hasOwnProperty('id')) {
                this.getCurrentUser();
            }

            return this.permissions.indexOf(required) !== -1;
        },
    }
};