export default {
    GOOGLE_API_KEY: process.env.MIX_GOOGLE_API_KEY,
    WS_URL: process.env.MIX_SKYLOVE_WS_URL
};