import axios from 'axios';

axios.defaults.headers.common['Authorization'] = localStorage.getItem('user-token') || '';

axios.interceptors.response.use(undefined, err => {
    let res = err.response;

    if (res !== undefined) {
        if (res.status === 401) {
            console.log('Intercepted! 401 response handled!');
            localStorage.removeItem('user-token');
            window.location.href = '/login';
        }

        if (res.status === 403) {
            console.log('Intercepted! 403 response handled!');
            window.location.href = '/';
        }
    }

    return Promise.reject(err);
});

export default axios
