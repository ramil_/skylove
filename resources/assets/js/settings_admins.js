
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import Vue from 'vue'
import Admins from './components/settings_admins'
import router from './router'
import store from './store'

new Vue({
    el: '#app',
    router,
    store,
    template: '<Admins/>',
    components: { Admins },
});

