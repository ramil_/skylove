import { DASHBOARD_REQUEST, DASHBOARD_ERROR, DASHBOARD_SUCCESS } from '../actions/dashboard'
import Vue from 'vue'
//import Vuex from 'vuex'
import axios from 'axios'

//Vue.use(Vuex)

const dashboard = { status: '', counts: {} }

const instance = axios.create();
instance.defaults.headers.common['Authorization'] = localStorage.getItem('user-token') || '';

const actions = {
  [DASHBOARD_REQUEST]: ({commit, dispatch}) => {
    console.log(localStorage);
    commit(DASHBOARD_REQUEST)
    instance.get('/api/dashboard/mock')
      .then(response => {
        commit(DASHBOARD_SUCCESS, response)
      })
      .catch(response => {
        commit(DASHBOARD_ERROR)
        // if resp is unauthorized, logout, to
        console.log(response);
        // dispatch(AUTH_LOGOUT)
      })
  },
}

const mutations = {
  [DASHBOARD_REQUEST]: (state) => {
    dashboard.status = 'loading'
  },
  [DASHBOARD_SUCCESS]: (state, response) => {
    dashboard.status = 'success'
    Vue.set(dashboard, 'counts', response.data)
  },
  [DASHBOARD_ERROR]: (state) => {
    dashboard.status = 'error'
  },
  //[AUTH_LOGOUT]: (state) => {
  // state.profile = {}
  //}
}

export default {
  state,
  actions,
  mutations,
}
